<?php
	$xml_path = $post['xml_path'];
	if(file_exists(__DIR__.'/'.$xml_path)){
		$exe_path = realpath(__DIR__.'/iChecker/iChecker.exe');
		$temp_file_path = __DIR__.'/TEMP/'.$_SESSION['UserID'];
        if(!file_exists($temp_file_path)){
            mkdir($temp_file_path, 0777, true);
        }

        $temp_file = $temp_file_path.'/'. pathinfo($xml_path, PATHINFO_FILENAME).'.xml';
        if(file_exists($temp_file)){
        	unlink($temp_file);
        }
        copy(__DIR__."/".$xml_path, $temp_file);
        chmod($temp_file,0777); 
		$exe_cmd = str_replace("/","\\", $exe_path.' '.$temp_file);
		exec($exe_cmd);
		if(file_exists($temp_file)){
			unlink($temp_file);
		}
		$data = '';
		if(file_exists(__DIR__.'/iChecker/Output/'.$_SESSION['UserID'].'/Logs')){
			$_dir =  new DirectoryIterator(__DIR__.'/iChecker/Output/'.$_SESSION['UserID'].'/Logs');
			$_dirpath = $base_url."/xml_viewer.php?url=iChecker/Output/".$_SESSION['UserID']."/Logs";
			if(file_exists($_dir)){
				foreach ($_dir as $fileinfo) {
		            if (!$fileinfo->isDot()) {
		            	$data .= "<li><a target='_blank' href='".$_dirpath."/".$fileinfo->getFilename()."'>".$fileinfo->getFilename()."</a></li>";
		            }
		        }
		    }
		}
		if(empty($data)){
			$data = '<li style="color:red;">iChecker validation failed!</li>';
		}
		$results = array("success" => true, "message" => "Sucessfully process iChecker Validations", "data" => $data);
	}else{
		$results = array("success" => false, "message" => "Error: xml file data not found!");
	}
?>