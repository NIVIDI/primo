<?php
	$bundle = $post['bundle'];
	
	if(!empty($bundle)){
		$numofdocs = 0;
	    $sql = "SELECT * FROM segregated_files WHERE prec_bundle = '{$bundle}' ";
	    $result = mysqli_query($con, $sql);
	    $total_done = 0;
	    $file_segregrated = array();
	    if($result->num_rows){
	        while($row1 = $result->fetch_assoc()){
	            $numofdocs ++;
	            if(!empty($row1['wms_job_id'])){
	                $wms_status = GetWMSValue("SELECT StatusString FROM primo_view_Jobs WHERE JobId = '{$row1['wms_job_id']}' AND ProcessCode = 'QC' ","StatusString",$conWMS);
	                if(strtolower($wms_status) == 'done'){
	                    $total_done ++;
	                }
	            }

	            $file_segregrated[] = $row1;
	        }
	    }

	    if(empty($numofdocs)){
	    	$results = array("success" => false, "message" => "Cannot deliver bundle({$bundle}) with empty source file!");
	    }else{
	    	if((int) $numofdocs > (int) $total_done){
	    		$results = array("success" => false, "message" => "Error delivering bundle({$bundle}), some source batched files not yet completed!");
	    	}else{
	    		//check for sources files if already has generated .key file
	    		$sql = "SELECT * FROM files_for_batching WHERE prec_bundle = '{$bundle}' ";
	    		$source_result = mysqli_query($con, $sql);

	    		$process_result = array();
	    		if($source_result->num_rows){
	    			while($row = $source_result->fetch_assoc()){
	    				$key_filaname = generateKeyFilename($row['file_name']);
	    				$path = $SourceFilePath.'/'.$bundle.'/deliverables/'.$key_filaname;
	    				if(!file_exists($path)){
	    					$process_result[] = array("success" => false, "message" => "Source File({$row['file_name']}) has no .key file");
	    				}
	    			}
	    		}

	    		if(!empty($process_result)){
		    		$message = '';
					foreach($process_result as $response){
						$style = "display: block; color: red;";
						if($response['success']){
							$style = "display: block; color: green;";
						}
						$message .= "<li style='{$style}'>{$response['message']}</li>";
					}
					$results = array("success" => false, "message" => "{$message}");
				}else{
					generateSequenceNumber($bundle);
					
					//process delivery, zip all deliverables and send to PD together with the dollar files
					$file_name = generateDollarFileName($bundle);
					$path_tozip = $SourceFilePath.'/'.$bundle.'/deliverables';
					$dollar_path = $SourceFilePath.'/'.$bundle.'/'.$file_name.'.$$$';

					//generate/update dollar file
					generateDollarFile($bundle, 'deliver');
					if(!file_exists($dollar_path)){
						$results = $results = array("success" => false, "message" => "Error: dollar file not yet generated, please check and review!!");
					}else{
						zipFilesV2($path_tozip, $file_name);

						//move zip file to file manager
						$filemager_path = __DIR__.'/'.$data_inventory_path.'/Delivered Files/'.$bundle;
						if(!file_exists($filemager_path)){
							mkdir($filemager_path, 0777, true);
						}
						rename($path_tozip.'/'.$file_name.'.zip', $filemager_path.'/'.$file_name.'.zip');
						


						//create attachment for email
						$attachments = array();
						$attachments[] = array("path" => $filemager_path.'/'.$file_name.'.zip', "name" => $file_name.'.zip');
						$attachments[] = array("path" => $dollar_path, "name" => $file_name.'.$$$');
						
						$subject = '';
						$body = '';
						$mail = sendMail($subject, $body, $attachments);
						if($mail['success']){
							//update bundle to complete
							$sql = "UPDATE files_for_batching SET status = 'Completed' WHERE prec_bundle = '{$bundle}' ";
							ExecuteQuery($sql,$con);
							$results = array("success" => true, "message" => "Successfully send bundle zip and dollar file to PD.");
						}else{
							$results = array("success" => false, "message" => $mail['message']);
						}
					}
				}
            }
	    }
	}else{
		$results = array("success" => false, "message" => "Error cannot find specified bundle!");
	}
?>