<?php
    ini_set('session.save_path',getcwd(). '/');
    session_start();
   
    if(session_destroy()) {
       header("Location: login.php");
    }
?>