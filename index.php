﻿<?php
include "conn.php";
use Laravie\Parser\Xml\Reader;
use Laravie\Parser\Xml\Document;

$Task=empty($_GET['Task']) ? '': $_GET['Task'];
if ($Task==''){
	$Task= empty($_SESSION['Task']) ? 'STYLING': $_SESSION['Task'];
}
$_SESSION['Task']=$Task;

$BatchID=empty($_GET['BatchID']) ? '': $_GET['BatchID'];
$_SESSION['BatchID']=$BatchID;
	
$Status= !empty($_GET['Status']) ? $_GET['Status'] : '';
//GET TASK ID
$TaskID = '';
$sql="SELECT * FROM wms_Processes Where ProcessCode='$Task'";	
					
	$rs=odbc_exec($conWMS,$sql);
	 
	while(odbc_fetch_row($rs))
	{
		$TaskID=odbc_result($rs,"ProcessID");
	}

//GET taskeditorsetting
$sql="SELECT * FROM tbltaskeditorsetting Where TaskID='$TaskID'";

$Source='';
$Styling='';
$XMLEditor='';
$SequenceLabeling='';
$TextCategorization='';
$DataEntry='';
$TreeView='';
if ($result=mysqli_query($con,$sql))
{
// Fetch one and one row
	while ($row=mysqli_fetch_row($result))
	{
		$Source=$row[1];
		$Styling=$row[2];
		$XMLEditor=$row[3];
		$SequenceLabeling=$row[4];
		$TextCategorization=$row[5];
		$DataEntry=$row[6];
		$TreeView=$row[7];
	}
}


$api_token = getAPIKey($GGUserName,$GGPassword,$GGProductionMode);

include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>
<script>
function myFunction95() {
    var x = document.getElementById("myDIV95");
	var x1 = document.getElementById("myDIV80");
	var x2 = document.getElementById("myDIV79");
	var x3 = document.getElementById("myDIVEdited");
	
    
	x.style.display = "block";
	x1.style.display = "none";
	x2.style.display = "none";
	x3.style.display = "none";
}
function myFunction80() {
    var x = document.getElementById("myDIV95");
	var x1 = document.getElementById("myDIV80");
	var x2 = document.getElementById("myDIV79");
    var x3 = document.getElementById("myDIVEdited");
	
	x1.style.display = "block";
	x.style.display = "none";
	x2.style.display = "none";
	x3.style.display = "none";
}
function myFunction79() {
    var x = document.getElementById("myDIV95");
	var x1 = document.getElementById("myDIV80");
	var x2 = document.getElementById("myDIV79");
    var x3 = document.getElementById("myDIVEdited");
	
	x2.style.display = "block";
	x.style.display = "none";
	x1.style.display = "none";
	x3.style.display = "none";
	
}
function myFunctionEdited() {
    var x = document.getElementById("myDIV95");
	var x1 = document.getElementById("myDIV80");
	var x2 = document.getElementById("myDIV79");
    var x3 = document.getElementById("myDIVEdited");
	
	x3.style.display = "block";
	x.style.display = "none";
	x1.style.display = "none";
	x2.style.display = "none";
	
}

function check() {
    if(document.getElementById('password').value ===
            document.getElementById('confirm_password').value) {
        document.getElementById('message').innerHTML = "";
		document.getElementById("Button").disabled = false;
    } else {
        document.getElementById('message').innerHTML = "password not match";
		document.getElementById("Button").disabled = true;
    }
}

function LoadStyles(){
	var response=document.getElementById("Joblist");
	//var jTextArea=document.getElementById("jTextArea").value;
	var jTextArea = "index";
	var data = 'data='+encodeURIComponent(jTextArea);
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange=function(){
	    if (xmlhttp.readyState==4 && xmlhttp.status==200){
	      	response.innerHTML=xmlhttp.responseText;
	    }
	}
  	xmlhttp.open("POST","ListOfStyles.php",true);
    //Must add this request header to XMLHttpRequest request for POST
  	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  	xmlhttp.send(data);
}
</script>
<!--code mirror-->
<link rel="stylesheet" href="lib/codemirror.css">
<link rel="stylesheet" href="addon/fold/foldgutter.css" />
<link rel="stylesheet" href="addon/dialog/dialog.css">
<link rel="stylesheet" href="addon/search/matchesonscrollbar.css">
<!--   <link rel="stylesheet" href="addon/hint/show-hint.css">
  
    <script src="addon/hint/show-hint.js"></script>
  <script src="addon/hint/xml-hint.js"></script>
  <script src="addon/hint/html-hint.js"></script>
   -->
<script src="lib/codemirror.js"></script>
<script src="addon/fold/foldcode.js"></script>
<script src="addon/fold/foldgutter.js"></script>
<script src="addon/fold/brace-fold.js"></script>
<script src="addon/fold/xml-fold.js"></script>
<script src="addon/fold/markdown-fold.js"></script>
<script src="addon/fold/comment-fold.js"></script>
<script src="mode/javascript/javascript.js"></script>
<script src="mode/xml/xml.js"></script>
<script src="mode/markdown/markdown.js"></script>
  
<script src="addon/search/searchcursor.js"></script>
<script src="addon/search/search.js"></script>
<script src="addon/search/jump-to-line.js"></script>
<script src="addon/dialog/dialog.js"></script>
<script src="addon/edit/matchtags.js"></script>
  
<style type="text/css">
    .CodeMirror {border-top: 1px solid black; border-bottom: 1px solid black; height: 32vw;}
	.CodeMirror-selected  { background-color: skyblue !important; }
    .CodeMirror-selectedtext { color: white; }
    .styled-background { background-color: #ff7; }

    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
</style>
<script src="addon/display/fullscreen.js"></script>
<!--View Current Status-->
<?php
	$sql="SELECT * FROM primo_view_Jobs Where /*ProcessCode='$Task' AND*/ BatchID='{$BatchID}'";	
		
	$rs=odbc_exec($conWMS,$sql);
	$ctr = odbc_num_rows($rs);
	$FileStatus='';
	$Filename='';
	$JobID='';
	$Jobname='';
	$StatusString='';
	$LastUpdate='';
	$Relevancy='';
	$GGJobID='';
	$SourceURL='';
	$PageNo='';
	$PrioNumber='';
	$DocumentType='';
	while(odbc_fetch_row($rs))
	{
			 
		$FileStatus=odbc_result($rs,"StatusString");
		$Filename=odbc_result($rs,"Filename");
		$JobID=odbc_result($rs,"JobId");
		$Jobname=odbc_result($rs,"Jobname");
		$StatusString=odbc_result($rs,"StatusString");
		$LastUpdate=odbc_result($rs,"LastUpdate");
		$Relevancy=odbc_result($rs,"Relevancy");
		$GGJobID=odbc_result($rs,"GGJobID");
		$SourceURL=odbc_result($rs,"SourceURL");
		$PageNo=odbc_result($rs,"PageNo");
		$PrioNumber=odbc_result($rs,"Transpriority");
		$DocumentType=odbc_result($rs,"DocumentType");
		
		
	}

	$bundle_path = '';
	if(!empty($JobID)){
		$primo_jobssql = "SELECT * FROM segregated_files WHERE wms_job_id = '{$JobID}' LIMIT 1";
		$primo_jobresult = mysqli_query($con,$primo_jobssql)->fetch_assoc();
		if(!empty($primo_jobresult)){
			$bundle_path = $primo_jobresult['prec_bundle'];
		}
	}

	$file_path = 'uploadFiles/SourceFiles/'.$bundle_path;
	$_SESSION['FileName']=$Filename;
	$_SESSION['JobID']=$JobID;
	$sxfilename = pathinfo($Filename, PATHINFO_FILENAME);
    $nfile=$sxfilename.".xml";
	$sXMLFile = $file_path.'/'.$nfile;
 	$index_error_msg = '';
 	if(empty($GGJobID)){
 		$index_error_msg .= "<div class='alert alert-error alert-dismissible'>
	                    		<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
	                    		<h4><i class='icon fa fa-info'></i> Warning!</h4>
	                    		Batch file has no equivalent GG Job ID
	                		</div>";
 	}

 	if(empty($JobID)){
		$index_error_msg .= "<div class='alert alert-error alert-dismissible'>
	                    		<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
	                    		<h4><i class='icon fa fa-info'></i> Warning!</h4>
	                    		Batch file not found
	                		</div>";
	}


	$fileVal= !empty($_GET['file']) ? $_GET['file'] : '';
	if ($fileVal==''){
		$fileVal= empty($_SESSION['file']) ? '': $_SESSION['file'];
	}

	$_SESSION['file']=$fileVal;
	$sFileVal = $Filename; //$fileVal;

	$file= explode('.',(empty($sFileVal) ? '':$sFileVal));
	$client_xml_path = $file_path.'/'. $file[0] . '.xml';	

	$sXML="";

	if (file_exists($client_xml_path)) {   
		$sXML = file_get_contents($client_xml_path);
		$sXML = str_replace("&lt;", "<", $sXML);
		$sXML = str_replace("&gt;", ">", $sXML);
	}

	$filenameni = $file[0];
	$innodomXML = "";
	$innodom_path = $file_path.'/sources/'.$file[0].'_response.xml';
	if (file_exists($innodom_path)) {   
		$innodomXML = file_get_contents($innodom_path);
		$innodomXML = reformatXML($innodomXML);
		$custom_xml = new Custom_XML($innodomXML);
		
		//remove xml version at the top
		$dom = dom_import_simplexml($custom_xml);
      	$innodomXML = $dom->ownerDocument->saveXML($dom->ownerDocument->documentElement);
	}

?>
<script type="text/javascript">
	 Mousetrap.bind('shift+k', function(e) {
	 	console.log('here');
	    highlight([6, 7, 8, 9]);
	    return false;
	});
</script>
<div class="content-wrapper">
    <input type="hidden" name="api_token" id="api_token" value="<?= $api_token;?>">
    <section class="content-header">
      	<h1>File: <?php echo empty($sFileVal) ? '': $sFileVal;?></h1>
      	<ol class="breadcrumb">
        	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        	<li class="active">Document Editor</li>
			<!--<li><a href="fullscr.php?page=Enrich&file=<?=$filename;?>&BatchID=<?=$BatchID;?>&Task=<?=$Task;?>"><i class="fa fa-copy"></i> Split View</a></li>-->
      	</ol>
    </section>
    <section class="content">
    	<div class="row">
    		<div class="col-sm-12" id="index_error_msg"><?= $index_error_msg;?></div>
    	</div>
    	<?php  
    		if(!empty($JobID)){
    	?>
		    	<div class="row">
		        	<div class="col-md-4" id="editor_tab_panel">
		          		<div class="nav-tabs-custom">
		            		<ul class="nav nav-tabs pull-right">
		            			<?php
		            				if ($Source==1){
						  				echo '<li class="tab_right"><a href="#tab_3-2" id="pdf_source_tab" data-toggle="tab">Source</a></li>';
									}
					 			?>
		            			<li  class="tab_left active"><a href="#allocationDetails" data-toggle="tab">Allocation</a></li>
					  			<li class="tab_left"><a href="#JobQueue" data-toggle="tab" >Validation</a></li>
		              			<li class="tab_left"><a href="#STYLINGPanel" onclick="STYLINGTAGS.loadTags();" data-toggle="tab" >Styling</a></li>
		              			<li class="tab_left"><a href="#MappingPanel" id="MappingPanelTab" data-toggle="tab" >Mapping</a></li>
		              			<li class="pull-left header" style="cursor:pointer;" onclick="TAB.hideShow2();"><i class="fa fa-th"></i> </li>
		              		</ul>
							<div class="tab-content" >
		          	 			<div class="tab-pane tab_left_pane" id="STYLINGPanel"  style="overflow-y: scroll; height:35vw;">
		          	 				<div class="col-sm-12">
		          	 					<a href="javascript:void(0);" title="Add New Tag" class="pull-right" onclick="STYLINGTAGS.new();"><i class="fa fa-plus" aria-hidden="true"></i></a>
		          	 					<a href="javascript:void(0);" title="Delete Tag" style="color:red;" class="pull-left" onclick="STYLINGTAGS.delete();"><i class="fa fa-minus" aria-hidden="true"></i></a>
		          	 				</div>
		          	 				<div class="col-sm-12" style="margin-top: 20px;">
			          	 				<ul class="nav nav-pills nav-stacked">
				         					<div id="styling_tags">
				         						
				         					</div>
				         				</ul>
				         			</div>
								</div>
		          	 			<div class="tab-pane tab_left_pane" id="JobQueue"  style="overflow-y: scroll; height:35vw;">
				          	 		<ul class="nav nav-pills nav-stacked">
					         			<div id="ValidationList"></div>
					         		</ul>
								</div>
								<div class="tab-pane tab_left_pane" id="MappingPanel"  style="overflow-y: scroll; height:35vw;">
									<div id="mapping_div">
										<div class="col-sm-12" style="text-align:center;"><small class="text-muted">Fetching data...</small></div>
						         	</div>
						        </div>
						        <div class="tab-pane tab_left_pane" id="tab_3-2">
									<?php
										$fileVal=$file_path."/".$Filename;
										$info = pathinfo( $fileVal);
										$extension = !empty($info["extension"]) ? $info["extension"]:'';
										$snewile =str_replace("." . $extension,".pdf",$fileVal);	
										$snewile =str_replace("." . $extension,".PDF",$snewile);	
						 				if ($extension == "pdf" || $extension == "PDF") {
											echo '<embed id="source_embed" src="/'.$fileVal.'" style="width:100%; height:37vw;" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html"></embed>';
						 				}elseif($extension == "txt"){
											$nfilename=$file_path."/".pathinfo($fileVal, PATHINFO_FILENAME).".txt";
											echo '<iframe id="source_embed" src="'.$nfilename.'" style="width:100%; height:37vw;" frameborder="none" ></iframe>';
										}else{
								 			$nfilename=$file_path."/".pathinfo($fileVal, PATHINFO_FILENAME).".html";

											if ($Filename!=''){
												echo '<iframe id="source_embed" src="'.$nfilename.'" style="width:100%; height:37vw;" frameborder="none" ></iframe>';
											}
										}
									?>
								</div>
		            		
								<div class="tab-pane tab_left_pane active" id="allocationDetails" >
									<div class="box-body no-padding">
		              					<ul class="nav nav-pills nav-stacked">
		              						<li><a href="#"><i class="fa fa-tasks"></i><b>TASK: <span id="Task1"><?php echo $Task;?></span></b></a></li>
		              						<li><a href="<?php echo $SourceURL;?>" target="_blankk"><i class="fa fa-file-o"></i>FileName: <u><span id="filename"><?php echo $Filename;?></span></u></a></li>
		              						<!-- 	<li><a href="<?php echo $SourceURL;?>" target="_blankk"><i class="fa fa-file-o"></i>Source URL: <u><span id="filename"><?php echo $SourceURL;?></span></u></a></li> -->
		              						<li><a href="#"><i class="fa fa-folder"></i>Bundle: <u><?php echo $bundle_path;?></u></a></li>
					    					<li><a href="#"><i class="fa fa-folder"></i>JobName: <u><?php echo $Jobname;?></u></a></li>
					    					<!-- <li><a href="#"><i class="fa fa-folder"></i>Priority Number: <u><?php echo $PrioNumber;?></u></a></li> -->
					     					<li><a href="#"><i class="fa fa-folder"></i>Document Type: <u><span id="DocType"><?php echo $DocumentType;?></span></u></a></li>
											<li><a href="#"><i class="fa fa-folder"></i>PageNo: <u><span id="PageNo"><?php echo $PageNo;?></span></u></a></li>
											<li><a href="#"><i class="fa fa-line-chart"></i>Status: <u><span id="Status"><?php echo $StatusString;?></span></u></a></li>
		                					<li><a href="#"><i class="fa fa-clock-o"></i>Last Updated: <u><?php echo $LastUpdate;?></u></a></li>
		                					
		                					<?php 
		                						if ($TreeView==1){ ?>
													<!-- https://wb.innodatalabs.com/zoning/#/job/<?php echo $GGJobID;?>?token=dXNlci10ZXN0LWZiYWQ5OThmMmYxNzNiNDM3NDE0YjQxOWZkNjhkMzAwMDVkN2QzMDc6 -->
													<li><a href="https://wb.innodatalabs.com/zoning-review/#/job/<?php echo $GGJobID;?>?token=<?= $TokenVAL;?>" target="blank" id='GoldenGateLink1'><i class="fa fa-square"></i> <u>Zoning</u></li>
													<li><a href="https://wb.innodatalabs.com/zoning/#/job/<?php echo $GGJobID;?>?token=<?= $TokenVAL;?>" target="blank" id='GoldenGateLink'><i class="fa fa-link"></i>Link: <u>Full Screen (Transformation)</u></li>
						 							<li><a href="#" onClick="GetJobStatus()"><i class="fa fa-spinner"></i>GG Status: <u><span id="GGStatus"></span></u></a></li>
						 							<!-- <li><a href="#" onClick="TestGetJobStatus()"><i class="fa fa-refresh"></i>Test Status: <u><span id="TestGGStatus"></span></u></a></li> -->
													<?php
														$innoXML= str_replace(".pdf", "_response.xml", $Filename);
														$innoXML= str_replace(".PDF", "_response.xml", $innoXML)
													?>
						 							<li><a href="<?= $file_path;?>/sources/<?php echo $innoXML;?>" target="_blank" ><i class="fa fa-file-excel-o"></i><u>Innodom XML</u></a></li>
						 							<input type="hidden" value ="<?php echo $GGJobID;?>" id="GGJobID">
													<input type="hidden" value ="<?php echo $Task;?>" id="Task">
						 							<input type="hidden" value ="<?php echo $TokenVAL;?>" id="TokenVal">
						 							<input type="hidden" value ="Not Yet Validated" id="ValidateTrigger">
											<?php } ?>
											
											<?php
												$PDFImage='none';

		                						if ($_SESSION['Task']=='CONTENTREVIEW'){
		                							$ext = pathinfo($Filename, PATHINFO_EXTENSION);
													if (strtoupper($ext)=='PDF'){
														
														$PDFImage='display';
														if (file_exists($file_path."/".$JobID."/".pathinfo($Filename,PATHINFO_FILENAME ).".html")){
															echo '<li><a href="'.$file_path.'/'.$JobID.'/'.pathinfo($Filename,PATHINFO_FILENAME ).'.html" target="_blank"><i class="fa fa-gear"></i>View HTML</a></li>';  
														}else{
															echo '<li id="HTMLCon"><a href="#" onclick="ConvertPDFHTML()"><i class="fa fa-gear"></i>Convert PDF to HTML </a></li>';  
														}
													}
												}
		               						?>
											<!-- <li style="display: block" id="isPDFImage"><a><i class="fa fa-question-circle"></i><input type="checkbox" id="PDFImage"> PDF Image?</a></li> -->
		 									<li style="display: block" id="JobRepost"><a><i class="fa fa-question-circle"></i>
		 										<button onclick="JobRewind()">Rewind</button>
												<!--<button onclick="JobRepost()">Repost</button>-->
											</li>

						 					<?php if ($Task=='WRITING'){ ?>

													<li>
														<a href="#" ><i class="fa fa-info"></i> Draft Type: 
															<u>
																<select id="DraftType">
																	<option value="New">New</option>
																	<option value="Ammendment">Ammendment</option>
																</select>
															</u> 
														</a>
													</li>
													<li>
														<a href="#" ><i class="fa fa-info"></i> Category: 
															<u>
																<select id="Category">
																	<option value="Air Pollution">Air Pollution</option>
																	<option value="Construction">Construction</option>
																	<option value="Corporate Standards">Corporate Standards</option>
																	<option value="Emergency Response">Emergency Response</option>
																	<option value="Energy">Energy</option>
																	<option value="Equipment">Equipment</option>
																	<option value="General">General</option>
																	<option value="General Safety">General Safety</option>
																	<option value="Guidelines">Guidelines</option>
																	<option value="Marine">Marine</option>
																	<option value="Materials">Materials</option>
																	<option value="Mining">Mining</option>
																	<option value="Nature Conservation">Nature Conservation</option>
																	<option value="Noise Pollution">Noise Pollution</option>
																	<option value="Offshore">Offshore</option>
																	<option value="Planning">Planning</option>
																	<option value="Pollution Prevention">Pollution Prevention</option>
																	<option value="Products">Products</option>
																	<option value="Protection of Workers">Protection of Workers</option>
																	<option value="Transport">Transport</option>
																	<option value="Waste">Waste</option>
																	<option value="Water">Water</option>
																	<option value="Workplace">Workplace</option>
																</select>
															</u>
														</a> 
													</li>
													<li><a href="#" onClick="GetJobStatus()"><i class="fa fa-edit"></i><button onclick="SetInfo()">Set Info</button></li>
													<script type="text/javascript">
														
														function SetInfo(){
															var strValue =CKEDITOR.instances['editor1'].getData();
															var DraftType= document.getElementById("DraftType").value;
															var Category= document.getElementById("Category").value;
															strHTML="<p><b>Draft Type:</b> "+ DraftType+"</p>"+"<p><b>Category:</b> "+ Category+"</p>"+strValue;
															// editor.insertHtml(strHTML);

															CKEDITOR.instances.editor1.setData(strHTML); 

														}
													</script>
											<?php } 
												
												if ($Status=='' && $StatusString=='Allocated'){
												 	$Start="inline";
													$Resume ="none";
													$Completed="none";
													$Pending="none";
													$Hold="none";
													 
												}
												elseif($StatusString=='Ongoing'){
												 	$Start="none";
													$Resume ="none";
													$Completed="inline";
													$Pending="inline";
													 $Hold="inline";
												}
												elseif($StatusString=='Pending'){
													$Start="none";
													$Resume ="inline";
													$Completed="none";
													$Pending="none";
													 $Hold="none";
												}
												elseif($StatusString=='Done'){
													$Start="none";
													$Resume ="none";
													$Completed="none";
													$Pending="none";
													 $Hold="none";
												}
												else{
													$Start="none";
													$Resume ="none";
													$Completed="none";
													$Pending="none";
													$Hold="none";
												}

												$GetNextBatch="none";
											?>
										</ul>
		            					<div class="box-footer with-border">
										   	<div class="box-tools">
											 	<li style='display: <?php echo $Start;?>' id="Start">
											 		<button type="button" class="btn btn-default  pull-right"  data-toggle="modal" data-target="#modal-Start"  onclick="Javascript:SetTextBoxValue1(<?php echo $BatchID;?>)" style='display: <?php echo $Start;?>'><i class="fa fa-hourglass-start" ></i> Start</button>
											 	</li>
											 	<li style='display:  <?php echo $Completed;?>'  id="Completed"> 
											 		<button type="button" class="btn btn-default  pull-right"  data-toggle="modal" data-target="#modal-success"  onclick="Javascript:SetTextBoxValue(<?php echo $BatchID;?>)" style="width:150px"  ><i class="fa fa-check"></i> Set as completed</button>
											 	</li>
											  	<li style='display:  <?php echo $Hold;?>'  id="Hold"> 
											 		<button type="button" class="btn btn-default  pull-right"  data-toggle="modal" data-target="#modal-Hold"  onclick="Javascript:SetTextBoxValue3(<?php echo $BatchID;?>)"  style="width:150px" ><i class="fa  fa-hand-stop-o"></i> Hold</button>
											  	</li>
											 	<li style='display:  <?php echo $Pending;?>'  id="Pending">
											 		<button type="button" class="btn btn-default pull-right"  data-toggle="modal" data-target="#modal-Pending"  onclick="Javascript:SetTextBoxValue2(<?php echo $BatchID;?>)"  style="width:150px"  ><i class="fa fa-hourglass-2" ></i> Pending</button>
											 	</li>
											 	<li style='display:  <?php echo $Resume;?>'  id="Resume">
											 		<button type="button" class="btn btn-default  pull-right"  data-toggle="modal" data-target="#modal-Start"   style="width:150px" onclick="Javascript:SetTextBoxValue1(<?php echo $BatchID;?>)"  ><i class="fa fa-hourglass-start"></i> Resume</button>
											 	</li>
											  	<li style='display:  <?php echo $GetNextBatch;?>'  id="GetNext">
											  		<a class="btn btn-default  pull-right"  href="GetNextBatch.php?page=Enrich&Task=<?php echo $Task;?>&fullscr=1"><i class="fa  fa-hand-grab-o"></i> Get Next Batch</a>
											  	</li>
											</div>
										</div>
									</div>
		            			</div>
							</div>
		      			</div>
		        		<?php if ($SequenceLabeling==1){ ?>		 
		          				<div class="box box-solid">
		            				<div class="box-header with-border">
		              					<h3 class="box-title">Sequence Labelling-<small>Confidence Level Filter</small></h3>
										<div class="box-tools">
		                					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		              					</div>
		            				</div>
									<?php 
										if ($FileStatus!='Done'){
											$file= explode('.',$sFileVal);
											$above95=GetWMSValue("Select * from tblConfidenceLevel WHERE Filename='$file[0]' AND Type='95% and up'","Count",$conWMS);
											$above80=GetWMSValue("Select * from tblConfidenceLevel WHERE Filename='$file[0]' AND Type='80 to 94%'","Count",$conWMS);
											$above70=GetWMSValue("Select * from tblConfidenceLevel WHERE Filename='$file[0]' AND Type='79% and below'","Count",$conWMS);
					
									?>
		            					<div class="box-body no-padding">
		              						<ul class="nav nav-pills nav-stacked">
					  						<?php 
					  							if ($above95==0){
													echo '<li><a onclick="return false;" ><i class="fa fa-circle text-green"></i> 95% and up <small>(Total Refs.'.$above95.')</small></a></li>';
					 							}else{
													echo '<li><a href="#" onclick="myFunction95()"><i class="fa fa-circle text-green"></i> 95% and up <small>(Total Refs.'.$above95.')</small></a></li>';
					 							}
					  							
					  							if ($above80==0){
					  								echo '<li> <a onclick="return false;" ><i class="fa fa-circle text-red"></i> 80-94% <small>(Total Refs.'.$above80.')</small></a></li>';
					  							}else{
					  								echo '<li><a href="#" onclick="myFunction80()"><i class="fa fa-circle text-red"></i> 80-94% <small>(Total Refs.'.$above80.')</small></a></li>';
					 	 						}
					   							
					   							if ($above70==0){
					    							echo '<li><a onclick="return false;" ><i class="fa fa-circle text-yellow"></i> 79% and below <small>(Total Refs.'.$above70.')</small></a></li>';
												}else{
						   							echo '<li><a href="#" onclick="myFunction79()"><i class="fa fa-circle text-yellow"></i> 79% and below <small>(Total Refs.'.$above70.')</small></a></li>';
												}
											?>
		                						<li><a href="#" onclick="myFunctionEdited()"><i class="fa fa-circle text-light-blue"></i> edited</a></li>
		              						</ul>
		            					</div>
									<?php } ?>
		            			</div>
						<?php } ?>	
						
						<div class="box box-solid" style="display: none" id="myDIV95">
				            <div class="box-header with-border">
				              	<h3 class="box-title">95% and up</h3>
								<div class="box-tools">
				                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				              	</div>
				            </div>
		        			<div class="box-body no-padding">
					            <ul class="nav nav-pills nav-stacked">
								<?php
								  	if (file_exists($file_path."/$file[0].htm")) {   
									 
										$sTxt =  file_get_contents($file_path."/$file[0].htm"); 
										$arrLine= explode("<div ", $sTxt);
										$ctrL=0;
										foreach($arrLine as $sLtr){
											$ctrL++;
											$arr_string = explode('<span class="above95">',$sLtr);
											$ctr=0;
											//foreach loop to display the returned array
											foreach($arr_string as $str){
												if ($ctr!=0){
													$lVal=explode('</span>',$str); 
													echo "<li><a onclick='FindTerm(\"". $lVal[0] . "\")' href='#tab_2-2'><i class='fa fa-circle text-green'></i>" .$lVal[0] ."</a></li>";
												?>
												 
												<?php
												}
												$ctr++;
											}
										}
									}
								?>
								</ul>
							</div>
		        		</div>
						<div class="box box-solid" style="display: none"  id="myDIV80">
				            <div class="box-header with-border">
				              	<h3 class="box-title">80-94%</h3>
								<div class="box-tools">
				                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				              	</div>
				            </div>
				            <div class="box-body no-padding">
				              	<ul class="nav nav-pills nav-stacked">
							    <?php
								  	$file= explode('.',$sFileVal);
												 
									if (file_exists($file_path."/$file[0].htm")) {   
									 
										$sTxt =  file_get_contents($file_path."/$file[0].htm"); 
										$arrLine= explode("<div ", $sTxt);
										$ctrL=0;
										foreach($arrLine as $sLtr){
											$ctrL++;
											$arr_string = explode('<span class="above80">',$sLtr);
											$ctr=0;
											//foreach loop to display the returned array
											foreach($arr_string as $str){
												if ($ctr!=0){
													$lVal=explode('</span>',$str); 
													echo "<li><a onclick='FindTerm(\"". $lVal[0] . "\")' href='#tab_2-2'><i class='fa fa-circle text-red'></i>" .$lVal[0] ."</a></li>";
												 
												}
												$ctr++;
											}
										}
									}
								?>
								</ul>
				            </div>
		          		</div>
				  		<div class="box box-solid" style="display: none"  id="myDIV79">
				            <div class="box-header with-border">
				              	<h3 class="box-title">79% and below</h3>
								<div class="box-tools">
				                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				              	</div>
				            </div>
				            <div class="box-body no-padding">
				              	<ul class="nav nav-pills nav-stacked">
							  	<?php
							  		$file= explode('.',$sFileVal);
											 
									if (file_exists($file_path."/$file[0].htm")) {   
									 
										$sTxt =  file_get_contents($file_path."/$file[0].htm"); 
										$arrLine= explode("<div ", $sTxt);
										$ctrL=0;
										foreach($arrLine as $sLtr){
											$ctrL++;
												
											$arr_string = explode('<span class="below79">',$sLtr);
											$ctr=0;
											//foreach loop to display the returned array
											foreach($arr_string as $str){
												if ($ctr!=0){
													$lVal=explode('</span>',$str); 
													
												echo "<li><a onclick='FindTerm(\"". $lVal[0] . "\")' href='#tab_2-2'><i class='fa fa-circle text-yellow'></i>" .$lVal[0] ."</a></li>";
												}
												$ctr++;
											}
										}
									}
								?>
				              	</ul>
				            </div>
				        </div>
		            	<div class="box box-solid" style="display: none"  id="myDIVEdited">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Edited</h3>
								<div class="box-tools">
				                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				              	</div>
				            </div>
				            <div class="box-body no-padding">
				              	<ul class="nav nav-pills nav-stacked">
							  	<?php
							  		$file= explode('.',$sFileVal);
											 
									if (file_exists($file_path."/$file[0].htm")) {   
									 
										$sTxt =  file_get_contents($file_path."/$file[0].htm"); 
										$arr_string = explode('<span class="edited">',$sTxt);
										$ctr=0;
										//foreach loop to display the returned array
										foreach($arr_string as $str){
											if ($ctr!=0){
												$lVal=explode('</span>',$str); 
												
											?>
											<li><a href="#tab_2-2"><i class="fa fa-circle text-yellow"></i><?php echo $lVal[0];?></a></li>
											<?php
											}
											$ctr++;
										}
									}
								?>
				              	</ul>
				            </div>
		            	</div>
				  
				  		<?php if ($TextCategorization==1){ ?>
				  				<div class="box box-solid">
						            <div class="box-header with-border">
						              	<h3 class="box-title">ATC Result</h3>
										<div class="box-tools">
						                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						              	</div>
						            </div>
		            				<div class="box-body no-padding">
										<form method="POST" action="#"> 
		              						<ul class="nav nav-pills nav-stacked" id="dynamic-list">
											<?php
												$sql="Select * from tblATCResult where BatchID='".$JobID."'";
				 
												$rs=odbc_exec($conWMS,$sql);
												$ctr = odbc_num_rows($rs);
												while(odbc_fetch_row($rs))
												{
													$SearchString=odbc_result($rs,"SearchString");
													$ReplaceString=odbc_result($rs,"ReplaceString");

													$ctr=odbc_result($rs,"TotalReplacement");
					

													echo "<li>
															<a onclick='FindTermATC(\"".trim($ReplaceString)."\")' href='#'><i class='fa fa-book'></i>".$SearchString." to ".$ReplaceString."
											                	<span class='pull-right-container'>
											                 		<span class='label label-success pull-right'>{$ctr}</span>
											                	</span>
										              		</a>
													  	</li>";
					 							}
											?>	   
		              						</ul>
		              					</form>
					  				</div>
					  				<div class="box-footer with-border"></div>
								</div>
				  		<?php } ?>
		        	</div>
		        	<div class="col-md-8" id="editor_tab">
			     		<div class="nav-tabs-custom">
		            		<ul class="nav nav-tabs pull-right">
							<?php
								echo '<li class"tab_right"><a href="#tab_1-0-Innodom" data-toggle="tab" id="inndom_tab">Innodom</a></li>';
								
								
								if ($XMLEditor==1){
									echo '<li class="tab_right" onclick="RefreshEditor()"> <a href="#tab_1-1"  onclick="RefreshEditor()" data-toggle="tab">XML Editor</a></li>';
								}
								
								if ($Task=='WRITINGQC' || $Task=='FINALREVIEW'){
									echo '<LI class"tab_right" onClick="LoadFeedbackList()"><a href="#tab_4_2"  data-toggle="tab" >Feedback Form</a></LI>';
								}
								
								if ($Styling==1){
									echo '<li class"tab_right"><a href="#tab_2-2" data-toggle="tab">Styling</a></li>';
								}
					
				 				if ($TreeView==1){
					  				echo '<li id="gg_tab"><a href="#tab_2-1" data-toggle="tab">Golden Gate</a></li>';
								}
				 			?>
					  
		              			<li class="tab_right dropdown">
		                			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
		                  				ML<span class="caret"></span>
		                			</a>
		                			<ul class="dropdown-menu">
									<?php
										$sql = "SELECT * FROM tblmlconfig INNER JOIN tbltaskml ON tblmlconfig.id = tbltaskml.MLID Where TaskID='$TaskID'";
										if ($result=mysqli_query($con,$sql)){
							  				// Fetch one and one row
							  				while ($row=mysqli_fetch_row($result)){
												$MLName = $row[1];
												$Stat = GetFieldValue("Select * from tblstatus where MLName='$MLName' AND Process='$Task' AND Jobname='$BatchID'","Jobname",$con);
												if ($Stat!=''){
													echo '<li role="presentation" ><a role="menuitem" tabindex="-1" href="preloader/index.php?APIURL=../'.$row[2].'&FileURL='.$file_path.'/'.$sFileVal.'&MLName='.$row[1].'&FileName='.$sFileVal.'&RedirectURL=https://10.160.0.88/primoDataForAI/index.php&ID='.$row[0].'"><i class="fa fa-fw fa-check"></i>'.$row[1].'</a></li>';
											  	}else{
													echo '<li role="presentation" ><a role="menuitem" tabindex="-1" href="preloader/index.php?APIURL=../'.$row[2].'&FileURL='.$file_path.'/'.$sFileVal.'&MLName='.$row[1].'&FileName='.$sFileVal.'&RedirectURL=https://10.160.0.88/primoDataForAI/index.php&ID='.$row[0].'"><i class="fa fa-fw  fa-circle-o"></i>'.$row[1].'</a></li>';
												}
											}
										}
						 			?>				  
									</ul>
		              			</li>
		              			<li class="pull-left header" style="cursor:pointer;" onclick="TAB.hideShow();"><i class="fa fa-th"></i> </li>
		            		</ul>
					   		<div class="tab-content" >
								<div class="tab-pane tab_right_pane" id="tab_2-1" >
									<div class="row">
										<div class="col-lg-6">
										<?php   
											if ($Task=='QC'){
												echo '<iframe src="https://wb.innodatalabs.com/zoning/#/job/'.$GGJobID.'?token='.$TokenVAL.'"   style="width:200%; height:37vw;" id="GoldenGateFrame"  frameBorder="0" scrolling="auto"></iframe>';
											}else{
												echo '<iframe src="https://wb.innodatalabs.com/zoning/#/job/'.$GGJobID.'?token='.$TokenVAL.'"   style="width:200%; height:37vw;" id="GoldenGateFrame"  frameBorder="0" scrolling="auto"></iframe>';
											}
										?>
							 			</div>
									</div>
								</div>
								<div class="tab-pane tab_right_pane" id="tab_1-0-Innodom" >
									<div class="form-group" style="width:100%; height:65vh;">
										<textarea id="innodom_xml_tab" rows="100" name="code"><?= $innodomXML; ?></textarea>
									</div>
								</div>
								<div class="tab-pane tab_right_pane" id="tab_1-1"  >
									<fieldset>
										<div class="form-group" style="width:100%; height:65vh;">
											<?php
												echo '<textarea id="code" rows="100" spellcheck="true"  name="code">'.$sXML.'</textarea>';
											?>
						 					<textarea id="spellcheckText" rows="100" spellcheck="true" style="width:100%; height:65vh;display:none"></textarea>
											<script id="script">
												var prToggle=0;
												/*
												 * Demonstration of code folding
												 */
						 
							 					var te_html = document.getElementById("code");						
							 					var editor_html = CodeMirror.fromTextArea(te_html, {
													mode: "text/xml",
													lineNumbers: true,
													matchTags: {bothTags: true},
													lineWrapping: true,
													extraKeys: {"Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); }},
													foldGutter: true,
													styleActiveLine: true,
													styleActiveSelected: true,
													styleSelectedText: true,
													autoRefresh: true,
													indentUnit: 4,
													indentWithTabs: true,
													readOnly: false,
													smartIndent: true,

													gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
												});
							   
							   					editor_html.on ('beforeChange',function(){
							    					DisableTag(prToggle);
							   					});

												editor_html.refresh();
												editor_html.setSize("100%","65vh"); 

												function jumpToLine(prLineNo,prCol,prLength){
								  
														editor_html.refresh();
														editor_html.setCursor(prLineNo);
														// alert(prLength);
														editor_html.setSelection({line: prLineNo-1, ch: prCol-prLength}, {line: prLineNo-1, ch:prCol+prLength});

														// editor_html.markText({line: prLineNo-1, ch: prCol}, {line: prLineNo, ch:1}, {className: "styled-background"});

														// var line = editor_html.getLineHandle(prLineNo);
														// editor_html.setLineClass(line,'background','line-error');
												}
											</script>
											<br>
											<div class="pull-right">
								 				<span id="saveStatus" ></span>
								 				<input type="button" class="btn btn-default .btn-sm" onclick="iChecker.validate();" value="iChecker Validations">
								 				<input type="button" class="btn btn-primary .btn-sm" onclick="Plugins.show()" value="Plugins">
												<input type="button" class="btn btn-info .btn-sm" onclick="SpellCheck()" value="Modify XML" id="SpellCheck">
												<button type="button" class="btn btn-danger .btn-sm" id="Validate" onclick="ValidateXML()">Validate</button>
												<button type="button" class="btn btn-success .btn-sm" id="btnSave" onclick="saveXML()">Save</button>
											</div>
										</div>
									</fieldset>
									<?php
							
					  					if ($fileVal!=''){
						  					$file= str_replace(".pdf",".log",$sFileVal);
					  						$file= str_replace(".PDF",".log",$file);
					  						if (file_exists($file_path."/$file")) {
						  			?>
												<table style="width: 100%" cellpadding="0" cellspacing="0">
													<tr>
													  	<td style="width: 80px;"></td>	
													  	<td style="width: 300px;"><b>Description</b></td>
													  	<td style="width: 100px;"><b>Line No</b></td>
													  	<td style="width: 100px;"><b></b></td>
													</tr>
												</table>
												<div style="overflow: auto;height: 100px; width: 100%;">
		  											<table style="width: 100%;" cellpadding="0" cellspacing="0">
													    <?php
															$Casefile = fopen($file_path."/$file","r");
															while(! feof($Casefile)){
																$keyword=fgets($Casefile);
																if(trim($keyword)!=""){ 
																	$ctr=1;
																	$lineNo="";
																	$cats = explode("\t", $keyword);
																	$td = '';
																	foreach($cats as $cat) {
																		$cat = trim($cat);
																		if ($ctr==1){
																			$td ='<td style="width: 300px;">'.$cat.'</td>';
																		}elseif($ctr==2){
																			$lineNo=$cat;
																			$td ='<td style="width: 100px;">   <?php echo $cat;?></td>';
																		}
																		else{
																			$td= '<td style="width: 100px;"><a href="#" onClick="jumpToLine('.$lineNo.' ,'.$cat.');" >Check</a></td>';
																		}
																		$ctr++;
																	} 
																	echo '<tr>
																			<td style="width: 80px;" align="center"><input type="checkbox"></td>
																			'.$td.'
																		</tr>';
		   				  										}
						 									}
						 								?>
		  											</table>
												</div>
												<br>
									<?php
											}
		  								}
			  						?>
		              			</div>
		               			<div  class="tab-pane tab_right_pane"  id="tab_4_2">
								   	<div class="box-body pad">
										<div class="col-lg-12">
										  	<div class="form-group">
					                        	<label>Level of Issue</label>
					                        	<Select class="form-control" id="LevelofIssue">
					                          		<option value="Minor">Minor</option>
					                          		<option value="Medium">Medium</option>
					                          		<option value="High">High</option>
					                        	</Select>
					                      	</div>
					                      	<div class="form-group">
					                        	<label>Type of Issue</label><br>
					                         	<Select class="form-control" id="TypeOfIssue">
					                          		<option value="WRITING">Summary WRITING</option>
					                        	</Select>
					                      	</div>
					                      	<div class="form-group">
					                        	<label>Description of Issue</label>
					                        	<textarea  class="form-control" id="Description"></textarea>  
					                      	</div>
					                     	<div class="form-group">
					                        	<button class="btn-success" type="button" onclick="SaveFeedback()">Save</button>
					                        	<button class="btn-danger" type="button" onclick="ClearFeedbackForm()">Cancel</button>
					                      	</div>
										</div>
					                  	<span id ="FeedbackList"></span>
					              	</div>
								</div>
								<div class="tab-pane tab_right_pane" id="tab_2-2">
					    			<form1 method ="post" action="">
		                				<div class="box-body pad">
											<div id="editor1" name="editor1">
												<?php
													$path_parts = pathinfo($Filename);
													$nFilename=$path_parts['filename'];
													$file_path_styling = $file_path.'/'.$filenameni.'_response.html';
													if(file_exists($file_path_styling)){
														$HTMLFile = file_get_contents($file_path_styling);
														echo $HTMLFile;
													}else{
														echo "";
													}
												?>	
											</div>
										</div>
										<script>
											initSample();
										</script>
										<?php
											$sql="SELECT * FROM tblstyles";
											$ctr=0;
											if ($result=mysqli_query($con,$sql)){
						  						// Fetch one and one row
						  						while ($row=mysqli_fetch_array($result)){
													$StyleName=$row['StyleName'];
													$Inline=$row['Inline'];

													if ($row['ctrlKey']==1){
														$ctrl='CKEDITOR.CTRL';
													}else{
														$ctrl='';
													}

													if ($row['Shftkey']==1){
														$Shift='CKEDITOR.SHIFT';
													}else{
														$Shift='';
													}
													$keyVal=ord($row['KeyVal']);
													$ShortcutKey='';

													if ($ctrl!=''){
														$ShortcutKey=$ctrl;
													}

													if ($Shift!=''){
														if ($ctrl!=''){
															$ShortcutKey=$ShortcutKey.' + '.$Shift;
														}else{
															$ShortcutKey=$Shift;
														}
													}

													if ($keyVal!=''){
														if ($Shift!=''){
															if ($ctrl!=''){
															$ShortcutKey=$ctrl.' + '.$Shift.' + '.$keyVal;
															}
															else{
																$ShortcutKey=$Shift.' + '.$keyVal;
															}
														}
														else{
															if ($ctrl!=''){
																$ShortcutKey=$ctrl.' + '.$keyVal;
															}
														}
													}else{
														$ShortcutKey='';
													}
													
													if ($keyVal!=''){
										
														if ($ctr==0){
																$key="if ( event.data.keyCode == $ShortcutKey ) {                
																		this.fire( 'saveSnapshot' );
																		var style = new CKEDITOR.style( styles[ $ctr ] ),
																			elementPath = this.elementPath();
																		
																		this[ style.checkActive( elementPath ) ? 'removeStyle' : 'applyStyle' ]( style );
																		this.fire( 'saveSnapshot' );
																	}
																	";
																		
																		
															}
															else{
																$key=$key."if ( event.data.keyCode == $ShortcutKey ) {                
																		this.fire( 'saveSnapshot' );
																		var style = new CKEDITOR.style( styles[ $ctr ] ),
																			elementPath = this.elementPath();
																		
																		this[ style.checkActive( elementPath ) ? 'removeStyle' : 'applyStyle' ]( style );
																		this.fire( 'saveSnapshot' );
																}
																	"
																	;
															}
															
															
															
															$ctr++;
													}
												}
											}
					 					?>	
										<script>
											CKEDITOR.on('instanceReady', function (ev) {

										        // Create a new command with the desired exec function
										        var overridecmd = new CKEDITOR.command(editor1, {
										            exec: function(editor1){
										                // Replace this with your desired save button code
										                alert("test");
										            }
										        });

										        // Replace the old save's exec function with the new one
										        // ev.editor1.commands.save.exec = overridecmd.exec;
										    });
										</script>
										<div class="box-footer">
						   					<div class="pull-right">
												<input type="hidden" name="fileVal" value="<?php echo "$file[0].xml";?>">
												<!-- <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-shortcut" ><i class="fa fa-keyboard-o"></i> Shortcut key</button> -->
												<?php
											 	 	$dispSave='none';
												 	if ($Status==''){
														if ($StatusString=='Ongoing'){
															$dispSave='block';
														}else{
															$dispSave='none';
														}
												 	}
												?>
												<button onclick="saveHTMLFile()" id="SaveButton" style="display: <?=$dispSave;?>">Save</button>
											</div>
										</div>
						 
								 	</form1>
							 	</div>
		            		</div>
					 	</div>
						<form method="post" action="SetAsCompleted.php">
							<div class="modal modal-primary fade" id="modal-success">
							  	<div class="modal-dialog">
									<div class="modal-content">
								  		<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									  			<span aria-hidden="true">&times;</span>
									  		</button>
											<h4 class="modal-title">Set As Completed</h4>
								  		</div>
								  		<div class="modal-body">
								   			<input type="hidden" name="BatchID" id="BatchID" value="<?php echo "$BatchID";?>">
								   			<input type="hidden" id="RelevantValue" name="RelevantValue" value="">
											<p>Are you sure you want to set this batch as completed?</p>
								  		</div>
								  		<div class="modal-footer">
											<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								 			<button type="button" class="btn btn-outline" onclick="saveXMLAndComplete()" data-dismiss="modal">Complete</button>
								 		</div>
									</div>
							  	</div>
							</div>
						</form>
						<div class="modal modal-info fade" id="modal-shortcut">
					  		<div class="modal-dialog">
								<div class="modal-content">
						  			<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  				<span aria-hidden="true">&times;</span>
										</button>
										<h4 class="modal-title">Shortcut key</h4>
						  			</div>
						  			<div class="modal-body">
						  				<table>
											<tr>
											  	<td><b>Stylename</b></td>
											  	<td>&emsp;&emsp;&emsp;</td>
											  	<td><b>Shortcut Key</b></td>
											  	<td>&emsp;&emsp;&emsp;</td>
											  	<td></td>
											</tr>
											<?php
												$sql="SELECT * FROM tblstyles";
												$ctr=0;
												if ($result=mysqli_query($con,$sql)){
						  							// Fetch one and one row
						  							while ($row=mysqli_fetch_row($result)){
								
														if ($row[3]==1){
															$Inline='checked';
														}else{
															$Inline='';
														}

														if ($row[4]==1){
															$ctrl='CTRL';
														}else{
															$ctrl='';
														}


														if ($row[5]==1){
															$Shift='Shift';
														}else{
															$Shift='';
														}
														$keyVal=$row[6];


														$ShortcutKey='';

														if ($ctrl!=''){
															$ShortcutKey=$ctrl;
														}

														if ($Shift!=''){
															if ($ctrl!=''){
																$ShortcutKey=$ShortcutKey.'+'.$Shift;
															}else{
																$ShortcutKey=$Shift;
															}
														}

														if ($keyVal!=''){
															if ($Shift!=''){
																if ($ctrl!=''){
																	$ShortcutKey=$ctrl.'+'.$Shift.'+'.$keyVal;
																}else{
																	$ShortcutKey=$Shift.'+'.$keyVal;
																}
															}else{
																if ($ctrl!=''){
																	$ShortcutKey=$ctrl.'+'.$keyVal;
																}
															}
														}else{
															$ShortcutKey='';
														}
														echo '<tr>
																<td>'.$row[1].'</td>  
																<td>&emsp;&emsp;&emsp;</td>
																<td>'.$ShortcutKey.'</td>
																<td>&emsp;&emsp;&emsp;</td>
							 									<td><input type="Color"  name="Color" value="'.$row[2].'"></td>
							 								</tr>';
													}
						  						}
											?>
										</table>
						  			</div>
						  			<div class="modal-footer">
										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
							 		</div>
								</div>
					  		</div>
						</div>
						<form method="post" action="StartJob.php">
							<div class="modal modal-primary fade" id="modal-Start">
							  	<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											 	<span aria-hidden="true">&times;</span>
											</button>
											<h4 class="modal-title">Job Started</h4>
										</div>
									  	<div class="modal-body">
									   		<input type="hidden" name="BatchID1"  id="BatchID1"  value="<?php echo "$BatchID";?>">
									   		<input type="hidden" name="Task"  id="Task"  value="<?php echo "$Task";?>">
											<p>Are you sure you want to start this batch?</p>
									  	</div>
									  	<div class="modal-footer">
											<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
											<button type="button" data-dismiss="modal" class="btn btn-outline" onclick="StartJob()">Start</button>
									  	</div>
									</div>
								</div>
							</div>
						</form>
						<form method="post" action="PendingJob.php">
							<div class="modal modal-warning fade" id="modal-Pending">
								<div class="modal-dialog">
									<div class="modal-content">
									  	<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										  		<span aria-hidden="true">&times;</span>
										  	</button>
											<h4 class="modal-title">Job Pending</h4>
									  	</div>
									  	<div class="modal-body">
									   		<input type="hidden" name="BatchID2" id="BatchID2" value="<?php echo "$BatchID";?>">
											<p>Are you sure you want to set this batch as Pending?</p>
									  	</div>
									  	<div class="modal-footer">
											<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
											<button type="button" class="btn btn-outline" data-dismiss="modal" onclick="PendingJob()">Pending</button>
									  	</div>
									</div>
								</div>
							</div>
						</form>
		        	</div>
			  	</div>
		<?php } ?>
    </section>
</div>
 

<div class="modal fade" id="modal-progress">
  <div class="modal-dialog  modal-sm">
  <div class="modal-content">
     <p align="center">
     <img src="images/Preloader_3/Preloader_3.gif">
    </p>
     <p align="center" id="pgCaption">
    Converting HTML to XML.
    </p>
    <p align="center"><font color="blue">please wait...</font></p>
    <br>
  </div>
  <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
 
<form method="post" action="HoldJob.php">
    <div class="modal modal-danger fade" id="modal-Hold">
        <div class="modal-dialog">
        	<div class="modal-content">
          		<div class="modal-header">
          			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
            			<span aria-hidden="true">&times;</span>
            		</button>
          			<h4 class="modal-title">Hold Batch</h4>
          		</div>
          		<div class="modal-body">
           			<input type="hidden" name="BatchID3" value="<?php echo "$BatchID";?>">
          			<p>Are you sure you want to put this batch on hold?</p>
          			<p>Remarks: <textarea rows="10" cols="80" class="form-control" name="Remarks"></textarea></p>
          		</div>
          		<div class="modal-footer">
          			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
          			<button type="Submit" class="btn btn-outline">Hold</button>
          		</div>
        	</div>
        </div>
    </div>
</form>

<div id="myModalEdit" class="modal fade in" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
                <button type="button" class="btn btn-flat btn-sm btn-success" id="applyAttributeEdit">Apply</button>
                <button type="button" class="btn btn-flat btn-sm btn-default" data-dismiss="modal">Cancel</button>
          </div>
    </div>
  </div>
</div>
<div class="modal fade" id="tags_modal">
    <div class="modal-dialog"> 
      	<div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	            </button>
	            <h4 class="modal-title">Add New Tag</h4>
	        </div>
          	<div class="modal-body">
	            <form role="form" id="tag_form">
	            	<input type="hidden" name="tagid" id="tagid" value="0">
	                <div class="form-group">
	                    <label>Tag <span class="text-muted">(example: P1, P1, P3, L1)</span></label>
	                    <input type="text" name="tag" id="tag" class="form-control" req="true" message="Tag is required!">
	                </div>
	                <div class="form-group">
	                    <label>Description <span class="text-muted">(example: Paragraph 1, Paragraph 2)</span></label>
	                    <input type="text" name="tag_name" id="tag_name" class="form-control" req="true" message="Description is required!">
	                </div>
	                <div class="form-group">
	                    <label>Shortcut Key <span class="text-muted">(example: Ctrl+1, Shift+2)</span></label>
	                    <input type="text" name="tag_shortcut" id="tag_shortcut" class="form-control">
	                </div>
	            </form>
          	</div>
          	<div class="modal-footer">
              	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              	<input type="submit" class="btn btn-primary" id="submit_tag" value="Submit">
          	</div>
      	</div>
  	</div>
</div>
<div class="modal modal-danger" id="modal-delete">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	    		<span aria-hidden="true">&times;</span>
	    	</button>
	    	<input type="hidden" id="FeedbackID">
	    </div>
	    <div class="modal-body">
	        <div class="form-group">
	           <p>Are you sure you want to delete this Feedback?</p>
	        </div>
	    </div>
		<div class="modal-footer">
	      	<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
	     	<button type="Submit" class="btn btn-outline" data-dismiss="modal" onclick="DeleteFeedbackRecord()">Delete</button>
	  	</div>
	</div>
</div>
<div class="modal fade" id="modal-validate">
  	<div class="modal-dialog  modal-sm">
  		<div class="modal-content">
     		<p align="center">
     			<img src="images/Preloader_2/Preloader_2.gif">
    		</p>
     		<p align="center">
    			Validating XML...
    		</p>
  		</div>
  	</div>
</div>

<div class="modal modal-success" id="modal-Comments">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    			<span aria-hidden="true">&times;</span>
    		</button>
    		<input type="hidden" id="FeedbackID">
     		<h4 class="modal-title">Comment Box</h4>
      	</div>
    	<div class="modal-body">
          	<div class="box box-success">
             	<div class="box-body chat" id="chat-box" style="overflow-y: scroll;height: 40vh;"></div>
            	<div class="box-footer">
              		<div class="input-group">
                		<input class="form-control" placeholder="Type message..." id="comment">
						<div class="input-group-btn">
                  			<button type="button" class="btn btn-success" onclick="saveComment()"><i class="fa fa-plus"></i></button>
                		</div>
              		</div>
            	</div>
          	</div>
    	</div>
	</div>
</div>
<div class="modal fade" id="modal-plugins">
    <div class="modal-dialog"> 
	    <div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	            </button>
	            <h4 class="modal-title">Plugins</h4>
	        </div>
	        <div class="modal-body">
	            <form role="form" id="modal-plugins-form" enctype="multipart/form-data">
                    
					    <?php
					    	$base_path = realpath(__DIR__.'/..');
          					$plugins_dir = $base_path.'/Plugins';

					    	if(file_exists($plugins_dir)){
								$_dir = new DirectoryIterator($plugins_dir);
								$x = 0;
				                foreach ($_dir as $fileinfo) {
				                    if (!$fileinfo->isDot()) {
				                    	$x++;
				                    	echo '<div class="form-group">
				                    				<input type="radio" name="plugins_name" plugin_name="'.$fileinfo->getFilename().'" class="form-check-input" id="'.$fileinfo->getFilename().$x.'" onClick="Plugins.select();">
					    							<label class="form-check-label" for="'.$fileinfo->getFilename().$x.'">'.$fileinfo->getFilename().'</label>
					    						</div>
					    					';
					    
									}
				                }
				            }
					    ?>
				</form>
	        </div>
	        <div class="modal-footer">
	            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
	            <input type="submit" class="btn btn-primary" id="plugins-submit" value="Submit">
	        </div>
	    </div>
  	</div>
</div>
<script type="text/javascript">
	function StyleDoc (prStyle,prType) {
		
	 	var editor =CKEDITOR.instances['editor1'];
		var selectedHtml = "";
		var selection = editor.getSelection();
		if (selection) {
		    selectedHtml = getSelectionHtml(selection);
		}
	 	var strHTML;
		if (prType==1){
			strHTML='<span class="'+ prStyle +'">' + selectedHtml + '</span>';
			
		}
		else{
			strHTML='<div class="'+ prStyle +'">' + selectedHtml + '</div>';
			
			 
		}
		 
		editor.insertHtml(strHTML);
		 
	}

	function getRangeHtml(range) {
	    var content = range.extractContents();
	    // `content.$` is an actual DocumentFragment object (not a CKEDitor abstract)
	    var children = content.$.childNodes;
	    var html = '';
	    for (var i = 0; i < children.length; i++) {
	        var child = children[i];
	        if (typeof child.outerHTML === 'string') {
	            html += child.outerHTML;
	        } else {
	            html += child.textContent;
	        }
	    }
	    return html;
	}

	/**
	    Get HTML of a selection.
	*/
	function getSelectionHtml(selection) {
	    var ranges = selection.getRanges();
	    var html = '';
	    for (var i = 0; i < ranges.length; i++) {
	        html += getRangeHtml(ranges[i]);
	    }
	    return html;
	}

	function FindNext (prPosition) {
		var value = CKEDITOR.instances['editor1'].getData();
	
		var editor =CKEDITOR.instances['editor1'];
		
		CKEDITOR.instances['editor1'].focus();
	 
		var range = editor.createRange();
		var node = editor.document.getBody().getFirst();
		var parent = node.getParent();
		 range.collapse();

		range.setStart(parent,prPosition);
		//range.setStart(range.root,prPosition);
		//range.setStart(range.root, prPosition );
		//editor.getSelection().selectRanges( [ range ] );
		range.scrollIntoView();
		 
	}

	function FindTerm (findString) {
		 
		var editor =CKEDITOR.instances['editor1'];
		var documentWrapper = editor.document; // [object Object] ... CKEditor object
		var sel = editor.getSelection();

		var documentNode = documentWrapper.$; // [object HTMLDocument] .... DOM object
		var elementCollection = documentNode.getElementsByTagName('span');
		
		var rangeObjForSelection = new CKEDITOR.dom.range( editor.document );

	 	var nodeArray = [];
		for (var i = 0; i < elementCollection.length; ++i) {
						 
				nodeArray[i] = new CKEDITOR.dom.element( elementCollection[ i ] );
				
				if (nodeArray[i].getText()==findString){
					sel.selectElement(nodeArray[i] );
					 nodeArray[i].scrollIntoView();
					//editor.getSelection().selectRanges( rangeObjForSelection );
					//rangeObjForSelection.selectNodeContents( nodeArray[ i ] );
					break;
				}
				
			 
		}
	}



	function FindTermATC (findString) {
		
		var editor =CKEDITOR.instances['editor1'];
		var documentWrapper = editor.document; // [object Object] ... CKEditor object
		var sel = editor.getSelection();

		var documentNode = documentWrapper.$; // [object HTMLDocument] .... DOM object
		var elementCollection = documentNode.getElementsByTagName('*');
		 
		var rangeObjForSelection = new CKEDITOR.dom.range( editor.document );
	 
	 	var nodeArray = [];
		for (var i = 0; i < elementCollection.length; ++i) {
				nodeArray[i] = new CKEDITOR.dom.element( elementCollection[ i ] );
				if (nodeArray[i].getText()==findString){

					sel.selectElement(nodeArray[i] );
					nodeArray[i].scrollIntoView();
					 //CKEDITOR.instances['jTextArea'].insertHtml(nodeArray[i].getOuterHtml() + "<li><p>"+ titleCase(CKEDITOR.instances["editor1"].getSelection().getSelectedText()) +"</p></li>");
					//AddNewLine();
					//editor.getSelection().selectRanges( rangeObjForSelection );
					//rangeObjForSelection.selectNodeContents( nodeArray[ i ] );

					break;
				}
		}
	}

	function GenerateXML(){
		 editor_html.setValue("");
	 	$("#modal-progress").modal();
		var jTextArea = CKEDITOR.instances['editor1'].getData();
		var data = 'data='+encodeURIComponent(jTextArea);
                 
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange=function(){
            if (xmlhttp.readyState==4 && xmlhttp.status==200){
              //response.innerHTML=xmlhttp.responseText;
              
              editor_html.setValue(xmlhttp.responseText);
               $('#modal-progress').modal('hide');
            }
          }
          xmlhttp.open("POST","saveFile.php",true);
                //Must add this request header to XMLHttpRequest request for POST
          xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          xmlhttp.send(data);
	}
</script>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
<script>
	GetJobStatus();
		
	var task = "<?= $Task; ?>";
	var file_status = "<?= $FileStatus; ?>";
	if (file_status !='Done' && file_status !=''){
		window.setInterval(function(){
			//GetJobStatus();
			
		}, 20000);
		
	}

	$('#submit_tag').click(function(){
		if(Form.validate('#tag_form')){
			STYLINGTAGS.submit();
		}
	});
	var TAB = {
		status : 'show',
		status2 : 'show',
		hideShow : function(){
			if(TAB.status == 'show'){
				$('#editor_tab_panel').css({display : "none"});
				$('#editor_tab').removeAttr('class');
				$('#editor_tab').addClass('col-sm-12');
				TAB.status = 'hide';
			}else{
				$('#editor_tab_panel').css({display : ""}).removeAttr('class').addClass('col-sm-4');
				$('#editor_tab').removeAttr('class').addClass('col-sm-8');
				TAB.status = 'show';
			}
		},
		hideShow2 : function(){
			if(TAB.status2 == 'show'){
				$('#editor_tab_panel').removeAttr('class').addClass('col-sm-6');
				$('#editor_tab').removeAttr('class').addClass('col-sm-6');
				TAB.status2 = 'hide';
			}else{
				$('#editor_tab_panel').removeAttr('class').addClass('col-sm-4');
				$('#editor_tab').removeAttr('class').addClass('col-sm-8');
				TAB.status2 = 'show';
			}
		}
	};
	var STYLINGTAGS = {
		new : function(){
			$('#tags_modal .modal-title').html('Add New Tag');
			$('#tag').val('');
			$('#tag_name').val('');
			$('#tagid').val('0');
			$('#tag_shortcut').val('');
			$('#tags_modal').modal({
	            backdrop: 'static',
	            keyboard: false
	        });
		},
		submit : function(){
			var data = $('#tag_form').serializeArray();
			data.push({name: 'action', value: 'tags'});
			data.push({name: 'process', value: 'add'});

			Page.loading('Processing ...');

			$.post('postdata.php', data, function(response){
				Swal.close();
				try{	
					var result = JSON.parse(response);
					if(result.success){
						Page.success(result.message);
						$('#tags_modal').modal('hide');
						$('#tag').val('');
						STYLINGTAGS.loadTags();
					}else{
						Page.error(result.message);
					}
				}catch(e){
					Page.error(e);
				}
			});
		},
		loadTags : function(){
			var data = [
				{name: 'action', value: 'tags'},
				{name: 'process', value: 'get'}
			];
			$('#styling_tags').html('<span style="text-align:center;" class="text-muted"><i>Please wait while fetching data!</i></span>');
			$.post('postdata.php', data, function(response){
				try{
					var result = JSON.parse(response);
					if(result.success){
						console.log(result.data);
						if(result.data.length){
							$('#styling_tags').html('');
							$.each(result.data, function(i, v){
								var shortcutni = (v.shortcut != null && (v.shortcut).length > 0) ? "("+v.shortcut+")" : "";
								var shortcutnii = (v.shortcut != null && (v.shortcut).length > 0) ? v.shortcut : "";
								
								var shortcutniid = '_'+shortcutnii.replace("+","_");
								$('#styling_tags').append(
									$('<li>').append(
										$('<input>').attr({type: 'checkbox', name:'delete_chk', 'tag_id': v.id}).addClass('delete_styling_chk'),
										$('<a>').attr({href: 'javascript:void(0)', title: v.name}).click(function(){
											var strVal = editor_html.getSelection();
											var n = strVal.length;
											var res= strVal;//.substring(0,1);
											var a;
											a = "<"+v.tag+">" + res + "</"+v.tag+">";
											editor_html.replaceSelection(a);
										}).append(
											$('<i>').addClass('fa fa-gear')
										).html(v.tag).css({marginLeft: '10px'}),
										$('<span>').html(shortcutni).addClass("text-muted").css({marginLeft: '20px', fontSize: '11px'}).attr({id:shortcutniid}),
										$('<span>').append(
										   	$('<a>').attr({href: 'javascript:void(0)', title: 'Edit Tag'}).html('Edit').addClass('pull-right').click(function(){
										   		$('#tags_modal .modal-title').html('Edit Tag');
										   		$('#tag').val(v.tag);
										   		$('#tagid').val(v.id);
												$('#tag_name').val(v.name);
												$('#tag_shortcut').val(v.shortcut);
												$('#tags_modal').modal({
										            backdrop: 'static',
										            keyboard: false
										        });
										   	})	
										),
									)
								);
							});
						}else{
							$('#styling_tags').html('<span style="text-align:center;" class="text-muted">No Available Tags</span>');
						}
					}else{
						$('#styling_tags').html('<span style="text-align:center;color:red" class="text-muted">'+result.message+'</span>');
					}
				}catch(e){
					$('#styling_tags').html('<span style="text-align:center;color:red" class="text-muted">'+e+'</span>');
				}
			});
		},
		delete : function(){
			var _chk = $('.delete_styling_chk');
			var list = [];
			if(_chk.length){
				$.each(_chk, function(i, v){
					if($(v).is(':checked')){
						list.push($(v).attr('tag_id'));
					}
				});
			}

			if(list.length > 0){
				Swal.fire({
	                title: "Are you sure want to delete selected tags?",
	                text: '',
	                icon: 'warning',
	                showCancelButton: true,
	                confirmButtonColor: '#3085d6',
	                cancelButtonColor: '#d33',
	                confirmButtonText: 'Yes',
	                allowOutsideClick: false
	            }).then((result) => {
	                if (result.isConfirmed) {
	                    STYLINGTAGS.submitDelete(list);
	                }
	            })
			}else{
				Page.error('Please select tag on the list to delete!');
			}
		},
		submitDelete : function(obj){
			var data = [
				{name: 'action', value: 'tags'},
				{name: 'process', value: 'delete'},
				{name: 'ids', value: JSON.stringify(obj)}
			];

			Page.loading('Deleting ...');

			$.post('postdata.php', data, function(response){
				Swal.close();
				try{	
					var result = JSON.parse(response);
					if(result.success){
						Page.success(result.message);
						STYLINGTAGS.loadTags();
					}else{
						Page.error(result.message);
					}
				}catch(e){
					Page.error(e);
				}
			});
		}
	}

	var innodom_html = document.getElementById("innodom_xml_tab");						
	var editor_html_innodom = CodeMirror.fromTextArea(innodom_html, {
		mode: "text/xml",
		lineNumbers: true,
		matchTags: {bothTags: true},
		lineWrapping: true,
		extraKeys: {"Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); }},
		foldGutter: true,
		styleActiveLine: true,
		styleActiveSelected: true,
		styleSelectedText: true,
		autoRefresh: true,
		indentUnit: 4,
		indentWithTabs: true,
		readOnly: true,
		smartIndent: true,
		gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
	});

	editor_html_innodom.on ('beforeChange',function(){
		DisableTag(1);
	});

	editor_html_innodom.refresh();
	editor_html_innodom.setSize("100%","65vh"); 

	$('#MappingPanelTab').click(function(){
		$('.tab_right').removeClass("active");
		$('#gg_tab').removeAttr('class');
		$('#gg_tab').addClass('tab_right');
		$('#inndom_tab').closest('li').addClass('active');

		$('.tab_right_pane').removeClass('active');
		$('#tab_1-0-Innodom').addClass('active');
	});

	$('#inndom_tab').click(function(){
		$('.tab_left').removeClass("active");
		$('#MappingPanelTab').closest('li').addClass('active');

		$('.tab_left_pane').removeClass('active');
		$('#MappingPanel').addClass('active');
	});

	//reload pdf source
	$('#pdf_source_tab').click(function(){
		$('#source_embed').attr({src: $('#source_embed').attr('src')});
	});

	$('#MappingPanelTab').click(function(){
		MAPPINGTAGS.init();
	});
	
	var MAPPINGTAGS = {
		getSelection : function(el){
			var strVal = editor_html_innodom.getSelection();
			if(strVal.length > 0){
				$(el).val(strVal);
			}
		},
		save_and_replace : function(){
			Swal.fire({
	            title: "Are you sure you want to replace the existing meta data on XML Editor?",
	            text: '',
	            icon: 'warning',
	            showCancelButton: true,
	            confirmButtonColor: '#3085d6',
	            cancelButtonColor: '#d33',
	            confirmButtonText: 'Yes',
	            allowOutsideClick: false
	        }).then((result) => {
	            if (result.isConfirmed) {
	                MAPPINGTAGS.save('save_and_replace');
				}
	        });
		},
		save : function(processval){
			var data = $('#mapping_form').serializeArray();
			data.push({name: 'action', value: 'mapping'});
			data.push({name: 'process', value: 'save'});
			data.push({name: 'indicator', value: processval});
			Page.loading('Processing...');
			$.post('postdata.php', data, function(response){
				Swal.close();
				try{
					var result = JSON.parse(response);
					if(result.success){
						Page.success(result.message);
						if(processval == 'save_and_replace'){
							var data2 = {"data": $('#filenametopass').val() };
							$.post('XMLReader.php', data2, function(xml){
								editor_html.setValue('');
								editor_html.setValue(xml.trim());
							});
						}
					}else{
						Page.error(result.message);
					}
				}catch(e){
					Page.error(e);
				}
			});

		},
		init : function(){
			var data = [];
			data.push({name: 'action', value: 'mapping'});
			data.push({name: 'process', value: 'generate_form'});
			data.push({name: 'bundle', value: '<?=$bundle_path;?>'});
			data.push({name: 'filaname', value: '<?= $Filename;?>'});

			$('#mapping_div').html('<div class="col-sm-12" style="text-align:center;"><small class="text-muted">Fetching data...</small></div>');
			$.post('postdata.php', data, function(response){
				try{
					var result = JSON.parse(response);
					if(result.success){
						$('#mapping_div').html(result.html);
					}else{
						$('#mapping_div').html('<div class="col-sm-12" style="text-align:center;"><small class="text-muted" style="color:red !important;">'+result.message+'</small></div>');
					}
				}catch(e){
					$('#mapping_div').html('<div class="col-sm-12" style="text-align:center;"><small class="text-muted" style="color:red !important;">'+e+'</small></div>');
				}
			});
		}
	};

	var XMLValidation = {
		'click_logs' : function(line){
			var l = line - 3;
			editor_html.setCursor(l-1);
		}
	}

	var iChecker = {
		validate : function(){
			var editor_value = editor_html.getSelection();
			var data = [
                    {name:'action', value: 'iChecker'},
                    {name: 'xml_path', value: '<?= $client_xml_path;?>'}
               	];
            Page.loading('Processing ...');
            $.post('postdata.php', data, function(response){
                Swal.close();
                try{
                    var result = JSON.parse(response);
                    if(result.success){
                        Swal.fire({
                            allowOutsideClick: false,
                            title: result.message,
                            icon: 'info',
                            html: result.data,
                            showCloseButton: false,
                            showCancelButton: false,
                            focusConfirm: false,
                            confirmButtonText:'<i class="fa fa-thumbs-up"></i> OK',
                        }).then((wal_res) => {
                            if (wal_res.isConfirmed) {
                                
                            }
                        })
                    }else{
                        Page.error(result.message);
                    }
                }catch(e){
                    Page.error(e);
                }
            });
		}
	}

	var Plugins = {
		show: function(){
			$('#modal-plugins').modal({
               	backdrop: 'static',
                keyboard: false
            });
			$('#modal-plugins-form .form-check-input').prop("checked", false);
		},
		selected : '',
		hide: function(){
			$('#modal-plugins').modal('hide');
		},
		select: function(){
			Plugins.selected = '';
			var radio = $('#modal-plugins-form .form-check-input');
			var len = radio.length;
			if(len > 0){
				for(var i = 0; i < len; i++){
					if($(radio[i]).is(":checked")){
						Plugins.selected = $(radio[i]).attr('plugin_name');
					}
				}
			}
		},
		submit : function(){
			var editor_value = editor_html.getSelection();
			var data = [
                    {name:'action', value: 'plugins'},
                    {name: 'plugin', value: Plugins.selected},
                    {name: 'selected_value', value: editor_value},
                    {name: 'xml_path', value: '<?= $client_xml_path;?>'}
                ];
            Page.loading('Processing ...');
            $.post('postdata.php', data, function(response){
                Swal.close();
                try{
                    var result = JSON.parse(response);
                    if(result.success){
                        Swal.fire({
                            allowOutsideClick: false,
                            title: '<strong>Result</strong>',
                            icon: 'info',
                            html: result.message,
                            showCloseButton: false,
                            showCancelButton: false,
                            focusConfirm: false,
                            confirmButtonText:'<i class="fa fa-thumbs-up"></i> OK',
                        }).then((wal_res) => {
                            if (wal_res.isConfirmed) {
                                Plugins.hide();
								if(editor_value.length > 0){ //highlighted text
                                	editor_html.replaceSelection(result.data);
                                }else{ //selected all
                                	editor_html.setValue("");
                                	editor_html.setValue((result.data).trim());
                                }
                            }
                        })
                    }else{
                        Page.error(result.message);
                    }
                }catch(e){
                    Page.error(e);
                }
            });
		}
	}

	$('#plugins-submit').click(function(){
		if(Plugins.selected.length > 0){
			Swal.fire({
                title: "Are you sure want to use selected plugin?",
                html: '<strong>Submitting the selected plugin will update the XML transformation in XML Editor!</strong>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                allowOutsideClick: false
            }).then((result) => {
                if (result.isConfirmed) {
                    Plugins.submit();
                }
            })
		}else{
			Page.error("Please select sReplace plugins to process!");
		}
	});
</script>