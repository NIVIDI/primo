<!-- PUBLIC -//Carswell//ENTITIES Keying Entities//EN" -->

<!-- one file shared by all Keying DTDs -->

<!ENTITY Aa     SDATA "&Aacute;" --LATIN CAPITAL LETTER A WITH ACUTE-->
<!ENTITY aa     SDATA "&aacute;" --LATIN SMALL LETTER A WITH ACUTE-->
<!ENTITY Ac     SDATA "&Acirc;"  --LATIN CAPITAL LETTER A WITH CIRCUMFLEX-->
<!ENTITY ac     SDATA "&acirc;"  --LATIN SMALL LETTER A WITH CIRCUMFLEX-->
<!ENTITY AElig  SDATA "&AElig;"  --LATIN CAPITAL AE DIPTHONG (LIGATURE)-->
<!ENTITY aelig  SDATA "&aelig;"  --LATIN SMALL AE DIPTHONG (LIGATURE)-->
<!ENTITY Ag     SDATA "&Agrave;" --LATIN CAPITAL LETTER A WITH GRAVE-->
<!ENTITY ag     SDATA "&agrave;" --LATIN SMALL LETTER A WITH GRAVE-->
<!ENTITY alpha  SDATA "&alpha;"  --GREEK SMALL LETTER ALPHA-->
<!ENTITY Amacr  SDATA "&Amacr;"  --LATIN CAPITAL A WITH BAR-->
<!ENTITY amacr  SDATA "&amacr;"  --LATIN SMALL A WITH BAR-->
<!ENTITY amp    SDATA "&amp;"    --ampersand-->
<!ENTITY ape    SDATA "&ape;"    --APPROXIMATE, EQUALS-->
<!ENTITY Aring  SDATA "&Aring;"  --LATIN CAPITAL LETTER A WITH RING-->
<!ENTITY aring  SDATA "&aring;"  --LATIN SMALL LETTER A WITH RING-->
<!ENTITY atilde SDATA "&atilde;" --LATIN SMALL LETTER A WITH TILDE-->
<!ENTITY Au     SDATA "&Auml;"   --LATIN CAPITAL LETTER A WITH DIAERESIS-->
<!ENTITY au     SDATA "&auml;"   --LATIN SMALL LETTER A WITH DIAERESIS-->
<!ENTITY beta   SDATA "&beta;"   --GREEK SMALL BETA-->
<!ENTITY block  SDATA "&block;"  --block-->
<!ENTITY bull   SDATA "&bull;"   --bullet-->
<!ENTITY caret  SDATA "&caret;"  --caret symbol, insertion mark-->
<!ENTITY cap    SDATA "&cap;"    --Upper case C, tipped over forward, intersection-->
<!ENTITY Cc     SDATA "&Ccedil;" --LATIN CAPITAL LETTER C WITH CEDILLA-->
<!ENTITY cc     SDATA "&ccedil;" --LATIN SMALL LETTER C WITH CEDILLA-->
<!ENTITY cents  SDATA "&cents;"  --cent sign-->
<!ENTITY check  SDATA "&check;"  --check mark-->
<!ENTITY cir    SDATA "&cir;"    --circle, open-->
<!ENTITY cirf   SDATA "&cirf;"   --circle, filled-->
<!ENTITY cmacr  SDATA "&cmacr;"  --small c, macron (bar above letter)-->
<!ENTITY copy   SDATA "&copy;"   --copyright sign-->
<!ENTITY dagger SDATA "&dagger;" --dagger-->
<!ENTITY darr   SDATA "&darr;"   --down arrow-->
<!ENTITY deg    SDATA "&deg;"    --degree sign--> 
<!ENTITY Delta  SDATA "&Delta;"  --GREEK CAPITAL DELTA-->
<!ENTITY delta  SDATA "&delta;"  --GREEK SMALL DELTA-->
<!ENTITY diams  SDATA "&diams;"  --diamond suit (solid)-->
<!ENTITY divide SDATA "&divide;" --division sign-->
<!ENTITY Dmacr  SDATA "&Dmacr;"  --LATIN CAPITAL D WITH BAR-->
<!ENTITY Ea     SDATA "&Eacute;" --LATIN CAPITAL LETTER E WITH ACUTE-->
<!ENTITY ea     SDATA "&eacute;" --LATIN SMALL LETTER E WITH ACUTE-->
<!ENTITY Ecaron SDATA "&Ecaron;" --LATIN CAPITAL LETTER E WITH CARON-->
<!ENTITY ecaron SDATA "&ecaron;" --LATIN SMALL LETTER E WITH CARON-->
<!ENTITY Ec     SDATA "&Ecirc;"  --LATIN CAPITAL LETTER E WITH CIRCUMFLEX-->
<!ENTITY ec     SDATA "&ecirc;"  --LATIN SMALL LETTER E WITH CIRCUMFLEX-->
<!ENTITY ee     SDATA "&ee;"     --exponential base of the natural logarithm-->
<!ENTITY Eg     SDATA "&Egrave;" --LATIN CAPITAL LETTER E WITH GRAVE-->
<!ENTITY eg     SDATA "&egrave;" --LATIN SMALL LETTER E WITH GRAVE-->
<!ENTITY Emacr  SDATA "&Emacr;"  --capital e, macron -->
<!ENTITY emacr  SDATA "&emacr;"  --small e, macron -->
<!ENTITY epsiv  SDATA "&epsiv;"  --varepsilon (variation on epsilon: more script-like)-->
<!ENTITY equiv	SDATA "&equiv;"	 --triple bar-->
<!ENTITY esh    SDATA "&esh;"    --Latin small s; integral operator-->
<!ENTITY eta    SDATA "&eta;"    --GREEK SMALL LETTER ETA-->
<!ENTITY Eu     SDATA "&Euml;"   --LATIN CAPITAL LETTER E WITH DIAERESIS-->
<!ENTITY eu     SDATA "&euml;"   --LATIN SMALL LETTER E WITH DIAERESIS-->
<!ENTITY euro   SDATA "&euro;"   --Euro currency symbol-->
<!ENTITY female SDATA "&female;" --circle with a crossed line underneath-->
<!ENTITY fnof   SDATA "&fnof;"   --FUNCTION OF (italic small f)-->
<!ENTITY forall SDATA "&forall;" --FOR ALL (upside down capital A)-->
<!ENTITY gamma  SDATA "&gamma;"  --small gamma, Greek-->
<!ENTITY Gback  SDATA "&Gback;"  --back g in Nisga'a language; upper case g with underline-->
<!ENTITY gback  SDATA "&gback;"  --back g in Nisga'a language; lower case g with underline-->
<!ENTITY ge     SDATA "&ge;"     --GREATER-THAN-OR-EQUAL-->
<!ENTITY gt     SDATA "&gt;"     --GREATER THAN-->
<!ENTITY heart  SDATA "&heart;"  --HEART SUIT SYMBOL-->
<!ENTITY hellip SDATA "&hellip;" --HORIZONTAL ELLIPSIS-->
<!ENTITY Ia     SDATA "&Iacute;" --LATIN CAPITAL LETTER I WITH ACUTE-->
<!ENTITY ia     SDATA "&iacute;" --LATIN SMALL LETTER I WITH ACUTE-->
<!ENTITY ibreve SDATA "&ibreve;" --LATIN SMALL LETTER I WITH BREVE-->
<!ENTITY Ic     SDATA "&Icirc;"  --LATIN CAPITAL LETTER I WITH CIRCUMFLEX-->
<!ENTITY ic     SDATA "&icirc;"  --LATIN SMALL LETTER I WITH CIRCUMFLEX-->
<!ENTITY Igrave SDATA "&Igrave;" --LATIN CAPITAL LETTER I WITH GRAVE-->
<!ENTITY igrave SDATA "&igrave;" --LATIN SMALL LETTER I WITH GRAVE-->
<!ENTITY imacr  SDATA "&imacr;"  --lower case i with macron (bar on top)-->
<!ENTITY Iogon  SDATA "&Iogon;"  --LATIN CAPITAL LETTER I WITH OGONEK-->
<!ENTITY iogon  SDATA "&iogon;"  --LATIN SMALL LETTER I WITH OGONEK-->
<!ENTITY iquest SDATA "&iquest;" --inverted question mark-->
<!ENTITY Iu     SDATA "&Iuml;"   --LATIN CAPITAL LETTER I WITH DIAERESIS-->
<!ENTITY iu     SDATA "&iuml;"   --LATIN SMALL LETTER I WITH DIAERESIS-->
<!ENTITY Kback  SDATA "&Kback;"  --back k in Nisga'a language; upper case k with underline-->
<!ENTITY kback  SDATA "&kback;"  --back k in Nisga'a language; lower case k with underline-->
<!ENTITY laquo  SDATA "&laquo;"  --LEFT ANGLE QUOTATION MARK-->
<!ENTITY larr   SDATA "&larr;"   --left arrow-->
<!ENTITY lcedil SDATA "&lcedil;" --lower case l with cedilla-->
<!ENTITY ldquo  SDATA "&ldquo;"  --LEFT DOUBLE QUOTATION MARK-->
<!ENTITY le     SDATA "&le;"     --LESS THAN OR EQUAL-->
<!ENTITY lsquo  SDATA "&lsquo;"  --LEFT SINGLE QUOTATION MARK-->
<!ENTITY lstrok SDATA "&lstrok;" --SMALL L, STROKE-->
<!ENTITY lt  	SDATA "&lt;"	 --LESS THAN-- >
<!ENTITY male   SDATA "&male;"   --circle with a crossed line top sideways-->
<!ENTITY mdash  SDATA "&mdash;"  --em dash-->

<!ENTITY mu     SDATA "&mu;"     --GREEK SMALL LETTER MU-->
<!ENTITY ndash  SDATA "&ndash;"  --en dash-->
<!ENTITY nbsp   SDATA "&nbsp;"   --no-break space-->
<!ENTITY ncedil SDATA "&ncedil;" --lower case n with cedilla-->
<!ENTITY ne     SDATA "&ne;"     --not equal to sign-->
<!ENTITY ntilde SDATA "&ntilde;" --LATIN SMALL LETTER N WITH TILDE-->
<!ENTITY Ntilde SDATA "&Ntilde;" --LATIN CAPITAL LETTER N WITH TILDE-->
<!ENTITY nu     SDATA "&nu;"     --GREEK SMALL LETTER NU-->
<!ENTITY Oa     SDATA "&Oacute;" --LATIN CAPITAL LETTER O WITH ACUTE-->
<!ENTITY oa     SDATA "&oacute;" --LATIN SMALL LETTER O WITH ACUTE-->
<!ENTITY obreve SDATA "&obreve;" --LATIN SMALL LETTER O WITH BREVE-->
<!ENTITY Oc     SDATA "&Ocirc;"  --LATIN CAPITAL LETTER O WITH CIRCUMFLEX-->
<!ENTITY oc     SDATA "&ocirc;"  --LATIN SMALL LETTER O WITH CIRCUMFLEX-->
<!ENTITY oelig  SDATA "&oelig;"  --LATIN OE DIPTHONG (LIGATURE)-->
<!ENTITY OElig  SDATA "&OElig;"  --LATIN CAPITAL LETTER OE DIPTHONG (LIGATURE)-->
<!ENTITY Og     SDATA "&Ograve;"     --LATIN CAPITAL LETTER O WITH GRAVE-->
<!ENTITY og     SDATA "&ograve;" --LATIN SMALL LETTER O WITH GRAVE-->
<!ENTITY oinfin SDATA "&oinfin;" --infinity symbol in circle-->
<!ENTITY omacr  SDATA "&omacr;"  --SMALL O, MACRON-->
<!ENTITY Omega  SDATA "&Omega;"  --GREEK CAPITAL LETTER OMEGA-->
<!ENTITY omega  SDATA "&omega;"  --GREEK SMALL LETTER OMEGA-->
<!ENTITY Oogon  SDATA "&Oogon;"  --LATIN CAPITAL LETTER O WITH OGONEK-->
<!ENTITY oogon  SDATA "&oogon;"  --LATIN SMALL LETTER O WITH OGONEK-->
<!ENTITY oplus  SDATA "&oplus;"  --PLUS SIGN IN CIRCLE-->
<!ENTITY ordm   SDATA "&ordm;"   --MASCULINE ORDINAL INDICATOR-->
<!ENTITY Oslash SDATA "&Oslash;" --LATIN CAPITAL LETTER O WITH SLASH-->
<!ENTITY oslash SDATA "&oslash;" --LATIN SMALL LETTER O WITH SLASH-->
<!ENTITY Otilde SDATA "&Otilde;" --LATIN CAPITAL LETTER O WITH TILDE-->
<!ENTITY otilde SDATA "&otilde;" --LATIN SMALL LETTER O WITH TILDE-->
<!ENTITY Ou     SDATA "&Ouml;"   --LATIN CAPITAL LETTER O WITH DIAERESIS-->
<!ENTITY ou     SDATA "&ouml;"   --LATIN SMALL LETTER O WITH DIAERESIS-->
<!ENTITY para   SDATA "&para;"   --pilcrow sign-->
<!ENTITY part   SDATA "&part;"   --PARTIAL DIFFERENTIAL-->
<!ENTITY pi     SDATA "&pi;"     --GREEK SMALL LETTER PI-->
<!ENTITY Phi    SDATA "&Phi;"    --GREEK CAPITAL LETTER PHI-->

<!ENTITY plusmn SDATA "&plusmn;" --plus or minus sign-->
<!ENTITY Pmacr  SDATA "&Pmacr;"  --capital P, macron-->
<!ENTITY pound  SDATA "&pound;"  --pound sign-->
<!ENTITY Ppeso  SDATA "&Ppeso;"  --Philippine peso-->
<!ENTITY Prime  SDATA "&Prime;"  --double prime-->
<!ENTITY prime  SDATA "&prime;"  --single prime-->
<!ENTITY Psi    SDATA "&Psi;"    --GREEK CAPITAL LETTER PSI-->
<!ENTITY psi    SDATA "&psi;"    --GREEK SMALL LETTER PSI-->
<!ENTITY radic  SDATA "&radic;"  --square root-->
<!ENTITY rarr   SDATA "&rarr;"   --rightward arrow-->
<!ENTITY raquo  SDATA "&raquo;"  --RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK-->
<!ENTITY rdquo  SDATA "&rdquo;"  --RIGHT DOUBLE QUOTATION MARK-->
<!ENTITY reg	SDATA "&reg;"	 --circledR =registered sign-->
<!ENTITY rsquo  SDATA "&rsquo;"  --RIGHT SINGLE QUOTATION MARK-->
<!ENTITY rx     SDATA "&rx;"     --pharmaceutical prescription (Rx)-->
<!ENTITY scaron SDATA "&scaron;" --small s with caron-->
<!ENTITY Scaron SDATA "&Scaron;" --upper case s with caron-->
<!ENTITY schwa  SDATA "&schwa;"  --SCHWA (UPSIDE DOWN SMALL E)-->
<!ENTITY sect   SDATA "&sect;"   --section sign-->
<!ENTITY Sigma  SDATA "&Sigma;"  --GREEK CAPITAL LETTER SIGMA-->
<!ENTITY smile  SDATA "&smile;"  --UP CURVE-->
<!ENTITY square SDATA "&square;" --white square for checkbox-->

<!ENTITY sup-e      SDATA "&sup-e;"    --superscript e-->
<!ENTITY sup-egre   SDATA "&sup-egre;" --superscript egre-->
<!ENTITY sup-er     SDATA "&sup-er;"   --superscript er-->
<!ENTITY sup-ere    SDATA "&sup-ere;"  --superscript ere-->
<!ENTITY sup-ers    SDATA "&sup-ers;"  --superscript ers-->
<!ENTITY sup-r      SDATA "&sup-r;"    --superscript r-->
<!ENTITY sup-rs     SDATA "&sup-rs;"   --superscript rs-->
<!ENTITY sup-re     SDATA "&sup-re;"   --superscript re-->
<!ENTITY sup-res    SDATA "&sup-res;"  --superscript res-->
<!ENTITY sup-lle    SDATA "&sup-lle;"  --superscript lle-->
<!ENTITY sup-me     SDATA "&sup-me;"   --superscript me-->
<!ENTITY sup-os     SDATA "&sup-os;"   --superscript os-->

<!ENTITY sum	SDATA "&sum;"    --summation operator-->
<!ENTITY tau    SDATA "&tau;"    --GREEK SMALL LETTER TAU-->
<!ENTITY Theta  SDATA "&Theta;"  --CAPITAL THETA, GREEK-->
<!ENTITY there4 SDATA "&there4;" --therefore symbol-->
<!ENTITY thetas SDATA "&thetas;" --STRAIGHT THETA, GREEK-->
<!ENTITY tilde  SDATA "&tilde;"  --TILDE-->
<!ENTITY times  SDATA "&times;"  --multiplication sign-->
<!ENTITY timesb SDATA "&timesb;" --times box ("x" in a box)-->
<!ENTITY trade  SDATA "&trade;"  --trade mark sign-->
<!ENTITY Ua     SDATA "&Uacute;" --LATIN CAPITAL LETTER U WITH ACUTE-->
<!ENTITY ua     SDATA "&uacute;" --LATIN SMALL LETTER U WITH ACUTE-->
<!ENTITY uarr   SDATA "&uarr;"   --UPWARD ARROW-->
<!ENTITY Uc     SDATA "&Ucirc;"  --LATIN CAPITAL LETTER U WITH CIRCUMFLEX-->
<!ENTITY uc     SDATA "&ucirc;"  --LATIN SMALL LETTER U WITH CIRCUMFLEX-->
<!ENTITY Ug     SDATA "&Ugrave;" --LATIN CAPITAL LETTER U WITH GRAVE-->
<!ENTITY ug     SDATA "&ugrave;" --LATIN SMALL LETTER U WITH GRAVE-->
<!ENTITY utri   SDATA "&utri;"   --upward triangle-->
<!ENTITY utrif  SDATA "&utrif;"  --upward triangle filled-->
<!ENTITY Uu     SDATA "&Uuml;"   --LATIN CAPITAL LETTER U WITH DIAERESIS-->
<!ENTITY uu     SDATA "&uuml;"   --LATIN SMALL LETTER U WITH DIAERESIS-->
<!ENTITY verbar SDATA "&verbar;" --VERTICAL BAR-->
<!ENTITY Xback  SDATA "&Xback;"  --back x in Gitxsan language; upper case x with underline-->
<!ENTITY xback  SDATA "&xback;"  --back x in Gitxsan language; lower case x with underline-->
<!ENTITY yen    SDATA "&yen;"	 --YEN CHARACTER-->
<!ENTITY yuml   SDATA "&yuml;"   --LATIN SMALL LETTER Y WITH DIAERESIS-->
<!ENTITY Scirc  SDATA "&Scirc;"  --LATIN CAPITAL LETTER S WITH CIRCUMFLEX-->
<!ENTITY scirc  SDATA "&scirc;"  --LATIN SMALL LETTER S WITH CIRCUMFLEX-->

<!ENTITY logo      SDATA "&logo;"   --Carswell feather and name -->
<!ENTITY newline   PI "[newline]"   --new line-->
<!ENTITY newpage   PI "[newpage]"   --new page-->
<!ENTITY dottab    PI "[dottab]"    --dotted tab to right margin-->
<!ENTITY ellipsis  PI "[ellipsis]"  --centered ellipsis-->
<!ENTITY ellip10   "__________"     --10-dash ellipsis-->
<!ENTITY ellip35   "___________________________________" --35-dash ellipsis-->

<!-- added Jan. 21, 2009 (Prec-1610.inno) -->

<!ENTITY ouml   SDATA "&ouml;"   --REVERSED SMALL LETTER E-->
