<?php
include "conn.php";

$sql="SELECT * FROM tblUserAccess Where UserID='{$_SESSION['UserID']}'";
 
if ($result=mysqli_query($con,$sql))
{
// Fetch one and one row
	while ($row=mysqli_fetch_row($result))
	{
		$ACQUIRE=$row[1];
		$ENRICH=$row[2];
		$DELIVER=$row[3];
		$USER_MAINTENANCE=$row[4];
		$EDITOR_SETTINGS=$row[5];
		$ML_SETTINGS=$row[6];
		$TRANSFORMATION=$row[7];
		$TRANSMISSION=$row[8];
	}
}

include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Prioritization
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Prioritization</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
  
   <?php


$sql="SELECT * FROM tblmlconfig";

?>
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  
                  <th>SourceURL</th>
                  <th>CourtName</th>
                  
                  <th>Priority</th>
                </tr>
                </thead>
                <tbody>
                <?php
                			 
                        $strSQL = "SELECT * from tblJurisdiction ORDER BY Priority ASC";
                     
                				$objExec= odbc_exec($conWMS,$strSQL);
                				while ($row = odbc_fetch_array($objExec)) 
                				{
                ?>
                          <tr>
                            <td><?php echo $row["JurisdictionID"];?></td>
                            <!-- <td><?php echo $row["SourceUrl"];?></td> -->
                            <td><?php echo $row["URL"];?></td>
                            <td><?php echo $row["Jurisdiction"];?></td>
                          
                            <td><?php echo $row["Priority"];?></td>
                          </tr>
                <?php
                				}
                ?>	  
                </tbody>
                
              </table>
            </div>
            
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
