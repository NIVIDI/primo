<?php
include "conn.php";

// error_reporting(0);
$JobID=$_POST['data'];
$filename = trim($_POST['filename']);
$filename_without_ext = pathinfo($filename, PATHINFO_FILENAME);
$sql = "SELECT * FROM segregated_files WHERE filename = '{$filename}' LIMIT 1";
$sql_result = mysqli_query($con, $sql)->fetch_assoc();
$bundle_dir = !empty($sql_result['prec_bundle']) ? $sql_result['prec_bundle'] : '';
$segrated_id = !empty($sql_result['id']) ? $sql_result['id'] : '';

$token = getAPIKey($GGUserName,$GGPassword,$GGProductionMode);


$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://api.innodata.com/v1.1/jobs/'.$JobID.'/rewind');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);

curl_setopt($ch, CURLOPT_USERPWD, $token . ":" . $token);  
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
$jobj = json_decode($result);


if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}else{
    $xml_path = $SourceFilePath.'/'.$bundle_dir.'/'.$filename_without_ext.'.xml';
    $xml_path_meta = $SourceFilePath.'/'.$bundle_dir.'/'.$filename_without_ext.'_meta.xml';
    $xml_path_innodom = $SourceFilePath.'/'.$bundle_dir.'/sources/'.$filename_without_ext.'_response.xml';

    //delete equivalent BMP files
    $bmp_file_path = $SourceFilePath.'/'.$bundle_dir.'/'.$filename_without_ext.'/images';
    if(file_exists($bmp_file_path)){
        $bmp_dir = new DirectoryIterator($bmp_file_path);
        foreach ($bmp_dir as $fileinfo) {
            if (!$fileinfo->isDot()) {
                $image_without_ext = pathinfo($fileinfo->getFilename(), PATHINFO_FILENAME);
                if(file_exists($SourceFilePath.'/'.$bundle_dir.'/deliverables/'.$image_without_ext.'.BMP')){
                    unlink($SourceFilePath.'/'.$bundle_dir.'/deliverables/'.$image_without_ext.'.BMP');
                }

                if(file_exists($bmp_file_path.'/'.$fileinfo->getFilename())){
                    rrmdir($bmp_file_path.'/'.$fileinfo->getFilename());
                }
            }
        }
    }

    //check cropping and delete
    if(file_exists($SourceFilePath.'/'.$bundle_dir.'/'.$filename_without_ext)){
        rrmdir($SourceFilePath.'/'.$bundle_dir.'/'.$filename_without_ext);
    }

    //delete deliverables that are already compiled
    $sql_deliverables = "SELECT * FROM deliverables_segregated_files WHERE segregated_id = '{$id}' LIMIT 1";
    $deliverables_data = mysqli_query($con, $sql_deliverables)->fetch_assoc();
    if(!empty($deliverables_data)){
        $deliverable_path = $SourceFilePath.'/'.$bundle_dir.'/deliverables/'.$deliverables_data['filename'];
        if(file_exists($deliverable_path)){
            unlink($deliverable_path);
        }

        $sql_delete = "DELETE FROM deliverables_segregated_files WHERE segregated_id = '{$segrated_id}'";
        ExecuteQuery($sql_delete,$con);
    }

    //delete xml                            
    if(file_exists($xml_path)){
        unlink($xml_path);
    }

    //delete meta
    if(file_exists($xml_path_meta)){
        unlink($xml_path_meta);
    }

    //delete innodom
    if(file_exists($xml_path_innodom)){
        unlink($xml_path_innodom);
    }
}
curl_close($ch);

?>