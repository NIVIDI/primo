<?php
include "conn.php";

$sql="SELECT * FROM tblUserAccess Where UserID=' $_SESSION[UserID]'";
 
if ($result=mysqli_query($con,$sql))
{
// Fetch one and one row
  while ($row=mysqli_fetch_row($result))
  {
    $ACQUIRE=$row[1];
    $ENRICH=$row[2];
    $DELIVER=$row[3];
    $USER_MAINTENANCE=$row[4];
    $EDITOR_SETTINGS=$row[5];
    $ML_SETTINGS=$row[6];
    $TRANSFORMATION=$row[7];
    $TRANSMISSION=$row[8];
  }
}
include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>List of Delivered Document</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List of Completed Document</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
  
  
            <div class="col-md-12">
                  <div class="box box-primary">
                          <div class="box-header with-border">
                              <form method="post" action="">
                                  <div>
                                          <label> Jurisdiction:</label>
                                          <select name="Search" id="Search" >
                                            <?php
                                            $sql="Select DISTINCT Jurisdiction From tblJurisdiction ORDER BY Jurisdiction";
                                              $rs=odbc_exec($conWMS,$sql);
                                              $ctr = odbc_num_rows($rs);
                                              while(odbc_fetch_row($rs))
                                              {
                                                $Val=odbc_result($rs,"Jurisdiction");
                                                echo "<option value='".$Val."'>".$Val."</option>";
                                              }
                                              
                                              if ($_POST['Search']==''){
                                                    echo "<option value='' selected>All</option>";
                                              }
                                              else{
                                                    echo "<option value='".$_POST['Search']."' selected>".$_POST['Search']."</option>"; 
                                                    echo "<option value='' >All</option>";
                                              }

                                            ?> 
                                          </select>
                                        <button type="submit" class="btn btn-primary x-small"><i class="fa  fa-search"></i> Search</button> 
                                    </div>
                              </form>
                             
                          </div>
                          <!-- /.box-header -->
                          <div class="box-body">
                                  <form method="post" action="Deliver.php">
                                 
                                          <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                      <?php
                                      if ($_SESSION['UserType']=='Admin'){
                                        ?>
                                      <th><input type="Checkbox" id="chk_new"  onclick="checkAll('chk');" ></th>
                                      <?php
                                      }
                                      ?>
                                              <th>Batch Name</th>
                                              <th>Jurisdiction</th>
                                              <th>Source URL</th>
                                              <th>Source Title</th>
                                              <th>FileName</th>
                                              <th>Title</th>
                                              <th>Register</th>
                                              <th>Type</th>
                                              <th>Priority</th>
                                              <th>Topic</th>
                                              <th>Sub Topic</th>
                                              <th>Originating Date</th>
                                              <th>State Date</th>
                                              <th>Status</th>
                                              <th>User</th>
                                              <th>Last Update</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                    <?php
                                    
                             

                                    if ($_SESSION['UserType']=='Admin'){

                                        if (empty($_POST['Search'])){
                                            $strSQL="SELECT * FROM primo_view_Jobs Where StatusString='Transmitted'";  
                                        }
                                        else{
                                            $strSQL="SELECT * FROM primo_view_Jobs Where StatusString='Transmitted' AND Filaname='".$_POST['Search']."'";   
                                        }
                                     
                                    }else{
                                        if (empty($_POST['Search'])){
                                              $strSQL="SELECT * FROM primo_view_Jobs Where StatusString='Transmitted'  AND AssignedTo='$_SESSION[login_user]'";  
                                        }
                                        else{

                                            $strSQL="SELECT * FROM primo_view_Jobs Where StatusString='Transmitted'  AND AssignedTo='$_SESSION[login_user]' AND Filaname='".$_POST['Search']."'";  
                                        }
                                    }

                                    // }
                                    // else{
                                    //  $strSQL="SELECT * FROM primo_view_Jobs INNER JOIN tblRecord ON primo_view_Jobs.Filename =tblRecord.Filename Where  ProcessCode IN ('QC') AND statusstring ='DONE' AND AssignedTo='$_SESSION[login_user]' AND JOBNAME NOT IN (SELECT  JObname FROM primo_view_Jobs Where ProcessCode='TRANSMISSION' AND statusstring ='DONE' ) ORDER BY JOBName";  
                                    // }
                                    // $strSQL="Select * from tblRecord";
                                    $_SESSION['strSQL']=$strSQL;
                                    $objExec= odbc_exec($conWMS,$strSQL);
                                    
                                    $Jobname='';
                                    while ($row = odbc_fetch_array($objExec)) 
                                    {
                                        $filename="uploadFiles/".$row["Filename"];
                                    
                                    ?>
                                            <tr>

                                    <?php

                                    if ($_SESSION['UserType']=='Admin'){
                                    ?>
                                      <td> <input type="Checkbox" id="chk" name="chk[]" value="<?php echo $row["RecordID"];?>"></td>
                                    <?php
                                    }
                                    ?>
                                      
                                              <td><a href="index.php?file=<?php echo $filename;?>&BatchID=<?php echo $row["BatchId"];?>&Status=Completed"><?php echo $row["JobName"];?></a></td>
                                              <td></td>
                                              <td><?php echo $row["SourceURL"];?></td>
                                              <td><?php echo $row["SourceTitle"];?></td>
                                              <td><?php echo $row["Filename"];?></td>
                                              <td><?php echo $row["Title"];?></td>
                                              <td><?php echo $row["Register"];?></td>
                                              <td><?php echo $row["Type"];?></td>
                                              <td><?php echo $row["Priority"];?></td>
                                              <td><?php echo $row["Topic"];?></td>
                                              <td><?php echo $row["SubTopics"];?></td>
                                              <td><?php echo $row["OriginatingDate"];?></td>
                                              <td><?php echo $row["StateDate"];?></td>
                                              <td><?php echo $row["Status"];?></td>
                                              <td><?php echo $row["AssignedTo"];?></td> 
                                              <td><?php echo $row["LastUpdate"];?></td> 
                                      </tr>
                                    <?php
                                   
                                    }
                                    ?>    
                                            </tbody>
                                            
                                          </table>
                                    
                                    <?php
                                      if ($_SESSION['UserType']=='Admin'){
                                        ?>
                                     <p align="right">
                                        <!-- <button type="submit" class="btn btn-warning"><i class="fa  fa-send"></i> Deliver</button></p> -->
                                      <?php
                                      }
                                      ?>
                                       <a href="ExportToexcel.php" onclick="return theFunction();" target="_blank" class="pull-right">Export To Excel</a> 
                                        </form>
                          </div>
                  </div>
            </div>
        </div>
    </section>
</div>
 
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>

