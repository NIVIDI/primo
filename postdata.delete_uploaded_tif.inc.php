<?php
	
	$files = json_decode($post['files']);
	
	if(!empty($files)){
		$response_results = array();
		foreach($files as $file){
			if(file_exists($file->file)){
				rrmdir($file->file);
				$response_results[] = array("success" => true, "message" => "Successfull deleted ({$file->name})");
			}else{
				$response_results[] = array("success" => false, "message" => "File not exist in server, cannot be deleted ({$file->name})");
			}
		}

		$message = '';
		foreach($response_results as $response){
			$style = "display: block; color: red;";
			if($response['success']){
				$style = "display: block; color: green;";
			}
			$message .= "<li style='{$style}'>{$response['message']}</li>";
		}
		$results = array("success" => true, "message" => "{$message}");
	}else{
		$results = array("success" => false, "message" => "No file selected to delete!");
	}
?>