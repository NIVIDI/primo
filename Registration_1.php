<?php
include "conn.php";
$registration_results = !empty($_SESSION['registration_results']) ? $_SESSION['registration_results']: array();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $txtFiles=$_FILES['txtFiles']['name'];
    if ($txtFiles!='' && !empty($txtFiles[0])) {
        $dirname="uploadfiles/SourceFiles";
        try{
            $BookID = null;
            $for_batching = (!empty($_POST['for_batching']) && strtoupper($_POST['for_batching']) == 'YES') ? true : false;
            $results = MultipleFileUploadEX('txtFiles',$BookID,$dirname,$conWMS,$GGUserName,$GGPassword,$GGProductionMode, $for_batching, $_POST);
            $_SESSION['registration_results'] = $results;
            header("Location: Registration.php?page=Acquire");
            exit;
        }catch(Exception $e){
           $registration_results[0] = array("response" => "error", "message" => $e->getMessage(), "filename" => '');
        }
        
    }else{
        $registration_results[0] = array("response" => "error", "message" => 'Error uploading file: File cannot be empty', "filename" => '');
    }
}

unset($_SESSION['registration_results']);
include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Register File</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Register File</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php
                    if(!empty($registration_results)){
                        $alert = '';
                        foreach($registration_results as $key => $result){
                            if($result['response'] == 'success'){
                                $alert = 'success'; 
                            }else{
                                $alert = 'danger'; 
                            }

                            echo '<div class="alert alert-'.$alert.' alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-info"></i> '.strtoupper($result['response']).'!</h4>
                                    '.$result['message'].' : Filename('.$result['filename'].')
                                </div>';
                        }
                    }

                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border"></div>
                    <div class="box-body">
                        <form role="form" method="Post" Action="<?= $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data">
                            
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>For Segregation <small class="text-muted">(Yes = registered file will not undergo WMS it direct to GG)</small></label>
                                    <select name="for_batching" id="for_batching" class="form-control" required>
                                        <option>No</option>
                                        <option>Yes</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Reference Code <small class="text-muted">(sample: precship1838.inno)</small></label>
                                    <input type="text" name="reference_code" id="reference_code" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Shipment Date</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" name="preship_date" id="datepicker" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Precedents Bundle <small class="text-muted">(sample: Lit4_Bundle36)</small></label>
                                    <input type="text" name="prec_bundle" id="prec_bundle" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>No. of PDF Files</label>
                                    <input type="number" name="num_pdf_files" id="num_pdf_files" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Days Turn Around Time</label>
                                    <input type="number" name="tat" id="tat" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Data Inventory Files</label>
                                    <input type="file" class="form-control" name="txtFiles[]" multiple="multiple" required>
                                </div>
                                <div class="form-group pull-right">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                    <button type="reset" class="btn btn-danger" >Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="box-footer">
                        <input type="hidden" class="form-control" placeholder="" name="BookID" value="<?php echo $BookID;?>">
                        <input type="hidden" class="form-control" placeholder="" name="TransType" value="<?php echo $TransType;?>">
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
<script>
    $('#datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayHighlight: true,
    });
</script>

