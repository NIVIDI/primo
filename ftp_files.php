<?php
include "conn.php";
include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>


<div class="content-wrapper">
    <section class="content-header">
        <h1>Client Sources FTP Files</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Client Sources FTP Files</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box box-primary">
                    <div class="box-header with-border"></div>
                    <div class="box-body">
                        <div class="col-sm-6">
                            <div class="row"><h3>FTP Files: <small id="ftp_path"></small></h3></div>
                            <div class="row"><a href="javascript:void(0);" title="Refresh" onclick="FTP.refresh();"><i class="fa fa-refresh" aria-hidden="true"></i></a></div>
                            <div class="row" style="margin-right: 1px !important;">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <th width="5%"><input type="checkbox" id="check_all_ftp" onclick="Common.checkAll(this, 'tr_ftp', 'button_ftp');"></th>
                                            <th>Filename</th>
                                            <th>Type</th>
                                            <th>Size</th>
                                            <th>Modified</th>
                                        </thead>
                                        <tbody id="tbody_ftp_files"></tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 30px;">
                                <button type="submit" style="margin-right: 15px;" class="btn btn-primary pull-right button_ftp" disabled id="button_ftp_download">Download</button>
                                <button type="submit" style="margin-right: 15px;" class="btn btn-warning pull-right" id="button_ftp_back" onclick="FTP.back();">Back</button>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row" style="margin-left: 1px !important;"><h3>Downloaded Files: <small id="downloaded_path"></small></h3></div>
                            <div class="row" style="margin-left: 1px !important;"><a href="javascript:void(0);" title="Refresh" onclick="DL.refresh();"><i class="fa fa-refresh" aria-hidden="true"></i></a></div>
                            <div class="row" style="margin-left: 1px !important;">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <th width="5%"><input type="checkbox" id="check_all_dl" onclick="Common.checkAll(this, 'tr_dl', 'button_dl');"></th>
                                            <th>Filename</th>
                                            <th>Type</th>
                                            <th>Size</th>
                                            <th>Modified</th>
                                        </thead>
                                        <tbody id="tbody_dl_files"></tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 30px;">
                                <button type="submit" style="margin-right: 15px;" class="btn btn-primary pull-right button_dl" disabled id="button_segregate">Segregate</button>
                                <button type="submit" style="margin-right: 15px;" class="btn btn-danger pull-right button_dl" disabled id="button_dl_delete" onclick="DL.delete();">Delete</button>
                                <button type="submit" style="margin-right: 15px;" class="btn btn-warning pull-right" id="button_dl_back" onclick="DL.back();">Back</button>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer"></div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="segregate_file_modal">
    <div class="modal-dialog" style="width: 50%"> 
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title">Files For Segregation</h4>
          </div>
          <div class="modal-body">
              <form role="form" id="new_segregated_form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Reference Code <span class="text-muted">(sample: precship1838)</span></label>
                        <input type="text" name="reference_code" id="reference_code" class="form-control" req="true" message="Reference Code is required">
                    </div>
                    <div class="form-group">
                        <label>Shipment Date</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" name="preship_date" id="preship_date" req="true" message="Shipment Date is required"required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Precedents Bundle <span class="text-muted">(sample: Lit-4_Bundle36)</span></label>
                        <input type="text" name="prec_bundle" id="prec_bundle" class="form-control" req="true" message="Prec Bundle is required">
                    </div>
                    <div class="form-group">
                        <label>Days Turn Around Time</label>
                        <input type="number" name="tat" id="tat" class="form-control" req="true" value="6" message="TAT is required">
                    </div>

                    <div class="form-group">
                        <input class="form-check-input" name="auto_segregate" type="checkbox" value="1"  id="auto_segregate"/>
                        <label class="form-check-label" for="auto_segregate">
                            Auto Segregate <span style="font-size: 12px" class="text-muted">(Note: When enabled, the file will automatically segregated/split)</span>
                        </label>
                    </div>
                    <div class="form-group" id="div_auto_upload_to_GG" style="display:none;">
                        <input class="form-check-input" name="auto_upload_to_GG" type="checkbox" value="1"  id="auto_upload_to_GG"/>
                        <label class="form-check-label" for="auto_upload_to_GG">
                            Auto Upload To GG <span style="font-size: 12px" class="text-muted">(Note: When enabled, the file segregrated will automatically uploaded to GG for processing)</span>
                        </label>
                    </div>
              </form>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-primary" id="submit_segregate_file" value="Submit">
          </div>
      </div>
  </div>
</div>

<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
<script type="text/javascript">
    $('#preship_date').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayHighlight: true,
    });
    var file_dir = '<?= $ftp_base_path;?>';
    var dir = file_dir;
    var previous_dir = dir;
    var current_dir = dir;

    
    $('#button_ftp_download').click(function(){
        Swal.fire({
            title: "You want to download FTP files?",
            text: '',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            allowOutsideClick: false
        }).then((result) => {
            if (result.isConfirmed) {
                FTP.download();
            }
        })
        
    });
    var FTP = {
        init : function(){
            FTP.load(dir);
        },
        back : function(){
            if(file_dir != current_dir){
                FTP.load(previous_dir);
            }
        },
        download : function(){
            var list = Common.getCheckRow('tr_ftp', current_dir);
            if(list.length > 0){
                var data = [
                    {name:'action', value: 'ftp_download'},
                    {name:'files', value: JSON.stringify(list)}
                ];
                Swal.fire({
                      title: 'Downloading ...',
                      showConfirmButton: false,
                      onBeforeOpen () {
                         Swal.showLoading ()
                      },
                      onAfterClose () {
                         Swal.hideLoading()
                      },
                      allowOutsideClick: false,
                      allowEscapeKey: false,
                      allowEnterKey: false
                });
                $.post('postdata.php', data, function(response){
                    Swal.close();
                    try{
                        var result = JSON.parse(response);
                        if(result.success){
                            Common.success(result.message);
                            Common.uncheck('tr_ftp', 'button_ftp');
                            DL.refresh();
                        }else{
                            Common.error(result.message);
                        }
                    }catch(e){
                        Common.error(e);
                    }
                });
            }else{
                Common.error('Cannot process FTP download, no files selected!');
            }
        },
        refresh : function(){
            FTP.load(current_dir);
        },
        load : function(dir){
            $('.button_ftp').prop('disabled', true);
            $('#ftp_path').html(':'+dir);
            current_dir = dir;
            var splitted_dir = dir.split('/');
            if(splitted_dir.length > 2){
                previous_dir = '';
                
                for(var i = 0; i < splitted_dir.length - 1; i++){
                    var concat = '/';
                    if(i == splitted_dir.length - 2){
                        concat = '';
                    }
                    previous_dir += splitted_dir[i]+concat;  
                }
            }

            var data = [
                {name: 'action', value: 'fetch_ftp'},
                {name: 'dir', value: dir}
            ];
            $('#tbody_ftp_files').html('');
            $('#tbody_ftp_files').append(
                $('<tr>').append(
                    $('<td>').attr({colspan:'5'}).css({textAlign: 'center'}).html('<i>Please wait while fetching FTP files</i>')
                )
            );
            $.post('postdata.php', data, function(res){
                try{
                    $('#tbody_ftp_files').html('');
                    var response = JSON.parse(res);
                    if(response.success){
                        if(response.data.length > 0){
                            $.each(response.data, function(i, v){
                                var _chk = '';
                                var path_to_fetch = dir+'/'+v.filename;
                                var  href = '<a href="javascript:void(0);" onclick="FTP.load(\''+path_to_fetch+'\');">'+v.filename+'</a>';
                                var type = "Folder";
                                if(parseInt(v.type) != 2){
                                    _chk = '<input type="checkbox" filename = "'+v.filename+'" id="tr_ftp_'+v.filename+'" class="tr_ftp" onclick="Common.checkButton(\'tr_ftp\', \'button_ftp\');">';
                                    href = v.filename;
                                    type = 'File';
                                }

                                $('#tbody_ftp_files').append(
                                    $('<tr>').append(
                                        $('<td>').html(_chk),
                                        $('<td>').html(href),
                                        $('<td>').html(type),
                                        $('<td>').html(((v.size/1000).toFixed(2))+' KB'), 
                                        $('<td>').html(v.modified)
                                    )
                                );
                            });
                        }else{
                            $('#tbody_ftp_files').append(
                                $('<tr>').append(
                                    $('<td>').attr({colspan:'5'}).css({textAlign: 'center'}).html('No File Available')
                                )
                            );
                        }
                    }else{
                        $('#tbody_ftp_files').append(
                            $('<tr>').append(
                                $('<td>').attr({colspan:'5'}).css({textAlign: 'center', color: 'red'}).html(response.message)
                            )
                        );
                    }
                }catch(e){
                    $('#tbody_ftp_files').append(
                        $('<tr>').append(
                            $('<td>').attr({colspan:'5'}).css({textAlign: 'center', color: 'red'}).html(response.message)
                        )
                    );
                }
            });
        }
    }

    var Common = {
        success : function(msg){
            Swal.fire({
                allowOutsideClick: false,
                title: '<strong>Submit Status</strong>',
                icon: 'info',
                html: msg,
                showCloseButton: false,
                showCancelButton: false,
                focusConfirm: false,
                confirmButtonText:'<i class="fa fa-thumbs-up"></i> OK',
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    //FTP.load();
                }
            })
        },
        error : function (msg){
            Swal.fire({
                allowOutsideClick: false,
                icon: 'error',
                title: 'Oops...',
                html: "<strong>"+msg+"</strong>"
            });
        },
        uncheck : function(_class, button_class){
             $('.'+_class).prop("checked", false);
            Common.checkButton(_class, button_class);
        },
        getCheckRow : function(_class, dir_param){
            var row = [];
            var _chk = $('.'+_class);
            if(_chk.length > 0){
                $.each(_chk, function(i,v){
                    if($(v).is(':checked')){
                        row.push({filename: $(v).attr('filename'), directory: dir_param});
                    }
                });
            }
            return row;
        },
        checkAll : function(el, _class, button_class){
            var _chk_status = $(el).is(":checked");
            $('.'+_class).prop("checked", false);
            if(_chk_status){
                $('.'+_class).prop('checked', true);
            }

            Common.checkButton(_class, button_class);
        },
        checkButton : function(_class, button_class){
            var _chk = $('.'+_class);
            var enable_button = false;
            if(_chk.length > 0){
                $.each(_chk, function(i, v){
                      if($(v).is(":checked")){
                          enable_button = true;
                      }
                });
            }
            $('.'+button_class).prop('disabled', true);
            
            if(enable_button){
                $('.'+button_class).prop('disabled', false);
            }
        }
    
    }



    var dlfile_dir = '<?= $download_base_path.'/'.$ftp_base_path;?>';//'<?= $download_base_path;?>';//'
    var dldir= dlfile_dir;
    var dlprevious_dir = dldir;
    var dlcurrent_dir = dldir;
    
     $('#button_segregate').click(function(){
        let dir_split = dlcurrent_dir.split('/');
        let filename_dir = '';
        if(dir_split.length > 0){
            $.each(dir_split, function(i,v){
                if((v.toLowerCase()).includes('bundle')){
                    let lit_dir = dir_split[i - 1];
                    let concat_dir = '';
                    if((lit_dir.toLowerCase()).includes('lit')){
                        concat_dir = lit_dir+'_';
                    }
                    $('#prec_bundle').val(concat_dir+''+v);
                }
                if(i >= 3){
                    filename_dir += "/"+v;
                }
            }); 
        }

        var bundle = $('#prec_bundle').val();
        if((bundle.trim()).length > 0){
            Swal.fire({
                  title: 'Fetching Data ...',
                  showConfirmButton: false,
                  onBeforeOpen () {
                     Swal.showLoading ()
                  },
                  onAfterClose () {
                     Swal.hideLoading()
                  },
                  allowOutsideClick: false,
                  allowEscapeKey: false,
                  allowEnterKey: false
            });

            var data = [
                {name:'action', value: 'fetch_existing_bundle'},
                {name:'bundle', value: bundle}
            ];
            $.post('postdata.php', data, function(response){
                Swal.close();
                try{
                    var result = JSON.parse(response);
                    var list = Common.getCheckRow('tr_dl', dlcurrent_dir);
                    
                    if(result.success){
                        if(result.data !== null){
                            $('#reference_code').val(result.data.reference_code);
                            $('#preship_date').val(result.data.precship_date);
                            $('#tat').val(result.data.tat);
                             
                        }else{
                            var data2 = {'dir' : filename_dir+'/'+ list[0].filename, action: 'fetch_ftp', 'file_detail' : '1'};
                            $.post('postdata.php', data2, function(res){
                                var res2 = JSON.parse(res);
                                if(res2.success){
                                    var ship_date = (typeof res2.data.modified != 'undifined') ? res2.data.modified : '';
                                    $('#preship_date').val(ship_date);
                                }
                            });

                            $('#tat').val(6); 
                        }
                        $('#segregate_file_modal').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }else{
                        Common.error(result.message);
                    }
                }catch(e){
                    Common.error(e);
                }
            });
        }

       
       
    });

    $('#submit_segregate_file').click(function(){
        if(Form.validate('#new_segregated_form')){
            DL.segregate();
        }
    });
    
    var DL = {
        init : function(){
            DL.load(dldir);
        },
        back : function(){
            if(dlfile_dir != dlcurrent_dir){
                DL.load(dlprevious_dir);
            }
        },
        refresh : function(){
            DL.load(dlcurrent_dir);
        },
        delete : function(){
            Swal.fire({
                title: "Are you sure want to delete selected files?",
                text: '',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                allowOutsideClick: false
            }).then((result) => {
                if (result.isConfirmed) {
                    DL.processDelete();
                }
            })
        },
        processDelete : function(){
            var list = Common.getCheckRow('tr_dl', dlcurrent_dir);
            if(list.length > 0){
                var data = [
                    {name:'action', value: 'delete_download'},
                    {name:'files', value: JSON.stringify(list)}
                ];
                Swal.fire({
                      title: 'Deleting ...',
                      showConfirmButton: false,
                      onBeforeOpen () {
                         Swal.showLoading ()
                      },
                      onAfterClose () {
                         Swal.hideLoading()
                      },
                      allowOutsideClick: false,
                      allowEscapeKey: false,
                      allowEnterKey: false
                });
                $.post('postdata.php', data, function(response){
                    Swal.close();
                    try{
                        var result = JSON.parse(response);
                        if(result.success){
                            DL.refresh();
                            Common.success(result.message);
                            Common.uncheck('tr_dl', 'button_dl');
                        }else{
                            Common.error(result.message);
                        }
                    }catch(e){
                        Common.error(e);
                    }
                });
            }else{
                Common.error('Cannot process file deletion, no files selected!');
            }
        },
        load : function(dldir){
            $('.button_dl').prop('disabled', true);
            $('#downloaded_path').html(':'+dldir);
            dlcurrent_dir = dldir;
            var splitted_dir = dldir.split('/');
            if(splitted_dir.length > 2){
                dlprevious_dir = '';
                
                for(var i = 0; i < splitted_dir.length - 1; i++){
                    var concat = '/';
                    if(i == splitted_dir.length - 2){
                        concat = '';
                    }
                    dlprevious_dir += splitted_dir[i]+concat;  
                }
            }
            
            var data = [
                {name: 'action', value: 'fetch_dl'},
                {name: 'dir', value: dldir}
            ];
            $('#tbody_dl_files').html('');
            $('#tbody_dl_files').append(
                $('<tr>').append(
                    $('<td>').attr({colspan:'5'}).css({textAlign: 'center'}).html('<i>Please wait while fetching Downloaded files</i>')
                )
            );
            $.post('postdata.php', data, function(res){
                try{
                    $('#tbody_dl_files').html('');
                    var response = JSON.parse(res);
                    if(response.success){
                        if(response.data.length > 0){
                            $.each(response.data, function(i, v){
                                var _chk = '';
                                var path_to_fetch = dldir+'/'+v.filename;
                                var  href = '<a href="javascript:void(0);" onclick="DL.load(\''+path_to_fetch+'\');">'+v.filename+'</a>';
                                var type = "Folder";
                                if(parseInt(v.type) != 2){
                                    _chk = '<input type="checkbox" filename = "'+v.filename+'" id="tr_dl_'+v.filename+'" class="tr_dl" onclick="Common.checkButton(\'tr_dl\', \'button_dl\');">';
                                    href ='<a href="'+path_to_fetch+'" target="_blank">'+v.filename+'</a>';
                                    type = "File";
                                }

                                $('#tbody_dl_files').append(
                                    $('<tr>').append(
                                        $('<td>').html(_chk),
                                        $('<td>').html(href),
                                        $('<td>').html(type),
                                        $('<td>').html(((v.size/1000).toFixed(2))+' KB'),
                                        $('<td>').html(v.modified)
                                    )
                                );
                            });
                        }else{
                            $('#tbody_dl_files').append(
                                $('<tr>').append(
                                    $('<td>').attr({colspan:'5'}).css({textAlign: 'center'}).html('No File Available')
                                )
                            );
                        }
                    }else{
                        $('#tbody_dl_files').append(
                            $('<tr>').append(
                                $('<td>').attr({colspan:'5'}).css({textAlign: 'center', color: 'red'}).html(response.message)
                            )
                        );
                    }
                }catch(e){
                    $('#tbody_dl_files').append(
                        $('<tr>').append(
                            $('<td>').attr({colspan:'5'}).css({textAlign: 'center', color: 'red'}).html(response.message)
                        )
                    );
                }
            });
        },
        segregate : function(){
            var list = Common.getCheckRow('tr_dl', dlcurrent_dir);
            
            if(list.length > 0){
                var data = $('#new_segregated_form').serializeArray();
                data.push(
                    {name:'action', value: 'downloaded_to_segregate'},
                    {name:'files', value: JSON.stringify(list)}
                );
                
                Swal.fire({
                      title: 'Segregating ...',
                      showConfirmButton: false,
                      onBeforeOpen () {
                         Swal.showLoading ()
                      },
                      onAfterClose () {
                         Swal.hideLoading()
                      },
                      allowOutsideClick: false,
                      allowEscapeKey: false,
                      allowEnterKey: false
                });
                $.post('postdata.php', data, function(response){
                    Swal.close();
                    try{
                        var result = JSON.parse(response);
                        if(result.success){
                            Common.success(result.message);
                            Common.uncheck('tr_dl', 'button_dl');
                            $('#segregate_file_modal').modal('hide');
                        }else{
                            Common.error(result.message);
                        }
                    }catch(e){
                        Common.error(e);
                    }
                });
            }else{
                Common.error('Cannot process file segregation, no files selected!');
            }
        }
    }

    FTP.init();
    DL.init();

    $('#auto_segregate').click(function(){
        $('#div_auto_upload_to_GG').hide();
        $('#auto_upload_to_GG').prop('checked', false);
        if($(this).is(':checked')){
            $('#div_auto_upload_to_GG').show();
        }
    });
</script>