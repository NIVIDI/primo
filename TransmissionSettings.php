<?php
include "conn.php";
include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Transmission Settings
          
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Transmission Settings</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
    
              <?php
                   $sql="SELECT * FROM tblTransmission";
              ?>
              <div class="col-md-12">
                    <div class="box box-primary">
                          <div class="box-header with-border">
                             <h3 class="box-title"><button type="button" class="btn btn-block btn-primary"  onclick="location.href='addnew_Transmission.php'">Add New</button></h3>
                          </div>
                          <!-- /.box-header -->
                          <div class="box-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                          				          <th>ID</th>
                                            <th>Transmission Type</th>
                                            <th>FTP Site</th>
                                            <th>Directory</th>
                                  				  <th>User Name</th>
                                  				  <th>Password</th>
                                  				  <th>EmailAddress</th>
                                  				  <th>CC</th>
                                  				  <th>Subject</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                    				        <?php
                                    				if ($result=mysqli_query($con,$sql))
                                            {
                                                while ($row=mysqli_fetch_row($result))
                                                {
                                        
                                     
                                    ?>
                                                          <tr>
                                                            <td><?php echo $row[0];?></td>
                                                            <td><?php echo $row[1];?></td>
                                          				          <td><?php echo $row[2];?></td>
                                                            <td><?php echo $row[3];?></td>
                                                  				  <td><?php echo $row[4];?></td>
                                                  				  <td><?php echo $row[5];?></td>
                                                  				  <td><?php echo $row[6];?></td>
                                                  				  <td><?php echo $row[7];?></td>
                                                  				  <td><?php echo $row[8];?></td>
                                                            <td>
                                          				              <button type="button" class="btn btn-xs btn-info"  onclick="location.href='addnew_Transmission.php?UID=<?php echo $row[0];?>&TransType=Update'">Update</button>
                                          				              <button class="btn btn-xs btn-danger"  data-toggle="modal" data-target="#modal-danger" onclick="Javascript:SetTextBoxValue(<?php echo $row[0];?>)">Delete</button></td>
                                                          </tr>
                                    <?php
                    	                           }
                    	 
                                            }
                                    ?>	  
                                    </tbody>
                                    <tfoot>
                                        <tr>
                          				          <th>ID</th>
                                            <th>Transmission Type</th>
                                            <th>FTP Site</th>
                                            <th>Directory</th>
                                  				  <th>User Name</th>
                                  				  <th>Password</th>
                                  				  <th>EmailAddress</th>
                                  				  <th>CC</th>
                        			              <th>Subject</th>
                        			               <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                          </div>
                    </div>
              </div>
         </div>
    </section>
</div>
<div class="modal fade" id="modal-access">
  <div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">User Access</h4>
    </div>
    <form method="post" action="saveAccess.php">
      <div class="box-body">
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">ACQUIRE <input type="checkbox" name="ACQUIRE"></label>
      </div>
      <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">ENRICH <input type="checkbox" name="ENRICH"></label>
      </div>
      <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">DELIVER <input type="checkbox" name="ENRICH"></label>
      </div>
      <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">USER MAINTENANCE <input type="checkbox" name="ENRICH"></label>
      </div>
      <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">EDITOR SETTINGS <input type="checkbox" name="ENRICH"></label>
      </div>
      
      <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">ML SETTINGS <input type="checkbox" name="ENRICH"></label>
      </div>
      <span id='message'></span>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      <input type="submit" class="btn btn-primary" id="Button" value="Change Password">
      <input type="hidden" class="btn btn-primary" name="tID" value="">
      <input type="hidden" class="btn btn-primary" name="RedirectPage" value="index.php">
      </div>
    </form>
  </div>
  </div>
</div>
<form method="GET" action="saveTransmissionSetting.php">
    <div class="modal modal-danger fade" id="modal-danger">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Record Deletion</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete this record?</p>
            <input type="hidden" name="txtID" id="txtID" />
            <input type="hidden" name="TransType" id="TransType" value="Delete" />
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>

            <button type="submit" class="btn btn-outline">Ok</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
</form>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>

 
  
    

