<?php
use Smalot\PdfParser\Parser;
use Smalot\PdfParser\XObject\Image;

	$source_id = $post['id'];
	$filename = empty($post['filename']) ? '' : $post['filename'];

	if(!empty($source_id)){
		$numofdocs = 0;
	    $sql = "SELECT * FROM segregated_files WHERE batched_parent_id = '{$source_id}' ORDER BY page_start ASC";
	    $result = mysqli_query($con, $sql);
	    $total_done = 0;
	    $file_segregrated = array();
	    if($result->num_rows){
	        while($row1 = $result->fetch_assoc()){
	            $numofdocs ++;
	            if(!empty($row1['wms_job_id'])){
	                $wms_status = GetWMSValue("SELECT StatusString FROM primo_view_Jobs WHERE JobId = '{$row1['wms_job_id']}' AND ProcessCode = 'QC' ","StatusString",$conWMS);
	                if(strtolower($wms_status) == 'done'){
	                    $total_done ++;
	                }
	            }
	            $sql_deliverables = "SELECT * FROM deliverables_segregated_files WHERE segregated_id = '{$row1['id']}' LIMIT 1";
	            $deliverables_data = mysqli_query($con, $sql_deliverables)->fetch_assoc();
				if(!empty($deliverables_data)){
					$deliverable_path = $SourceFilePath.'/'.$row1['prec_bundle'].'/deliverables/'.$deliverables_data['filename'];
					if(file_exists($deliverable_path)){
						unlink($deliverable_path);
					}

					$sql_delete = "DELETE FROM deliverables_segregated_files WHERE segregated_id = '{$row1['id']}'";
	            	ExecuteQuery($sql_delete,$con);
				}

	            $file_segregrated[] = $row1;
	        }
	    }

	    if(empty($numofdocs)){
	    	$results = array("success" => false, "message" => "Error compiling file, no file has been segregated under source({$filename})");
	    }else{
	    	if((int) $numofdocs > (int) $total_done){
	    		$results = array("success" => false, "message" => "Error compiling file, some batched file not yet completed!");
	    	}else{
	    		$COURTID = pathinfo($filename, PATHINFO_FILENAME);
	    		$key_file = '<!DOCTYPE ROOT PUBLIC "-//Carswell//DTD Precedent Keying//EN">';
	    		$key_file .= PHP_EOL.'<ROOT>';
				$key_file .= PHP_EOL.'<COURTFILE ID="'.$COURTID.'">';
	    		$bundle = '';
	    		$array_to_compile = array();
	    		$x = 0;
	    		$errors = array();
	    		foreach($file_segregrated as $data){
	    			$bundle = $data['prec_bundle'];
	    			$xml_file = pathinfo($data['filename'], PATHINFO_FILENAME).'.xml';
	    			$xml_file_path = $SourceFilePath.'/'.$data['prec_bundle'].'/'.$xml_file;
	    			if(file_exists($xml_file_path)){
	    				$xml_content = file_get_contents($xml_file_path);
	    				$xml_content = replaceXMLInvalidTag($xml_content);
	    				libxml_use_internal_errors(true);
	    				$xml = simplexml_load_string($xml_content);
	    				if (false === $xml) {
	    					foreach(libxml_get_errors() as $error) {
						        $errors[] = $error->message.' --- '.$xml_file.PHP_EOL;
						    }
						}else{

		    				$xml_type = strtoupper(substr((string) $xml->attributes()->LABEL, 0,1));
		    				$yyyymmdd = !empty($xml->DATE) ? (string) $xml->DATE->attributes()->YYYYMMDD : '00000000';
		    				$array_to_compile[$yyyymmdd][$x] = array('data' => $data, 'path' => $xml_file_path, "type" => $xml_type, 'xml' => $xml_content);  
		    				$x++;
		    			}
					}
	    		}

	    		if(!empty($errors)){
	    			$results = array("success" => false, "message" => $errors);
	    			echo json_encode($results);
	    			exit;
	    		}

	    		ksort($array_to_compile);
	    		
	    		if(!empty($array_to_compile)){
	    			$y = 0;
	    			foreach($array_to_compile as $key => $values){
	    				$y++;
	    				foreach($values as $value){
	    					$xml   = simplexml_load_string($value['xml']);
	    					$filename2 = pathinfo($value['data']['filename'], PATHINFO_FILENAME);

	    					$nValue = $value['type'].$y;
	    					$has_N_tag = false;
	    					if(!empty($xml->N)){
	    						$xml->N = $nValue;
	    						$has_N_tag = true;
	    					}

	    					if(!$has_N_tag){
	    						libxml_use_internal_errors(true);
	    						$doc = new DOMDocument;
					            $doc->preserveWhiteSpace = false;
					            $doc->loadxml($value['xml']);
					            $doc->formatOutput = true;
              					$xml = reformatXML($doc->savexml());
              					$xml = new Custom_XML($xml);
	    						if($xml === false){
	    							$err_msg = '';
	    							foreach(libxml_get_errors() as $error) {
								        $err_msg .= $error->message.PHP_EOL;
								    }
	    							$results = array("success" => false, "message" => $err_msg);
	    						}
	          					$xml->prependChild('N', $nValue);

          					}else{
          						$xml = $xml->asXML();
          						if(!is_object($xml)){
          							$xml = new Custom_XML($xml);
          						}
							}
							
							$new_filename = $COURTID.$nValue.'.PDF';
							$sql_insert = "INSERT INTO deliverables_segregated_files(`segregated_id`, `filename`) VALUES('{$value['data']['id']}', '{$new_filename}') ";
							ExecuteQuery($sql_insert,$con);

							$deliverable_path = $SourceFilePath.'/'.$value['data']['prec_bundle'].'/deliverables/'.$new_filename;
							if(file_exists($deliverable_path)){
								unlink($deliverable_path);
							}

							$old_deliverable_path = $SourceFilePath.'/'.$value['data']['prec_bundle'].'/deliverables/'.$value['data']['filename'];
							if(file_exists($old_deliverable_path)){
								rename($old_deliverable_path, $deliverable_path);
							}else{
								$segregated_source = $SourceFilePath.'/'.$value['data']['prec_bundle'].'/'.$value['data']['filename'];
								$temp_file_path = __DIR__.'/TEMP/'.$_SESSION['UserID'];
					            if(!file_exists($temp_file_path)){
					                mkdir($temp_file_path, 0777, true);
					            }

					            $cmd = __DIR__.'/mupdf/mutool convert -o '.$temp_file_path.'/'.$filename2.'_%d.png '.$segregated_source.' 1-N';
		            			exec($cmd);

								//remove summary sheet and cover page here
								$parser = new Parser();
							    $pdf    = $parser->parseFile($segregated_source);
							    $pages  = $pdf->getPages();

							    $x = 0;
							    $pdf = new \Imagick();
							    foreach ($pages as $page) {
							      	$x ++;
							      	if($x >= 3){
							      		$pdf->addImage(new \Imagick($temp_file_path.'/'.$filename2.'_'.$x.'.png'));
										unlink($temp_file_path.'/'.$filename2.'_'.$x.'.png');
									}else{
										unlink($temp_file_path.'/'.$filename2.'_'.$x.'.png');
									}
								}

								$pdf->writeImages($deliverable_path, true);
							}

							//remove xml version at the top
	    					$dom = dom_import_simplexml($xml);
	    					$dom->formatOutput = true;
              				$xml = $dom->ownerDocument->saveXML($dom->ownerDocument->documentElement, LIBXML_NOEMPTYTAG);
							$xml = returnBackReplaceInvalidXMLTag($xml);

							//replace  segregated pdf name to client specific pdf name
							$xml = str_replace($filename2.'.PDF', $new_filename, $xml);

							file_put_contents($value['path'], $xml);
	    					$key_file .= PHP_EOL.$xml;
	    				}
	    			}
	    		}

	    		$key_file .= PHP_EOL."</COURTFILE>";
	    		$key_file .= PHP_EOL."</ROOT>";

	    		$key_filaname = generateKeyFilename($filename);
	    		$deliverables_path = $SourceFilePath.'/'.$bundle.'/deliverables/';
	    		if(!file_exists($deliverables_path)){
	    			mkdir($deliverables_path, 0777, true);
	    		}

	    		if(file_exists($deliverables_path.'/'.$key_filaname)){
	    			unlink($deliverables_path.'/'.$key_filaname);
	    		}

	    		$file = fopen($deliverables_path.'/'.$key_filaname, 'w+');
              	fputs($file, trim($key_file));
              	fclose($file);

              	//validate DTD
              	$parser_path = __DIR__.'/XMLValidator/NSGMLS.exe';
				$cat_path = __DIR__.'/XMLValidator/PRE.cat';
				$dtd_path = __DIR__.'/XMLValidator/precedent.dtd';

				$key_file = str_replace(array('<!DOCTYPE ROOT PUBLIC "-//Carswell//DTD Precedent Keying//EN">'), '<!DOCTYPE ROOT SYSTEM "'.$dtd_path.'">', $key_file);

				$temp_dir = $SourceFilePath.'/'.$bundle.'/'.$_SESSION['UserID'];
        		if(!file_exists($temp_dir)){
		            mkdir($temp_dir, 0777, true);
		        }

		        $temp_file_path = $temp_dir.'/'.$key_filaname;
        		$file = fopen($temp_file_path, 'w+');
        		fputs($file, $key_file);
        		fclose($file);

				$cmd = '"'.$parser_path.'" -f "'.$temp_dir.'/'.pathinfo($key_filaname, PATHINFO_FILENAME).'.log" -s -c "'.$cat_path.'" "'.$temp_file_path.'"';
        		shell_exec($cmd);

        		if(file_exists($temp_dir.'/'.pathinfo($key_filaname, PATHINFO_FILENAME).'.log')){
		            $logs = str_replace(array($parser_path, $cat_path, $temp_dir.'/'), '', trim(file_get_contents($temp_dir.'/'.pathinfo($key_filaname, PATHINFO_FILENAME).'.log')));
		            $log_msg = '';
		                
		            if(empty($logs)){
		                $log_msg = "XML successfully validated!";
		            }else{
		                $logs = explode(PHP_EOL,$logs);
		                $x = 0;
		                foreach($logs as $log){
		                    $x ++;
		                    $exploded_logs = explode('.xml:', $log);
		                    $error_line = !empty($exploded_logs[1]) ? $exploded_logs[1] : 0;
		                    $explode2 = explode(':', $error_line);
		                    $line = !empty($explode2[0]) ? $explode2[0] : 0;
		                    $log_msg .= $x." ".$log.PHP_EOL; 
		                }
		            }

		            $file = fopen($SourceFilePath.'/'.$bundle.'/'.pathinfo($key_filaname, PATHINFO_FILENAME).'.logs', 'w+');
		            fputs($file, $log_msg);
        			fclose($file);
		        }

              	$results = array("success" => true, "message" => "Successfully Compiled Source ({$filename}), please review the .key file under compiled deliverables!");
            }
	    }
	}else{
		$results = array("success" => false, "message" => "Error cannot find source id!");
	}
?>