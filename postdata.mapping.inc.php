<?php
use Vyuldashev\XmlToArray\XmlToArray;
require "ArrayToXml.php";

	$process = $post['process'];
	$filename = empty($post['filaname']) ? '' : $post['filaname'];
	$bundle = empty($post['bundle']) ? '': $post['bundle'];
	if(!empty($process)){
		switch($process){
			case 'save':
				try{
					$xml = "";
					$meta = $post['meta_tags'];
					$arr_xml = new ArrayToXML();
					$xml = $arr_xml->buildXML($meta);
					//remove empty tags
					$doc = new DOMDocument;
		            $doc->preserveWhiteSpace = false;
		            $doc->loadxml($xml);
		            $xpath = new DOMXPath($doc);

		            foreach($xpath->query('//*[not(normalize-space())]') as $node ) {
		                $node->parentNode->removeChild($node);
		            } 
		            $doc->formatOutput = true;
		            $xml = $doc->savexml();
		            
		            //remove xml version at the top
		            $custom_xml = new Custom_XML($xml);
	                $dom = dom_import_simplexml($custom_xml);
	                $xml = $dom->ownerDocument->saveXML($dom->ownerDocument->documentElement);

	                //replace unnecessary tags
		            $xml = str_replace("<data>", "<meta_tags>",preg_replace('/><+/', '>'.PHP_EOL.'<', $xml));
		            $xml = str_replace("</data>", "</meta_tags>", $xml);
		            $xml = str_replace(array("<PARTY2>"), "<PARTY>",$xml);
		            $xml = str_replace(array("<PARTYROLE2>"), "<PARTYROLE>",$xml);
		            $xml = str_replace(array("</PARTY2>"), "</PARTY>",$xml);
		            $xml = str_replace(array("</PARTYROLE2>"), "</PARTYROLE>",$xml);
		            $xml = str_replace(array('LABEL=""'), ">",$xml);
		            $xml = str_replace(array(' >'), "",$xml);
		            $xml = ltrim($xml);
		            $xml = reformatXML($xml);
					
					$fname = pathinfo($filename, PATHINFO_FILENAME);
					$file_path_meta = $SourceFilePath.'/'. $bundle.'/'.$fname.'_meta.xml';
					if(file_exists($file_path_meta)){
			    		unlink($file_path_meta);
			    	}

			    	$months = array('jan' => '01', 'feb' => '02', 'mar' => '03', 'apr' => '04', 'may' => '05', 'jun' => '06', 'jul' => '07', 'aug' => '08', 'sep' => '09', 'oct' => '10', 'nov' => '11', 'dec' => '12');
			    	$date_tag = !empty($meta['DATE'][0]['%']) ? preg_replace('/\s+/', ' ', str_replace(array(',','.'),'',trim($meta['DATE'][0]['%']))): '';
			    	$yyyy = '0000';
			    	$mm = '00';
			    	$dd = '00';
			    	if(!empty($date_tag)){
			    		$exploded_date = explode(' ', $date_tag);
			    		$cnt_date = count($exploded_date);
			    		if($cnt_date == 1){
			    			$yyyy = str_pad((int) $exploded_date[0], 4, 0, STR_PAD_LEFT);
			    		}elseif($cnt_date == 2){
			    			if(is_numeric($exploded_date[0]) && strlen($exploded_date[0]) == 4){
			    				$yyyy = $exploded_date[0];
			    				$mm = !empty($months[strtolower(substr($exploded_date[1], 0, 3))]) ? $months[strtolower(substr($exploded_date[1], 0, 3))] : '00';
			    			}else{
			    				$mm = !empty($months[strtolower(substr($exploded_date[0], 0, 3))]) ? $months[strtolower(substr($exploded_date[0], 0, 3))] : '00';
			    				$yyyy = str_pad((int) $exploded_date[1], 4, 0, STR_PAD_LEFT);
			    			}
			    		}elseif($cnt_date == 3){
			    			if(is_numeric($exploded_date[2]) && strlen($exploded_date[2]) == 4){
			    				$yyyy = $exploded_date[2];

			    				if(!is_numeric($exploded_date[1]) && is_numeric($exploded_date[0])){
				    				$mm = !empty($months[strtolower(substr($exploded_date[1], 0, 3))]) ? $months[strtolower(substr($exploded_date[1], 0, 3))] : '00';
				    				$dd = str_pad((int) $exploded_date[0], 2, 0, STR_PAD_LEFT);
				    			}

				    			if(is_numeric($exploded_date[1]) && !is_numeric($exploded_date[0])){
				    				$mm = !empty($months[strtolower(substr($exploded_date[0], 0, 3))]) ? $months[strtolower(substr($exploded_date[0], 0, 3))] : '00';
				    				$dd = str_pad((int) $exploded_date[1], 2, 0, STR_PAD_LEFT);
				    			}
			    			}elseif(strlen($exploded_date[2]) <= 2 && is_numeric($exploded_date[2])){
			    				$dd = str_pad((int) $exploded_date[1], 2, 0, STR_PAD_LEFT);
				    			
				    			if(!is_numeric($exploded_date[1]) && is_numeric($exploded_date[0])){
				    				$mm = !empty($months[strtolower(substr($exploded_date[1], 0, 3))]) ? $months[strtolower(substr($exploded_date[1], 0, 3))] : '00';
				    				$yyyy = str_pad((int) $exploded_date[0], 4, 0, STR_PAD_LEFT);
				    			}

				    			if(is_numeric($exploded_date[1]) && !is_numeric($exploded_date[0])){
				    				$mm = !empty($months[strtolower(substr($exploded_date[0], 0, 3))]) ? $months[strtolower(substr($exploded_date[0], 0, 3))] : '00';
				    				$yyyy = str_pad((int) $exploded_date[1], 4, 0, STR_PAD_LEFT);
				    			}
				    		}

						}
			    	}

			    	$xml = str_replace('YYYYMMDD=""', 'YYYYMMDD="'.$yyyy.$mm.$dd.'" LABEL="Date: "', $xml);
			    	$xml = returnBackReplaceInvalidXMLTag($xml);
			        $file = fopen($file_path_meta, 'w+');
			        fputs($file, $xml);
			        fclose($file);

			        if($post['indicator'] == 'save_and_replace'){
			        	$file_path = $SourceFilePath.'/'. $bundle.'/'.$fname.'.xml';
				        	
			        	if(file_exists($file_path)){
			        		$xml = file_get_contents($file_path);
			        		//$xml = replaceXMLInvalidTag($xml);
			        		//$xml = preg_replace("/<\/GRAPHIC>(.*?)(<FREEFORM>)/", "$1$3", $xml); 
			        		$xml = preg_replace("/(\r\n|\r|\n)<FREEFORM>/", "$1ƒ<FREEFORM>", $xml); 
			        		$xml = preg_replace("/<\/GRAPHIC>[^ƒ]+ƒ<FREEFORM>/", "</GRAPHIC>\r\n<FREEFORM>", $xml); 
							//$xml = preg_replace("/(>)(\n)(<)/", "$1$3", $xml);
							//$xml = preg_replace("/(>)(\r\n)(<)/", "$1$3", $xml);
							//$xml = preg_replace("/(\r\n)(<)/", "$2", $xml);

							$exploded_xml = explode('<', $xml);

							$doc_type = $exploded_xml[1];
							$meta_tags = file_get_contents($file_path_meta);
							//$meta_tags = replaceXMLInvalidTag($meta_tags);
							$xml_final_output = str_replace('<meta_tags>', '', $meta_tags);
				            $xml_final_output = str_replace('</meta_tags>', '', $xml_final_output);
				            //$xml_final_output = str_replace(array('>'.PHP_EOL.'<', ">\r\n<", ">\n<"), '><', $xml_final_output);
				            //$xml_final_output = preg_replace("/(>)(\n)(<)/", "$1$3", $xml_final_output);
				            //$xml_final_output = preg_replace("/(>)(\r\n)(<)/", "$1$3", $xml_final_output);
				            
				            $xml_final_output = preg_replace("/(<\/GRAPHIC>)(\r\n|\r|\n)(<FREEFORM>)/", "$1$2$xml_final_output$3", $xml);
							
				            $xml_final_output = str_replace("&lt;", "<", $xml_final_output);
				            $xml_final_output = str_replace("&gt;", ">", $xml_final_output);
				            $xml_final_output = returnBackReplaceInvalidXMLTag($xml_final_output);
				            $xml_final_output = reformatXML($xml_final_output);
				            //$xml_final_output = preg_replace('/><+/', '>'.PHP_EOL.'<', $xml_final_output); //set new line for new tags
				            file_put_contents($file_path, $xml_final_output);

				            $exe_path = realpath(__DIR__.'/..');
				            $ereplace_exe = $exe_path.'/MetaDataSReplace\SReplace.NET.exe';
				            $sreplace_cmd = '"'.$ereplace_exe.'" "'.$file_path.'"';
				            exec($sreplace_cmd);
							$results = array("success" => true, "message" => "Successfully save and replace xml editot metadata");
						}else{
			        		$results = array("success" => false, "message" => "XML transformation not yet exists!");
			        	}
			        }else{
			        	$results = array("success" => true, "message" => "Successfully save meta mapping");
			        }
			        
			    }catch(Exception $e){
			    	$results = array("success" => false, "message" => $e->getMessage());
			    }
				break;

			case 'generate_form':
					ob_start();
						include "mapping_form.php";
					$html = ob_get_clean();
                    $results = array("success" => true, "message" => "success", "html" => $html);
				break;
			default:
				$results = array("success" => false, "message" => "Error cannot find submitted process!");
				break;
		}
	}else{
		$results = array("success" => false, "message" => "Error cannot find submitted process!");
	}

	function generateXMLTag($tag, $data, $xml){
		$tag = strtoupper($tag);
		foreach($data as $key => $values){
			if(is_array($values)){
				foreach($values as $key2 =>  $value){
					var_dump($key2.'key');
					var_dump($value.'value');
					if(!empty($key2)){
						$t = strtoupper($key2);
						$xml .= "<{$t}>{$value}</{$t}>".PHP_EOL;
					}else{
						$t = strtoupper($key);
						$xml .= "<{$t}>{$value}</{$t}>".PHP_EOL;
					}
				}
			}else{
				//var_dump($tag);
				$xml .= "<{$tag}>{$values}</{$tag}>".PHP_EOL;	
			}
		}
		return $xml;

	}

	function insertAfter(SimpleXMLElement $new, SimpleXMLElement $target) {
	    $target = dom_import_simplexml($target);
	    $new    = $target->ownerDocument->importNode(dom_import_simplexml($new), true);
	    if ($target->nextSibling) {
	        $target->parentNode->insertBefore($new, $target->nextSibling);
	    } else {
	        $target->parentNode->appendChild($new);
	    }
	}

?>