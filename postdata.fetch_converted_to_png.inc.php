<?php
$file_path = $post['dir'];
if(file_exists($file_path)){
    $dir = new DirectoryIterator($file_path);
    $data = array();
    foreach ($dir as $fileinfo) {
        if (!$fileinfo->isDot()) {
            $dirni = pathinfo($file_path, PATHINFO_FILENAME);
            $fileni = pathinfo($fileinfo->getFilename(),PATHINFO_FILENAME);
            $temp_file = __DIR__.'/TEMPNI/'.$dirni.'/'.$fileni.'.png';
            $editted = false;
            if(file_exists($temp_file)){
                $editted = true;
            }

        	$modified = date("Y-m-d H:i:s", $fileinfo->getMTime());
        	$data[] = array('tempfile' => $temp_file, 'editted' => $editted,'modified' => $modified,'filename' => $fileinfo->getFilename(), 'size' => $fileinfo->getSize(), 'type' => ($fileinfo->isFile()) ? 1 : 2);
        }
    }
    $results = array("success" => true, "message" => "success", 'data' => $data);
}else{
	$results = array("success" => false, "message" => "No File Available");
}
?>