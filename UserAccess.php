<?php
include "conn.php";

$UID = !empty($_GET['UID']) ? $_GET['UID'] : '';

include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	      <h1>
	       User Access
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="UserList.php"><i class="fa fa-dashboard"></i> User Maintenance</a></li>
	        <li class="active">User Access</li>
	      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      	<div class="row">
  
	   		<?php $sql="SELECT * FROM tblUser"; ?>
	        <div class="col-md-12">
	           	<form role="form" method="Post" Action="saveUserAccess.php">

	          		<div class="box box-primary">
	            		<div class="box-header with-border"></div>

	            		<div class="box-body">
			              		<p><b>User Name:</b><?php echo $_GET['Name'];?></p>
						   	
						   		<?php
								   $sql="SELECT * FROM tblUserAccess Where UserID='{$UID}'";
								   $ACQUIRE="";
									$ENRICH="";
									$DELIVER="";
									$USER_MAINTENANCE="";
									$EDITOR_SETTINGS="";
									$ML_SETTINGS="";
									$Transformation="";
									$Transmission="";
									$AQUISITIONREPORT="";
									$CONFIDENCELEVELREPORT="";
									$DATAENTRYSETTING="";
									$REPORTMANAGEMENT="";
									$PROJECTSETUP="";

								   if ($result=mysqli_query($con,$sql))
									{
									// Fetch one and one row
										while ($row=mysqli_fetch_array($result))
										{
											$ACQUIRE=$row["ACQUIRE"];
											$ENRICH=$row["ENRICH"];
											$DELIVER=$row["DELIVER"];
											$USER_MAINTENANCE=$row["USER_MAINTENANCE"];
											$EDITOR_SETTINGS=$row["EDITOR_SETTINGS"];
											$ML_SETTINGS=$row["ML_SETTINGS"];
											$TRANSFORMATION=$row["TRANSFORMATION"];
											$TRANSMISSION=$row["TRANSMISSION"];
											$AQUISITIONREPORT=$row["AQUISITIONREPORT"];
											$CONFIDENCELEVELREPORT=$row["ConfidenceLevelReport"];
											$TASKSETTING=$row["TaskSetting"];
											$DATAENTRYSETTING=$row["DataEntrySetting"];
											$REPORTMANAGEMENT=$row["REPORTMANAGEMENT"];
											$PROJECTSETUP=$row["PROJECTSETUP"];

										}
									}

									if ($PROJECTSETUP==1){
										$PROJECTSETUP='checked';
									}
									else{
										$PROJECTSETUP='';
									}

									if ($REPORTMANAGEMENT==1){
										$REPORTMANAGEMENT='checked';
									}
									else{
										$REPORTMANAGEMENT='';
									}
									if ($AQUISITIONREPORT==1){
										$AQUISITIONREPORT='checked';
									}
									else{
										$AQUISITIONREPORT='';
									}
									if ($ACQUIRE==1){
										$ACQUIRE='checked';
									}
									else{
										$ACQUIRE='';
									}
									
									if ($ENRICH==1){
										$ENRICH='checked';
									}
									else{
										$ENRICH='';
									}
									
									if ($DELIVER==1){
										$DELIVER='checked';
									}
									else{
										$DELIVER='';
									}
									
									if ($USER_MAINTENANCE==1){
										$USER_MAINTENANCE='checked';
									}
									else{
										$USER_MAINTENANCE='';
									}
									
									if ($EDITOR_SETTINGS==1){
										$EDITOR_SETTINGS='checked';
									}
									else{
										$EDITOR_SETTINGS='';
									}
									
									if ($ML_SETTINGS==1){
										$ML_SETTINGS='checked';
									}
									else{
										$ML_SETTINGS='';
									}
									if ($TRANSFORMATION==1){
										$TRANSFORMATION='checked';
									}
									else{
										$TRANSFORMATION='';
									}
									if ($TRANSMISSION==1){
										$TRANSMISSION='checked';
									}
									else{
										$TRANSMISSION='';
									}
								   
								   if ($CONFIDENCELEVELREPORT==1){
										$CONFIDENCELEVELREPORT='checked';
									}
									else{
										$CONFIDENCELEVELREPORT='';
									}
									if ($DATAENTRYSETTING==1){
										$DATAENTRYSETTING='checked';
									}
									else{
										$DATAENTRYSETTING='';
									}
									
								   if ($TASKSETTING==1){
										$TASKSETTING='checked';
									}
									else{
										$TASKSETTING='';
									}
								?>
						   
						   
								<input type="checkbox"  name="ACQUIRE"  <?php echo $ACQUIRE;?>>ACQUIRE <BR>
								<input type="checkbox" name="ENRICH"  <?php echo $ENRICH;?>>ENRICH<BR>
								<input type="checkbox"  name="DELIVER"  <?php echo $DELIVER;?>>DELIVER<BR>
								<input type="checkbox" name="USER_MAINTENANCE"   <?php echo $USER_MAINTENANCE;?>>USER MAINTENANCE<BR>					   
								<input type="checkbox"  name="EDITOR_SETTINGS" <?php echo $EDITOR_SETTINGS;?>>EDITOR SETTINGS <BR>
								<input type="checkbox" name="ML_SETTINGS"   <?php echo $ML_SETTINGS;?>>ML SETTINGS<BR>
								<input type="checkbox" name="Transformation"   <?php echo $TRANSFORMATION;?>>TRANSFORMATION SETTINGS<BR>
								<input type="checkbox" name="Transmission"   <?php echo $TRANSMISSION;?>>TRANSMISSION SETTINGS<BR>
								<input type="checkbox" name="PROJECTSETUP"   <?php echo $PROJECTSETUP;?>>PROJECT SETUP<BR>
								<input type="checkbox" name="TaskSetting"   <?php echo $TASKSETTING;?>>TASK SETTINGS<BR>
								<input type="checkbox" name="DATAENTRYSETTING"   <?php echo $DATAENTRYSETTING;?>>DATAENTRYSETTINGT<BR>
								<input type="checkbox" name="REPORTMANAGEMENT"   <?php echo $REPORTMANAGEMENT;?>>REPORT MANAGEMENT<BR>

								<input type="checkbox" name="AQUISITIONREPORT"   <?php echo $AQUISITIONREPORT;?>>ACQUISITION REPORT<BR>
								<input type="checkbox" name="CONFIDENCELEVELREPORT"   <?php echo $CONFIDENCELEVELREPORT;?>>CONFIDENCE LEVEL REPORT<BR>
								<br> <br>
								<p><b>User Report</b></p>

								<table id="example2" class="table table-bordered table-hover">
									<thead>
										<tr>
											<th></th>
											<th>Report Name</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$strSQL="SELECT * From tblreport";

											if ($result=mysqli_query($con,$strSQL))
											{
												// Fetch one and one row
												while ($row=mysqli_fetch_row($result))
												{
													$ReportID=$row[0];
													$Val='';

													$sql1="SELECT * FROM tbluserreport WHERE ReportID='$ReportID' AND UserID='$UID'";

													if ($result1=mysqli_query($con,$sql1))
													{
														while ($row1=mysqli_fetch_row($result1))
														{
															$Val=$row1[0];
														}
													}
										?>


														<tr>

															<?php
																if ($Val==''){
															?>
																	<td width="20px"><input type="Checkbox" id="chk" name="chk[]" value="<?php echo $ReportID;?>"></td>
															<?php
																}
															else{
															?>
																	<td width="20px"><input type="Checkbox" id="chk" name="chk[]" value="<?php echo $ReportID;?>" checked></td>
															<?php
																}
															?>
																	<td><?php echo $row[1];?></td> 

														</tr>
										<?php
												}
											}
										?>	 
									</tbody>
								</table>
						 
	              		</div>
                  		<div class="box-footer">
							<input type="hidden" class="form-control" placeholder="" name="UID" value="<?= $UID;?>">
			            	<button type="submit" class="btn btn-primary">Save</button>
							<button type="reset" class="btn btn-danger" onclick="location.href='UserList.php'">Cancel</button>
	              		</div>
	           		</div>
			          
	            </form>
				  
	        </div>
	    </div>
	</section>
</div>
 

<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>