<?php
	$UserId = $_SESSION['login_user'];
	$user_image = 'dist/img/user2-160x160.jpg';
	if (file_exists("images/user/".$UserId.".jpg")) {  
		$user_image = "images/user/{$UserId}.jpg";
	}
?>
<header class="main-header">
    <!-- Logo -->
    <a href="<?= ($_SESSION['UserType'] =='Admin') ?  'index.php?page=Enrich&file=&BatchID=&Task=STYLING': 'Dashboard.php'?>" class="logo">
      	<!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><img src="innodata.png" class="img-circle"></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg pull-left"><img src="innodata.png" class="img-circle" alt="User Image">&nbsp;<b>p</b>rimo</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
	    <!-- Sidebar toggle button-->
	    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button"><span class="sr-only">Toggle navigation</span></a>
		<div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <?php //include ("Notifications.php");?>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?php echo $_SESSION['EName'];?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                      		<img src="<?= $user_image;?>"  class="img-circle" alt="">
                       		<p>
                           		<?php echo $_SESSION['EName'];?>
                            	<small><?php echo $_SESSION['UserType'];?></small>
                          	</p>
                        </li>
                        <li class="user-footer">
                          	<div class="pull-left">
                     			<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">Change Password</button>
                          	</div>
                          	<div class="pull-right">
                            	<a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                          	</div>
                        </li>
                    </ul>
                </li>
                <li><a href="javascript:void();"><?= $version;?></a></li>
                <li><a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a></li>
            </ul>
        </div>
    </nav>
</header>