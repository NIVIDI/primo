<?php
   include ("conn.php");
   $error = '';
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 
      
      $myusername = mysqli_real_escape_string($con,$_POST['username']);
      $mypassword = mysqli_real_escape_string($con,$_POST['password']); 
      
      $sql = "SELECT * FROM tbluser WHERE UserName = '$myusername' and Password = '$mypassword'";
	 // echo $sql;
      $result = mysqli_query($con,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
     // $active = $row['active'];
      
      $count = mysqli_num_rows($result);
      
      // If result matched $myusername and $mypassword, table row must be 1 row
		
      if($count == 1) {
        // session_register("myusername");
         $_SESSION['login_user'] = $myusername;
		 
		  
		 if ($result1=mysqli_query($con,$sql))
		  {
		  // Fetch one and one row
		  while ($row1=mysqli_fetch_row($result1))
			{
				 $_SESSION['UserID'] = $row1[0];
				 $_SESSION['EName'] = $row1[3];
				 $_SESSION['UserType'] = $row1[4];
				 
				 
			}
		  }
		  
		  if ($_SESSION['UserType']=='Admin'){
			  header("location: AcquisitionReport.php");
		  }
		  else{
			 $sql="SELECT * FROM primo_view_Jobs Where ProcessCode='ENRICHMENT' AND statusstring in('Allocated','Pending','Ongoing')  AND AssignedTo='$_SESSION[login_user]'";						 
			 
			$rs=odbc_exec($conWMS,$sql);
			$ctr = odbc_num_rows($rs);
			if ($ctr<=0){
			$sqls="EXEC usp_PRIMO_AUTOALLOCATE  @UserName=".$myusername.", @ProcessCode=ENRICHMENT";
			ExecuteQuerySQLSERVER ($sqls,$conWMS);
			}
			
			header("location: Dashboard.php");
		  }
      }else {
         $error = "Your Login Name or Password is invalid";
      }
   }
?>


<!DOCTYPE HTML>
<html lang="zxx">

<head>
	<title>primo: digital content transformation platform</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<meta name="keywords" content=""
	/>
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- Meta tag Keywords -->
	<!-- css files -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">


	<!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->
	<!-- web-fonts -->
	 
	<!-- //web-fonts -->
	<style>
		.right {
		    position: absolute;
			top:15%;
		    right: 0%;
		    width: 40%;
		    padding: 10px;
		}
	</style>
	<link rel="icon" href="innodata.png">
</head>

<body>
	<div class="sub-main-w3 right">
		<form action="" method="post">
				<div class="col-xs-12">
			      	<div class="form-group has-feedback">
			        	<input type="text" class="form-control" name="username" required placeholder="Username">
			        	<span class="glyphicon glyphicon-user form-control-feedback"></span>
			      	</div>
			     </div>
				<div class="col-xs-12">
			      	<div class="form-group has-feedback">
			        	<input type="password" class="form-control" name="password" required placeholder="Password">
			        	<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			      	</div>
			    </div>
				<div class="col-xs-12" style="color:red;padding-bottom: 20px"><?php echo $error;?> </div>
	        	<div class="col-xs-12">
	        		<button type="submit" class="btn btn-primary btn-block btn-flat">Start Your Journey</button>
	        		<label style="margin-top: 20px;"><?= $version;?></label>
	        	</div>
	    </form>
	</div>
</body>
</html>