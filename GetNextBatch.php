<?php

include ("conn.php");

$Task=!empty($_GET['Task']) ? $_GET['Task'] : '';
$page= !empty($_GET['page']) ? $_GET['page'] : '';
$fullscr= !empty($_GET['fullscr']) ? $_GET['fullscr'] : '';

$result = array();
if($Task == 'QC'){
	$sqls = "SELECT TOP 1 a.* 
			FROM primo_view_Jobs a 
				INNER JOIN (SELECT * FROM primo_view_Jobs )  b ON b.JobId = a.JobId AND b.ProcessCode = 'STYLING'
			Where a.ProcessCode='QC' AND b.StatusString = 'Done' AND a.StatusString = 'New' AND a.AssignedTo IS NULL ORDER BY a.BatchId ASC";

	$rs=odbc_exec($conWMS,$sqls);
	$Batchname = '';
	while(odbc_fetch_row($rs))
	{	
		$Batchname = odbc_result($rs,"BatchID");
	}

	$user_in_wms = $_SESSION['UserID'];
	if(!empty($Batchname)){
		allocateBatchToUser($Batchname, $user_in_wms);
		header("Location: index.php?page=Enrich&file={$filename}&BatchID={$Batchname}&Task={$Task}");
		exit;
	}

	$_SESSION['page_error'] = "\nError: No available batch that is ready for QA!";
	header("Location: error.php");
	exit;

}else{
	$sqls="EXEC USP_PRIMO_AUTOALLOCATE  @UserName='".$_SESSION['login_user']."', @ProcessCode='".$Task."',@WorkflowId='".$WorkflowID."'";

	$result = ExecuteQuerySQLSERVER ($sqls,$conWMS);
}

if(!empty($result)){
	$_SESSION['page_error'] = "\nError: {$result}";
	header("Location: error.php");
	exit;
}

$sql="SELECT * FROM primo_view_Jobs Where ProcessCode='$Task' AND statusstring in('Allocated','Pending','Ongoing')  AND AssignedTo='{$_SESSION['login_user']}'";	

$rs=odbc_exec($conWMS,$sql);
$ctr = odbc_num_rows($rs);
if($ctr == 0){
	$_SESSION['page_error'] = "No batch file to fetch, please register a file.";
	header("Location: error.php");
	exit;
}

$sFilename = '';
while(odbc_fetch_row($rs))
{	
	$sFilename=odbc_result($rs,"Filename");
	$filename= odbc_result($rs,"Filename");
	$Batchname=odbc_result($rs,"BatchID");
	$snFilename ="uploadFiles/SourceFiles/".odbc_result($rs,"JobId")."/".odbc_result($rs,"Filename");
}

$result = ExecuteQuerySQLSERVER ("Update primo_Integration SET Status='".$Task."' Where Filename='".$sFilename."'",$conWMS);
if(!empty($result)){
	$_SESSION['page_error'] = "\nError: {$result}";
	header("Location: error.php");
	exit;
}

if ($Task=='CONTENTREVIEW'){
	if (file_exists($snFilename)){
		$sHTML = file_get_contents($snFilename);
		$sHTML =str_replace("<link ", "<pre ", $sHTML);
		$sHTML =str_replace("</link>", "</pre>", $sHTML);
		$sHTML =str_replace("<script", "<pre", $sHTML);
		$sHTML =str_replace("</script>", "</pre>", $sHTML);
		file_put_contents($snFilename, $sHTML);
	}
}

if ($Task=='WRITING'||$Task=='WRITINGQC'||$Task=='FINALREVIEW'){
	$fullscr=1;
}

if ($fullscr==1){
	header("Location: fullscr.php?file={$filename}&BatchID={$Batchname}&Task={$Task}");
}else{
	header("Location: index.php?page=Enrich&file={$filename}&BatchID={$Batchname}&Task={$Task}");
}

?>
 

