<?php
	$tag = !empty($_POST['tag']) ? trim($_POST['tag']) : '';
	$process = !empty($_POST['process']) ? $_POST['process'] : '';
   $shortcut_key = !empty($_POST['tag_shortcut']) ? str_replace(' ','',trim($_POST['tag_shortcut'])) : '';
	try{
		if(!empty($process)){
			switch($process){
				case 'add': 
                  $tagid = !empty($_POST['tagid']) ? $_POST['tagid'] : '';
						//donot insert duplicate tag
						$sql = "SELECT COUNT(*) as total_tag FROM styling_tags WHERE tag = '{$tag}' AND id <> '{$tagid}' LIMIT 1";
						$cnt_result = mysqli_query($con, $sql)->fetch_assoc();
						if(empty($cnt_result['total_tag'])){
							$tag_name = !empty($_POST['tag_name']) ? $_POST['tag_name'] : '';
							$sql2 = "INSERT INTO styling_tags (`id`, `name`, `tag`, `created_by`,`shortcut`) VALUES ('{$tagid}', '{$tag_name}', '{$tag}','{$_SESSION['UserID']}', '{$shortcut_key}') ON DUPLICATE KEY UPDATE `name` = '{$tag_name}', `tag` = '$tag', `shortcut` = '{$shortcut_key}' ";
							ExecuteQuery($sql2,$con);
   							$results = array("success" => true, "message" => "Successfully submitted tag!");
						}else{
							$results = array("success" => false, "message" => "Tag name({$tag}) already exist!");
						}
   					break;

   				case 'get':
   						$sql="SELECT * FROM styling_tags ORDER by tag ASC";
   						$sql_result = mysqli_query($con, $sql);
   						$data = array();
   						if($sql_result->num_rows){
   							while($row = $sql_result->fetch_assoc()){
   								$data[] = $row;
   							}
   						}

   						$results = array("success" => true, "message" => "success", "data" => $data);
   					break;

   				case 'delete':
   						$ids = json_decode($_POST['ids']);
   						if(!empty($ids)){
   							foreach($ids as $id){
   								$sql = "DELETE FROM styling_tags WHERE id = '{$id}' ";
   								ExecuteQuery($sql,$con);
   							}
   						}

   						$results = array("success" => true, "message" => "Successfully deleted selected tags");
   					break;

   				default:
   					$results = array("success" => false, "message" => "Cannot use the default process!");
   					break;
			}
    	}else{
   			$results = array("success" => false, "message" => "Cannot find the process!");
   		}
   	}catch(Exception $e){
   		$results = array("success" => false, "message" => $e->getMessage());
   	}
?>