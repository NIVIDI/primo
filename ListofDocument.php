<?php
include "conn.php";
include("header.php");
include("header_nav.php");
include ("sideBar.php");	
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
	     <?php
    	     $Task= empty($_GET['Task']) ? '': $_GET['Task'];
           $lTask= empty($_GET['lTask'])? '': $_GET['lTask'];
	     ?>
        List of Document on Queue (<?php echo $Task;?>)
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">List of Document on Queue </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
  
  
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
               
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1"  class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Filename</th>
                        <th>Assignee</th>
                        <th>Date Registered</th>
                    </tr>
                </thead>
                <tbody>
        				<?php
        		 
        				
                      if ($lTask==""){
                          $sql="SELECT * FROM primo_view_Jobs Where ProcessCode='$Task' and statusstring<>'Done'";   
                      }
                      else{
                          $sql="SELECT * FROM primo_view_Jobs Where ProcessCode='$Task' AND StatusString <> 'Done'";      
                      }
          				    
                      $objExec= odbc_exec($conWMS,$sql);
          		
          				 
          				  while ($row = odbc_fetch_array($objExec)) 
          				  {
          				    $filename="uploadFiles/".$row["Filename"];
        			?>
                      <tr>
                          <td><a href="index.php?file=<?= $filename;?>&BatchID=<?= $row["BatchId"];?>&Status=<?= $Task;?>"><?php echo $row["JobName"];?></a></td>
                          <td><a href="<?php echo $row["SourceUrl"];?>" target="_blank"><?php echo $row["Filename"];?></a></td>
                          <td><?php echo $row["AssignedTo"];?></td>
                          <td><?php echo $row["DateRegistered"];?></td>
                      </tr>
				      <?php
				            }
				      ?>	  
                </tbody>
                
              </table>
            </div>
            
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
