<?php
include "conn.php";
$sql="SELECT * FROM tblmlconfig";
$previousMonth =  date("Y-m", strtotime ( '-1 month' , strtotime ( date('Y-m-d') ) )) ;
$From= empty($_POST['From']) ? $previousMonth: $_POST['From'];
$To=empty($_POST['To']) ? date('Y-m'): $_POST['To'];
$pFrom = $From;
$pTo = $To;
$myDate = new DateTime($From);
$From = $myDate->format('m/d/Y');

$myDate = new DateTime($To);
$To = $myDate->format('m/d/Y');
$date1 = date('Y-m-d', strtotime($To. ' + 1 month'));
$myDate = new DateTime( $date1);
$To = $myDate->format('m/d/Y');

include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Collection Summary
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Collection Summary</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
  
   <?php


$sql="SELECT * FROM tblmlconfig";

?>
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
               
              <form method="post" action="">
                       From:<input type="Month" Name="From" value="<?php echo $pFrom;?>"> To: <input type="Month" Name="To" value="<?php echo $pTo;?>"> <button type="submit" class="btn btn-primary small"><i class="fa  fa-search"></i> Search</button>
              </form>
             
            <div class="box-body">
                <div class="row">
    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<?php
        
        $strSQL="SELECT        dbo.SN_Executions.ConfigName, dbo.SN_Executions.MainURL, COUNT(dbo.PRIMO_Integration.RefId) AS DownloadedCount FROM            dbo.SN_Executions LEFT OUTER JOIN dbo.PRIMO_Integration ON dbo.SN_Executions.ExecutionId = dbo.PRIMO_Integration.ExecutionId WHERE EndDate>='$From' AND EndDate<'$To' GROUP BY dbo.SN_Executions.ConfigName, dbo.SN_Executions.MainURL";
        $objExec= odbc_exec($conWMS,$strSQL);
         
        $r = odbc_num_rows($objExec);
          ?>
    <script type="text/javascript">
    

// Create the chart
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Collection Report'
    },
    subtitle: {
        text: 'Click the columns to view the monthly frequency.'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total file downloaded.'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.0f}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> of total<br/>'
    },

    "series": [
        {
            "name": "Summary",
            "colorByPoint": true,
            "data": [
      <?php
        while ($row = odbc_fetch_array($objExec)) 
        {
          $i ++;
          
          if ($i==$r){
            
      ?>
                {
                    "name": "<?php echo $row['ConfigName'];?>",
                    "y": <?php echo $row['DownloadedCount'];?>,
                    "drilldown": "<?php echo $row['ConfigName'];?>"
                }
      <?php
          }
          else
          {
            ?>
        {
                    "name": "<?php echo $row['ConfigName'];?>",
                    "y": <?php echo $row['DownloadedCount'];?>,
                    "drilldown": "<?php echo $row['ConfigName'];?>"
                },
            <?php
          }
        }
      ?>
                
               
            ]
        }
    ],
    "drilldown": {
        "series": [
    <?php
      $objExec= odbc_exec($conWMS,$strSQL);
        $r = odbc_num_rows($objExec);
      while ($row = odbc_fetch_array($objExec)) 
        {
          $i ++;
          
          if ($i!=$r){
    ?>
    
            {
                "name": "<?php echo $row['ConfigName'];?>",
                "id": "<?php echo $row['ConfigName'];?>",
                "data": [
        
        <?php
        $strSQL1="SELECT        dbo.SN_Executions.ConfigName, dbo.SN_Executions.MainURL, COUNT(dbo.PRIMO_Integration.RefId) AS DownloadedCount,DATENAME(month,EndDate ) AS MonthVal,Month(EndDate) FROM            dbo.SN_Executions LEFT OUTER JOIN dbo.PRIMO_Integration ON dbo.SN_Executions.ExecutionId = dbo.PRIMO_Integration.ExecutionId WHERE EndDate>='$From' AND EndDate<'$To' AND dbo.SN_Executions.ConfigName='". $row['ConfigName']."'  GROUP BY dbo.SN_Executions.ConfigName, dbo.SN_Executions.MainURL,DATENAME(month,EndDate ),Month(EndDate) ORDER BY Month(EndDate) ASC";


        $objExec1= odbc_exec($conWMS,$strSQL1);
         
        $r1 = odbc_num_rows($objExec1);
        while ($row1 = odbc_fetch_array($objExec1)) 
        {
          $j ++;
          
          if ($j!=$r1){
        ?>
        
                    [
                        "<?php echo $row1['MonthVal'];?>",
                        <?php echo $row1['DownloadedCount'];?>
                    ],
          <?php
          }
          else{
            
          ?>
                    [
                       "<?php echo $row1['MonthVal'];?>",
                        <?php echo $row1['DownloadedCount'];?>
                    ]
      <?php
          }
        }
      ?>          
                ]
            },
      <?php
        }
        else
        {
          ?>
            {
                "name": "<?php echo $row['ConfigName'];?>",
                "id": "<?php echo $row['ConfigName'];?>",
                "data": [
                    <?php
        $strSQL1="SELECT        dbo.SN_Executions.ConfigName, dbo.SN_Executions.MainURL, COUNT(dbo.PRIMO_Integration.RefId) AS DownloadedCount,DATENAME(month,EndDate ) AS MonthVal,Month(EndDate) FROM            dbo.SN_Executions LEFT OUTER JOIN dbo.PRIMO_Integration ON dbo.SN_Executions.ExecutionId = dbo.PRIMO_Integration.ExecutionId WHERE EndDate>='$From' AND EndDate<'$To' AND dbo.SN_Executions.ConfigName='". $row['ConfigName']."'  GROUP BY dbo.SN_Executions.ConfigName, dbo.SN_Executions.MainURL,DATENAME(month,EndDate ),Month(EndDate) ORDER BY Month(EndDate) ASC";
        $objExec1= odbc_exec($conWMS,$strSQL1);
         
        $r1 = odbc_num_rows($objExec1);
        while ($row1 = odbc_fetch_array($objExec1)) 
        {
          $j ++;
          
          if ($j!=$r1){
        ?>
        
                    [
                        "<?php echo $row1['MonthVal'];?>",
                        <?php echo $row1['DownloadedCount'];?>
                    ],
          <?php
          }
          else{
            
          ?>
                    [
                       "<?php echo $row1['MonthVal'];?>",
                        <?php echo $row1['DownloadedCount'];?>
                    ]
      <?php
          }
      ?>          
                ]
            }
      <?php
        }
      }
        }
      ?>
            
        ]
    }
});
    </script>
            </div>
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ConfigName</th>
                  <th>Main URL</th>
                  <th>Total Files Downloaded</th>
                </tr>
                </thead>
                <tbody>
        <?php
        
        $strSQL="SELECT dbo.SN_Executions.ConfigName, dbo.SN_Executions.MainURL, COUNT(PRIMO_Integration.RefId) AS DownloadedCount FROM dbo.SN_Executions LEFT OUTER JOIN  dbo.PRIMO_Integration ON dbo.SN_Executions.ExecutionId = PRIMO_Integration.ExecutionId WHERE EndDate>='$From' AND EndDate<='$To' GROUP BY dbo.SN_Executions.ConfigName, dbo.SN_Executions.MainURL";
        $objExec= odbc_exec($conWMS,$strSQL);
    
         
        while ($row = odbc_fetch_array($objExec)) 
        {
          $url_info = parse_url($row["MainURL"]);
        //$objResult=odbc_fetch_array($objExec,$i);   
?>
                <tr>
                  <td><a href="Collections.php?Configuration=<?php echo $row["ConfigName"];?>" target='_blank'><?php echo $row["ConfigName"];?></a></td>
                  <td><?php echo $url_info['host'];?></td>
                   <td><?php echo $row["DownloadedCount"];?></td>
                </tr>
          <?php
        }
         
       
      ?>    
                </tbody>
                <tfoot>
                <tr>
                  <th>Court Name</th>
                  <th>Main URL</th>
                  <th>Total Files Downloaded</th>
                </tr>
                </tfoot>
              </table>
          </div>
            </div>
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
