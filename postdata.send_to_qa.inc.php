<?php
	$bundle = $post['bundle'];
	
	if(!empty($bundle)){
		$numofdocs = 0;
	    $sql = "SELECT * FROM segregated_files WHERE prec_bundle = '{$bundle}' ";
	    $result = mysqli_query($con, $sql);
	    $total_done = 0;
	    $file_segregrated = array();
	    if($result->num_rows){
	        while($row1 = $result->fetch_assoc()){
	            $numofdocs ++;
	            if(!empty($row1['wms_job_id'])){
	                $wms_status = GetWMSValue("SELECT StatusString FROM primo_view_Jobs WHERE JobId = '{$row1['wms_job_id']}' AND ProcessCode = 'QC' ","StatusString",$conWMS);
	                if(strtolower($wms_status) == 'done'){
	                    $total_done ++;
	                }
	            }

	            $file_segregrated[] = $row1;
	        }
	    }

	    if(empty($numofdocs)){
	    	$results = array("success" => false, "message" => "Cannot send bundle({$bundle}) to QA with empty source file!");
	    }else{
	    	if((int) $numofdocs > (int) $total_done){
	    		$results = array("success" => false, "message" => "Error sending bundle({$bundle}), some source batched files not yet completed!");
	    	}else{
	    		//check for sources files if already has generated .key file
	    		$sql = "SELECT * FROM files_for_batching WHERE prec_bundle = '{$bundle}' ";
	    		$source_result = mysqli_query($con, $sql);

	    		$process_result = array();
	    		if($source_result->num_rows){
	    			while($row = $source_result->fetch_assoc()){
	    				$key_filaname = generateKeyFilename($row['file_name']);
	    				$path = $SourceFilePath.'/'.$bundle.'/deliverables/'.$key_filaname;
	    				if(!file_exists($path)){
	    					$process_result[] = array("success" => false, "message" => "Source File({$row['file_name']}) has not yet compile!");
	    				}
	    			}
	    		}

	    		if(!empty($process_result)){
		    		$message = '';
					foreach($process_result as $response){
						$style = "display: block; color: red;";
						if($response['success']){
							$style = "display: block; color: green;";
						}
						$message .= "<li style='{$style}'>{$response['message']}</li>";
					}
					$results = array("success" => false, "message" => "{$message}");
				}else{
					//create attachment for email
					$attachments = array();
						
					$subject = 'TPCPP Bundle '.$bundle;
					$body = trim($post['email_msg']);
					$mail = sendMail($subject, $body, $attachments, $post['email_to'], $post['email_cc']);
					if($mail['success']){
						//update bundle to complete
						$sql = "UPDATE files_for_batching SET status = 'QA' WHERE prec_bundle = '{$bundle}' ";
						ExecuteQuery($sql,$con);

						$sql_insert_qa = "INSERT INTO `qa_review`(`bundle`, `created_by`, `to`, `cc`, `message`) 
											VALUES('{$bundle}', '{$_SESSION['UserID']}', '{$post['email_to']}', '{$post['email_cc']}', '{$post['email_msg']}')
											ON DUPLICATE KEY UPDATE 
											`created_by` = '{$_SESSION['UserID']}', 
											`to` = '{$post['email_to']}',
											`cc` = '{$post['email_cc']}',
											`message` = '{$post['email_msg']}',
											`status` = ''
										";
					    ExecuteQuery($sql_insert_qa,$con);
						$results = array("success" => true, "message" => "Successfully notify QA for bundle({$bundle}).");
					}else{
						$results = array("success" => false, "message" => $mail['message']);
					}
				}
            }
	    }
	}else{
		$results = array("success" => false, "message" => "Error cannot find specified bundle!");
	}
?>