<?php
$sxfilename = pathinfo($filename, PATHINFO_FILENAME);
$bundle_path = $bundle;
$docket_value1 = '';
$docket_value2 = '';
$docket_value3 = '';
$docket_label1 = '';
$docket_label2 = '';
$docket_label3 = '';
	
$client_xml_path = $SourceFilePath."/{$bundle_path}/{$sxfilename}.xml";

$meta_obj = array();
$meta_path = $base_url."/uploadFiles/SourceFiles/{$bundle_path}/{$sxfilename}_meta.xml";
if(file_exists($SourceFilePath."/{$bundle_path}/{$sxfilename}_meta.xml")){
	$meta_xml = file_get_contents($SourceFilePath."/{$bundle_path}/{$sxfilename}_meta.xml");
	$meta_xml = replaceXMLInvalidTag($meta_xml);
	$meta = simplexml_load_string($meta_xml);//simplexml_load_file($SourceFilePath."/{$bundle_path}/{$sxfilename}_meta.xml");
	$meta_obj = (array) $meta;
	
	
	
}else{

	if(file_exists($client_xml_path)){
		try{
			$meta_xml = file_get_contents($client_xml_path);
			$meta_xml = preg_replace("/(\r\n|\r|\n)<\/FREEFORM>/", "$1ƒ</FREEFORM>", $meta_xml);
			$meta_xml = preg_replace("/(\r\n|\r|\n)<FREEFORM>[^ƒ]+ƒ<\/FREEFORM>(\r\n|\r|\n)<\/[A-Z]+>/", "</meta_tags>", $meta_xml);
			$meta_xml = preg_replace("/(<\/GRAPHIC>)/", "ƒ$1", $meta_xml);
			$meta_xml = preg_replace("/^[^ƒ]+ƒ<\/GRAPHIC>/", "<meta_tags>", $meta_xml);
			
			
			$meta_xml = replaceXMLInvalidTag($meta_xml);
			$meta = simplexml_load_string($meta_xml);

			$meta_obj = (array) $meta;
			
		}catch(Exception $e){
			var_dump($e->getMessage());

			exit;
		}
	}
}

if(!empty($meta_obj['DOCKET'])){
	if(is_array($meta_obj['DOCKET'])){
		//print_r((array) $meta->DOCKET->attributes());
		$x = 0;

		foreach($meta->DOCKET as $key => $docket){
			$x ++;
			$label = "docket_label{$x}";
			$$label = !empty($docket->attributes()) ? str_replace('....XXXX....XXXX....','&',(string) $docket->attributes()['LABEL'][0]) : '';
		}
		$docket_value1 = !empty($meta_obj['DOCKET'][0]) ? str_replace('....XXXX....XXXX....','&',$meta_obj['DOCKET'][0]) : '';
		$docket_value2 = !empty($meta_obj['DOCKET'][1]) ? str_replace('....XXXX....XXXX....','&',$meta_obj['DOCKET'][1]) : '';;
		$docket_value3 = !empty($meta_obj['DOCKET'][2]) ? str_replace('....XXXX....XXXX....','&',$meta_obj['DOCKET'][2]) : '';
	}else{
		$docket_value1 = !empty($meta_obj['DOCKET']) ? str_replace('....XXXX....XXXX....','&',$meta_obj['DOCKET']) : '';
		$x = 0;
		foreach($meta->DOCKET as $key => $docket){
			$x ++;
			$label = "docket_label{$x}";
			$$label = !empty((string) $docket->attributes()['LABEL']) ? str_replace('....XXXX....XXXX....','&', (string) $docket->attributes()['LABEL']): '';
		}
	}
}

?>
<div class="col-sm-12">
	<a href="<?= $meta_path.'?time='.time();?>" target="_blank">Preview</a>
</div>
<div class="col-sm-12" style="margin-top: 20px;">
	<form id="mapping_form">
		<input type="hidden" name="filaname" id="filenametopass" value="<?= $filename;?>">
		<input type="hidden" name="bundle" value="<?= $bundle_path;?>">
	 		<fieldset class="box-border">
		    <legend class="box-border">DOCTI</legend>
		    <div class="control-group">
		        <input type="text" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" value="<?= !empty($meta_obj['DOCTI']) ? str_replace('....XXXX....XXXX....','&',$meta_obj['DOCTI']) : '' ?>" id="meta_docti" name="meta_tags[DOCTI]"/>
		    </div>
		</fieldset>
		<fieldset class="box-border">
		    <legend class="box-border">SUBJECT</legend>
		    <?php
		    	$subject1 = '';
		    	$subject2 = '';
		    	$subject3 = '';
		    	$subject4 = '';
		    	$subject5 = '';
		    	if(!empty($meta_obj['SUBJECT'])){
			    	if(is_array($meta_obj['SUBJECT'])){
			    		$subject1 = !empty($meta_obj['SUBJECT'][0]) ? str_replace('....XXXX....XXXX....','&',$meta_obj['SUBJECT'][0]) : '';
				    	$subject2 = !empty($meta_obj['SUBJECT'][1]) ? str_replace('....XXXX....XXXX....','&',$meta_obj['SUBJECT'][1]) : '';
				    	$subject3 = !empty($meta_obj['SUBJECT'][2]) ? str_replace('....XXXX....XXXX....','&',$meta_obj['SUBJECT'][2]) : '';
				    	$subject4 = !empty($meta_obj['SUBJECT'][3]) ? str_replace('....XXXX....XXXX....','&',$meta_obj['SUBJECT'][3]) : '';
				    	$subject5 = !empty($meta_obj['SUBJECT'][4]) ? str_replace('....XXXX....XXXX....','&',$meta_obj['SUBJECT'][4]) : '';
			    	}else{
			    		$subject1 = str_replace('....XXXX....XXXX....','&',(string) $meta_obj['SUBJECT']);
			    	}
		    	}	
		    ?>
		    <div class="control-group">
		        <input type="text" style="margin-bottom: 5px;" value="<?=  $subject1;?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[SUBJECT][]"/>
		        <input type="text" style="margin-bottom: 5px;" value="<?=  $subject2;?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[SUBJECT][]"/>
		        <input type="text" style="margin-bottom: 5px;" value="<?=  $subject3;?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[SUBJECT][]"/>
		        <input type="text" style="margin-bottom: 5px;" value="<?=  $subject4;?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[SUBJECT][]"/>
		        <input type="text" style="margin-bottom: 5px;" value="<?=  $subject5;?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[SUBJECT][]"/>
		    </div>
		</fieldset>
		<fieldset class="box-border">
		    <legend class="box-border">DATE</legend>
		    <div class="control-group">
		    	<input type="hidden" class="form-control" name="meta_tags[DATE][0][@YYYYMMDD]"/>
		        <input type="text" class="form-control" value="<?= !empty($meta_obj['DATE']) ? $meta_obj['DATE'] : '' ?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[DATE][0][%]"/>
		    </div>
		</fieldset>
		<fieldset class="box-border">
		    <legend class="box-border">DECIDER</legend>
		    <div class="control-group">
		        <input type="text" class="form-control" value="<?= !empty($meta_obj['DECIDER']) ? str_replace('....XXXX....XXXX....','&',$meta_obj['DECIDER']) : '' ?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[DECIDER]"/>
		    </div>
		</fieldset>
		<fieldset class="box-border">
		    <legend class="box-border">CASEREF</legend>
		    <div class="control-group">
		    	<label>SOC</label>
		        <input type="text" class="form-control" value="<?= !empty($meta_obj['CASEREF']->SOC) ? str_replace('....XXXX....XXXX....','&',$meta_obj['CASEREF']->SOC) : '' ?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[CASEREF][SOC]"/>
		    </div>
		    <div class="control-group">
		    	<label>CITE</label>
		        <input type="text" class="form-control" value="<?= !empty($meta_obj['CASEREF']->CITE) ? str_replace('....XXXX....XXXX....','&',$meta_obj['CASEREF']->CITE) : '' ?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[CASEREF][CITE]"/>
		    </div>
		    <fieldset class="box-border">
		    	<legend class="box-border" style="font-size: 12px !important;">CORRELAT</legend>
		    	<div class="control-group">
		    		<label style="font-size: 12px !important;">CITE</label>
				    <input type="text" class="form-control" value="<?= !empty($meta_obj['CASEREF']->CORRELAT->CITE) ? str_replace('....XXXX....XXXX....','&',$meta_obj['CASEREF']->CORRELAT->CITE) : '' ?>" onclick="MAPPINGTAGS.getSelection(this);" type="text" name="meta_tags[CASEREF][CORRELAT][CITE]"/>
				</div>
		    </fieldset>
		</fieldset>
		<fieldset class="box-border">
		    <legend class="box-border">DOCKETS</legend>
		    <fieldset class="box-border">
		    	<legend class="box-border" style="font-size: 12px !important;">DOCKET</legend>
		    	<div class="control-group">
		    		<label style="font-size: 12px !important;">LABEL</label>
				    <input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$docket_label1);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[DOCKET][0][@LABEL]"/>
		    		<label style="font-size: 12px !important;">VALUE</label>
				    <input type="text" value="<?= str_replace('....XXXX....XXXX....','&',$docket_value1); ?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[DOCKET][0][%]"/>
				</div>
		    </fieldset>
		    <fieldset class="box-border">
		    	<legend class="box-border" style="font-size: 12px !important;">DOCKET</legend>
		    	<div class="control-group">
		    		<label style="font-size: 12px !important;">LABEL</label>
				    <input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$docket_label2);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[DOCKET][1][@LABEL]"/>
					<label style="font-size: 12px !important;">VALUE</label>
				    <input type="text" value="<?= str_replace('....XXXX....XXXX....','&',$docket_value2); ?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[DOCKET][1][%]"/>
				</div>
		    </fieldset>
		    <fieldset class="box-border">
		    	<legend class="box-border" style="font-size: 12px !important;">DOCKET</legend>
		    	<div class="control-group">
		    		<label style="font-size: 12px !important;">LABEL</label>
				    <input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$docket_label3);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[DOCKET][2][@LABEL]"/>
					<label style="font-size: 12px !important;">VALUE</label>
				    <input type="text" value="<?= str_replace('....XXXX....XXXX....','&',$docket_value3); ?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[DOCKET][2][%]"/>
				</div>
		    </fieldset>
		</fieldset>
		<fieldset class="box-border">
		    <legend class="box-border">COURTJUR</legend>
		    <div class="control-group">
		    	<input type="text" class="form-control" value="<?= !empty($meta_obj['COURTJUR']) ? str_replace('....XXXX....XXXX....','&',$meta_obj['COURTJUR']) : '' ?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COURTJUR]"/>
		    </div>
		</fieldset>
		<?php
				$partyblock_party1 = '';
				$partyblock_party2 = '';
				if(!empty($meta_obj['PARTYBLOCK']->PARTY)){
					if(is_array($meta_obj['PARTYBLOCK']->PARTY) || is_object($meta_obj['PARTYBLOCK']->PARTY)){
						$partyblock_party1 = $meta_obj['PARTYBLOCK']->PARTY[0];
						$partyblock_party2 = $meta_obj['PARTYBLOCK']->PARTY[1];
					}
				}

				$partyblock_role1 = '';
				$partyblock_role2 = '';
				if(!empty($meta_obj['PARTYBLOCK']->PARTYROLE)){
					if(is_array($meta_obj['PARTYBLOCK']->PARTYROLE) || is_object($meta_obj['PARTYBLOCK']->PARTYROLE)){
						$party = (array) $meta_obj['PARTYBLOCK']->PARTYROLE;
						$partyblock_role1 = $meta_obj['PARTYBLOCK']->PARTYROLE[0];
						$partyblock_role2 = $meta_obj['PARTYBLOCK']->PARTYROLE[1];
					}
				}
		?>
		<fieldset class="box-border">
		    <legend class="box-border">PARTYBLOCK</legend>
		    <div class="control-group">
		    	<label>LINE</label>
		    	<input type="text" class="form-control" value="<?= !empty($meta_obj['PARTYBLOCK']->LINE) ? str_replace('....XXXX....XXXX....','&',$meta_obj['PARTYBLOCK']->LINE) : '' ?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[PARTYBLOCK][LINE]"/>
		    </div>
		    <div class="control-group">
		    	<label>PARTY</label>
		    	<input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$partyblock_party1);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[PARTYBLOCK][PARTY]"/>
		    </div>
		    <div class="control-group">
		    	<label>PARTYROLE</label>
		    	<input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$partyblock_role1);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[PARTYBLOCK][PARTYROLE]"/>
		    </div>
		    <div class="control-group">
		    	<label>CONNECTOR</label>
		    	<input type="text" class="form-control" value="<?= !empty($meta_obj['PARTYBLOCK']->CONNECTOR) ? $meta_obj['PARTYBLOCK']->CONNECTOR : '' ?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[PARTYBLOCK][CONNECTOR]"/>
		    </div>
		    <div class="control-group">
		    	<label>PARTY</label>
		    	<input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$partyblock_party2);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[PARTYBLOCK][PARTY2]"/>
		    </div>
		    <div class="control-group">
		    	<label>PARTYROLE</label>
		    	<input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$partyblock_role2);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[PARTYBLOCK][PARTYROLE2]"/>
		    </div>
		</fieldset>
		<fieldset class="box-border">
		    <legend class="box-border">COUNSELBLOCK</legend>
		    <fieldset class="box-border">
		    	<legend class="box-border" style="font-size: 12px !important;">LAWFIRM</legend>
		    	<div class="control-group">
		    		<label style="font-size: 12px !important;">FIRMNAME</label>
				    <input type="text" class="form-control" value="<?= !empty($meta_obj['COUNSELBLOCK']->LAWFIRM->FIRMNAME) ? str_replace('....XXXX....XXXX....','&',$meta_obj['COUNSELBLOCK']->LAWFIRM->FIRMNAME) : ''; ?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][LAWFIRM][FIRMNAME]"/>
				    <label style="font-size: 12px !important;">FIRMTYPE</label>
				    <input type="text" class="form-control" value="<?= !empty($meta_obj['COUNSELBLOCK']->LAWFIRM->FIRMTYPE) ? str_replace('....XXXX....XXXX....','&',$meta_obj['COUNSELBLOCK']->LAWFIRM->FIRMTYPE): ''; ?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][LAWFIRM][FIRMTYPE]"/>
				</div>
		    </fieldset>
		    <?php
		    	$address_line1 = '';
				$address_line2 = '';
				$address_line3 = '';
				$address_line4 = '';
				$address_line5 = '';
				$address_line6 = '';
				$address_line7 = '';
				$address_line8 = '';
				$address_line9 = '';
				$address_line10 = '';
				if(!empty($meta_obj['COUNSELBLOCK']->ADDRESS)){
					if(is_array($meta_obj['COUNSELBLOCK']->ADDRESS) || is_object($meta_obj['COUNSELBLOCK']->ADDRESS)){
						$address_line1 = $meta_obj['COUNSELBLOCK']->ADDRESS->LINE[0];
						$address_line2 = $meta_obj['COUNSELBLOCK']->ADDRESS->LINE[1];
						$address_line3 = $meta_obj['COUNSELBLOCK']->ADDRESS->LINE[2];
						$address_line4 = $meta_obj['COUNSELBLOCK']->ADDRESS->LINE[3];
						$address_line5 = $meta_obj['COUNSELBLOCK']->ADDRESS->LINE[4];
						$address_line6 = $meta_obj['COUNSELBLOCK']->ADDRESS->LINE[5];
						$address_line7 = $meta_obj['COUNSELBLOCK']->ADDRESS->LINE[6];
						$address_line8 = $meta_obj['COUNSELBLOCK']->ADDRESS->LINE[7];
						$address_line9 = $meta_obj['COUNSELBLOCK']->ADDRESS->LINE[8];
						$address_line10 = $meta_obj['COUNSELBLOCK']->ADDRESS->LINE[9];
					}
				}
		    ?>
		    <fieldset class="box-border">
		    	<legend class="box-border" style="font-size: 12px !important;">ADDRESS</legend>
		    	<div class="control-group">
		    		<label style="font-size: 12px !important;">LINE</label>
				    <input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$address_line1);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][ADDRESS][LINE][]"/>
				    <label style="font-size: 12px !important;">LINE</label>
				    <input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$address_line2);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][ADDRESS][LINE][]"/>
				    <label style="font-size: 12px !important;">LINE</label>
				    <input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$address_line3);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][ADDRESS][LINE][]"/>
				    <label style="font-size: 12px !important;">LINE</label>
				    <input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$address_line4);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][ADDRESS][LINE][]"/>
				    <label style="font-size: 12px !important;">LINE</label>
				    <input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$address_line5);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][ADDRESS][LINE][]"/>
				    <label style="font-size: 12px !important;">LINE</label>
				    <input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$address_line6);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][ADDRESS][LINE][]"/>
				    <label style="font-size: 12px !important;">LINE</label>
				    <input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$address_line7);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][ADDRESS][LINE][]"/>
				    <label style="font-size: 12px !important;">LINE</label>
				    <input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$address_line8);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][ADDRESS][LINE][]"/>
				    <label style="font-size: 12px !important;">LINE</label>
				    <input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$address_line9);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][ADDRESS][LINE][]"/>
				    <label style="font-size: 12px !important;">LINE</label>
				    <input type="text" class="form-control" value="<?= str_replace('....XXXX....XXXX....','&',$address_line10);?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][ADDRESS][LINE][]"/>
				</div>
		    </fieldset>
		    <fieldset class="box-border">
		    	<legend class="box-border" style="font-size: 12px !important;">COUNSEL</legend>
		    	<div class="control-group">
		    		<label style="font-size: 12px !important;">FIRSTNAME</label>
				    <input type="text" class="form-control"  value="<?= !empty($meta_obj['COUNSELBLOCK']->COUNSEL->FIRSNAME) ? str_replace('....XXXX....XXXX....','&',$meta_obj['COUNSELBLOCK']->COUNSEL->FIRSNAME): ''; ?>" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][COUNSEL][FIRSNAME]"/>
				    <label style="font-size: 12px !important;">LASTNAME</label>
				    <input type="text" value="<?= !empty($meta_obj['COUNSELBLOCK']->COUNSEL->LASTNAME) ? str_replace('....XXXX....XXXX....','&',$meta_obj['COUNSELBLOCK']->COUNSEL->LASTNAME): ''; ?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][COUNSEL][LASTNAME]"/>
				</div>
		    </fieldset>
		    <?php
		    	$counselblock_line1 = '';
				$counselblock_line2 = '';
				$counselblock_line3 = '';
				$counselblock_line4 = '';
				$counselblock_line5 = '';
				$counselblock_line6 = '';
				$counselblock_line7 = '';
				$counselblock_line8 = '';
				$counselblock_line9 = '';
				$counselblock_line10 = '';
				if(!empty($meta_obj['COUNSELBLOCK']->LINE)){
					if(is_array($meta_obj['COUNSELBLOCK']->LINE) || is_object($meta_obj['COUNSELBLOCK']->LINE)){
						$counselblock_line1 = str_replace('....XXXX....XXXX....','&',$meta_obj['COUNSELBLOCK']->LINE[0]);
						$counselblock_line2 = str_replace('....XXXX....XXXX....','&',$meta_obj['COUNSELBLOCK']->LINE[1]);
						$counselblock_line3 = str_replace('....XXXX....XXXX....','&',$meta_obj['COUNSELBLOCK']->LINE[2]);
						$counselblock_line4 = str_replace('....XXXX....XXXX....','&',$meta_obj['COUNSELBLOCK']->LINE[3]);
						$counselblock_line5 = str_replace('....XXXX....XXXX....','&',$meta_obj['COUNSELBLOCK']->LINE[4]);
						$counselblock_line6 = str_replace('....XXXX....XXXX....','&',$meta_obj['COUNSELBLOCK']->LINE[5]);
						$counselblock_line7 = str_replace('....XXXX....XXXX....','&',$meta_obj['COUNSELBLOCK']->LINE[6]);
						$counselblock_line8 = str_replace('....XXXX....XXXX....','&',$meta_obj['COUNSELBLOCK']->LINE[7]);
						$counselblock_line9 = str_replace('....XXXX....XXXX....','&',$meta_obj['COUNSELBLOCK']->LINE[8]);
						$counselblock_line10 = str_replace('....XXXX....XXXX....','&',$meta_obj['COUNSELBLOCK']->LINE[9]);
					}
				}
		    ?>
		    <div class="control-group">
		    	<label>LINE</label>
		    	<input type="text" value="<?= $counselblock_line1;?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][LINE][]"/>
		    </div>
		    <div class="control-group">
		    	<label>LINE</label>
		    	<input type="text" value="<?= $counselblock_line2;?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][LINE][]"/>
		    </div>
		    <div class="control-group">
		    	<label>LINE</label>
		    	<input type="text" value="<?= $counselblock_line3;?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][LINE][]"/>
		    </div>
		    <div class="control-group">
		    	<label>LINE</label>
		    	<input type="text" value="<?= $counselblock_line4;?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][LINE][]"/>
		    </div>
		    <div class="control-group">
		    	<label>LINE</label>
		    	<input type="text" value="<?= $counselblock_line5;?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][LINE][]"/>
		    </div>
		    <div class="control-group">
		    	<label>LINE</label>
		    	<input type="text" value="<?= $counselblock_line6;?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][LINE][]"/>
		    </div>
		    <div class="control-group">
		    	<label>LINE</label>
		    	<input type="text" value="<?= $counselblock_line7;?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][LINE][]"/>
		    </div>
		    <div class="control-group">
		    	<label>LINE</label>
		    	<input type="text" value="<?= $counselblock_line8;?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][LINE][]"/>
		    </div>
		    <div class="control-group">
		    	<label>LINE</label>
		    	<input type="text" value="<?= $counselblock_line9;?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][LINE][]"/>
		    </div>
		    <div class="control-group">
		    	<label>LINE</label>
		    	<input type="text" value="<?= $counselblock_line10;?>" class="form-control" onclick="MAPPINGTAGS.getSelection(this);" name="meta_tags[COUNSELBLOCK][LINE][]"/>
		    </div>
		</fieldset>
	</form>
</div>
<div class="col-sm-12">
	<button class="btn btn-primary pull-right" onclick="MAPPINGTAGS.save('save');" id="save_mapping_btn">Save</button>
	<button class="btn btn-success pull-right" onclick="MAPPINGTAGS.save_and_replace();" id="save_mapping_and_replace_btn" style="margin-right: 10px;">Save And Replace XML Meta</button>
</div>