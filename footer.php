</div>
<div class="modal fade" id="modal-default">
  	<div class="modal-dialog">
		<div class="modal-content">
		  	<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  	<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Change Password</h4>
		  	</div>
		  	<form method="post" action="ChangePassword.php">
			  	<div class="box-body">
				 	<div class="form-group">
				  		<label for="inputEmail3" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" id="password"  name="Password"  placeholder="Password">
				  		</div>
					</div>
					<div class="form-group">
				  		<label for="inputPassword3" class="col-sm-2 control-label">Confirm</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" id="confirm_password" name="Confirm" placeholder="Confirm" onkeyup="check();">
				  		</div>
					</div>
					<span id='message'></span>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
					<input type="submit" class="btn btn-primary" id="Button" value="Change Password">
					<input type="hidden" class="btn btn-primary" name="UserName" value="<?php echo $_SESSION['login_user'];?>">
					<input type="hidden" class="btn btn-primary" name="RedirectPage" value="index.php">
				</div>
		  	</form>
		</div>
	</div>
</div>
<div class="modal modal-info fade" id="modal-success">
        <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Profile Picture</h4>
          </div>
          <div class="modal-body">
              <div class="form-group" id="divAttachment">
                <label>Profile Pic</label><br>
                  <input type="file" class="form-control" id="txtAttachFile"  >
              </div>

          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
          <button type="Submit" class="btn btn-outline" data-dismiss="modal" onclick="saveQuery()">Save</button>
          </div>
        </div>
        </div>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="js/jquery-3.4.1.min.js"></script>
<!--<script src="js/jquery-2.js"></script>-->
<script src="js/jquery.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="source/stable/jquery.layout.js"></script>

<script src="js/themeswitchertool.js"></script> 
<script type="text/javascript" src="source/stable/callbacks/jquery.layout.resizePaneAccordions.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
 <script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="form.js"></script>
<script src="node_modules/sweetalert2/dist/sweetalert2.min.js"></script>
<!-- CK Editor -->
<script src="plugins/iCheck/icheck.min.js"></script>
 
<script type="text/javascript" language="JavaScript">

        function SetTextBoxValue($prVal) {
			$('#BatchID').val($prVal);
            $('#txtID').val($prVal);
        }
		function SetTextBoxValue1($prVal) {
			 
            $('#BatchID1').val($prVal);
            $('#tID').val($prVal);
        }
		function SetTextBoxValue2($prVal) {
			$('#BatchID2').val($prVal);
		}

        function SetTextBoxValue3($prVal) {
			$('#BatchID3').val($prVal);
		}

        function checkAll(checkId){
	        var inputs = document.getElementsByTagName("input");
	        for (var i = 0; i < inputs.length; i++) { 
	            if (inputs[i].type == "checkbox" && inputs[i].id == checkId) { 
	                if(inputs[i].checked == true) {
	                    inputs[i].checked = false ;
	                } else if (inputs[i].checked == false ) {
	                    inputs[i].checked = true ;
	                }
	            }  
	        }  
	    }

	    function _getVal(el){
      		return document.getElementById(el);
  		}

		function saveQuery(){
		  var file = _getVal("txtAttachFile").files[0];
		  
		 
		  var formdata = new FormData();
		  formdata.append("txtAttachFile", file);
		  
		  var ajax = new XMLHttpRequest();
		 
		  ajax.open("POST", "saveQuery.php");
		  ajax.send(formdata);
		  setTimeout(LoadContent, 1000);
		  alert("Query is successfully added!");
		}

		function DisplayImageAttachment(){
		        
		    var response=document.getElementById("FileList");
		    //var jTextArea=document.getElementById("jTextArea").value;
		    response.innerHTML=="";
		    prBookID=document.getElementById("BookID").value;
		    var jTextArea = prBookID+"/" +"Images";
		    var data = 'data='+encodeURIComponent(jTextArea);
		    var xmlhttp = new XMLHttpRequest();
		    xmlhttp.onreadystatechange=function(){
		     	if (xmlhttp.readyState==4 && xmlhttp.status==200){
		          response.innerHTML=xmlhttp.responseText;
		        }
		     }
		    xmlhttp.open("POST","ReadFile.php",true);
		            //Must add this request header to XMLHttpRequest request for POST
		    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		      
		    xmlhttp.send(data);
		      
		}
</script>
 
<script src="script.js"></script>
<script src="GoldenGate.js"></script>
<script src="WMSButton.js"></script>
<script src="js/Feedback.js"></script>
<script src="addons/addons.js"></script>
<script src="bower_components/ckeditor/custom-rightclick.js"></script>
<script>
	$(function () {
	    $('#example1').DataTable();
	    $('#example2').DataTable({
	      'paging'      : true,
	      'lengthChange': false,
	      'searching'   : false,
	      'ordering'    : true,
	      'info'        : true,
	      'autoWidth'   : false
	    })
	});
</script>
</body>
</html>
