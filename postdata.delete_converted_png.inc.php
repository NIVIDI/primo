<?php
	
	$files = json_decode($post['ids']);
	if(!empty($files)){
		$response_results = array();
		foreach($files as $file){
			if(file_exists($file->file)){
				
				$temp_dir = pathinfo(pathinfo($file->file,PATHINFO_DIRNAME), PATHINFO_FILENAME);
				$temp_file = pathinfo($file->file,PATHINFO_FILENAME);
				
				if(is_dir($file->file)){
					$temp_path = __DIR__.'/TEMPNI/'.$temp_file;
					if(file_exists($temp_path)){
						rrmdir($temp_path);
					}
				}else{
					$temp_path = __DIR__.'/TEMPNI/'.$temp_dir.'/'.$temp_file.'.png';
					if(file_exists($temp_path)){
						rrmdir($temp_path);
					}
				
				}
				rrmdir($file->file);
				$response_results[] = array("success" => true, "message" => "Successfull deleted ({$file->name})");
			}else{
				$response_results[] = array("success" => false, "message" => "File not exist in server, cannot be deleted ({$file->name})");
			}
		}

		$message = '';
		foreach($response_results as $response){
			$style = "display: block; color: red;";
			if($response['success']){
				$style = "display: block; color: green;";
			}
			$message .= "<li style='{$style}'>{$response['message']}</li>";
		}
		$results = array("success" => true, "message" => "{$message}");
	}else{
		$results = array("success" => false, "message" => "No file selected to delete!");
	}
?>