var Form = {
      validate : function(id){
        var ret = true;
               
        var formInput = $(id+" .form-group .form-control");
         if(typeof formInput != 'undefined'){
            if(formInput.length > 0){
               for(var i = 0; i < formInput.length; i++){
                  if(typeof $(id+' #'+formInput[i].id).attr("req") != 'undefined'){
                     $(id+" #"+formInput[i].id).parents('.form-group').attr("class","form-group");
                     $(id+" #"+formInput[i].id).parents('.form-group').find('small').remove();
                     
                     if(formInput[i].value.trim().length == 0){
                        $(id+" #"+formInput[i].id).parents('.form-group').attr("class","form-group has-error");
                        $(id+" #"+formInput[i].id).parents('.form-group').append('<small class="help-block" style="color:#dd4b39 !important">'+$(id+' #'+formInput[i].id).attr("message")+'</small>');
                        ret = false;
                     }else{
                        if(typeof $(id+" #"+formInput[i].id).attr("type") != 'undefined'){
                           var type =  $(id+" #"+formInput[i].id).attr("type").trim();
                           var vE = $(id+" #"+formInput[i].id).val();
                           if(type == "email" ){
                              var emailRes = Form.isEmail(vE);
                              if(!emailRes){ 
                                 $(id+" #"+formInput[i].id).parents('.form-group').attr("class","form-group has-error");
                                 $(id+" #"+formInput[i].id).parents('.form-group').append('<small class="help-block" style="color:#dd4b39 !important">Invalid Email</small>');
                                 $(id+" #"+formInput[i].id).focus();
                                 ret = false;
                              }
                           }
                        }
                     }
                  }  
               }
            }
         }
         return ret;
       },
      isEmail : function(email){
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
      }
}