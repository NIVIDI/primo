<?php
include "conn.php";

$getUID = !empty($_GET['UID']) ? $_GET['UID'] : '';
$sql="SELECT * FROM tblUser Where ID='{$getUID}'";
$UID='';
$UserName= '';
$Password='';
$Name= '';
$UserType='';

if ($result=mysqli_query($con,$sql))
{
  // Fetch one and one row
  while ($row=mysqli_fetch_row($result))
	{
		$UID=$row[0];
		$UserName=$row[1];
		$Password=$row[2];
		$Name=$row[3];
		$UserType=$row[4];
	}
}

include("header.php");
include("header_nav.php");
include ("sideBar.php");  

?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>User List</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  		      <li><a href="UserList.php"><i class="fa fa-dashboard"></i> User Maintenance</a></li>
            <li class="active">User List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
  
        <?php $sql="SELECT * FROM tblUser";?>
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border"></div>
                    <!-- /.box-header -->
                    <form role="form" method="Post" Action="saveUser.php">
                          <div class="box-body">
                              <div class="col-lg-6">
                                  <div class="form-group">
                                      <label >User Type</label>
                                      <select class="form-control" name="UserType">
  							                          <option value="Admin">Admin</option>
  							                          <option value="Operator">Operator</option>
  							                          <option value="QA">QA</option>
                                  			  <?php 
                                  					  if ($UserType!=''){
                                  								?>
                                  								<option value="<?php echo $UserType;?>" selected><?php echo $UserType;?></option>
                                  								<?php
                                  						}
                                  				?>
  							                       </select>
                                  </div>
  					                      <div class="form-group">
                                      <label>Name</label>
  						                        <input type="text" class="form-control" placeholder="Name" name="Name" value="<?php echo $Name;?>">
                                  </div>
                                  <div class="form-group">
                                      <label>User name</label>
  						                        <input type="text" class="form-control" placeholder="User Name" name="UserName" value="<?php echo $UserName;?>">
                                  </div>
  					                      <div class="form-group">
                                      <label>Password</label>
  						                        <input type="password" class="form-control" placeholder="password" name="password" value="<?php echo $Password;?>">
                                  </div>
  					                      <div class="form-group">
                                      <label>Task</label>
  						                        <table id="example2" class="table table-bordered table-hover">
                                          <thead>
                                              <tr>
  				                                        <th></th>
                                                  <th>Process</th>
                                              </tr>
                                          </thead>
  				                                <tbody>
  				                                <?php
  				                                    $strSQL="SELECT wms_Processes.ProcessId,ProcessCode,Description FROM   wms_WorkFlowProcesses INNER JOIN wms_Processes ON   wms_WorkFlowProcesses.ProcessId= wms_Processes.ProcessId Where WorkflowID=$WorkflowID ORDER BY RefID ASC";
  				                                    $objExec= odbc_exec($conWMS,$strSQL);
  		
  				 
                                  				    while ($row = odbc_fetch_array($objExec)) 
                                  				    {
                                          					$TaskID=$row["ProcessId"];
                                          					$Val='';

                                                    if ($UID==''){
                                                        $Val='';
                                                    }
                                                    else{
                                                        $sql1="SELECT * FROM tblusertask WHERE TaskID='$TaskID' AND UserID='$UID'";
                                             
                                                        if ($result1=mysqli_query($con,$sql1))
                                                        {
                                                          while ($row1=mysqli_fetch_row($result1))
                                                          {
                                                            $Val=$row1[0];
                                                          }
                                                        }
                                                    }
                                      						
                                  			  ?>
            					                              <tr>
            					
                                                        <?php
            				                                        if ($Val==''){
            					                                  ?>
            					                                           <td width="20px"><input type="Checkbox" id="chk" name="chk[]" value="<?php echo $row["ProcessId"];?>"></td>
            				                                    <?php
            				                                        }
            				                                        else{
            					                                  ?>
            					                                           <td width="20px"><input type="Checkbox" id="chk" name="chk[]" value="<?php echo $row["ProcessId"];?>" checked></td>
            					                                  <?php
            				                                        }
            				                                    ?>
                                                                <td><?php echo $row["Description"];?></td> 
            				                                </tr>
  					                             <?php } ?>	 
  				                               </tbody>
                                      </table>
                                  </div>
  					                  </div>
                          </div>
                          <div class="box-footer">
      				                  <input type="hidden" class="form-control" placeholder="" name="UID" value="<?php echo $UID;?>">
      				                  <button type="submit" class="btn btn-primary">Save</button>
      					                <button type="reset" class="btn btn-danger" onclick="location.href='UserList.php'">Cancel</button>
      				            </div>
                    </form>
                </div>
		          </div>
          </div>
    </section>
</div>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>


