<?php
include "conn.php";
include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>

<script src="code/highcharts.js"></script>
<script src="code/highcharts-3d.js"></script>
<script src="code/modules/data.js"></script>
<script src="code/modules/drilldown.js"></script>
<script src="bower_components/ckeditor/4.10.1/ckeditor.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
<?php
$dtTo= empty($_POST['dtTo']) ? '' : $_POST['dtTo'];
$dtFrom= empty($_POST['dtFrom']) ? '' : $_POST['dtFrom'];
 
if ($dtTo==''){
  $dtTo =date("m/d/Y");
}
 
if ($dtFrom==''){
  $dtFrom =date("m/d/Y"); 
}

$prDateFrom = $dtFrom.' 12:00:00 AM';
$prDateTO = $dtTo.' 11:59:59 PM';
$username=$_SESSION['login_user'];
  $NewChapters=  GetWMSValue("Select Count(*) as TotalCount from primo_view_jobs  Where StatusString='New' AND LastUpdate>='$prDateFrom' AND LastUpdate<='$prDateTO' AND ProcessCode='STYLING' and SourceURL<>'test'","TotalCount",$conWMS);
 
  if ($NewChapters==''){
    $NewChapters=0;
  }

   $OnGoing=  GetWMSValue("Select Count(*) as TotalCount from  primo_view_jobs  Where StatusString IN ('Allocated','Started','Ongoing') AND LastUpdate>='$prDateFrom' AND LastUpdate<='$prDateTO' AND ProcessCode='STYLING' And AssignedTo='$username'","TotalCount",$conWMS);
 
  if ($OnGoing==''){
    $OnGoing=0;
  }

   $Hold=  GetWMSValue("Select Count(*) as TotalCount from  primo_view_jobs  Where StatusString='Hold' AND LastUpdate>='$prDateFrom' AND LastUpdate<='$prDateTO' AND ProcessCode='STYLING' And AssignedTo='$username'","TotalCount",$conWMS);
 
  if ($Hold==''){
    $Hold=0;
  }
    $Transmitted=  GetWMSValue("Select Count(*) as TotalCount from primo_view_jobs  Where StatusString='Done' AND LastUpdate>='$prDateFrom' AND LastUpdate<='$prDateTO' AND ProcessCode='STYLING' And AssignedTo='$username'","TotalCount",$conWMS);
 
  if ( $Transmitted==''){
     $Transmitted=0;
  }
?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $NewChapters;?></h3>

              <p>New Batches</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a style="cursor: pointer"  class="small-box-footer" href="ChapterList.php?dtFrom=<?php echo $prDateFrom; ?>&dtTo=<?php echo $prDateTO; ?>&Status=New" onclick="window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbar=yes,resizable=yes,width=950,height=650'); return false;">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo  $OnGoing;?></h3>

              <p>On-Going</p>
            </div>
            <div class="icon">
              <i class="fa  fa-ellipsis-h"></i>
            </div>
             <a style="cursor: pointer"  class="small-box-footer" href="ChapterList.php?dtFrom=<?php echo $prDateFrom; ?>&dtTo=<?php echo $prDateTO; ?>&Status=On-Going" onclick="window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbar=yes,resizable=yes,width=950,height=650'); return false;">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $Hold;?></h3>

              <p>Hold</p>
            </div>
            <div class="icon">
              <i class="fa fa-hand-stop-o "></i>
            </div>
             <a style="cursor: pointer"  class="small-box-footer" href="ChapterList.php?dtFrom=<?php echo $prDateFrom; ?>&dtTo=<?php echo $prDateTO; ?>&Status=Hold" onclick="window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbar=yes,resizable=yes,width=950,height=650'); return false;">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $Transmitted;?></h3>

              <p>Completed</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a style="cursor: pointer"  class="small-box-footer" href="ChapterList.php?dtFrom=<?php echo $prDateFrom; ?>&dtTo=<?php echo $prDateTO; ?>&Status=Done" onclick="window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbar=yes,resizable=yes,width=950,height=650'); return false;">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>



<div class="row">
<?php

$array = explode(',',GenerateGraphData($dtFrom,$dtTo,'New','ADM',$conWMS));
  $iCount=1;
   foreach ($array as $value)
  {
    if ($iCount==1){
      $A1=$value;
    }
    elseif ($iCount==2){
      $A2=$value;
    }
    elseif ($iCount==3){
      $A3=$value;
    }
    $iCount++;
  }
    
  $array = explode(',',GenerateGraphData($dtFrom,$dtTo,'On-Going','ADM',$conWMS));
  $iCount=1;
   foreach ($array as $value)
  {
    if ($iCount==1){
      $A6=$value;
    }
    elseif ($iCount==2){
      $A7=$value;
    }
    elseif ($iCount==3){
      $A8=$value;
    }
    $iCount++;
  }
   
  $array = explode(',',GenerateGraphData($dtFrom,$dtTo,'Hold','ADM',$conWMS));
  $iCount=1;
   foreach ($array as $value)
  {
    if ($iCount==1){
      $A11=$value;
    }
    elseif ($iCount==2){
      $A12=$value;
    }
    elseif ($iCount==3){
      $A13=$value;
    }
    $iCount++;
  }
   
    $array = explode(',',GenerateGraphData($dtFrom,$dtTo,'Done','ADM',$conWMS));
  $iCount=1;
   foreach ($array as $value)
  {
    if ($iCount==1){
      $A16=$value;
    }
    elseif ($iCount==2){
      $A17=$value;
    }
    elseif ($iCount==3){
      $A18=$value;
    }
    $iCount++;
  }
   




?>
        <div class="col-md-9">
          <div class="box box-primary">
            <form method="post">
            <div class="box-header with-border">
		          <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>

                  <input type="date" class="form-control" name="dtFrom" style="width: 200px" value="<?php echo $dtFrom;?>"><input type="date" class="form-control" name="dtTo" style="width: 200px" value="<?php echo $dtTo;?>"><button  class="form-control" style="width: 50px">Go</button>
                </div>
            </div>
            </form>
            <!-- /.box-header -->
          			<div class="box-body">
          				<div id="container"></div>

                 
                  <script type="text/javascript">
                    Highcharts.chart('container', {
                        chart: {
                            type: 'column',
                             options3d: {
                                  enabled: true,
                                  alpha: 15,
                                  beta: 15,
                                  viewDistance: 200,
                                  depth: 40
                              },
                              marginTop: 80,
                              marginRight: 40,
                              height:450
                        },
                        title: {
                            text: 'Batches Summary'
                        },
                         
                        plotOptions: {
                            column: {
                                depth: 25
                            }
                        },
                        xAxis: {
                            categories: ['Batches'],
                            labels: {
                                skew3d: true,
                                style: {
                                    fontSize: '16px'
                                }
                            }
                        },
                        yAxis: {
                            title: {
                                text: null
                            }
                        },
                         series: [{
                              name: 'New',
                              data: [<?php echo $A1;?>],
                              stack: 'male'
                          }, {
                              name: 'On-Going',
                              data: [<?php echo $A6;?>],
                              stack: 'male'
                          }, {
                              name: 'On-Hold',
                              data: [<?php echo $A11;?>],
                              stack: 'male'
                          }, {
                              name: 'Transmitted',
                              data: [<?php echo $A16;?>],
                              stack: 'male'
                          }]
                    });
                        </script> 
                  
            </div>
          
          </div>
          <!-- /. box -->

        </div>
          <div class="col-md-3">
            <div class="box box-solid">
                <div class="box-header">
                  <h3 class="box-title">Today's Performance</h3>
                   <div class="box-tools pull-right">
                  <button type="button" class="btn  btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                </div>
                <div class="box-body">

                  <?php 
                   
                    $Quota=  GetWMSValue("Select Quota from wms_WorkFlowProcesses Where ProcessID=7","Quota",$conWMS);
                    $Quota = empty($Quota) ? 0 : $Quota;
                    
                    $pdtTo =date("m/d/Y");
                 
                    $pdtFrom =date("m/d/Y"); 
                 
                  $pDateFrom = $pdtFrom.' 12:00:00 AM';
                  $pDateTO = $pdtTo.' 11:59:59 PM';
                  $username=$_SESSION['login_user'];

                    $Completed=  GetWMSValue("Select Count(*) as TotalCount from primo_view_jobs  Where StatusString='Done' AND LastUpdate>='$pDateFrom' AND LastUpdate<='$pDateTO' AND ProcessCode='DATAEXTRACTION' And AssignedTo='$username'","TotalCount",$conWMS);
                    $Completed = empty($Completed) ? 0 : $Completed;
                     $Productivity = empty($Quota) ? 0: (($Completed/$Quota)*100);

                  ?>

                    <div class="progress-group">
                      <span class="progress-text">Productivity</span>
                      <span class="progress-number"><b> <a style="cursor: pointer"  class="small-box-footer" href="TATPerformance.php?dtFrom=<?php echo $prDateFrom; ?>&dtTo=<?php echo $prDateTO; ?>&Status=Completed" onclick="window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbar=yes,resizable=yes,width=950,height=650'); return false;"><?php echo $Completed;?></b>/<?php echo $Quota;?></a></span>

                      <div class="progress sm">
                        <div class="progress-bar progress-bar-aqua" style="width: <?php echo  $Productivity;?>%"></div>
                      </div>
                    </div>
                     
                </div>
                  
              </div>
              
               <div class="box box-solid">
                <div class="box-header">
                  <h3 class="box-title">Top Performer</h3>
                   <div class="box-tools pull-right">
                  <button type="button" class="btn  btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                </div>
                <div class="box-body">

                  <?php 
                    
                    
                    
                  // $pdtTo =date("m/d/Y");
                 
                  // $pdtFrom =date("m/d/Y"); 

                  $pdtFrom= empty($_POST['dtTo']) ? date("m/d/Y") : $_POST['dtTo'];
                  $pdtFrom= empty($_POST['dtFrom']) ? date("m/d/Y") : $_POST['dtFrom'];
                  
                  $pDateFrom = $pdtFrom.' 12:00:00 AM';
                  $pDateTO = $pdtTo.' 11:59:59 PM';
                  
                  ?>

                     <ul class="products-list product-list-in-box">
<?php
    $sql ="select Top 5 UserName,EmployeeName, AVG(X3) As X3 from wms_view_ProductivityUserWithX3 WHERE Date>='$pDateFrom' AND Date<='$pDateTO' Group By EmployeeName,UserName ORDER BY X3 DESC";
 
    $rs=odbc_exec($conWMS,$sql);
    $ctr = odbc_num_rows($rs);
    while(odbc_fetch_row($rs))
    {
    
   
?>
                    <li class="item">
                        <div class="product-img">
                          <?php

                          $UserId=odbc_result($rs,"UserName");
                           
                          if (file_exists("images/User/".$UserId.".jpg")) {  

                          ?>

                          <img src="images/User/<?=$UserId;?>.jpg" class="img-circle" alt="">
                          <?php
                        }
                          else{
                          ?>
                          <img src="images/default.jpg" alt=""  class="img-circle">
                          <?php
                          }
                          ?>

                        </div>
                        <div class="product-info">
                          <a href="javascript:void(0)" class="product-title">
                            <span class="label label-info pull-right"><?=odbc_result($rs,"X3");?></span></a>
                          <span class="product-description">
                               <?=odbc_result($rs,"EmployeeName");?>
                              </span>
                        </div>
                      </li>
                     
<?php
 }
    
?>
                    </ul>
                     
                </div>
                  
              </div>
        </div>
        <!-- /.col -->
      </div>

      <!-- /.row -->
    </section>


    <!-- /.content -->
  </div>

<script>

   function _getVal(el){
  return document.getElementById(el);
}

function saveQuery(){
  var file = _getVal("txtAttachFile").files[0];
  
 
  var formdata = new FormData();
  formdata.append("txtAttachFile", file);
  
  var ajax = new XMLHttpRequest();
 
  ajax.open("POST", "saveQuery.php");
  ajax.send(formdata);
  setTimeout(LoadContent, 1000);
  alert("Query is successfully added!");
}

  function DisplayImageAttachment(){
        
      var response=document.getElementById("FileList");
      //var jTextArea=document.getElementById("jTextArea").value;
      response.innerHTML=="";
      prBookID=document.getElementById("BookID").value;
      var jTextArea = prBookID+"/" +"Images";
      var data = 'data='+encodeURIComponent(jTextArea);
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange=function(){
        if (xmlhttp.readyState==4 && xmlhttp.status==200){
          response.innerHTML=xmlhttp.responseText;
        }
      }
      xmlhttp.open("POST","ReadFile.php",true);
            //Must add this request header to XMLHttpRequest request for POST
      xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      
      xmlhttp.send(data);
      
  }
  
</script>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
