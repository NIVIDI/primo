<?php
include "conn.php";

$sql="SELECT * FROM tblUserAccess Where UserID=' $_SESSION[UserID]'";
 
if ($result=mysqli_query($con,$sql))
{
// Fetch one and one row
	while ($row=mysqli_fetch_row($result))
	{
		$ACQUIRE=$row[1];
		$ENRICH=$row[2];
		$DELIVER=$row[3];
		$USER_MAINTENANCE=$row[4];
		$EDITOR_SETTINGS=$row[5];
		$ML_SETTINGS=$row[6];
		$TRANSFORMATION=$row[7];
		$TRANSMISSION=$row[8];
	}
}
include("header.php");
include("header_nav.php");
include ("sideBar.php");

?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>ML Configuration</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">ML Configuration</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><button type="button" class="btn btn-block btn-primary">Add New</button></h3>
                    </div>
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Domain Name</th>
                                    <th>End Point</th>
                                    <th>API Key</th>
                                    <th>Default Key</th>
    				                        <th>Page</th>
    				                        <th>AutoLoad</th>
                                </tr>
                            </thead>
                            <tbody>
    				                <?php
                                $sql="SELECT * FROM tblmlconfig";
            
				                    if ($result=mysqli_query($con,$sql)){
                                        // Fetch one and one row
                                        while ($row=mysqli_fetch_row($result)){
                                            echo '<tr>
                                                      <td>'.$row[0].'</td>
                                                      <td>'.$row[1].'</td>
                                                      <td>'.$row[6].'</td>
                                                      <td>'.$row[3].'</td>
                                                      <td>'.$row[4].'</td>
                              			                  <td>'.$row[2].'</td>
                              				                <td>'.$row[5].'</td>
                                              </tr>';
                                        }
	                               }
                            ?>	  
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Domain Name</th>
                                    <th>End Point</th>
                                    <th>API Key</th>
                                    <th>Default Key</th>
                				            <th>AutoLoad</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
 
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>