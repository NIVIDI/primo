<?php
	$plugin = $post['plugin'];
	$selected_value = $post['selected_value'];
	$xml_path = $post['xml_path'];
	if(!empty($plugin)){
		$exe_path = realpath(__DIR__.'/..');
        $_dir = new DirectoryIterator($exe_path.'/Plugins/'.$plugin);
        $exe_file = '';
		foreach ($_dir as $fileinfo) {
            if (!$fileinfo->isDot()) {
            	//find exe file
            	if(strtoupper(pathinfo($fileinfo->getFilename(), PATHINFO_EXTENSION)) == 'EXE'){
            		$exe_file = $fileinfo->getFilename();
            		break;
            	}
            }
        }
        
        if(empty($exe_file)){
        	$results = array("success" => false, "message" => "Error: Plugin exe file not found!");
        	echo json_encode($results);
        	exit;
        }
        $exe = $exe_path.'/Plugins/'.$plugin.'/'.$exe_file;
        
		if(empty($selected_value) && file_exists($xml_path)){
        	$selected_value = file_get_contents($xml_path);
        }
        
		if(empty($selected_value)){
        	$results = array("success" => false, "message" => "Error: error reading xml value, please contact system administrator!");
        }else{
        	$temp_file_path = __DIR__.'/TEMP/'.$_SESSION['UserID'];
            if(!file_exists($temp_file_path)){
                mkdir($temp_file_path, 0777, true);
            }

            $temp_file = $temp_file_path.'/'. $plugin . '.xml';
            if(file_exists($temp_file)){
            	unlink($temp_file);
            }

          	$file = fopen($temp_file, 'w+');
          	fputs($file, $selected_value);
          	fclose($file);

          	$sreplace_cmd = '"'.$exe.'" "'.$temp_file.'"';
			exec($sreplace_cmd);

			$data = file_get_contents($temp_file);

			unlink($temp_file);
			$results = array("success" => true, "message" => "Sucessfully process plugin", "data" => $data);
        }

	}else{
		$results = array("success" => false, "message" => "Error: plugin cannot be empty!");
	}
?>