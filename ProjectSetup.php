<?php
include "conn.php";
include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Project Setup</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Project Setup</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
  
            <?php $sql="SELECT * FROM wms_Projects"; ?>
            <div class="col-md-12">
                <div class="box box-primary">
                      <div class="box-header with-border">
                          <h3 class="box-title pull-left">
                              <button type="button" class="btn btn-block btn-primary"  onclick="location.href='addnew_project.php'">Add New</button>
                          </h3>
                          <h3 class="box-title pull-right">
                                <button type="button" class="btn btn-block btn-success" onclick="VERSION.show();">Version</button>
                          </h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                          <table id="example2" class="table table-bordered table-hover">
                              <thead>
                                  <tr>
                  					          <th>Project ID</th>
                                      <th>Project Code</th>
                                      <th>Description</th>
                                      <th>Time Zone</th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php
                                  		$rs=odbc_exec($conWMS,$sql);
                                      $ctr = odbc_num_rows($rs);
                                      while(odbc_fetch_row($rs))
                                      {
                                  ?>
                                          <tr>
                                            <td><?php echo odbc_result($rs,"ProjectID");?></td>
                                            <td><?php echo odbc_result($rs,"ProjectCode");?></td>
                          				          <td><?php echo odbc_result($rs,"Description");?></td>
                                            <td><?php echo odbc_result($rs,"TimeZone");?></td>
                                            <td> 
              				                          <button type="button" class="btn btn-xs btn-info"  onclick="location.href='addnew_project.php?UID=<?php echo  odbc_result($rs,"ProjectID");?>&TransType=Update'">Update</button>
              				                          <button class="btn btn-xs btn-danger"  data-toggle="modal" data-target="#modal-danger" onclick="Javascript:SetTextBoxValue(<?php echo odbc_result($rs,"ProjectID");?>)">Delete</button>
                                            </td>
                                          </tr>
                                  <?php } ?>	  
                              </tbody>
                          </table>
                      </div>
                  
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="version_modal">
    <div class="modal-dialog" style="width: 50%">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title">Primo TPCPP Version</h4>
          </div>
          <div class="modal-body">
                <form role="form" id="version_form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Version</label>
                        <input type="text" value= "<?= $version;?>" name="project_version" id="project_version" class="form-control" req="true" message="Version is required">
                    </div>
                </form>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-primary pull-right" id="submit_project_version" value="Submit">
          </div>
      </div>
    </div>
</div>
  
<form method="GET" action="saveProject.php">

    <div class="modal modal-danger fade" id="modal-danger">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Record Deletion</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete this record?</p>
            <input type="hidden" name="txtID" id="txtID" />
            <input type="hidden" name="TransType" id="TransType" value="Delete" />
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>

            <button type="submit" class="btn btn-outline">Ok</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
</form>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
<script type="text/javascript">
    var VERSION = {
        show : function(){
            $('#version_modal').modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        submit : function(){
            var data = $('#version_form').serializeArray();
            data.push({name: 'action', value: 'submit_version'});

            Page.loading('Updating ...');

            $.post('postdata.php', data, function(response){
                Swal.close();
                try{
                    var result = JSON.parse(response);
                    if(result.success){
                        Page.success(result.message);
                    }else{
                        Page.error(result.message);
                    }
                }catch(e){
                    Page.error(e);
                }
            });
        }
    }

    $('#submit_project_version').click(function(){
        if(Form.validate('#version_form')){
            VERSION.submit();
        }
    });
</script>