<?php
	$bundle_name = $post['bundle'];
	
	if(!empty($bundle_name)){
		$process = $post['process'];
		switch($process){
			case 'generate':
					$latest_no = generateSequenceNumber($bundle_name);

					$dollar_name = generateDollarFileName($bundle_name);
					//process dollar file generation
					$content = generateDollarFile($bundle_name);

					$results = array("success" => true, "message" => "success", "sequence_number" => (empty($latest_no) ? '(Empty)': $latest_no), "dollar_content" => $content, "dollar_name" => $dollar_name);
				break;
			case 'update_bundle_sequence':
					$sequence_no = trim($post["sequence_no"]);
					if(empty($sequence_no)){
						$results = array("success" => false, "message" => "Sequence No. cannot be empty");
					}else{
						//check bundle if not yet delivered
						$sql = "SELECT * FROM files_for_batching WHERE prec_bundle = '{$bundle_name}' LIMIT 1";
						$bundle_data = mysqli_query($con, $sql)->fetch_assoc();
						if($bundle_data['status'] != 'Completed'){
							//check if sequence already exist
							$sql = "SELECT COUNT(*) as total_sequence, bundle_name FROM  bundle_sequence_number WHERE bundle_name <> '{$bundle_name}' AND sequence_no = '{$sequence_no}' LIMIT 1";
							$check_result = mysqli_query($con, $sql)->fetch_assoc();
							if($check_result['total_sequence']){
								$results = array("success" => false, "message" => "Sequence({$sequence_no}) already belong to bundle({$check_result['bundle_name']})");
							}else{
								$sql_update = "UPDATE bundle_sequence_number SET `sequence_no` = '{$sequence_no}' WHERE bundle_name = '{$bundle_name}' ";
								ExecuteQuery($sql_update, $con);
								$results = array("success" => true, "message" => "Successfully updated sequence no.");
							}
						}else{
							$results = array("success" => false, "message" => "Cannot update sequence({$sequence_no}) because bundle({$bundle_name}) already delivered");
						}
					}
				break;

			default:
					$results = array("success" => false, "message" => "Error cannot find dollar file process");
				break;
		}
			
	}else{
		$results = array("success" => false, "message" => "Error cannot find specified bundle name");
	}
?>