<?php
include "conn.php";
include("header.php");
include("header_nav.php");
include ("sideBar.php");

$bundle = !empty($_GET['bundle']) ? $_GET['bundle'] : '';
$where_sql = 'WHERE 1';

if(!empty($bundle)){
    $where_sql .= " AND a.prec_bundle = '{$bundle}' ";
}

$status_filter = !empty($_GET['status']) ? $_GET['status'] : '';
if(!empty($status_filter)){
	if($_SESSION['UserType'] == 'QA'){
		$where_sql .= " AND a.status = 'QA' ";
		$status_filter = 'QA';
	}else{
    	$where_sql .= " AND a.status = '{$status_filter}' ";
    }
}


?>
<link rel="stylesheet" href="lib/codemirror.css">
<script src="lib/codemirror.js"></script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Bundled Files</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Bundled Files</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box box-primary">
                 	<div class="box-header with-border"></div>
                    <div class="box-body">
                        <div class="col-sm-12">
                        	<div class="row"><h3>Lists of Compiled Bundle Files</h3></div>
                        	<div class="row" style="margin-bottom: 30px;padding-top: 30px;">
	                              <form class="form-inline" action="compiledfiles.php">
	                                  <input type="hidden" name="page" value="compile">
	                                  <div class="form-group">
	                                        <label for="filter_bundle">Bundle:</label>
	                                        <select name="bundle" id="bundle" class="form-control">
	                                            <option value="">Please Select</option>
	                                            <?php
	                                                $sql = "SELECT * FROM files_for_batching GROUP BY prec_bundle";
	                                                $sql_results = ExecuteMySql($con, $sql);
	                                                if($sql_results->num_rows){
	                                                    while($row = $sql_results->fetch_assoc()){
	                                                        $selected = '';
	                                                        if($bundle == $row['prec_bundle']){
	                                                            $selected = "selected";
	                                                        }
	                                                        echo "<option {$selected} value='{$row['prec_bundle']}'>{$row['prec_bundle']}</option>";
	                                                    }
	                                                }
	                                            ?>
	                                        </select>
	                                  </div>
	                                  <div class="form-group" style="margin-left: 25px;">
	                                        <label for="filter_status">Status:</label>
	                                        <select name="status" id="status" class="form-control">
	                                            <option value="">Please Select</option>
	                                            <?php
		                                            $type = mysqli_query($con, "SHOW COLUMNS FROM files_for_batching WHERE Field = 'status'")->fetch_assoc();
		                                            preg_match("/^enum\(\'(.*)\'\)$/", $type['Type'], $matches);
												    $enum = explode("','", $matches[1]);
												    foreach($enum as $value){
                                                        $selected = '';
                                                        if($status_filter == $value){
                                                            $selected = "selected";
                                                        }
                                                        echo "<option {$selected} value='{$value}'>{$value}</option>";
	                                                }
	                                            ?>
	                                        </select>
	                                  </div>
	                                  <button type="filter_bundle" class="btn btn-primary">Filter</button>
	                              </form>
	                        </div>
                        	<div class="row">
	                            <div class="table-responsive">
	                                <table id="example2" class="table table-bordered table-hover table-striped">
	                                    <thead>
	                                        <tr>
	                                            <th>Bundle</th>
	                                            <th>Precship Date</th>
	                                            <th>No. of Files</th>
	                                            <th>Reference Code</th>
	                                            <th>TAT <small class="text-muted">(Days)</small></th>
	                                            <th>File Status</th>
	                                            <th>QA Status</th>
	                                            <th width="15%"></th>
	                                        </tr>
	                                    </thead>
	                                    <tbody>
	                                    	<?php
	                                    		$sql = "SELECT a.*, COUNT(a.file_name) as total_files, SUM(a.tat) as total_tat, a.tat , b.bundle as qa_bundle, ifnull(b.status, '') as qa_status

	                                    			FROM files_for_batching a 
	                                    			LEFT JOIN qa_review b ON b.bundle = a.prec_bundle

	                                    			{$where_sql} 
	                                    			GROUP BY a.prec_bundle ORDER BY a.prec_bundle DESC";

	                                    		$sql_result = mysqli_query($con, $sql);
	                                    		if($sql_result->num_rows){
	                                    			while($row = $sql_result->fetch_assoc()){
	                                    				$numofdocs = 0;
		                                                $sql2 = "SELECT * FROM segregated_files WHERE prec_bundle = '{$row['prec_bundle']}' ";
		                                                $result = mysqli_query($con, $sql2);
		                                                $total_done = 0;
		                                                if($result->num_rows){
		                                                    while($row1 = $result->fetch_assoc()){
		                                                          $numofdocs ++;
		                                                          if(!empty($row1['wms_job_id'])){
		                                                              $wms_status = GetWMSValue("SELECT StatusString FROM primo_view_Jobs WHERE JobId = '{$row1['wms_job_id']}' AND ProcessCode = 'QC' ","StatusString",$conWMS);
		                                                              if(strtolower($wms_status) == 'done'){
		                                                                  $total_done ++;
		                                                              }
		                                                          }
		                                                    }
		                                                }

		                                                $deliver_btn = '';
	                                      					
		                                                $status = "<label class='text-muted'><i>{$total_done} out of {$numofdocs} completed</i></label>";

		                                                $qa_status = !empty($row['qa_status']) ? $row['qa_status'] : 'Ongoing QA Validation';

		                                                if(empty($row['qa_bundle'])){
		                                                	$qa_status = '';
		                                                }

		                                                $for_qa_btn = '';
		                                                if($total_done == $numofdocs && $_SESSION['UserType'] != 'QA'){
		                                                	$for_qa_btn = "<a href='javascript:void(0);' bundle='{$row['prec_bundle']}' file_id = '{$row['id']}' onclick='Compiled.sendToQA(this);' title='Send Notification to QA' class='btn btn-info btn-sm'>
	                                          								<span class='glyphicon glyphicon-envelope'></span>
	                                          								Send to QA
	                                      								</a>";
		                                                }

		                                                if($row['qa_status'] == 'Passed' && $_SESSION['UserType'] != 'QA'){
		                                                	$deliver_btn = "<a href='javascript:void(0);' bundle='{$row['prec_bundle']}' file_id = '{$row['id']}' onclick='Compiled.deliver(this);' title='Deliver Files under this bundle' class='btn btn-success btn-sm'>
	                                          								<span class='glyphicon glyphicon glyphicon-list-alt'></span>
	                                          								Deliver
	                                      								</a>";
		                                                }

	                                      				if($row['status'] == 'Completed'){
	                                      					$status = "<label class='text-muted'><i>{$total_done} out of {$numofdocs} completed and delivered</i></label>";
		                                               	}

		                                               	$qa_btn = '';
		                                               	if($_SESSION['UserType'] == 'QA'){
		                                               		$qa_btn = "<a href='javascript:void(0);' filename='{$row['file_name']}' bundle='{$row['prec_bundle']}' file_id = '{$row['id']}' onclick='Compiled.qaUpdate(this);' title='Update status Passed/Failed' class='btn btn-success btn-sm'>
                                                                  <span class='glyphicon glyphicon glyphicon-list-alt'></span>
                                                                  Update
                                                              </a>";
                                                		}

		                                               	echo "<tr>
	                                    							<td>{$row['prec_bundle']}</td>
	                                    							<td>{$row['precship_date']}</td>
	                                    							<td>{$row['total_files']}</td>
	                                    							<td>{$row['reference_code']}</td>
	                                    							<td>{$row['tat']}</td>
	                                    							<td><small class='text-muted'><i>{$status}</i></small></td>
	                                    							<td><small class='text-muted'><i>{$qa_status}</i></small></td>
	                                    							<td style='text-align:center'>
	                                    								<a href='javascript:void(0);' bundle='{$row['prec_bundle']}' file_id = '{$row['id']}' onclick='Compiled.view(this);' title='View Details' class='btn btn-warning btn-sm'>
	                                          								<span class='glyphicon glyphicon-eye-open'></span>
	                                          								View
	                                      								</a>
	                                      								{$for_qa_btn}
	                                      								{$deliver_btn}
	                                      								{$qa_btn}
	                                      							</td>
	                                    					</tr>";
	                                    			}
	                                    		}
	                                    	?>
	                                    </tbody>
	                                </table>
	                            </div>
	                        </div>
                        </div>
                    </div>
                    <div class="box-footer"></div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="view_bundle_details_modal">
    <div class="modal-dialog" style="width:50%;">
      	<div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	            </button>
	            <h4 class="modal-title">Bundle File</h4>
	        </div>
	        <div class="modal-body">
	            <div class="col-sm-12">
	             	<div class="nav-tabs-custom" style="margin-top: 25px;">
            			<ul class="nav nav-tabs">
              				<li class="active"><a href="#source_tab" class="tab_bundle" type="source" onclick="Compiled.tab(this)" data-toggle="tab" aria-expanded="true">Sources</a></li>
              				<li class=""><a href="#deliverables_tab" class="tab_bundle" type="deliverables" data-toggle="tab" onclick="Compiled.tab(this)" aria-expanded="true">Deliverables</a></li>
              				<?php
              					if($_SESSION['UserType'] != 'QA'){
              						echo '<li class=""><a href="#dollar_tab" class="tab_bundle" data-toggle="tab" type="dollar" onclick="Compiled.tab(this)" aria-expanded="true">Dollar File</a></li>';
              					}
              				?>
              				
              			</ul>
            			<div class="tab-content">
				            <div class="tab-pane active" id="source_tab">
			            		<div class="table-responsive">
				            		<table class="table table-responsive table-bordered table-striped">
				            			<thead>
				            				<th>ID</th>
				            				<th>Filename</th>
				            				<th>Size</th>
				            				<th>TAT</th>
				            				<th>Total Docs</th>
				            			</thead>
				            			<tbody id="tbody_sources">
				            				
				            			</tbody>
				            		</table>	
				            	</div>
				            </div>
				            <div class="tab-pane" id="deliverables_tab">
				                <div class="table-responsive" style="max-height: 700px; overflow-y:scroll;">
				            		<table class="table table-responsive table-bordered table-striped">
				            			<thead>
				            				<th>Filename</th>
				            				<th>Size</th>
				            			</thead>
				            			<tbody id="tbody_deliverables">
				            				
				            			</tbody>
				            		</table>	
				            	</div>
				            </div>
				            <div class="tab-pane" id="dollar_tab">
				            	<div class="row" id="dollar_file_loading" style="display:none; text-align: center;">
				            		<small class="muted"><i>Please wait while fetching data!</i></small>
				            	</div>
				            	<div class="row" style="margin-top: 10px">
				            		<h5>Dollar File Sequence No.: <span id="latest_sequence_no"><strong></strong><span></h5>
				            	</div>
				            	<div class="row" style="margin-top: 10px">
				            		<h5>View File: <span id="view_dollar_file"><span></h5>
				            	</div>
				            	<div class="row" style="margin-top: 20px">

				            		<h4><strong>Dollar File Content</strong></h4>
				            		<hr/>
				                	<textarea rows="30" style="width: 100%" id="dollar_content" name="dollar_content"></textarea>
				                </div>
				            </div>
              			</div>
					</div>
				</div>
	        </div>
	        <div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
	        </div>
      	</div>
  	</div>
</div>

<div class="modal fade" id="view_bundle_sequence">
    <div class="modal-dialog">
      	<div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	            </button>
	            <h4 class="modal-title">Bundle Sequence</h4>
	        </div>
	        <div class="modal-body">
	            <form role="form" id="bundle_sequence_form">
	            	<input type="hidden" name="bundle" id="bundle_name" class="form-control">
                    <div class="form-group">
                        <label>Sequence No.</label>
                        <input type="number" name="sequence_no" id="sequence_no" class="form-control" req="true" message="Sequence No. is required">
                    </div>
                </form>
	        </div>
	        <div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              	<input type="submit" class="btn btn-primary" id="submit_bundle_sequence" value="Update">
	        </div>
      	</div>
  	</div>
</div>

<div class="modal fade" id="qa_update_status_modal">
    <div class="modal-dialog">
      	<div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	            </button>
	            <h4 class="modal-title">Update Bundle Status</h4>
	        </div>
	        <div class="modal-body">
	            <form role="form" id="qa_update_status_form">
	            	<input type="hidden" name="bundle" id="bundle_name_qa_status" class="form-control">
                    <div class="form-group">
                        <label>Status</label>
                        <select name="qa_status" id="qa_status" class="form-control" req="true" message="Status is required">
                        	<option value="Passed">Passed</option>
                        	<option value="Failed">Failed</option>
                        </select>
                    </div>
                    <div class="form-group">
                    	<label>Files</label>
                        <input type="file" class="form-control" id="qa_files" name="qa_files[]" multiple="multiple" req="true" message="Files is required">
                    </div>
                </form>
	        </div>
	        <div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              	<input type="submit" class="btn btn-primary" id="submit_qa_update_status" value="Submit">
	        </div>
      	</div>
  	</div>
</div>

<div class="modal fade" id="send_to_QA_modal">
    <div class="modal-dialog">
      	<div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	            </button>
	            <h4 class="modal-title">Notify QA</h4>
	        </div>
	        <div class="modal-body">
	            <form role="form" id="sendToQAForm">
	            	<input type="hidden" name="bundle" id="bundle_name_qa" class="form-control">
                    <div class="form-group">
                        <label>To</label>
                        <input type="text" name="email_to" id="email_to" class="form-control" req="true" message="Mail To is required">
                    </div>
                    <div class="form-group">
                        <label>CC (<span class="text-muted">comma separated</span>)</label>
                        <input type="text" name="email_cc" id="email_cc" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Message</label>
                        <textarea rows="10" style="width: 100%" name="email_msg" id="email_msg" class="form-control" req="true" message="Message is required"></textarea>
                    </div>
                </form>
	        </div>
	        <div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              	<input type="submit" class="btn btn-primary" id="submit_send_to_qa_email" value="Send">
	        </div>
      	</div>
  	</div>
</div>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
<script type="text/javascript">
	var prToggle=1;
	
	var te_html = document.getElementById("dollar_content");						
	var editor_html = CodeMirror.fromTextArea(te_html, {
		mode: "text/xml",
		lineNumbers: true,
		matchTags: {bothTags: true},
		lineWrapping: true,
		extraKeys: {"Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); }},
		foldGutter: true,
		styleActiveLine: true,
		styleActiveSelected: true,
		styleSelectedText: true,
		autoRefresh: true,
		indentUnit: 4,
		indentWithTabs: true,
		readOnly: true,
		smartIndent: true,

		gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
	});

	editor_html.on ('beforeChange',function(){
		DisableTag(prToggle);
	});

	editor_html.refresh();
	editor_html.setSize("100%","65vh");

	$('#submit_bundle_sequence').click(function(){
		if(Form.validate('#bundle_sequence_form')){
			Compiled.submitBundleSequence();
		}
	});

	$('#submit_send_to_qa_email').click(function(){
		if(Form.validate('#sendToQAForm')){
			Compiled.submitSendToQA();
		}
	});

	$('#submit_qa_update_status').click(function(){
		if(Form.validate('#qa_update_status_form')){
			Compiled.submitQAStatus();
		}
	});

	var Compiled = {
		view: function(elem){
			var id = $(elem).attr('file_id');
			var bundle = $(elem).attr('bundle');

			$('#view_bundle_details_modal .modal-title').html('Bundle File ('+bundle+')');
			$('#view_bundle_details_modal').modal({
	            backdrop: 'static',
	            keyboard: false
	        });

			$('.tab_bundle').attr('bundle', bundle);
	        Compiled.getSourcesFiles(bundle);
			Compiled.getDeliverableFiles(bundle);
			Compiled.getDollarFiles(bundle);
		},
		qaUpdate : function(el){
			var bundle = $(el).attr('bundle');
			$('#qa_update_status_modal .modal-title').html('Update Bundle Status of ('+bundle+')');
			$('#bundle_name_qa_status').val(bundle);
			$('#qa_update_status_modal').modal({
	            backdrop: 'static',
	            keyboard: false
	        });
		},
		submitQAStatus : function(){
			var formdata = $('#qa_update_status_form').serializeArray();
			var data = new FormData();  
			data.append('action', 'qa_update_bundle_status');

			$.each(formdata, function(name, obj){
				data.append(obj.name, obj.value);
			});

			var files = $("#qa_files").get(0).files;
            
            for (var i = 0; i < files.length; i++) {
                data.append("files[]", files[i]);
            }

            Page.loading("Processing ...");
            $.ajax({
                  url : 'postdata.php',
                  type : 'POST',
                  data : data,
                  processData: false,  // tell jQuery not to process the data
                  contentType: false,  // tell jQuery not to set contentType
                  success : function(data) {
                       Swal.close();
                       try{ 
                          var res = JSON.parse(data);
                          if(res.success){
                               Swal.fire({
					                allowOutsideClick: false,
					                title: '<strong>Bundle Updated Status</strong>',
					                icon: 'info',
					                html: '<strong>'+res.message+'</strong>',
					                showCloseButton: false,
					                showCancelButton: false,
					                focusConfirm: false,
					                confirmButtonText:'<i class="fa fa-thumbs-up"></i> OK',
					            }).then((result) => {
					                if (result.isConfirmed) {
					                    location.reload();
					                }
					            });
                          }else{
                               Page.error(res.message);
                          }
                       }catch(e){
                          Page.error(e);
                       }
                  },
                  error: function (err) {
                      Swal.close();
                      Page.error(err);
                  }
            });
		},
		deliver : function(el){
			var bundle = $(el).attr('bundle');
			Swal.fire({
                title: "Are you sure want to deliver bundle ("+bundle+")?",
                html: '<strong>It will send dollar file and bundle zip to PD and will set the bundle status to completed</strong>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                allowOutsideClick: false
            }).then((result) => {
                if (result.isConfirmed) {
                    Compiled.submitDeliver(bundle);
                }
            })
		},
		sendToQA : function(el){
			var bundle = $(el).attr('bundle');
			Swal.fire({
                title: "Are you sure want to send bundle ("+bundle+") for QA?",
                html: '<strong>It will notify the QA that the selected bundle is ready for checking.</strong>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                allowOutsideClick: false
            }).then((result) => {
                if (result.isConfirmed) {
                    Compiled.showQAForm(bundle);
                }
            })
		},
		showQAForm : function(bundle){
			$('#bundle_name_qa').val(bundle);
			$('#email_to').val('');
			$('#email_cc').val('');
			$('#email_msg').val('');
			$('#send_to_QA_modal').modal({
	            backdrop: 'static',
	            keyboard: false
	        });
		},
		submitSendToQA : function(){
			var data = $('#sendToQAForm').serializeArray();
			data.push({name: 'action', value: 'send_to_qa'});

			Page.loading('Processing...');
			$.post('postdata.php', data, function(response){
				Swal.close();
				try{
					var result = JSON.parse(response);
					if(result.success){
						Swal.fire({
			                allowOutsideClick: false,
			                title: '<strong>Notification to QA</strong>',
			                icon: 'info',
			                html: result.message,
			                showCloseButton: false,
			                showCancelButton: false,
			                focusConfirm: false,
			                confirmButtonText:'<i class="fa fa-thumbs-up"></i> OK',
			            }).then((result) => {
			                if (result.isConfirmed) {
			                    location.reload();
			                }
			            })
					}else{
						Page.error(result.message);
					}
				}catch(e){
					Page.error(e);
				}
			});
		},
		submitDeliver : function(bundle){
			var data = {"action": 'deliver', "bundle" : bundle};

			Page.loading('Processing...');

			$.post('postdata.php', data, function(response){
				Swal.close();
				try{
					var result = JSON.parse(response);
					if(result.success){
						Swal.fire({
			                allowOutsideClick: false,
			                title: '<strong>Delivering Bundle Result</strong>',
			                icon: 'info',
			                html: result.message,
			                showCloseButton: false,
			                showCancelButton: false,
			                focusConfirm: false,
			                confirmButtonText:'<i class="fa fa-thumbs-up"></i> OK',
			            }).then((result) => {
			                if (result.isConfirmed) {
			                    location.reload();
			                }
			            })
					}else{
						Page.error(result.message);
					}
				}catch(e){
					Page.error(e);
				}
			});	
		},
		tab : function(elem){
			var bundle = $(elem).attr('bundle');
			var type = $(elem).attr('type')
			if(type == 'source'){
				Compiled.getSourcesFiles(bundle);
			}

			if(type == 'deliverables'){
				Compiled.getDeliverableFiles(bundle);
			}

			if(type == 'dollar'){
				Compiled.getDollarFiles(bundle);
			}
		},
		getDeliverableFiles : function(bundle){
			var dir = 'uploadfiles/SourceFiles/'+bundle+'/deliverables';
			var data = [{name:'action', value: 'fetch_dl'}, {name:'dir', value: dir}];

			$('#tbody_deliverables').html("<tr><td colspan='2' style='text-align:center'><i>Please wait while fetching data...</i></td></tr>");
			$.post('postdata.php', data, function(response){
				try{
					var result = JSON.parse(response);
					if(result.success){
						if(result.data.length > 0){
							$('#tbody_deliverables').html('');
							$.each(result.data, function(i,v){
								var _href = "<a href='<?= $base_url;?>/uploadfiles/SourceFiles/"+bundle+"/deliverables/"+v.filename+"' target='_blank'>"+v.filename+"</a>";
								var pathinfo = v.filename.split('.');
								if(typeof pathinfo[1] != 'undefined'){
									if(pathinfo[1] == 'key'){
										_href = "<a href='<?= $base_url;?>/xml_viewer.php?url=uploadfiles/SourceFiles/"+bundle+"/deliverables/"+v.filename+"' target='_blank'>"+v.filename+"</a>";
									}
								}
								$('#tbody_deliverables').append(
									$('<tr>').append(
										$('<td>').html(_href),
										$('<td>').html(((v.size /1000).toFixed(2))+" KB"),
									)
								);
							});
						}else{
							$('#tbody_deliverables').html("<tr><td colspan='2' style='text-align:center; color:red;'>No Files Available</td></tr>");	
						}
					}else{
						$('#tbody_deliverables').html("<tr><td colspan='2' style='text-align:center; color:red;'>"+result.message+"</td></tr>");
					}
				}catch(e){
					$('#tbody_deliverables').html("<tr><td colspan='2' style='text-align:center; color:red;'>"+e+"</td></tr>");
				}
			});
		},
		getDollarFiles : function(bundle){
			var data = {"action": 'dollar', "bundle": bundle, 'process':'generate'};

			$('#dollar_file_loading').css({display: ''}).html('<small class="muted"><i>Please wait while fetching data!</i></small>');
			editor_html.setValue("");
			$('#latest_sequence_no').html('');
			$('#view_dollar_file').html('');
						
			$.post('postdata.php', data, function(response){
				try{
					var result = JSON.parse(response);
					if(result.success){
						$('#dollar_file_loading').css({display: 'none'});
						$('#view_dollar_file').html('<a  target="_blank" href="<?= $base_url?>/uploadfiles/SourceFiles/'+bundle+'/'+result.dollar_name+'.$$$">'+result.dollar_name+'.$$$</a>');
						$('#latest_sequence_no').html("<strong><a href='javascript:void(0)' sequence_no = '"+result.sequence_number+"' bundle='"+bundle+"' onclick='Compiled.viewDollarSequence(this);'>"+result.sequence_number+"</a></strong>");

						editor_html.setValue(result.dollar_content);
						//$('#dollar_content').val(result.dollar_content);
					}else{
						$('#dollar_file_loading').css({display: '',color:'red'}).html('<small>'+result.message+'</small>');
					}
				}catch(e){
					$('#dollar_file_loading').css({display: '',color:'red'}).html('<small>'+e+'</small>');
				}
			});
		},		
		viewDollarSequence : function(el){
			var bundle = $(el).attr('bundle');
			var sequence_no = $(el).attr('sequence_no');
			$('#sequence_no').val(sequence_no);
			$('#bundle_name').val(bundle);
			$('#view_bundle_sequence').modal({
	            backdrop: 'static',
	            keyboard: false
	        });
		},
		submitBundleSequence : function(){
			var data = $('#bundle_sequence_form').serializeArray();
			data.push({name: 'action', value: 'dollar'}, {name:'process', value: 'update_bundle_sequence'})
			
			Page.loading('Processing...');

			$.post('postdata.php', data, function(response){
				Swal.close();
				try{
					var result = JSON.parse(response);
					if(result.success){
						$('#view_bundle_sequence').modal('hide');
						Page.success(result.message);
						Compiled.getDollarFiles($('#bundle_name').val());
					}else{
						Page.error(result.message);
					}
				}catch(e){
					Page.error(e);
				}
			});
			
		},
		getSourcesFiles : function(bundle){
			var data = [{name:'action', value: 'get-source-files'}, {name:'bundle', value: bundle}];

			$('#tbody_sources').html("<tr><td colspan='5' style='text-align:center'><i>Please wait while fetching data...</i></td></tr>");
			$.post('postdata.php', data, function(response){
				try{
					var result = JSON.parse(response);
					if(result.success){
						if(result.data.length > 0){
							$('#tbody_sources').html('');
							$.each(result.data, function(i,v){
								var _hrefid = "<a href='<?= $base_url;?>/for_batching_view.php?page=Acquire&id="+v.id+"' target='_blank'>"+v.id+"</a>";
								var _href = "<a href='<?= $base_url;?>/uploadfiles/SourceFiles/"+bundle+"/"+v.file_name+"' target='_blank'>"+v.file_name+"</a>";
								$('#tbody_sources').append(
									$('<tr>').append(
										$('<td>').html(_hrefid),
										$('<td>').html(_href),
										$('<td>').html(v.size +' KB'),
										$('<td>').html(v.tat),
										$('<td>').html(v.total_docs)
									)
								);
							});
						}else{
							$('#tbody_sources').html("<tr><td colspan='5' style='text-align:center; color:red;'>No Files Available</td></tr>");	
						}
					}else{
						$('#tbody_sources').html("<tr><td colspan='5' style='text-align:center; color:red;'>"+result.message+"</td></tr>");
					}
				}catch(e){
					$('#tbody_sources').html("<tr><td colspan='5' style='text-align:center; color:red;'>"+e+"</td></tr>");
				}
			});
		}
	}
</script>