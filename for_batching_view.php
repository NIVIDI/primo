<?php
include "conn.php";
$id = empty($_GET['id']) ? '': $_GET['id'];
$sql = "SELECT * FROM files_for_batching WHERE id = '{$id}' ";
$file_forbatch_result = mysqli_query($con, $sql)->fetch_assoc();

$bundle_dir = !empty($file_forbatch_result['prec_bundle']) ? $file_forbatch_result['prec_bundle'] : '';
$filename_b = !empty($file_forbatch_result['file_name']) ? $file_forbatch_result['file_name'] : '';
$file_source = 'uploadfiles/SourceFiles/'.$bundle_dir.'/'.$filename_b;
$tat = !empty($file_forbatch_result['tat']) ? $file_forbatch_result['tat'] : '';
$precship_date =  !empty($file_forbatch_result['precship_date']) ? $file_forbatch_result['precship_date'] : '';
$reference_code =  !empty($file_forbatch_result['reference_code']) ? $file_forbatch_result['reference_code'] : '';
$main_dir = pathinfo($filename_b, PATHINFO_FILENAME);
include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>
<style>
   hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid rgba(0, 0, 0, 0.1);
  }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>File For Batching</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">File For Batching</li>
        </ol>
    </section>
    <section class="content">
        
        <?php 
            if(empty($file_forbatch_result)){
                echo '<div class="row">
                          <div class="col-sm-12" id="index_error_msg">
                                <div class="alert alert-error alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-info"></i> Warning!</h4>
                                    File ID not exist!
                                </div>
                          </div>
                      </div>'; 
            }else{
        ?>
              <div class="row">
                  <div class="col-md-12">
                       <div class="box box-primary">
                          <div class="box-header with-border pull-right"></div>
                          <div class="box-body">
                              <div class="col-sm-3">
                                    <div class="row" style="margin-bottom: 20px">
                                        <h5><i class="fa fa-file-o"></i> Filename: <a href="<?= $file_source?>" target="_blank"><?= $filename_b;?></a></h5>
                                        <h5><i class="fa fa-file-o"></i> Bundle: <strong><?= $bundle_dir;?></strong></h5>
                                    </div>
                                    <div class="row">
                                        <h4><strong>Set Page To Segregate</strong></h4>
                                        <hr/>
                                        <form id="form_page_split">
                                            <div class="form-group">
                                                <label>Page From</label>
                                                <input type="number" name="page_from" id="page_from" class="form-control" req="true" message="Page From is required">
                                            </div>
                                            <div class="form-group">
                                                <label>Page To</label>
                                                <input type="number" name="page_to" id="page_to" class="form-control" req="true" message="Page To is required">
                                            </div>
                                            <div class="form-group">
                                                <label class="form-check-label" for="flexCheckChecked">
                                                    Auto Upload To GG
                                                </label>
                                                <input class="form-check-input" type="checkbox" value=""  id="auto_upload_to_gg"/>
                                                
                                            </div>
                                        </form>  
                                        <button type="submit" class="btn btn-primary pull-right" id="submit_split">Split</button>
                                    </div>
                                    <div class="row" style="margin-top: 35px">
                                        <h4><strong>Segregated Files</strong>: <span id="total_segregated"></span></h4>
                                        <hr/>
                                        <div class="col-sm-12" style="margin-bottom: 30px;">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-hover table-bordered">
                                                    <thead>
                                                        <th width="5%"><input type="checkbox" id="check_all_batch" onclick="Splitting.checkAll(this, 'tr_chk');"></th>
                                                        <th>Filename</th>
                                                        <th>P Start</th>
                                                        <th>P End</th>
                                                        <th>Uploaded to GG</th>
                                                    </thead>
                                                    <tbody id="tbodySegregatedFiles"></tbody>
                                                </table>
                                            </div>
                                            <a href='<?= $base_url.'/'.$data_inventory_path.'/CONVERTED TO TIF FILES/'.$bundle_dir.'/'.$main_dir.'_'.$_SESSION['UserID'].'/'.$main_dir.'.zip';?>' download="<?= $main_dir.'.zip';?>" target="_blank" id="dl_tif_zip" style="display:none">DL</a>
                                        </div>
                                        <div class="col-sm-12">
                                             <button type="submit" class="btn btn-primary btn-xs pull-right btn-danger" disabled id="delete_page_split">Delete</button>
                                             <button type="submit" style="margin-right: 7px;" class="btn btn-primary btn-xs pull-right" disabled id="upload_to_golden_gate">Upload To GG</button>
                                             <button type="submit" style="margin-right: 7px;" title="Convert PDF Pages to PNG" class="btn btn-success btn-xs pull-right" disabled id="dl_and_convert_tif">Convert To TIF</button>
                                             <button type="submit" style="margin-right: 7px;" title="Convert To PNG" class="btn btn-info btn-xs pull-right" disabled id="convert_to_png">Convert To PNG</button>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 100px">
                                        <h4><strong>For Image Edit</strong>:</h4>
                                        <hr/>
                                        <div class="col-sm-12" style="margin-bottom: 30px;">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-hover table-bordered">
                                                    <thead>
                                                        <th width="5%"><input type="checkbox" id="check_all_tif" onclick="Splitting.checkAlliEdit(this, 'tr_chk_png');"></th>
                                                        <th>Filename</th>
                                                        <th></th>
                                                    </thead>
                                                    <tbody id="tbody_image_edit_files"></tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                             <button type="submit" class="btn btn-primary btn-xs pull-right btn-danger" disabled id="delete_page_png">Delete</button>
                                             <button type="submit" style="margin-right: 7px;" class="btn btn-info btn-xs pull-right" id="convert_to_pdf_btn" onclick="Splitting.convertBackToPDF();">Convert TO PDF</button>
                                             <button type="submit" style="margin-right: 7px;" class="btn btn-warning btn-xs pull-right" id="button_png_back" onclick="Splitting.imageEditBack();">Back</button>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 100px">
                                        <h4><strong>Uploaded TIF Files (Cleaned Files)</strong>:</h4>
                                        <hr/>
                                        <div class="col-sm-12" style="margin-bottom: 15px;">
                                            <button type="submit" style="margin-right: 15px;" class="btn btn-success btn-xs pull-left" id="upload_tif_files">Upload TIF</button> 
                                        </div>
                                        <div class="col-sm-12" style="margin-bottom: 30px;">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-hover table-bordered">
                                                    <thead>
                                                        <th width="5%"><input type="checkbox" id="check_all_tif" onclick="TIFFiles.checkAll(this, 'tr_chk_tif');"></th>
                                                        <th>Filename</th>
                                                        <th>Type</th>
                                                        <th>Size</th>
                                                    </thead>
                                                    <tbody id="tbody_dl_files"></tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                             <button type="submit" class="btn btn-primary btn-xs pull-right btn-danger" disabled id="delete_page_tif">Delete</button>
                                             <button type="submit" style="margin-right: 7px;" class="btn btn-warning btn-xs pull-right" id="button_tif_back" onclick="TIFFiles.back();">Back</button>
                                        </div>
                                    </div>
                              </div>
                              <div class="col-sm-9">
                                  <?= '<embed src="'.$file_source.'" style="width:100%; height:43vw;" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">';?>
                              </div>
                          </div>
                          <div class="box-footer"></div>
                      </div>
                  </div>
              </div>

        <?php
           } 
        ?>
    </section>
</div>
<div class="modal fade" id="upload_tif_modal">
    <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title">Upload TIF Zip File</h4>
          </div>
          <div class="modal-body">
              <form role="form" id="upload_tif_modal_form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>File <span class="text-muted">(zip file only)</span></label>
                        <input type="file" class="form-control" id="textFiles" name="txtFiles[]" multiple="multiple" req="true" message="Source Files is required">
                    </div>
              </form>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-primary" id="submit_tif_file" value="Submit">
          </div>
      </div>
  </div>
</div>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
<script type="text/javascript">
    $('#submit_split').click(function(){
          if(Form.validate('#form_page_split')){
              var page_from = parseInt($('#page_from').val());
              var page_to = parseInt($('#page_to').val());
              
              if(page_from < 1){
                 Splitting.error('Page from value should not less than 1');
                 return false;
              }

              if(page_to < 1){
                 Splitting.error('Page to value should not less than 1');
                 return false;
              }

              if(page_to < page_from){
                  Splitting.error('Page to cannot lower than page from');
                  return false;
              }

              $('#submit_split').attr('disabled', true);
           
              var auto_upload_to_gg = ($('#auto_upload_to_gg').is(":checked")) ? 1 : 0;
              var data = [
                  {name: 'page_from', value: page_from},
                  {name: 'page_to', value: page_to},
                  {name: 'action', value: 'split_page'},
                  {name: 'file_source', value: '<?= $file_source;?>'},
                  {name: 'filename', value: '<?= $filename_b;?>'},
                  {name: 'bundle_dir', value: '<?= $bundle_dir;?>'},
                  {name: 'prec_bundle', value: '<?= $bundle_dir;?>'},
                  {name: 'id', value: '<?= $id;?>'},
                  {name: 'precship_date', value: '<?= $precship_date;?>'},
                  {name: 'tat', value: '<?= $tat;?>'},
                  {name: 'reference_code', value: '<?= $reference_code;?>'},
                  {name: 'auto_upload_to_gg',  value: auto_upload_to_gg}
              ];
              
              Splitting.submit(data);
          }
    });

    $('#submit_tif_file').click(function(){
        if(Form.validate('#upload_tif_modal_form')){
            TIFFiles.submit();
        }
    });

    $('#delete_page_split').click(function(){
        Splitting.delete();
    });

    $('#delete_page_png').click(function(){
        Splitting.deletePng();
    });

    $('#upload_to_golden_gate').click(function(){
        Splitting.upload();
    });

    $('#dl_and_convert_tif').click(function(){
        Splitting.DLandConvertTIF();
    });

    $('#convert_to_png').click(function(){
        Splitting.convertToPNG();
    });

    $('#upload_tif_files').click(function(){
        $('#upload_tif_modal').modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    $('#delete_page_tif').click(function(){
        Swal.fire({
            allowOutsideClick: false,
            title: '<strong>Are you sure you want to delete selected files?</strong>',
            icon: 'warning',
            html: '',
            showCloseButton: true,
            showCancelButton: true,
            cancelButtonColor: '#d33',
            focusConfirm: false,
            confirmButtonText:'<i class="fa fa-thumbs-up"></i> Submit',
        }).then((result) => {
            if (result.isConfirmed) {
                TIFFiles.delete();
            }
        });
    });

    var dlfile_dir = '<?= $data_inventory_path.'/TIFFILES/'.$bundle_dir.'/'.$main_dir;?>';
    var dldir= dlfile_dir;
    var dlprevious_dir = dldir;
    var dlcurrent_dir = dldir;


    var TIFFiles = {
        init : function(){
            TIFFiles.load(dldir);
        },
        delete : function(){
            var list = TIFFiles.getCheckBatch();
            
            if(list.length > 0){
                var action = 'delete_uploaded_tif';
                var data = [
                    {name: 'files', value: JSON.stringify(list)},
                    {name: 'action', value: action},
                ];
                Page.loading('Deleting...');
                $.post('postdata.php', data, function(data){
                    Swal.close();
                    try{ 
                      var res = JSON.parse(data);
                      if(res.success){
                          Swal.fire({
                              allowOutsideClick: false,
                              title: '<strong>Submit Status</strong>',
                              icon: 'info',
                              html: res.message,
                              showCloseButton: false,
                              showCancelButton: false,
                              focusConfirm: false,
                              confirmButtonText:'<i class="fa fa-thumbs-up"></i> OK',
                          }).then((result) => {
                              if (result.isConfirmed) {
                                  TIFFiles.load(dlcurrent_dir);
                              }
                          });
                          
                      }else{
                           Splitting.error(res.message);
                      }
                   }catch(e){
                      Splitting.error(e);
                   }
                });
            }else{
                Splitting.error('No file selected.');
            }
        },
        getCheckBatch : function(){
            var list = [];
            var _chk = $('.tr_chk_tif');
            if(_chk.length > 0){
                $.each(_chk, function(i, v){
                      if($(v).is(":checked")){
                          list.push({file:$(v).attr("path"), name: $(v).attr("filename")});
                      }
                });
            }

            return list;
        },
        submit : function(){
            var totalfiles = document.getElementById('textFiles').files.length;
            var formData = new FormData();
            if(totalfiles > 0){
                formData.append("id", '<?= $id;?>');
                formData.append("action", "upload_tif_files");
                for (var index = 0; index < totalfiles; index++) {
                    formData.append("files[]", document.getElementById('textFiles').files[index]);
                }
                Swal.fire({
                    title: 'Processing ...',
                    showConfirmButton: false,
                    onBeforeOpen () {
                       Swal.showLoading ()
                    },
                    onAfterClose () {
                       Swal.hideLoading()
                    },
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false
                });

                $.ajax({
                      url : 'postdata.php',
                      type : 'POST',
                      data : formData,
                      processData: false,  // tell jQuery not to process the data
                      contentType: false,  // tell jQuery not to set contentType
                      success : function(data) {
                          Swal.close();
                           try{ 
                              var res = JSON.parse(data);
                              if(res.success){
                                  $('#upload_tif_modal').modal('hide');
                                  Swal.fire({
                                      allowOutsideClick: false,
                                      title: '<strong>Submit Status</strong>',
                                      icon: 'info',
                                      html: res.message,
                                      showCloseButton: false,
                                      showCancelButton: false,
                                      focusConfirm: false,
                                      confirmButtonText:'<i class="fa fa-thumbs-up"></i> OK',
                                  }).then((result) => {
                                      /* Read more about isConfirmed, isDenied below */
                                      if (result.isConfirmed) {
                                          TIFFiles.load(dlcurrent_dir);
                                      }
                                  })
                              }else{
                                   Splitting.error(res.message);
                              }
                           }catch(e){
                              Splitting.error(e);
                           }
                      },
                      error: function (err) {
                          Swal.close();
                          Splitting.error(err);
                      }
                });
            }else{
               Splitting.error("Zip File is required");
            }
        },
        load : function(dldir){
            $('#delete_page_tif').prop('disabled', true);
            dlcurrent_dir = dldir;
            var tif_dir = dldir.split('/');
            if(tif_dir.length > 2){
                dlprevious_dir = '';
                
                for(var i = 0; i < tif_dir.length - 1; i++){
                    var concat = '/';
                    if(i == tif_dir.length - 2){
                        concat = '';
                    }
                    dlprevious_dir += tif_dir[i]+concat;  
                }
            }
            
            var data = [
                {name: 'action', value: 'fetch_dl'},
                {name: 'dir', value: dldir}
            ];
            $('#tbody_dl_files').html('');
            $('#tbody_dl_files').append(
                $('<tr>').append(
                    $('<td>').attr({colspan:'4'}).css({textAlign: 'center'}).html('<i>Please wait while fetching upload TIF files</i>')
                )
            );
            $.post('postdata.php', data, function(res){
                try{
                    $('#tbody_dl_files').html('');
                    var response = JSON.parse(res);
                    if(response.success){
                        if(response.data.length > 0){
                            $.each(response.data, function(i, v){
                                var path_to_fetch = dldir+'/'+v.filename;
                                var _chk = '<input type="checkbox" path="'+path_to_fetch+'" filename = "'+v.filename+'" id="tr_tif_'+v.filename+'" class="tr_chk_tif" onclick="TIFFiles.checkButton();">';
                                    ;
                                var  href = '<a href="javascript:void(0);" onclick="TIFFiles.load(\''+path_to_fetch+'\');">'+v.filename+'</a>';
                                var type = "Folder";
                                if(parseInt(v.type) != 2){
                                    href ='<a href="'+path_to_fetch+'" target="_blank">'+v.filename+'</a>';
                                    type = "File";
                                }

                                $('#tbody_dl_files').append(
                                    $('<tr>').append(
                                        $('<td>').html(_chk),
                                        $('<td>').html(href),
                                        $('<td>').html(type),
                                        $('<td>').html(((v.size/1000).toFixed(2))+' KB')  
                                    )
                                );
                            });
                        }else{
                            $('#tbody_dl_files').append(
                                $('<tr>').append(
                                    $('<td>').attr({colspan:'4'}).css({textAlign: 'center', color: 'red'}).html('No File Available')
                                )
                            );
                        }
                    }else{
                        $('#tbody_dl_files').append(
                            $('<tr>').append(
                                $('<td>').attr({colspan:'4'}).css({textAlign: 'center', color: 'red'}).html(response.message)
                            )
                        );
                    }
                }catch(e){
                    $('#tbody_dl_files').append(
                        $('<tr>').append(
                            $('<td>').attr({colspan:'4'}).css({textAlign: 'center', color: 'red'}).html(response.message)
                        )
                    );
                }
            });
        },
        back : function(){
            if(dlfile_dir != dlcurrent_dir){
                TIFFiles.load(dlprevious_dir);
            }
        },
        checkAll : function(el, _class){
            var _chk_status = $(el).is(":checked");
            $('.'+_class).prop("checked", false);
            if(_chk_status){
                $('.'+_class).prop('checked', true);
            }
            TIFFiles.checkButton();
        },
        checkButton : function(){
            var _chk = $('.tr_chk_tif');
            var enable_ = false;
            if(_chk.length > 0){
                $.each(_chk, function(i, v){
                      if($(v).is(":checked")){
                          enable_ = true;
                      }
                });
            }
            $('#delete_page_tif').prop('disabled', true);
            
            if(enable_){
                $('#delete_page_tif').prop('disabled', false);
            }
        }
    };

    var imgedit_dir = '<?= $data_inventory_path.'/CONVERTED TO PNG FILES/'.$bundle_dir.'/'.$main_dir;?>';
    var imgeditdir= imgedit_dir;
    var imgeditprevious_dir = imgeditdir;
    var imgeditcurrent_dir = imgeditdir;

    var Splitting = {
        init : function(){
            Splitting.SegregatedFiles();
            Splitting.loadImageEdit(imgeditdir);
            setInterval(function() {
              Splitting.loadImageEdit(imgeditdir);
            }, 300000); // set to 5 minutes
        },
        submit : function(obj){
            Page.loading('Processing ...');
            $.post('postdata.php', obj, function(res){
                Swal.close();
                try{
                    var response = JSON.parse(res);
                    if(response.success){
                        $('#page_from').val('');
                        $('#page_to').val('');
                        $('#auto_upload_to_gg').prop('checked', false);
                        Splitting.success(response.message);
                    }else{
                        Splitting.error(response.message);
                    }
                }catch(e){
                    Splitting.error(e);
                }

                $('#submit_split').removeAttr('disabled');
            });
        },
        success : function(msg){
            Swal.fire({
                allowOutsideClick: false,
                title: '<strong>Submit Status</strong>',
                icon: 'info',
                html: msg,
                showCloseButton: false,
                showCancelButton: false,
                focusConfirm: false,
                confirmButtonText:'<i class="fa fa-thumbs-up"></i> OK',
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    Splitting.SegregatedFiles();
                }
            })
        },
        error : function (msg){
            Swal.fire({
                allowOutsideClick: false,
                icon: 'error',
                title: 'Oops...',
                html: "<strong>"+msg+"</strong>"
            });
        },
        SegregatedFiles : function(){
            var obj = [
                {name: 'action', value: 'get-segrated-files'},
                {name: 'id', value: '<?= $id;?>'}
            ];
            Swal.fire({
                  title: 'Processing ...',
                  showConfirmButton: false,
                  onBeforeOpen () {
                     Swal.showLoading ()
                  },
                  onAfterClose () {
                     Swal.hideLoading()
                  },
                  allowOutsideClick: false,
                  allowEscapeKey: false,
                  allowEnterKey: false
            });
            $('#upload_to_golden_gate, #delete_page_split').prop('disabled', true);
            $.post('postdata.php', obj, function(res){
                Swal.close();
                try{
                    var response = JSON.parse(res);
                    if(response.success){
                        $('#tbodySegregatedFiles').html('');
                        if(response.data.length > 0){
                            var cnt = 0;
                            $.each(response.data, function(i,v){
                                cnt ++;
                                var isUploadedToGG = '<span style="width: 45px;" class="btn btn-warning btn-xs">Not Yet</span>';
                                var _chck = '<input type="checkbox" split_id = "'+v.id+'" id="tr_chk_'+v.id+'" class="tr_chk" onclick="Splitting.checkButton();">';
                                if(parseInt(v.wms_job_id) > 0){
                                    isUploadedToGG = '<span style="width: 45px;" class="btn btn-success btn-xs">Done</span>';
                                    //_chck = '';
                                }
                                $('#tbodySegregatedFiles').append(
                                    $('<tr>').append(
                                          $('<td>').html(_chck),
                                          $('<td>').html('<a target="_blank" href="uploadfiles/SourceFiles/'+v.prec_bundle+'/'+v.filename+'">'+v.filename+'</a>'),
                                          $('<td>').html(v.page_start),
                                          $('<td>').html(v.page_end),
                                          $('<td>').html(isUploadedToGG)
                                    )
                                )  
                            });
                            $('#total_segregated').html('('+cnt+')');
                        }else{
                            $('#tbodySegregatedFiles').append(
                                $('<tr>').append(
                                      $('<td>').html('No data available').attr({colspan: '5'}).css({textAlign:'center'})
                                )
                            )  
                        }
                              
                    }else{
                        Splitting.error(response.message);
                    }
                }catch(e){
                    Splitting.error(e);
                }
            });
        },
        checkAlliEdit : function(el, _class){
            var _chk_status = $(el).is(":checked");
            $('.'+_class).prop("checked", false);
            if(_chk_status){
                $('.'+_class).prop('checked', true);
            }

            Splitting.checkButtonIEdit();
        },
        checkAll : function(el, _class){
            var _chk_status = $(el).is(":checked");
            $('.'+_class).prop("checked", false);
            if(_chk_status){
                $('.'+_class).prop('checked', true);
            }

            Splitting.checkButton();
        },
        checkButtonIEdit : function(){
            var _chk = $('.tr_chk_png');
            var enable = false;
            if(_chk.length > 0){
                $.each(_chk, function(i, v){
                    if($(v).is(":checked")){
                        enable = true;
                    }
                });
            }
            $('#convert_to_pdf_btn, #delete_page_png').prop('disabled', true);
            
            if(enable){
                $('#convert_to_pdf_btn, #delete_page_png').prop('disabled', false);
            }
        },
        checkButton : function(){

            var _chk = $('.tr_chk');
            var enable_gg_submit = false;
            if(_chk.length > 0){
                $.each(_chk, function(i, v){
                      if($(v).is(":checked")){
                          enable_gg_submit = true;
                      }
                });
            }
            $('#upload_to_golden_gate, #dl_and_convert_tif, #delete_page_split, #convert_to_png').prop('disabled', true);
            
            if(enable_gg_submit){
                $('#upload_to_golden_gate, #delete_page_split, #dl_and_convert_tif, #convert_to_png').prop('disabled', false);
            }
        },
        getCheckBatch : function (){
            var list = [];
            var _chk = $('.tr_chk');
            if(_chk.length > 0){
                $.each(_chk, function(i, v){
                      if($(v).is(":checked")){
                          list.push($(v).attr("split_id"))
                      }
                });
            }

            return list;
        },
        getCheckPng : function(){
            var list = [];
            var _chk = $('.tr_chk_png');
            if(_chk.length > 0){
                $.each(_chk, function(i, v){
                      if($(v).is(":checked")){
                          list.push({file:$(v).attr("path"), name: $(v).attr("filename"), bundle: '<?= $bundle_dir;?>'});
                      }
                });
            }

            return list;
        },
        delete : function(){
            var batches = Splitting.getCheckBatch();
            if(batches.length > 0){
                var action = 'delete_split';
                var data = [
                    {name: 'ids', value: JSON.stringify(batches)},
                    {name: 'action', value: action},
                ];
                Splitting.confirm('Are you sure want to delete?', data, action);
            }else{
                Splitting.error('No batched file selected.');
            }
        },
        deletePng : function(){
            var batches = Splitting.getCheckPng();
            if(batches.length > 0){
                var action = 'delete_converted_png';
                var data = [
                    {name: 'ids', value: JSON.stringify(batches)},
                    {name: 'action', value: action},
                ];
                Splitting.confirm('Are you sure want to delete?', data, action);
            }else{
                Splitting.error('No file selected.');
            }
        },
        DLandConvertTIF : function(){
            var batches = Splitting.getCheckBatch();
            if(batches.length > 0){
                var action = 'dl_convert_to_tif';
                var data = [
                    {name: 'ids', value: JSON.stringify(batches)},
                    {name: 'parent_id', value: '<?= $id?>'},
                    {name: 'action', value: action},
                ];
                Splitting.confirm('Are you sure want to Download and Convert to TIF files?', data, action);
            }else{
                Splitting.error('No batched file selected.');
            }
        },
        convertToPNG : function(){
            var batches = Splitting.getCheckBatch();
            if(batches.length > 0){
                var action = 'convert_to_png';
                var data = [
                    {name: 'ids', value: JSON.stringify(batches)},
                    {name: 'parent_id', value: '<?= $id?>'},
                    {name: 'action', value: action},
                ];
                Splitting.confirm('Are you sure want to convert selected files to PNG?', data, action);
            }else{
                Splitting.error('No batched file selected.');
            }
        },
        convertBackToPDF : function(){
            var batches = Splitting.getCheckPng();
            if(batches.length > 0){
                var action = 'convert_back_to_pdf';
                var data = [
                    {name: 'ids', value: JSON.stringify(batches)},
                    {name: 'action', value: action},
                ];
                Splitting.confirm('Are you sure want to convert selected file to pdf?', data, action);
            }else{
                Splitting.error('No file selected.');
            }
        },
        imageEditBack : function(){
            //if(imgeditdir != imgeditcurrent_dir){
                Splitting.loadImageEdit(imgeditprevious_dir);
            //}
        },
        loadImageEdit : function(imgeditdirx){
            imgeditdir = imgeditdirx;
            $('.tr_chk_png').prop('checked', false);
            $('#delete_page_png, #convert_to_pdf_btn').prop('disabled', true);
            imgeditcurrent_dir = imgeditdir;
            var png_dir = imgeditdir.split('/');
            if(png_dir.length > 2){
                imgeditprevious_dir = '';
                
                for(var i = 0; i < png_dir.length - 1; i++){
                    var concat = '/';
                    if(i == png_dir.length - 2){
                        concat = '';
                    }
                    imgeditprevious_dir += png_dir[i]+concat;  
                }
            }
            
            var data = [
                {name: 'action', value: 'fetch_converted_to_png'},
                {name: 'dir', value: imgeditdir}
            ];
            $('#tbody_image_edit_files').html('');
            $('#tbody_image_edit_files').append(
                $('<tr>').append(
                    $('<td>').attr({colspan:'4'}).css({textAlign: 'center'}).html('<i>Please wait while fetching files</i>')
                )
            );

            $.post('postdata.php', data, function(res){
                  try{
                      $('#tbody_image_edit_files').html('');
                      var response = JSON.parse(res);
                      if(response.success){
                          if(response.data.length > 0){
                              $.each(response.data, function(i, v){
                                  var path_to_fetch = imgeditdir+'/'+v.filename;
                                  var _chk = '<input type="checkbox" path="'+path_to_fetch+'" filename = "'+v.filename+'" id="tr_png_'+v.filename+'" class="tr_chk_png" onclick="Splitting.checkButtonIEdit();">';
                                      ;
                                  var  href = '<a href="javascript:void(0);" onclick="Splitting.loadImageEdit(\''+path_to_fetch+'\');">'+v.filename+'</a>';
                                  var type = "Folder";
                                  var imageEdit = '';
                                  if(parseInt(v.type) != 2){
                                      var color = "";
                                      if(v.editted){
                                          color = "green";
                                      }
                                      href ='<a href="'+path_to_fetch+'" target="_blank" style="color: '+color+'">'+v.filename+'</a>';
                                      type = "File";

                                      imageEdit = '<a href="<?= $image_edit_url;?>?file='+path_to_fetch+'&app=<?= $base_url;?>&callbackfile=image_edit_save.php" target="_blank">Edit</a>';
                                  }


                                  $('#tbody_image_edit_files').append(
                                      $('<tr>').append(
                                          $('<td>').html(_chk),
                                          $('<td>').html(href),
                                          $('<td>').html(imageEdit),
                                      )
                                  );
                              });
                          }else{
                              $('#tbody_image_edit_files').append(
                                  $('<tr>').append(
                                      $('<td>').attr({colspan:'3'}).css({textAlign: 'center', color: 'red'}).html('No File Available')
                                  )
                              );
                          }
                      }else{
                          $('#tbody_image_edit_files').append(
                              $('<tr>').append(
                                  $('<td>').attr({colspan:'3'}).css({textAlign: 'center', color: 'red'}).html(response.message)
                              )
                          );
                      }
                  }catch(e){
                      $('#tbody_image_edit_files').append(
                          $('<tr>').append(
                              $('<td>').attr({colspan:'3'}).css({textAlign: 'center', color: 'red'}).html(response.message)
                          )
                      );
                  }
              });
        },
        post : function(obj, action){
            Swal.fire({
                  title: 'Processing ...',
                  showConfirmButton: false,
                  onBeforeOpen () {
                     Swal.showLoading ()
                  },
                  onAfterClose () {
                     Swal.hideLoading()
                  },
                  allowOutsideClick: false,
                  allowEscapeKey: false,
                  allowEnterKey: false
            });
            $('#upload_to_golden_gate, #dl_and_convert_tif, #delete_page_split').prop('disabled', true);
            $.post('postdata.php', obj, function(res){
                Swal.close();
                try{
                    var response = JSON.parse(res);
                    if(response.success){
                        Splitting.success(response.message);
                        if(action == 'dl_convert_to_tif'){
                            TIFFiles.load(dlcurrent_dir);
                            console.log(action);
                            var dl = document.getElementById('dl_tif_zip');
                            dl.click();
                        }

                        if(action == 'convert_to_png' || action == 'delete_converted_png'){
                            Splitting.loadImageEdit(imgeditcurrent_dir);
                        }
                    }else{
                        Splitting.error(response.message);
                        $('#upload_to_golden_gate, #dl_and_convert_tif, #delete_page_split').prop('disabled', false);
                    }
                }catch(e){
                    Splitting.error(e);
                    $('#upload_to_golden_gate, #dl_and_convert_tif, #delete_page_split').prop('disabled', false);
                }
            });
        },
        confirm : function(msg, obj, action){
            Swal.fire({
                allowOutsideClick: false,
                title: msg,
                text: '',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Submit'
            }).then((result) => {
                if (result.isConfirmed) {
                    Splitting.post(obj, action);
                }
            })
        },
        upload : function (){
            var batches = Splitting.getCheckBatch();
            if(batches.length > 0){
                var action = 'upload_to_GG';
                var data = [
                    {name: 'ids', value: JSON.stringify(batches)},
                    {name: 'action', value: action},
                ];
                Splitting.confirm('Are you sure want to upload file to GG?', data, action);
            }else{
                Splitting.error('No batched file selected.');
            }
        }

    }

    Splitting.init();
    TIFFiles.init();
</script>
