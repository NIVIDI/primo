<?php
use Smalot\PdfParser\Parser;
use Smalot\PdfParser\XObject\Image;

$ids = json_decode($post['ids']);
$parent_id = !empty($post['parent_id']) ? $post['parent_id'] : '';

if(empty($parent_id)){
	$results = array("success" => false, "message" => "Error: Bundle ID not defined!", "data" => '');
	echo json_encode($results);
	exit;
}

if(!empty($ids)){
	$process_result = array();
	$sql = "SELECT * FROM files_for_batching WHERE id = '{$parent_id}'";
	$sql_result = mysqli_query($con, $sql)->fetch_assoc();


	$main_dir = !empty($sql_result['file_name']) ? (pathinfo($sql_result['file_name'], PATHINFO_FILENAME)) : '';
	$main_orig_dir = $main_dir;
	$main_dir .= '_'.$_SESSION['UserID'];
	$bundle_dir = !empty($sql_result['prec_bundle']) ? $sql_result['prec_bundle'] : '';
	if(file_exists(__DIR__.'/'.$data_inventory_path.'/CONVERTED TO TIF FILES/'.$bundle_dir.'/'.$main_dir)){
		rrmdir(__DIR__.'/'.$data_inventory_path.'/CONVERTED TO TIF FILES/'.$bundle_dir.'/'.$main_dir);
	}

	if(file_exists(__DIR__.'/TIFFILES/'.$bundle_dir.'/'.$main_dir)){
		rrmdir(__DIR__.'/TIFFILES/'.$bundle_dir.'/'.$main_dir);
	}

	foreach($ids as $id){
		$sql = "SELECT a.* FROM segregated_files a WHERE a.id = '{$id}'";
		$sql_result = mysqli_query($con, $sql)->fetch_assoc();
		$filename = !empty($sql_result['filename']) ? $sql_result['filename'] : '';
		
		$file_path = $SourceFilePath.'/'.$bundle_dir.'/'.$filename;
		
		$new_dir = pathinfo($file_path, PATHINFO_FILENAME);

		$tif_file_path = __DIR__.'/'.$data_inventory_path.'/CONVERTED TO TIF FILES/'.$bundle_dir.'/'.$main_dir.'/'.$new_dir;
		if(!file_exists($tif_file_path)){
			mkdir($tif_file_path, 0777, true);
		}

		$temp_file_path = __DIR__.'/TIFFILES/'.$bundle_dir.'/'.$main_dir.'/'.$new_dir;
		if(!file_exists($temp_file_path)){
			mkdir($temp_file_path, 0777, true);
		}
		//convert first to png 
		$cmd = __DIR__.'/mupdf/mutool convert -o '.$temp_file_path.'/'.$new_dir.'_%d.png '.$file_path.' 1-N';
		exec($cmd);
		
	    $x = 0;
	    $pages_to_split = array();
	    $parser = new Parser();
	    $pdf    = $parser->parseFile($file_path);
	    $pages  = $pdf->getPages();
	    $total_pages = COUNT($pages);
	    //convert png to tif
	    foreach ($pages as $page) {
	      	$x ++;
	      	$image = new Imagick($temp_file_path.'/'.$new_dir.'_'.$x.'.png');
			$image->setResolution(300, 300);
			$image->setImageFormat('jpeg');
			$image->setImageCompression(imagick::COMPRESSION_JPEG); 
			$image->setImageCompressionQuality(100);
			$image->writeImage($tif_file_path.'/'.$new_dir.'_'.$x.'.tif');
			unlink($temp_file_path.'/'.$new_dir.'_'.$x.'.png');
		} 

		$process_result[] = array("success" => true, "message" => "Successfully download and converted to tif file ({$filename}) to GG");
	}

	$path_tozip = __DIR__.'/'.$data_inventory_path.'/CONVERTED TO TIF FILES/'.$bundle_dir.'/'.$main_dir;
	$zip = zipFiles($path_tozip, $main_orig_dir);
	
	$message = '';
	foreach($process_result as $response){
		$style = "display: block; color: red;";
		if($response['success']){
			$style = "display: block; color: green;";
		}
		$message .= "<li style='{$style}'>{$response['message']}</li>";
	}
	$results = array("success" => true, "message" => "{$message}");
}else{
	$results = array("success" => false, "message" => "You have not selected split page to download!");
}
?>