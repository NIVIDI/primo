<?php
	include "conn.php";
	$xml = '';
	$url = !empty($_GET['url']) ? $_GET['url']: '';
	if(!empty($url)){
		$url = __DIR__.'/'.$url;
		if(file_exists($url)){
			$xml = file_get_contents($url);
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<title>XML Viewer</title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    	
		<link rel="stylesheet" href="lib/codemirror.css">
		<script src="lib/codemirror.js"></script>
		<script src="mode/javascript/javascript.js"></script>
		<script src="mode/xml/xml.js"></script>
		<script src="mode/markdown/markdown.js"></script>
  
		<style type="text/css">
		    .CodeMirror {border-top: 1px solid black; border-bottom: 1px solid black; height: 32vw;}
			.CodeMirror-selected  { background-color: skyblue !important; }
		    .CodeMirror-selectedtext { color: white; }
		    .styled-background { background-color: #ff7; }

		    .example-modal .modal {
		      position: relative;
		      top: auto;
		      bottom: auto;
		      right: auto;
		      left: auto;
		      display: block;
		      z-index: 1;
		    }

		</style>
		
	</head>
	<body>
		<fieldset>
			<textarea id="code" rows="100" spellcheck="true"  name="code"><?= htmlentities($xml, ENT_COMPAT, "UTF-8");?></textarea>
		</fieldset>
	</body>
	<script type="text/javascript">

		var te_html = document.getElementById("code");						
			var editor_html = CodeMirror.fromTextArea(te_html, {
			mode: "text/xml",
			lineNumbers: true,
			matchTags: {bothTags: true},
			lineWrapping: true,
			extraKeys: {"Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); }},
			foldGutter: true,
			styleActiveLine: true,
			styleActiveSelected: true,
			styleSelectedText: true,
			autoRefresh: true,
			indentUnit: 4,
			indentWithTabs: true,
			readOnly: true,
			smartIndent: true,

			gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
		});

		editor_html.refresh();
		editor_html.setSize("100%","100vh"); 

		
	</script>
</html>
