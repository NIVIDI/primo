<?php
include "conn.php";

$sql="SELECT * FROM tblUserAccess Where UserID=' $_SESSION[UserID]'";
 
if ($result=mysqli_query($con,$sql))
{
// Fetch one and one row
	while ($row=mysqli_fetch_row($result))
	{
		$ACQUIRE=$row[1];
		$ENRICH=$row[2];
		$DELIVER=$row[3];
		$USER_MAINTENANCE=$row[4];
		$EDITOR_SETTINGS=$row[5];
		$ML_SETTINGS=$row[6];
		$TRANSFORMATION=$row[7];
		$TRANSMISSION=$row[8];
	}
}
include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>User Maintenance</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User Maintenance</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                  <div class="box box-primary">
                        <div class="box-header with-border">
                           <h3 class="box-title"><button type="button" class="btn btn-block btn-primary"  onclick="location.href='addnew_user.php'">Add New</button></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                              <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                        					          <th>User ID</th>
                                            <th>User Name</th>
                                            <th>Name</th>
                                            <th>User Type</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                    				        <?php
                                      $sql="SELECT * FROM tblUser";
                    				          if ($result=mysqli_query($con,$sql)){
                                          // Fetch one and one row
                                          while ($row=mysqli_fetch_row($result)){
                                              echo '<tr>
                                                      <td>'.$row[0].'</td>
                                                      <td>'.$row[1].'</td>
                    				                          <td>'.$row[3].'</td>
                                                      <td>'.$row[4].'</td>
                                                      <td>
                                                          <a class="btn btn-xs btn-success"  href="UserAccess.php?UID='.$row[0].'&TransType=Update&Name='.$row[3].'">Access Rights</a>
                    				                              <a class="btn btn-xs btn-info"  href="addnew_user.php?UID='.$row[0].'&TransType=Update">Update</a>
                    				                              <button class="btn btn-xs btn-danger"  data-toggle="modal" data-target="#modal-danger" onclick="Javascript:SetValue('.$row[0].')">Delete</button></td>
                                                    </tr>';
                                          }
                    	 
                                      }
                                    ?>	  
                                    </tbody>
                                    <tfoot>
                                        <tr>
                    					              <th>User ID</th>
                                            <th>User Name</th>
                                            <th>Name</th>
                                            <th>User Type</th>
                    				                <th></th>
                                        </tr>
                                    </tfoot>
                              </table>
                        </div>
                  </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modal-access">
      <div class="modal-dialog">
        	<div class="modal-content">
            	  <div class="modal-header">
                		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                		  <span aria-hidden="true">&times;</span></button>
            		    <h4 class="modal-title">User Access</h4>
            	  </div>
            	  <form method="post" action="saveAccess.php">
            		  <div class="box-body">
            			 <div class="form-group">
            			  <label for="inputEmail3" class="col-sm-2 control-label">ACQUIRE <input type="checkbox" name="ACQUIRE"></label>
            			</div>
            			<div class="form-group">
            			  <label for="inputPassword3" class="col-sm-2 control-label">ENRICH <input type="checkbox" name="ENRICH"></label>
            			</div>
            			<div class="form-group">
            			  <label for="inputPassword3" class="col-sm-2 control-label">DELIVER <input type="checkbox" name="ENRICH"></label>
            			</div>
            			<div class="form-group">
            			  <label for="inputPassword3" class="col-sm-2 control-label">USER MAINTENANCE <input type="checkbox" name="ENRICH"></label>
            			</div>
            			<div class="form-group">
            			  <label for="inputPassword3" class="col-sm-2 control-label">EDITOR SETTINGS <input type="checkbox" name="ENRICH"></label>
            			</div>
            			
            			<div class="form-group">
            			  <label for="inputPassword3" class="col-sm-2 control-label">ML SETTINGS <input type="checkbox" name="ENRICH"></label>
            			</div>
            			<span id='message'></span>
            		  </div>
            		  <div class="modal-footer">
            			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            			<input type="submit" class="btn btn-primary" id="Button" value="Change Password">
            			<input type="hidden" class="btn btn-primary" name="tID" value="">
            			<input type="hidden" class="btn btn-primary" name="RedirectPage" value="index.php">
            		  </div>
            	  </form>
        	</div>
      </div>
</div>
<form method="GET" action="saveUser.php">

        <div class="modal modal-danger fade" id="modal-danger">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Record Deletion</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure you want to delete this record?</p>
                <input type="hidden" name="txtID" id="txtID"/>
                <input type="hidden" name="TransType" id="TransType" value="Delete" />
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>

                <button type="submit" class="btn btn-outline">Ok</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</form>

<script type="text/javascript" language="JavaScript">
    function SetValue($prVal) {
        document.getElementById('txtID').value = $prVal;
    }
</script>

<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>

