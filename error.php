<?php
include "conn.php";
include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Page Error</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Page Error</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-error alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i>Page Error:</h4>
                    <?= $_SESSION['page_error'];?>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
