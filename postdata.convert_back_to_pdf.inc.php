<?php
$files = json_decode($post['ids']);
if(!empty($files)){
	$response_results = array();
	$file_filters = array();
	$filename = '';
	$dir_path = '';
	$bundle = '';
	foreach($files as $file){
		if(file_exists($file->file)){
			if(strtolower(pathinfo($file->file, PATHINFO_EXTENSION)) == 'png'){
				$exploded_file = explode('_', pathinfo($file->file, PATHINFO_FILENAME));
				if(!empty($exploded_file[3])){
					$filename = $exploded_file[0].'_'.$exploded_file[1].'_'.$exploded_file[2];
					$dir_path = pathinfo($file->file, PATHINFO_DIRNAME);
					$bundle = $file->bundle;
					array_push($file_filters,$exploded_file[3]);
				}
			}
		}
	}
	sort($file_filters);
	$pdf_path = $SourceFilePath.'/'.$bundle.'/'.$filename.'.pdf';
	if(!empty($file_filters)){
		$pdf = new \Imagick();
		foreach($file_filters as $file_filter){
			$png_path = __DIR__.'/'.$dir_path.'/'.$filename.'_'.$file_filter.'.png';
			if(file_exists($png_path)){
				$pdf->addImage(new \Imagick($png_path));
			}
		}
		if(file_exists($pdf_path)){
			unlink($pdf_path);
		}
		$pdf->writeImages($pdf_path, true);
		$results = array("success" => true, "message" => "Successfully converted selected file to PDF");
	}else{
		$results = array("success" => false, "message" => "No selected converted to PDF!");
	}
}else{
	$results = array("success" => false, "message" => "No file selected to delete!");
}
?>