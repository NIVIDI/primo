<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:strip-space elements="*"/>
    <xsl:output indent="yes"/>
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:copy-of select="@*|node()"/>
        </xsl:copy>
    </xsl:template>    
    <xsl:template match="html">
        <Document>
            <xsl:apply-templates/>   
        </Document>
    </xsl:template>
    
    <xsl:template match="head">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="title">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="body">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="meta"/>
    
    <xsl:template match="div[@class='ocr_page']">
        <page pageseq="1" skewangle="0">
            <xsl:apply-templates/>
        </page>
    </xsl:template>
    
    <xsl:template match="div[@class='ocr_carea']">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="p">
        <xsl:variable name="titlep_value">
            <xsl:value-of select="substring-after(@title,'bbox ')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value1">
            <xsl:value-of select="substring-after($titlep_value,' ')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value1_final">
            <xsl:value-of select="substring-before($titlep_value,' ')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value2">
            <xsl:value-of select="substring-after($titlep_value1,' ')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value2_final">
            <xsl:value-of select="substring-before($titlep_value1,' ')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value3">
            <xsl:value-of select="substring-after($titlep_value2,' ')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value3_final">
            <xsl:value-of select="substring-before($titlep_value2,' ')"/>
        </xsl:variable>
        <block blockType="Text">
            <xsl:attribute name="l">
                <xsl:value-of select="$titlep_value1_final"/>
            </xsl:attribute>
            <xsl:attribute name="t">
                <xsl:value-of select="$titlep_value2_final"/>
            </xsl:attribute>
            <xsl:attribute name="r">
                <xsl:value-of select="$titlep_value3_final"/>
            </xsl:attribute>
            <xsl:attribute name="b">
                <xsl:value-of select="$titlep_value3"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </block>
    </xsl:template>
    
    <xsl:template match="span[@class='ocr_line']">
        <xsl:variable name="titlep_value">
            <xsl:value-of select="substring-before(substring-after(@title,'bbox '),';')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value1">
            <xsl:value-of select="substring-after($titlep_value,' ')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value1_final">
            <xsl:value-of select="substring-before($titlep_value,' ')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value2">
            <xsl:value-of select="substring-after($titlep_value1,' ')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value2_final">
            <xsl:value-of select="substring-before($titlep_value1,' ')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value3">
            <xsl:value-of select="substring-after($titlep_value2,' ')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value3_final">
            <xsl:value-of select="substring-before($titlep_value2,' ')"/>
        </xsl:variable>
        <line>
            <xsl:attribute name="l">
                <xsl:value-of select="$titlep_value1_final"/>
            </xsl:attribute>
            <xsl:attribute name="t">
                <xsl:value-of select="$titlep_value2_final"/>
            </xsl:attribute>
            <xsl:attribute name="r">
                <xsl:value-of select="$titlep_value3_final"/>
            </xsl:attribute>
            <xsl:attribute name="b">
                <xsl:value-of select="$titlep_value3"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </line>
    </xsl:template>
    
    <xsl:template match="span[@class='ocrx_word']">
        <formatting>
            <xsl:if test="ancestor::p[@class='ocr_par' and @lang='eng']">
                <xsl:attribute name="lang"><xsl:value-of select="'EnglishUnitedStates'"/></xsl:attribute>
                <xsl:attribute name="ff"><xsl:value-of select="'Times New Roman'"/></xsl:attribute>
                <xsl:attribute name="fs"><xsl:value-of select="'13.'"/></xsl:attribute>
            </xsl:if>
            <xsl:apply-templates/>
        </formatting>
    </xsl:template>
    <xsl:template match="strong">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="span[@class='ocrx_word']">
        <xsl:variable name="titlep_value">
            <xsl:value-of select="substring-before(substring-after(@title,'bbox '),';')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value1">
            <xsl:value-of select="substring-after($titlep_value,' ')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value1_final">
            <xsl:value-of select="substring-before($titlep_value,' ')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value2">
            <xsl:value-of select="substring-after($titlep_value1,' ')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value2_final">
            <xsl:value-of select="substring-before($titlep_value1,' ')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value3">
            <xsl:value-of select="substring-after($titlep_value2,' ')"/>
        </xsl:variable>
        <xsl:variable name="titlep_value3_final">
            <xsl:value-of select="substring-before($titlep_value2,' ')"/>
        </xsl:variable>
        <xsl:variable name="title_font_family_value">
            <xsl:value-of select="substring-before(substring-after(@title,'x_font '),';')"/>
        </xsl:variable>
        <xsl:variable name="title_font_size_value">
            <xsl:value-of select="substring-after(@title,'x_fsize ')"/>
        </xsl:variable>
        <formatting>
            <xsl:attribute name="lang">
                <xsl:value-of select="ancestor::p/@lang"/>
            </xsl:attribute>
            <xsl:attribute name="ff">
                <xsl:value-of select="$title_font_family_value"/>
            </xsl:attribute>
            <xsl:attribute name="fs">
                <xsl:value-of select="$title_font_size_value"/>
            </xsl:attribute>
            <xsl:if test="child::strong">
                <xsl:attribute name="bold">
                    <xsl:choose>
                        <xsl:when test="strong">1</xsl:when>
                    </xsl:choose>
                </xsl:attribute>
            </xsl:if>
            <word>
                <xsl:attribute name="l">
                    <xsl:value-of select="$titlep_value1_final"/>
                </xsl:attribute>
                <xsl:attribute name="t">
                    <xsl:value-of select="$titlep_value2_final"/>
                </xsl:attribute>
                <xsl:attribute name="r">
                    <xsl:value-of select="$titlep_value3_final"/>
                </xsl:attribute>
                <xsl:attribute name="b">
                    <xsl:value-of select="$titlep_value3"/>
                </xsl:attribute>
                <xsl:if test="child::strong">
                    <xsl:attribute name="bold">
                        <xsl:variable name="chr_length1">
                            <xsl:value-of select="string-length(child::strong)"/>
                        </xsl:variable>
                        <xsl:variable name="chr_length2">
                            <xsl:value-of select="$chr_length1 - 1"/>
                        </xsl:variable>
                        <xsl:for-each select="0 to $chr_length2">
                            <xsl:value-of select="position()-1"/>
                            <xsl:if test=".!=$chr_length2">
                                <xsl:text>,</xsl:text>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:attribute>
                </xsl:if>
                <xsl:attribute name="cc">
                    <xsl:variable name="chr_length">
                        <xsl:choose>
                            <xsl:when test="child::strong">
                                <xsl:value-of select="string-length(child::strong)"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="string-length(.)"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
					<xsl:variable name="wc_length">
					<xsl:value-of select="substring-before(substring-after(@title,'x_wconf '),';')"/>
					</xsl:variable>
                    <xsl:for-each select="1 to $chr_length">
                        <xsl:value-of select="$wc_length"/>
                        <xsl:if test=".!=$chr_length">
                            <xsl:text>, </xsl:text>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:attribute>
                <xsl:attribute name="wc">
                    <xsl:value-of select="substring-before(substring-after(@title,'x_wconf '),';')"/>
                </xsl:attribute>
                <xsl:apply-templates/>
            </word>
        </formatting>
    </xsl:template>
    
</xsl:stylesheet>