﻿CKEDITOR.plugins.add('saveHTML',
{
    init: function (editor) {
        var pluginName = 'saveHTML';
        editor.ui.addButton('saveHTML',
            {
                label: 'Save HTML File',
                command: 'saveHTML',
                icon: CKEDITOR.plugins.getPath('saveHTML') + 'save.png'
            });
        var cmd = editor.addCommand('saveHTML', { exec: saveHTMLFile });
    }
});
// function showMyDialog(e) {
    
// }