﻿/**
 * Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

/* exported initSample */

if ( CKEDITOR.env.ie && CKEDITOR.env.version < 9 )
	CKEDITOR.tools.enableHtml5Elements( document );

// The trick to keep the editor in the sample quite small
// unless user specified own height.
CKEDITOR.config.height ='72vh';
CKEDITOR.config.width = 'auto';

var initSample = ( function() {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );

	return function() {
		var editorElement = CKEDITOR.document.getById( 'editor' );

		// :(((
		if ( isBBCodeBuiltIn ) {
			editorElement.setHtml(
				'Hello world!\n\n' +
				'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
			);
		}

		// Depending on the wysiwygarea plugin availability initialize classic or inline editor.
		if ( wysiwygareaAvailable ) {
			var editor = CKEDITOR.replace( 'editor1',{
				
				contentsCss: [ 
								'https://cdn.ckeditor.com/4.8.0/full-all/contents.css', 'mystyle.css',
								'bower_components/ckeditor/CustomStyle.css',
							],
				
				
				//allowedContent     : true,
                //removePlugins     : 'forms',
				
				// This is optional, but will let us define multiple different styles for multiple editors using the same CSS file.
				bodyClass: 'document-editor',
				
				
				
 
				// Reduce the list of block elements listed in the Format dropdown to the most commonly used.
				format_tags: 'p;h1;h2;h3;pre',
				stylesSet: [
			/* Inline Styles */
			{ name: 'Marker', element: 'span', attributes: { 'class': 'marker' } },
			{ name: 'Cited Work', element: 'cite' },
			{ name: 'Inline Quotation', element: 'q' },

			/* Object Styles */
			{
				name: 'Special Container',
				element: 'div',
				styles: {
					padding: '5px 10px',
					background: '#eee',
					border: '1px solid #ccc'
				}
			},
			{
				name: 'Tablehead',
				element: 'p',
				attributes: { 'class': 'Tablehead' } ,
				styles: {
					padding: '0px 0px 0px 0in',
					'font-weight':'bold',
					'text-align':'center'
					
				}
			
			},
			{ name: 'Borderless Table', element: 'table', styles: { 'border-style': 'hidden', 'background-color': '#E6E6FA' } },
			{ name: 'Square Bulleted List', element: 'ul', styles: { 'list-style-type': 'square' } }
		],on: {
					key: function( event ) {
						if ( event.data.keyCode == CKEDITOR.CTRL + CKEDITOR.SHIFT + 49 ) {                
							 
		                   FormatDocument("s");
						}
						else if ( event.data.keyCode == CKEDITOR.CTRL + CKEDITOR.SHIFT + 50 ) {                
							 
		                  
		                   FormatDocument("sub");
						}
						else if ( event.data.keyCode == CKEDITOR.CTRL + CKEDITOR.SHIFT + 51 ) {                
							 
		                  
		                   FormatDocument("sup");
						}
						else if ( event.data.keyCode == CKEDITOR.CTRL + CKEDITOR.SHIFT + 52 ) {                
		                   InsertPageBreak();
						}
						else if ( event.data.keyCode == CKEDITOR.CTRL + CKEDITOR.SHIFT + 53 ) {                
		                   ConvertTextToTable();
						}
						else if ( event.data.keyCode == CKEDITOR.CTRL + CKEDITOR.SHIFT + 54 ) {                
		                   ShowDoublespace();
						}
						else if ( event.data.keyCode == CKEDITOR.CTRL + CKEDITOR.SHIFT + 55 ) {                
		                   HideMarker();
						}
						else if ( event.data.keyCode == CKEDITOR.CTRL + CKEDITOR.SHIFT + 56 ) {                
		                   SmallCaps();
						}
						else if ( event.data.keyCode == CKEDITOR.CTRL + CKEDITOR.SHIFT + 57 ) {                
		                  HideTrackChange();
						}
						else if ( event.data.keyCode == CKEDITOR.CTRL + CKEDITOR.SHIFT + 81 ) {                
		                  ShowTrackChange();
						}
						else if ( event.data.keyCode == CKEDITOR.CTRL + CKEDITOR.ALT + 77 ) {                
		                  HighlightText();
						}
						else if ( event.data.keyCode == CKEDITOR.CTRL + CKEDITOR.ALT + 78 ) {                
		                  RemoveEmphasis();
						}
						else if ( event.data.keyCode == CKEDITOR.CTRL + CKEDITOR.ALT +67 ) {                
		                    CopyWithFormatting();
		                    // alert("File successfully saved!");
						} 
						else if ( event.data.keyCode == CKEDITOR.CTRL + CKEDITOR.ALT +86 ) {                
		                    PasteWithFormatting();
		                    // alert("File successfully saved!");
						} 
						else if ( event.data.keyCode == CKEDITOR.CTRL + CKEDITOR.ALT +83 ) {                
		                    SectionDetection();
		                    // alert("File successfully saved!");
						} 
						else if ( event.data.keyCode == CKEDITOR.CTRL + CKEDITOR.SHIFT +83 ) {                
		                    saveHTMLFile();
		                    // alert("File successfully saved!");
						} 
					}
				}

			}
		);
		} else {
			editorElement.setAttribute( 'contenteditable', 'true' );
			CKEDITOR.inline( 'editor1' );

			// TODO we can consider displaying some info box that
			// without wysiwygarea the classic editor may not work.
		}
		
	};


function PasteWithFormatting(){
	var editor =CKEDITOR.instances['editor1'];
		var selectedHtml = "";
		var selection = editor.getSelection();
	 
		if (selection) {
		    selectedHtml = getSelectionHtml(selection);

		}
		 
	 	  
	 	editor.insertHtml(document.getElementById("clipval").value);
}

function CopyWithFormatting(){
	var editor =CKEDITOR.instances['editor1'];
		var selectedHtml = "";
		var selection = editor.getSelection();
	 	alert(selection);
		if (selection) {
		    selectedHtml = getSelectionHtml(selection);

		}
		 
	 	var strHTML;

		strHTML= selectedHtml;
		alert(strHTML);
		document.getElementById("clipval").value =strHTML;
	 	strHTML ="";
	 	editor.insertHtml(strHTML);
}
function SectionDetection(){
	$("#modal-progress").modal();
	document.getElementById("pgCaption").innerHTML="Detecting Section.";
	var BookID= document.getElementById("BookID").value;
    var Jobname= document.getElementById("Jobname").value;
    JobID = document.getElementById("GGJobID").value;
    FilenameWithoutExtension=document.getElementById("FilenameWithoutExtension").value;
    Filename = document.getElementById("filename").innerHTML;

    var jTextArea = CKEDITOR.instances['editor1'].getData();
    var data = 'data='+encodeURIComponent(jTextArea)+"&filename="+encodeURIComponent(Filename)+"&BookID="+BookID+"&Jobname="+Jobname;
   
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange=function(){
      if (xmlhttp.readyState==4 && xmlhttp.status==200){
        //response.innerHTML=xmlhttp.responseText;
          $('#modal-progress').modal('hide');
          CKEDITOR.instances.editor1.setData(xmlhttp.responseText); 
      }
    }
    xmlhttp.open("POST","SectionDetection.php",true);
          //Must add this request header to XMLHttpRequest request for POST
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(data);


}

 
function RemoveEmphasis(){
 
		var editor =CKEDITOR.instances['editor1'];
		var selectedHtml = "";
		var selection = editor.getSelection();
	 
		if (selection) {
		    selectedHtml = getSelectionHtml(selection);

		}
		 
	 	var strHTML;

		strHTML= selectedHtml;
		strHTML=  selectedHtml.replace("<p>","##p##");
		strHTML=  strHTML.replace("</p>","##/p##");

		strHTML=  strHTML.replace("<u>","##u##");
		strHTML=  strHTML.replace("</u>","##/u##");

		strHTML=  strHTML.replace("<i>","##i##");	
		strHTML=  strHTML.replace("</i>","##/i##");

		strHTML=  strHTML.replace("<b>","##b##");	
		strHTML=  strHTML.replace("</b>","##/b##");

		strHTML=  strHTML.replace("<em>","");	
		strHTML=  strHTML.replace("</em>","");
	 		 	

		strHTML=  strHTML.replace("##p##","<p>");
		strHTML=  strHTML.replace("##/p##","</p>\r\n");

		strHTML=  strHTML.replace("##u##","<u>");
		strHTML=  strHTML.replace("##/u##","</u>");

		strHTML=  strHTML.replace("##i##","<i>");	
		strHTML=  strHTML.replace("##/i##","</i>");

		strHTML=  strHTML.replace("##b##","<b>");	
		strHTML=  strHTML.replace("##/b##","</b>");
	 
	 	editor.insertHtml(strHTML);
	}


function HideTrackChange(){
		 
		var value = CKEDITOR.instances['editor1'].getData();
	 
	  	 
	  	strHTML=  value.replace(/ice-ins/gi,"ice-insa");
	  	strHTML=  strHTML.replace(/ice-del/gi,"ice-dela");
	 	 
		// editor_html.replaceSelection(strVal);
	 	 CKEDITOR.instances.editor1.setData(strHTML); 



	}
function ShowTrackChange(){
		  
		var value = CKEDITOR.instances['editor1'].getData();
	 	
	  
	 	strHTML=  value.replace(/ice-insa/gi,"ice-ins");
	  	strHTML=  strHTML.replace(/ice-dela/gi,"ice-del");
	 	
		// editor_html.replaceSelection(strVal);
	 	 CKEDITOR.instances.editor1.setData(strHTML); 



}
function VerticalTextOrientation(){
 
		var editor =CKEDITOR.instances['editor1'];
		var selectedHtml = "";
		var selection = editor.getSelection();
	 
		if (selection) {
		    selectedHtml = getSelectionHtml(selection);

		}
		 
	 	var strHTML;

		strHTML= selectedHtml;
		strHTML=  selectedHtml.replace("<p>","##p##");
		strHTML=  strHTML.replace("</p>","##/p##");

		strHTML=  strHTML.replace("<u>","##u##");
		strHTML=  strHTML.replace("</u>","##/u##");

		strHTML=  strHTML.replace("<i>","##i##");	
		strHTML=  strHTML.replace("</i>","##/i##");

		strHTML=  strHTML.replace("<b>","##b##");	
		strHTML=  strHTML.replace("</b>","##/b##");
	 	strHTML = '<span style="writing-mode: sideways-lr;">'+strHTML + "</span>";

		strHTML=  strHTML.replace("##p##","<p>");
		strHTML=  strHTML.replace("##/p##","</p>\r\n");

		strHTML=  strHTML.replace("##u##","<u>");
		strHTML=  strHTML.replace("##/u##","</u>");

		strHTML=  strHTML.replace("##i##","<i>");	
		strHTML=  strHTML.replace("##/i##","</i>");

		strHTML=  strHTML.replace("##b##","<b>");	
		strHTML=  strHTML.replace("##/b##","</b>");
	 
	 	editor.insertHtml(strHTML);
	}


function HighlightText(){
 	
		var editor =CKEDITOR.instances['editor1'];
		var selectedHtml = "";
		var selection = editor.getSelection();
	 	alert (selection);
		if (selection) {
		    selectedHtml = getSelectionHtml(selection);

		}
		 
	 	var strHTML;

		strHTML= selectedHtml;
		strHTML=  selectedHtml.replace("<p>","##p##");
		strHTML=  strHTML.replace("</p>","##/p##");

		strHTML=  strHTML.replace("<u>","##u##");
		strHTML=  strHTML.replace("</u>","##/u##");

		strHTML=  strHTML.replace("<i>","##i##");	
		strHTML=  strHTML.replace("</i>","##/i##");

		strHTML=  strHTML.replace("<b>","##b##");	
		strHTML=  strHTML.replace("</b>","##/b##");
	 	strHTML = '<span class="marker">'+strHTML + "</span>";

		strHTML=  strHTML.replace("##p##","<p>");
		strHTML=  strHTML.replace("##/p##","</p>\r\n");

		strHTML=  strHTML.replace("##u##","<u>");
		strHTML=  strHTML.replace("##/u##","</u>");

		strHTML=  strHTML.replace("##i##","<i>");	
		strHTML=  strHTML.replace("##/i##","</i>");

		strHTML=  strHTML.replace("##b##","<b>");	
		strHTML=  strHTML.replace("##/b##","</b>");
	 
	 	editor.insertHtml(strHTML);
	}


	function ShowDoublespace(){
		 
		var value = CKEDITOR.instances['editor1'].getData();
	 
	 	// strHTML=  value.replace(/  /gi," ");
	 	strHTML=  value.replace(/ /gi,"&nbsp;");
	 	strHTML=  strHTML.replace(/&nbsp;&nbsp;/gi,"<mark>##</mark>");
	 	strHTML=  strHTML.replace(/&nbsp;/gi," ");
	 	strHTML=  strHTML.replace(/<mark>##<\/mark>/gi,"<mark>&nbsp;&nbsp;</mark>");
	 	// strHTML=  strHTML.replace(/<mark>&nbsp;&nbsp;<\/mark>/gi,);
		// editor_html.replaceSelection(strVal);
	 	 CKEDITOR.instances.editor1.setData(strHTML); 



	}
	function HideMarker(){
		 
		var value = CKEDITOR.instances['editor1'].getData();
	 
	  
	 	strHTML=  value.replace(/<mark>/gi,"");
	 	strHTML=  strHTML.replace(/<\/mark>/gi,"");
	 	 
	 	
		// editor_html.replaceSelection(strVal);
	 	 CKEDITOR.instances.editor1.setData(strHTML); 



	}

	function ConvertTextToTable(){
	 
		var editor =CKEDITOR.instances['editor1'];
		var selectedHtml = "";
		var selection = editor.getSelection();
	 
		if (selection) {
		    selectedHtml = getSelectionHtml(selection);

		}
		 
	 	var strHTML;

		strHTML= selectedHtml;
		 
		 
		strHTML=strHTML.replace(/(<(p[^>]+)>)/gi, "<p>");
		strHTML=strHTML.replace(/(<(span[^>]+)>)/gi, "");
		strHTML=  strHTML.replace(/<\/span>/gi,"");
		strHTML=  strHTML.replace(/&nbsp;/gi," ");
		 
		 
		strHTML=  strHTML.replace(/<u>/gi,"##u##");
		strHTML=  strHTML.replace(/<\/u>/gi,"##/u##");

		strHTML=  strHTML.replace(/<i>/gi,"##i##");	
		strHTML=  strHTML.replace(/<\/i>/gi,"##/i##");

		strHTML=  strHTML.replace(/<b>/gi,"##b##");	
		strHTML=  strHTML.replace(/<\/b>/gi,"##/b##");
	 	

		strHTML = strHTML.replace(/<p><br><\/p>/gi,"</tr><tr>")
		strHTML = strHTML.replace(/<p><\/p>/gi,"</tr><tr>")
		strHTML = strHTML.replace(/<p> <\/p>/gi,"</tr><tr>")
		strHTML = strHTML.replace(/<p>/gi,"<td>")
		strHTML = strHTML.replace(/<\/p>/gi,"</td>")
		// strHTML=  strHTML.replace("##p##","<p>");
		// strHTML=  strHTML.replace("##/p##","</p>\r\n");

		strHTML=  strHTML.replace(/##u##/gi,"<u>");
		strHTML=  strHTML.replace(/##\/u##/gi,"</u>");

		strHTML=  strHTML.replace(/##i##/gi,"<i>");	
		strHTML=  strHTML.replace(/##\/i##/gi,"</i>");

		strHTML=  strHTML.replace(/##b##/,"<b>");	
		strHTML=  strHTML.replace(/##\/b##/gi,"</b>");
	  	strHTML ="<table><tr>"+strHTML+"</tr></table>";
	  	 
	 	editor.insertHtml(strHTML);
	}
	function InsertPageBreak(){
	 
		var editor =CKEDITOR.instances['editor1'];
		var selectedHtml = "";
		var selection = editor.getSelection();
	 
		if (selection) {
		    selectedHtml = getSelectionHtml(selection);

		}
		 
	 	var strHTML;

		strHTML= selectedHtml;
		alert(strHTML);
		 
	 	strHTML = '<p>"'+strHTML + "</p>";

		 
	  
	 	editor.insertHtml(strHTML);
	}
	function FormatDocument(prStyle){
		var editor =CKEDITOR.instances['editor1'];
		var selectedHtml = "";
		var selection = editor.getSelection();
	 
		if (selection) {
		    selectedHtml = getSelectionHtml(selection);

		}
		 
	 	var strHTML;

		strHTML= selectedHtml;
		strHTML=  selectedHtml.replace("<p>","##p##");
		strHTML=  strHTML.replace("</p>","##/p##");

		strHTML=  strHTML.replace("<u>","##u##");
		strHTML=  strHTML.replace("</u>","##/u##");

		strHTML=  strHTML.replace("<i>","##i##");	
		strHTML=  strHTML.replace("</i>","##/i##");

		strHTML=  strHTML.replace("<b>","##b##");	
		strHTML=  strHTML.replace("</b>","##/b##");
	 	strHTML = '<'+ prStyle +'>'+strHTML + "</" + prStyle + ">";

		strHTML=  strHTML.replace("##p##","<p>");
		strHTML=  strHTML.replace("##/p##","</p>\r\n");

		strHTML=  strHTML.replace("##u##","<u>");
		strHTML=  strHTML.replace("##/u##","</u>");

		strHTML=  strHTML.replace("##i##","<i>");	
		strHTML=  strHTML.replace("##/i##","</i>");

		strHTML=  strHTML.replace("##b##","<b>");	
		strHTML=  strHTML.replace("##/b##","</b>");
	 
	 	editor.insertHtml(strHTML);
	}

	function SmallCaps(){
 
		var editor =CKEDITOR.instances['editor1'];
		var selectedHtml = "";
		var selection = editor.getSelection();
	 
		if (selection) {
		    selectedHtml = getSelectionHtml(selection);

		}
		 
	 	var strHTML;

		strHTML= selectedHtml;
		strHTML=  selectedHtml.replace("<p>","##p##");
		strHTML=  strHTML.replace("</p>","##/p##");

		strHTML=  strHTML.replace("<u>","##u##");
		strHTML=  strHTML.replace("</u>","##/u##");

		strHTML=  strHTML.replace("<i>","##i##");	
		strHTML=  strHTML.replace("</i>","##/i##");

		strHTML=  strHTML.replace("<b>","##b##");	
		strHTML=  strHTML.replace("</b>","##/b##");
	 	strHTML = '<span style="font-variant:small-caps">'+strHTML + "</span>";

		strHTML=  strHTML.replace("##p##","<p>");
		strHTML=  strHTML.replace("##/p##","</p>\r\n");

		strHTML=  strHTML.replace("##u##","<u>");
		strHTML=  strHTML.replace("##/u##","</u>");

		strHTML=  strHTML.replace("##i##","<i>");	
		strHTML=  strHTML.replace("##/i##","</i>");

		strHTML=  strHTML.replace("##b##","<b>");	
		strHTML=  strHTML.replace("##/b##","</b>");
	 
	 	editor.insertHtml(strHTML);
	}

	function isWysiwygareaAvailable() {
		// If in development mode, then the wysiwygarea must be available.
		// Split REV into two strings so builder does not replace it :D.
		if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
			return true;
		}

		return !!CKEDITOR.plugins.get( 'wysiwygarea' );
	}
} )();

