CKEDITOR.on( 'instanceReady', function(e) {
 var e = e.editor;
	
	e.removeMenuItem('removediv');
	e.removeMenuItem('RemoveSpan');
	e.removeMenuItem('RemoveSection');
	
	//e.removeMenuItem('Editattribute');
	
	
	//Men Groups; different numbers needed for the group separator to show
	e.addMenuGroup("section", 500);
	e.addMenuGroup("div", 500);
	e.addMenuGroup("span", 500);
	e.addMenuGroup("RemoveDiv", 500);
	e.addMenuGroup("RemoveSpan", 500);
	e.addMenuGroup("RemoveSection", 500);
	e.addMenuGroup("Editattribute", 500);
	
	//Child Menu Groups; different numbers needed for the group separator to show
	e.addMenuGroup("group_p", 200);
	e.addMenuGroup("group_div", 200);
	e.addMenuGroup("group_span", 200);


	// Listener
	e.contextMenu.addListener(function(element, selection) {
		
		var selections = e.elementPath();
					
					console.log(selections);
					
					var lastElement = selections.lastElement;
					var SPAN = lastElement.$.nodeName;
					
					console.log('span = ' + SPAN);
					
					
					//console.log("className="+selections.blockLimit.$.className);
					console.log("nodeName  ="+selections.blockLimit.$.nodeName);
					
					var nodeName = selections.blockLimit.$.nodeName;
					var className = selections.blockLimit.$.className;
					var randomid =	selections.blockLimit.$.dataset.randomid;
					
			

							var editor = CKEDITOR.instances['editor1'];
							var selection = editor.getSelection();
							
							var selection = editor.getSelection();
							var ranges = selection.getRanges();
							var el = new CKEDITOR.dom.element("div");
							
							for (var i = 0, len = ranges.length; i < len; ++i) {
								el.append(ranges[i].cloneContents());
							}
							selectedHtml = el.getHtml();
							
						
							var res = selectedHtml.substr(0,11);
							var n = selectedHtml.startsWith('<div class=');
							
							//DIV
							if( (selectedHtml.length  == 0 && nodeName == "DIV") )
							{
								var editvalue = "CKEDITOR.TRISTATE_OFF";
								var removevalue = "CKEDITOR.TRISTATE_OFF";
							}
							else{
								var editvalue = "";
								var removevalue = "";
							}
						
							var removepsan = "CKEDITOR.TRISTATE_OFF";
							
							var editAttribute = "CKEDITOR.TRISTATE_OFF";
							
							/*
							if((SPAN == "SPAN" && selectedHtml.length  > 0))
							{
								var editAttribute = "CKEDITOR.TRISTATE_OFF";
							}
							else
							{
								var editAttribute = ""; 
							}
							*/
							
							
							
							
							//Section
							if( (selectedHtml.length  == 0 && nodeName == "SECTION") )
							{
								var removesection = "CKEDITOR.TRISTATE_OFF";
							}
							else{
								var removesection = "";
							}
		
		
		
		return {
			RemoveDiv     : removevalue,
			RemoveSpan 	  : removepsan,
			RemoveSection : removesection,
			Editattribute : editAttribute,
			section 	  : CKEDITOR.TRISTATE_ON,	
			div 		  : CKEDITOR.TRISTATE_ON,
			span 		  : CKEDITOR.TRISTATE_ON,	
		};
	});
	
		e.addMenuItems({
			RemoveSection: {
					label: "Remove Section",
					group: "RemoveSection",
					command : "add_RemoveSection",
					order:1
				}
		});
		
		
		e.addMenuItems({
			RemoveDiv: {
					label: "Remove div",
					group: "RemoveDiv",
					command : "removediv",
					order:1
				}
		});
		
		e.addMenuItems({
				    RemoveSpan: {
				        label: "Remove Span",
				        group: "RemoveSpan",
						command : "add_RemoveSpan"
				    }
		});
		
		
		e.addMenuItems({
				    Editattribute: {
				        label: "Edit Attribute",
				        group: "Editattribute",
						command : "add_editAttribute"
				    }
		});

		
		//COMMAND
		e.addCommand("add_RemoveSection", {
				requiredContent: "section",
				exec: function(a) {

						var el = a.elementPath().contains('section');							
						var className = el.$.className
						var randomid =	el.$.dataset.randomid
						
						var editor = CKEDITOR.instances['editor1'];
						var selection = a.getSelection();
						var ranges = selection.getRanges();
						
						for (var i = 0, len = ranges.length; i < len; ++i) {
							el.append(ranges[i].cloneContents());
						}							
						var selectedHtml = el.getHtml();
		
						var replaced_text1 = '<input data-randomid="'+randomid+'" type="hidden" value="'+className+'" data-cke-editable="1" contenteditable="false">';
						var replaced_text2 = '<input type="hidden" value="'+className+'" data-randomid="'+randomid+'" data-cke-editable="1" contenteditable="false">';						
						var finaltext = selectedHtml.replace(replaced_text1,"").replace(replaced_text2,"");
						
						Swal.fire({
							type:'question',
							title:'',
							html:'Are you Sure, you want to remove this tag ('+className+') ?',
							showCancelButton: true,
							confirmButtonText: 'Yes',
							confirmButtonColor: '#d33',
							cancelButtonText: 'No'
						}).then(function(isConfirm){
							
							el.remove();
							editor.insertHtml(finaltext);
						
							
						});
						
				}
		});
			
		e.addCommand("add_RemoveSpan", {
				requiredContent: "span",
				exec: function(a) {

						var el = a.elementPath().contains('span');							
						var className = el.$.className
						var randomid =	el.$.dataset.randomid
						
						var editor = CKEDITOR.instances['editor1'];
						var selection = a.getSelection();
						var ranges = selection.getRanges();
						
						for (var i = 0, len = ranges.length; i < len; ++i) {
							el.append(ranges[i].cloneContents());
						}							
						var selectedHtml = el.getHtml();
		
						var replaced_text1 = '<input data-randomid="'+randomid+'" type="hidden" value="'+className+'" data-cke-editable="1" contenteditable="false">';
						
						
						var replaced_text2 = '<input type="hidden" value="'+className+'" data-randomid="'+randomid+'" data-cke-editable="1" contenteditable="false">';	

						
						var finaltext = selectedHtml.replace(replaced_text1,"").replace(replaced_text2,"");
						
						Swal.fire({
							allowOutsideClick: false,
            				type:'question',
							title:'',
							html:'Are you Sure, you want to remove this tag ('+className+') ?',
							showCancelButton: true,
							confirmButtonText: 'Yes',
							confirmButtonColor: '#d33',
							cancelButtonText: 'No'
						}).then(function(isConfirm){
							
							el.remove();
							editor.insertHtml(finaltext);
						
							
						});
						
				}
			});
			
			
			e.addCommand("removediv", {			
				requiredContent: "div",
				exec:function(a){
							
								var el = a.elementPath().contains('div');							
								var className = el.$.className;
								var randomid =	el.$.dataset.randomid;
								
								
								var editor = CKEDITOR.instances['editor1'];
																
								var selection = a.getSelection();
								var ranges = selection.getRanges();
								
								for (var i = 0, len = ranges.length; i < len; ++i) {
									el.append(ranges[i].cloneContents());
								}
								var selectedHtml = el.getHtml();
																
								var replaced_text1 = '<input data-randomid="'+randomid+'" type="hidden" value="'+className+'" data-cke-editable="1" contenteditable="false">';
								var replaced_text2 = '<input type="hidden" value="'+className+'" data-randomid="'+randomid+'" data-cke-editable="1" contenteditable="false">';
								var finaltext = selectedHtml.replace(replaced_text1,"").replace(replaced_text2,"");

								
			
							Swal.fire({
								allowOutsideClick: false,
            					type:'question',
								title:'',
								text:'Are you Sure, you want to remove this DIV ('+className+') ?',
								showCancelButton: true,
								confirmButtonText: 'Yes',
								confirmButtonColor: '#d33',
								cancelButtonText: 'No'
							}).then(function(isConfirm){
								
								
								el.remove();
								editor.insertHtml(finaltext);
							
								
							});

				}
			});
			
			
			e.addCommand("add_editAttribute", {			
				requiredContent: "div",
				exec:function(a){
							var el = a.elementPath().contains('div');
							console.log(el);					
								
							if(el === null){
								console.log('here1');
								el = a.elementPath().contains('span');	
								if(el === null){
									el = a.elementPath().contains('section');	
								}
							}
							
							var className = el.$.className;
							var randomid =	el.$.dataset.randomid
							var attribute = el.$.attributes;
							
							const attrs = {}
							$.each(attribute, (index, attribute) => {							  
							  attrs[index] = attribute;
							});
							
							var atr= []
							$.each(attrs, (index, attr) => {							  
							
								atr.push({
										
										attribute :  attr.nodeName,
										value 	  : attr.nodeValue
									
								});
								
							});
							
							edit_attribute_modal(className,randomid,atr);
							
							
				}
			});

			
	
	
	
	e.addCommand("EditAttribute", {            
                requiredContent: "div",
                exec:function(a){
                            
                            var el = a.elementPath().contains('span'); 
                            var elementni = "span"; 
                            if(el === null){
                            	el = a.elementPath().contains('section'); 
                            	elementni = "section"; 
                            
                            	if(el === null){
                            		el = a.elementPath().contains('div'); 
                            		elementni = "div"; 
                            	}
                            }
                            var className = el.$.className;
                            var randomid =    el.$.dataset.randomid;
                            var attribute = el.$.attributes;
 

							let randomCode  = Math.floor(Math.random() * 9999999);
							var tag = $('#tagname').val()
							var editor = CKEDITOR.instances['editor1'];
															
							var selection = a.getSelection();
							var ranges = selection.getRanges();
							
							for (var i = 0, len = ranges.length; i < len; ++i) {
								el.append(ranges[i].cloneContents());
							}
							var selectedHtml = el.getHtml();
							var attribute = "";                                        
							var titleattribute = "";
							
							$('#EditInputform input').each(function(index){  
								var input = $(this);        
								if((input.val().trim()).length > 0){
									attribute += input.attr('name')+ "=" +"'"+input.val()+"'"+ " ";
								}
								var stringName = input.attr('name');
								var substringName = "title";
								
								if( stringName.includes(substringName) == true )
								{
									stringName = 'title';
								}
								
								titleattribute += stringName + "=" +"'"+input.val()+"'"+ " ";
									
							});
							
													
							var strHTML= '<'+elementni+' class='+tag+'  '+attribute+' data-randomID='+randomCode+'  >' + selectedHtml + '<input type="hidden" value='+tag+' data-randomID='+randomCode+'  /></'+elementni+'>';
							
							el.remove();
							editor.insertHtml(strHTML);
							$('#myModalEdit').modal('hide');    
                                    
                }
    });

    
	
	
    $(document).on('click', '#applyAttributeEdit', function(){

        e.execCommand( 'EditAttribute' );
         return false;
    
    });
	

	$.post('bower_components/ckeditor/data.json', function(result){
					
				var sectionmenus = [];
				var divmenus = [];
				var spanmenus = [];	
				var submenuitems = {};
			
			$.each(result, function(key,valueObj){
					
				
				if(result[key].element=='div'){
					divmenus["add_"+valueObj.name ] = CKEDITOR.TRISTATE_OFF;
				}
				else if(result[key].element=='span'){
					spanmenus["add_"+valueObj.name ] = CKEDITOR.TRISTATE_OFF;
				}
				else if(result[key].element=='section'){
					sectionmenus["add_"+valueObj.name] = CKEDITOR.TRISTATE_OFF;
				}
				
				submenuitems["add_"+valueObj.name] = {
								label 	: valueObj.name ,
								group 	: "group_span",
								command : "add_"+valueObj.name
							}
				
				
				
				if(result[key].hasattribute == 0){
					
							e.addCommand("add_"+valueObj.name , {
								exec: function(e) {
									addstyle(result[key].element, result[key].tag)
								}
							});
							
				}else{
						e.addCommand("add_"+valueObj.name, 
							{
								exec: function(e) {
									add_attribute_modal(result[key].element,result[key].tag);
								}
							});						
				}
				
				
				
								
				
			});
			e.addMenuItems({
					section: {
						label: "Section Style",
						group: "section",
						getItems: function() {
							return sectionmenus;
						}
					}
			});
			e.addMenuItems({
					div: {
						label: "(Block) DIV Style",
						group: "div",
						getItems: function() {
							return divmenus;
						}
					}
			});
			e.addMenuItems({
				span: {
					label: "(Inline) SPAN Style",
					group: "span",
					getItems: function() {
						return spanmenus;
					}
				}
			});
			
			e.addMenuItems(submenuitems);


	});


});   



function edit_attribute_modal(name,id,attributes) 
{
		var label = '', type = '', title = '', date = '', filename = '';
		if(attributes.length > 0){
			$.each(attributes, function(i,v){
				console.log(i);
				console.log(v);
				if(v.attribute.toUpperCase()  == 'LABEL'){
					label = v.value;
				}

				if(v.attribute.toUpperCase()  == 'TYPE'){
					type = v.value;
				}

				if(v.attribute.toUpperCase()  == 'TITLE'){
					title = v.value;
				}

				if(v.attribute.toUpperCase()  == 'DATE'){
					date = v.value;
				}

				if(v.attribute.toUpperCase()  == 'FILENAME'){
					filename = v.value;
				}
			});
		}
		var html = '<p>'+name+'</p>'
		html = '<input type="hidden" value="'+name+'" id="tagname">'

		html +=  '<form id="EditInputform">'+
			
			'<div class="form-group">'+
				'<label for="attribute">LABEL:</label>'+
				'<input type="text" class="form-control input-sm"  name="LABEL" id="LABEL" value="'+label+'">'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">TYPE:</label>'+
				'<input type="text" class="form-control input-sm"  name="TYPE" id="TYPE" value="'+type+'">'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">TITLE:</label>'+
				'<input type="text" class="form-control input-sm"  name="TITLE" id="TITLE" value="'+title+'">'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">DATE:</label>'+
				'<input type="text" class="form-control input-sm"  name="DATE" id="DATE" value="'+date+'">'+
			'</div>'+

			'<div class="form-group">'+
				'<label for="attribute">FILENAME:</label>'+
				'<input type="text" class="form-control input-sm"  name="FILENAME" id="FILENAME" value="'+filename+'">'+
			'</div>'+
			'</form>'
			
			
		$('#myModalEdit .modal-title').empty().html("Edit attribute - " + name);
		$('#myModalEdit .modal-body').html(html)
		$('#myModalEdit').modal();
		
		$.each(attributes, (index, attr) => {	
			if(((attr.value).trim()).length > 0){						  
				$("input#"+attr.attribute).val(attr.value);
			}
		});


}


function add_attribute_modal(category, name) {
		
		console.log('cat' + category);
		
		var html = '<p>'+name+'</p>'
		html = '<input type="hidden" value="'+name+'" id="tagname">'
		html += '<input type="hidden" value="'+category+'" id="categoryname">'
		
		
		
		if(name=='pub_cite')
		{			
					
			html +=  '<form id="inputfom">'+
			
			'<div class="form-group">'+
				'<label for="attribute">title:</label>'+
				'<input type="text" class="form-control input-sm" id="value" name="title" id="title" required>'+
			'</div>'+
				
			
			'</form>'
			
		}
		
		else if(name=='grey_material_cite')
		{
						
			
			html +=  '<form id="inputfom">'+
			
			'<div class="form-group">'+
				'<label for="attribute">title:</label>'+
				'<input type="text" class="form-control input-sm" id="value" name="title" id="title" required>'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">year:</label>'+
				'<input type="text" class="form-control input-sm" id="value" name="year" id="year" required>'+
			'</div>'+
			
		
			
			'</form>'
			
			
			
		}
		
		else if(name=='abbrev')
		{
			
			
			html +=  '<form id="inputfom">'+
			
			'<div class="form-group">'+
				'<label for="attribute">definition:</label>'+
				'<input type="text" class="form-control input-sm" id="value" name="definition" id="definition" required>'+
			'</div>'+
		
			'</form>'
			
			
			
			
		}
		
		else if(name=='legis_cite')
		{
			
			html +=  '<form id="inputfom">'+
			
			'<div class="form-group">'+
				'<label for="attribute">type:</label>'+
				'<input type="text" class="form-control input-sm" id="value" name="type" id="type" required>'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">title:</label>'+
				'<input type="text" class="form-control input-sm" id="value" name="legistitle" id="legistitle" required>'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">oj-ref:</label>'+
				'<input type="text" class="form-control input-sm" id="value" name="oj-ref" id="oj-ref" required>'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">section:</label>'+
				'<input type="text" class="form-control input-sm" id="value" name="section" id="section" required>'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">year:</label>'+
				'<input type="text" class="form-control input-sm" id="value" name="year" id="year" required>'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">number:</label>'+
				'<input type="number" class="form-control input-sm" id="value" name="number" id="number" required>'+
			'</div>'+
			
			'</form>'
		}
		
		else if(name=='url')
		{
			

			html +=  '<form id="inputfom">'+
			
			'<div class="form-group">'+
				'<label for="attribute">xlink:type:</label>'+
				'<input type="text" class="form-control input-sm" id="xlink:type" name="xlink:type" id="type" required>'+
			'</div>'+
			
			'</form>'
			
			
			
		}
		
		else if(name=='caseref')
		{
			
	
			
			html +=  '<form id="inputfom">'+
			
			'<div class="form-group">'+
				'<label for="attribute">type:</label>'+
				'<input type="text" class="form-control input-sm"  name="type" id="type" required>'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">partya:</label>'+
				'<input type="text" class="form-control input-sm"  name="partya" id="partya" required>'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">partyb:</label>'+
				'<input type="text" class="form-control input-sm"  name="partyb" id="partyb" required>'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">primary-reference:</label>'+
				'<input type="text" class="form-control input-sm"  name="primary-reference" id="primary-reference" required>'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">case-no:</label>'+
				'<input type="text" class="form-control input-sm" name="case-no" id="case-no" required>'+
			'</div>'+
					
			'</form>'
			
			
			
			
			
		}
		else if(name=='journal_cite')
		{
			

			html +=  '<form id="inputfom">'+
			
			'<div class="form-group">'+
				'<label for="attribute">reference:</label>'+
				'<input type="text" class="form-control input-sm" id="reference" name="reference" id="type" required>'+
			'</div>'+
			
			'</form>'
			
			
			
		}
		else if(name=='legis_extract')
		{
			
	
			
			html +=  '<form id="inputfom">'+
			
			'<div class="form-group">'+
				'<label for="attribute">id:</label>'+
				'<input type="text" class="form-control input-sm"  name="id" id="id" required>'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">type:</label>'+
				'<input type="text" class="form-control input-sm"  name="type" id="type" required>'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">title:</label>'+
				'<input type="text" class="form-control input-sm"  name="legistitle" id="legistitle" required>'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">section:</label>'+
				'<input type="text" class="form-control input-sm"  name="section" id="section" required>'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">year:</label>'+
				'<input type="text" class="form-control input-sm"  name="year" id="year" required>'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">chapter:</label>'+
				'<input type="text" class="form-control input-sm"  name="chapter" id="chapter" required>'+
			'</div>'+
					
			'</form>'
			
			
			
			
			
		}
		else if(name=='grey_material_extract')
		{
			
	
			
			html +=  '<form id="inputfom">'+

			
			'<div class="form-group">'+
				'<label for="attribute">type:</label>'+
				'<input type="text" class="form-control input-sm"  name="type" id="type" required>'+
			'</div>'+
			
			'<div class="form-group">'+
				'<label for="attribute">title:</label>'+
				'<input type="text" class="form-control input-sm"  name="grey_materialtitle" id="grey_materialtitle" required>'+
			'</div>'+
			
		
			'</form>'
			
			
			
			
			
		}
		
		
		
		$('#myModal .modal-title').empty().html("Add attribute - " + name);
		$('#myModal .modal-body').empty().html(html);
		$('#myModal').modal();
		
}

/*
$(document).on('click', '#applyAttributeEdit', function(){
	
		let randomCode  = Math.floor(Math.random() * 9999999);
		var tag = $('#tagname').val();
		var category = $('#categoryname').val();
		
		
		console.log('category' + category);
		return false;
	
		var editor = CKEDITOR.instances['editor1'];
		var selectedHtml = "";
		var selection = editor.getSelection();
		var ranges = selection.getRanges();
		var el = new CKEDITOR.dom.element("span");
		
		
		//var hey = el.elementPath().contains('span');
		
		
		
		for (var i = 0, len = ranges.length; i < len; ++i) {
			el.append(ranges[i].cloneContents());
		}
		
		console.log('ranges');
		console.log(ranges);
		
		console.log('hey');
		console.log(el);
		
		
		selectedHtml = el.getHtml();
		console.log(selectedHtml);
		
		
		ranges.selectNodeContents( editor.document.getBody() );
		// Delete the contents.
		ranges.deleteContents();
		
		//ranges.remove();
		el.remove();
		editor.insertHtml(selectedHtml);
		$('#myModalEdit').modal('hide');
	

});
*/



$(document).on('click', '#applyAttribute-btn', function(){
	
		let randomCode  = Math.floor(Math.random() * 9999999);
		var tag = $('#tagname').val();
		var category = $('#categoryname').val();

	
		var editor = CKEDITOR.instances['editor1'];
		var selectedHtml = "";
		var selection = editor.getSelection();
		var ranges = selection.getRanges();
		var el = new CKEDITOR.dom.element("div");
		
		for (var i = 0, len = ranges.length; i < len; ++i) {
			el.append(ranges[i].cloneContents());
		}
		selectedHtml = el.getHtml();
			
		if(selectedHtml == "")
		{
			alert("select text first");
			return false;
		}
		else
		{
			
			
			var attribute = "";
			
			var titleattribute = "";
			
			$('#inputfom input').each(function(index){  
				var input = $(this);		
				
				attribute += input.attr('name')+ "=" +"'"+input.val()+"'"+ " ";
				
				var stringName = input.attr('name');
				var substringName = "title";
				
				if( stringName.includes(substringName) == true )
				{
					stringName = 'title';
				}
				
				titleattribute += stringName + "=" +"'"+input.val()+"'"+ " ";
					
			});
			
			
			if(category == "div")
			{
				
				var strHTML= '<div class='+tag+'  title="'+titleattribute+'" '+attribute+' data-randomID='+randomCode+' >' + selectedHtml + '<input type="hidden" value='+tag+' data-randomID='+randomCode+'  /></div><span>&nbsp;</span>';
			}
			else{
				var strHTML= '<span class='+tag+'  title="'+titleattribute+'" '+attribute+' data-randomID='+randomCode+' >' + selectedHtml + '<input type="hidden" value='+tag+' data-randomID='+randomCode+'  /></span>';
			}
			
			
			
	
			editor.insertHtml(strHTML);
			$('#myModal').modal('hide');	
			
		}

		
});


function addstyle(category, name){
	
		let randomCode  = Math.floor(Math.random() * 9999999);
		
		var editor = CKEDITOR.instances['editor1'];
		var selectedHtml = "";
		var selection = editor.getSelection();
		var ranges = selection.getRanges();
		var el = new CKEDITOR.dom.element("div");
		for (var i = 0, len = ranges.length; i < len; ++i) {
			el.append(ranges[i].cloneContents());
		}
		selectedHtml = el.getHtml();
	    var strHTML;
		
		console.log(category+ " "+name);
		
		if(category=='span'){
			
			strHTML="<span class='"+ name +"' data-randomID='"+randomCode+"' >"+ selectedHtml +"<input type='hidden' value='"+name+"' data-randomID='"+randomCode+"' /></span>";
			editor.insertHtml(strHTML);
		}
		else if(category=='div'){
			
			strHTML="<div class='"+ name +"' data-randomID='"+randomCode+"'>"+ selectedHtml +"<input type='hidden' value='"+name+"'  data-randomID='"+randomCode+"' /></div><span>&nbsp;</span>";
			editor.insertHtml(strHTML);
		}
		else if(category=='section'){
		
			strHTML="<section class='"+ name +"' data-randomID='"+randomCode+"' >"+ selectedHtml +"<input type='hidden' value='"+name+"'  data-randomID='"+randomCode+"' /></section>";
			editor.insertHtml(strHTML);
		}
		 
}