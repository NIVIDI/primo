<?php
include "conn.php";
include("header.php");
include("header_nav.php");
include ("sideBar.php");
	
?>

<script type="text/javascript">
    function checkAll(checkId){
        var inputs = document.getElementsByTagName("input");
        console.log('here');
        for (var i = 0; i < inputs.length; i++) { 
            if (inputs[i].type == "checkbox" && inputs[i].id == checkId) { 
                if(inputs[i].checked == true) {
                    inputs[i].checked = false ;
                } else if (inputs[i].checked == false ) {
                    inputs[i].checked = true ;
                }
            }  
        }  
    }
</script>
<?php
  $bundle = !empty($_GET['bundle']) ? $_GET['bundle'] : '';
  $where_sql = '';

  if(!empty($bundle)){
      $where_sql .= " AND a.prec_bundle = '{$bundle}' ";
  }
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>List of Completed Document</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List of Completed Document</li>
        </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
              <div class="box-header with-border">
                    <div class="cols-sm-6">
                        <form class="form-inline" method="GET" action="ListofCompleted.php">
                          <input type="hidden" name="page" value="Enrich">
                            <div class="form-group">
                              <label> Bundle:</label>
                              <select name="bundle" id="bundle" class="form-control">
                                  <option value="">Please Select</option>
                                  <?php
                                      $sql = "SELECT * FROM files_for_batching GROUP BY prec_bundle";
                                      $sql_results = ExecuteMySql($con, $sql);
                                      if($sql_results->num_rows){
                                          while($row = $sql_results->fetch_assoc()){
                                              $selected = '';
                                              if($bundle == $row['prec_bundle']){
                                                  $selected = "selected";
                                              }
                                              echo "<option {$selected} value='{$row['prec_bundle']}'>{$row['prec_bundle']}</option>";
                                          }
                                      }
                                  ?>
                              </select>
                              <button type="submit" class="btn btn-primary x-small"><i class="fa  fa-search"></i> Search</button> 
                            </div>
                        </form>
                    </div>
                </div>
                <div class="box-body">
    			          <form method="post" action="Deliver.php">
    		               <table id="example1" class="table table-bordered table-striped">
                          <thead>
                              <tr>
                        					<?php
                        					      if ($_SESSION['UserType']=='Admin'){
                        						      echo '<th><input type="Checkbox" id="chk_new"  onclick="checkAll(\'chk\');" ></th>';
                        				        }
                        					?>
                                  <th>PR</th>
                                  <th>FileName</th>
                                  <th>Batched Name</th>
                                  <th>Status</th>
                                  <th>User</th>
                                  <th>Last Update</th>
                              </tr>
                          </thead>
                          <tbody>
    				              <?php
                                if ($_SESSION['UserType']=='Admin'){
                                    if (empty($_POST['Search'])){
                                        $strSQL="SELECT  * FROM primo_view_jobs Where ProcessCode='QC' AND statusstring='DONE' and status<>'Transmitted' AND GGJobID IS NOT NULL";  
                                    }else{
                                        $strSQL="SELECT * FROM primo_view_jobs Where ProcessCode='QC' AND statusstring='DONE' and  status<>'Transmitted' AND GGJobID IS NOT NULL AND Jurisdiction='".$_POST['Search']."'";   
                                    }
                                }else{
                                    if (empty($_POST['Search'])){
                                        $strSQL="SELECT * FROM primo_view_jobs Where ProcessCode='QC' AND statusstring='DONE' and status<>'Transmitted' AND AssignedTo='$_SESSION[login_user]'";  
                                    }
                                    else{

                                        $strSQL="SELECT  * FROM primo_view_jobs Where ProcessCode='QC' AND statusstring='DONE' and status<>'Transmitted' AND AssignedTo='$_SESSION[login_user]' AND Jurisdiction='".$_POST['Search']."'";  
                                    }
                                }
    				
                                $batched_name = '';
                                $_SESSION['strSQL']=$strSQL;
    				                    $objExec= odbc_exec($conWMS,$strSQL);
    				
    				                    $Jobname='';
                        				while ($row = odbc_fetch_array($objExec)) 
                        				{

                                    $filenamex = trim($row["Filename"]);
                                    
                                    
                                    if(!empty($bundle)){
                                        $sql_select = "SELECT COUNT(*) as total_bundle FROM segregated_files a WHERE a.filename = '{$filenamex}' AND a.wms_job_id = '{$row["JobId"]}' {$where_sql} ";
                                    
                                        $sql_select_count = mysqli_query($con, $sql_select)->fetch_assoc();
                                        
                                        if($sql_select_count['total_bundle']){
                                            $sql = "SELECT b.*  FROM deliverables_segregated_files b INNER JOIN segregated_files a ON b.segregated_id = a.id  WHERE a.filename = '{$filenamex}' AND a.wms_job_id = '{$row["JobId"]}' LIMIT 1";

                                            $sql_result = mysqli_query($con, $sql)->fetch_assoc();
                                            if($sql_result){
                                                $batched_name = $sql_result['filename'];
                                            }
                                            $td1 = '';
                                            if ($_SESSION['UserType']=='Admin'){
                                                $td1 = '<td> <input type="Checkbox" id="chk" name="chk[]" value="'.$row["BatchId"].'"></td>';
                                            }
                                            $Title= empty($row["Title"]) ? '': $row["Title"];
                                            $encoding = mb_detect_encoding($Title, mb_detect_order(), false);

                                            if($encoding == "UTF-8")
                                            {
                                                $$Title = mb_convert_encoding($Title, "UTF-8", "Windows-1252");    
                                            }
              
                                            $out = iconv(mb_detect_encoding($Title, mb_detect_order(), false), "UTF-8//IGNORE",$Title);
                                				    $filename="uploadFiles/".$filenamex;
                                            echo '<tr>'.$td1.'
                                                        <td><a href="index.php?file='.$filename.'&BatchID='.$row["BatchId"].'&Status=Completed">'.$row["JobName"].'</a></td>
                                                        <td>'.$filenamex.'</td>
                                                        <td>'.$batched_name.'</td>
                                                        <td>'.$row["StatusString"].'</td>
                                                        <td>'.$row["AssignedTo"].'</td> 
                                      				          <td>'.$row["LastUpdate"].'</td> 
            				                              </tr>';
                                        }
                                    }else{
                                        $sql = "SELECT b.*  FROM deliverables_segregated_files b INNER JOIN segregated_files a ON b.segregated_id = a.id  WHERE a.filename = '{$filenamex}' AND a.wms_job_id = '{$row["JobId"]}' LIMIT 1";
                                        $sql_result = mysqli_query($con, $sql)->fetch_assoc();
                                        if($sql_result){
                                            $batched_name = $sql_result['filename'];
                                        }
                                        $td1 = '';
                                        if ($_SESSION['UserType']=='Admin'){
                                            $td1 = '<td> <input type="Checkbox" id="chk" name="chk[]" value="'.$row["BatchId"].'"></td>';
                                        }
                                        $Title= empty($row["Title"]) ? '': $row["Title"];
                                        $encoding = mb_detect_encoding($Title, mb_detect_order(), false);

                                        if($encoding == "UTF-8")
                                        {
                                            $$Title = mb_convert_encoding($Title, "UTF-8", "Windows-1252");    
                                        }
          
                                        $out = iconv(mb_detect_encoding($Title, mb_detect_order(), false), "UTF-8//IGNORE",$Title);
                                        $filename="uploadFiles/".$filenamex;
                                        echo '<tr>'.$td1.'
                                                    <td><a href="index.php?file='.$filename.'&BatchID='.$row["BatchId"].'&Status=Completed">'.$row["JobName"].'</a></td>
                                                    <td>'.$filenamex.'</td>
                                                    <td>'.$batched_name.'</td>
                                                    <td>'.$row["StatusString"].'</td>
                                                    <td>'.$row["AssignedTo"].'</td> 
                                                    <td>'.$row["LastUpdate"].'</td> 
                                              </tr>';
                                    }
    				                    }
    				              ?>	  
                          </tbody>
                       </table>
    			  
    			            <?php
    					            if ($_SESSION['UserType']=='Admin'){
    						            /*echo '<p align="right">
                                    <input type="checkbox" name="SubmitToSniffing"  checked> Submit to sniffing 
    				                        <button type="submit" class="btn btn-warning"><i class="fa  fa-send"></i> Deliver</button>
                                  </p>';*/
    				              }
    					        ?>
                      <!--<a href="ExportToexcel.php" onclick="return theFunction();" target="_blank" class="pull-right">Export To Excel</a>-->
                    </form>
                </div>
          </div>
        </div>
      </div>
    </section>
</div>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
