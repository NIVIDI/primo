<?php
header("Access-Control-Allow-Origin: *");

if(!empty($_POST)){
	try{
		$save_dir = $_POST['save_dir'];
		$imgsrc = $_POST['imgsrc'];
		$filename = $_POST['filename'];
		$height = $_POST['height'];
		$width = $_POST['width'];
		$splited = explode(',', substr( $imgsrc , 5 ) , 2);
		$mime=$splited[0];
		$data=$splited[1];

		$mime_split_without_base64=explode(';', $mime,2);
		$mime_split=explode('/', $mime_split_without_base64[0],2);
		if(count($mime_split)==2)
		{
		    $extension=$mime_split[1];
		    if($extension=='jpeg')$extension='jpg';
		    $filename = $filename.'.'.$extension;
		}

		file_put_contents(__DIR__.'/'.str_replace('%20', ' ', $_POST['save_dir']), base64_decode($data) );
		if(!empty($height) && !empty($width)){
			$image = imagecreatefrompng(__DIR__.'/'.str_replace('%20', ' ', $_POST['save_dir']));
			$imgResized = imagescale($image , $width, $height);
			imagepng($imgResized, __DIR__.'/'.str_replace('%20', ' ', $_POST['save_dir']));
		}

		$temp_dir = pathinfo(pathinfo(str_replace('%20', ' ', $_POST['save_dir']),PATHINFO_DIRNAME), PATHINFO_FILENAME);
		$temp_file = pathinfo(str_replace('%20', ' ', $_POST['save_dir']),PATHINFO_FILENAME);

		if(!file_exists(__DIR__.'/TEMPNI/'.$temp_dir)){
			mkdir(__DIR__.'/TEMPNI/'.$temp_dir, 0777, true);
		}

		copy(__DIR__.'/'.str_replace('%20', ' ', $_POST['save_dir']), __DIR__.'/TEMPNI/'.$temp_dir.'/'.$temp_file.'.png');
		$result = array("success" => true, "message" => "Successfully save file to application!");
	}catch(Exception $e){
		$result = array("success" => false, "message" => $e->getMessage());
	}
}else{
	$result = array("success" => false, "message" => 'request method not allowed!');
}
echo json_encode($result);
?>