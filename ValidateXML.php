<?php
require "conn.php";

$post_data = $_POST['data'];
$fileVal=$_POST['filename'];

$sql = "SELECT * FROM segregated_files WHERE filename = '{$fileVal}' LIMIT 1";
$sql_result = mysqli_query($con, $sql)->fetch_assoc();

$bundle = !empty($sql_result['prec_bundle']) ? $sql_result['prec_bundle'] : '';
$filename = pathinfo($fileVal, PATHINFO_FILENAME);
$exploded_filename = explode('_',$filename);
$xml_file = $filename.'.xml';
$file_path =  $SourceFilePath.'/'.$bundle.'/'.$xml_file;
$parser_path = __DIR__.'/XMLValidator/NSGMLS.exe';
$cat_path = __DIR__.'/XMLValidator/PRE.cat';
$dtd_path = __DIR__.'/XMLValidator/precedent.dtd';

if(file_exists($file_path)){
    $xml = file_get_contents($file_path);
    if(!empty($xml)){
        $xml = '<!DOCTYPE ROOT SYSTEM "'.$dtd_path.'">'.PHP_EOL.'<ROOT>'.PHP_EOL.'<COURTFILE ID="'.$exploded_filename[0].'">'.PHP_EOL.$xml;
        $xml .= PHP_EOL.'</COURTFILE>'.PHP_EOL.'</ROOT>'.PHP_EOL;
        $temp_dir = $SourceFilePath.'/'.$bundle.'/'.$_SESSION['UserID'];
        if(!file_exists($temp_dir)){
            mkdir($temp_dir, 0777, true);
        }
        $temp_file_path = $temp_dir.'/'.$filename.'.xml';
        $file = fopen($temp_file_path, 'w+');
        fputs($file, $xml);
        fclose($file);

        if(file_exists($temp_dir.'/'.$filename.'.log')){
            unlink($temp_dir.'/'.$filename.'.log');
        }

        $cmd = '"'.$parser_path.'" -f "'.$temp_dir.'/'.$filename.'.log" -s -c "'.$cat_path.'" "'.$temp_file_path.'"';
        shell_exec($cmd);

        if(file_exists($temp_dir.'/'.$filename.'.log')){
            $logs = str_replace(array($parser_path, $cat_path, $temp_dir.'/'), '', trim(file_get_contents($temp_dir.'/'.$filename.'.log')));
            if(empty($logs)){
                echo "XML successfully validated!";
            }else{
                $logs = explode(PHP_EOL,$logs);
                $log_msg = '';
                $x = 0;
                foreach($logs as $log){
                    $x ++;
                    $exploded_logs = explode('.xml:', $log);
                    $error_line = !empty($exploded_logs[1]) ? $exploded_logs[1] : 0;
                    $explode2 = explode(':', $error_line);
                    $line = !empty($explode2[0]) ? $explode2[0] : 0;
                    $log_msg .= "<li style='cursor:pointer;' onclick='XMLValidation.click_logs({$line});'><small>".$x.".</small> <small style='font-size: 10px;color:red;margin-bottom:15px'>".$log."</small></li>";; 
                }

                echo $log_msg;
            }
        }
    }else{
        echo "XML File is empty!";
    }
}else{
    echo "No XML File Available!";
} 
?>