<?php
use Smalot\PdfParser\Parser;
use Smalot\PdfParser\XObject\Image;

$ids = json_decode($post['ids']);
$parent_id = !empty($post['parent_id']) ? $post['parent_id'] : '';

if(empty($parent_id)){
	$results = array("success" => false, "message" => "Error: Bundle ID not defined!", "data" => '');
	echo json_encode($results);
	exit;
}

if(!empty($ids)){
	$process_result = array();
	$sql = "SELECT * FROM files_for_batching WHERE id = '{$parent_id}'";
	$sql_result = mysqli_query($con, $sql)->fetch_assoc();


	$main_dir = !empty($sql_result['file_name']) ? (pathinfo($sql_result['file_name'], PATHINFO_FILENAME)) : '';
	$main_orig_dir = $main_dir;
	$main_dir .= '_'.$_SESSION['UserID'];
	$bundle_dir = !empty($sql_result['prec_bundle']) ? $sql_result['prec_bundle'] : '';
	
	foreach($ids as $id){
		$sql = "SELECT a.* FROM segregated_files a WHERE a.id = '{$id}'";
		$sql_result = mysqli_query($con, $sql)->fetch_assoc();
		$filename = !empty($sql_result['filename']) ? $sql_result['filename'] : '';
		
		$file_path = $SourceFilePath.'/'.$bundle_dir.'/'.$filename;
		
		$new_dir = pathinfo($file_path, PATHINFO_FILENAME);

		$png_file_path = __DIR__.'/'.$data_inventory_path.'/CONVERTED TO PNG FILES/'.$bundle_dir.'/'.$main_orig_dir.'/'.$new_dir;
		if(file_exists($png_file_path)){
			rrmdir($png_file_path);
		}

		if(!file_exists($png_file_path)){
			mkdir($png_file_path, 0777, true);
		}

		//convert first to png 
		$cmd = __DIR__.'/mupdf/mutool convert -o "'.$png_file_path.'/'.$new_dir.'_%d.png" '.$file_path.' 1-N';
		exec($cmd);
		
		//convert to specific 300 dpi and good quality compression
		$dirni = new DirectoryIterator($png_file_path);
	    foreach ($dirni as $fileinfo) {
	        if (!$fileinfo->isDot()) {
	        	$imagename = $fileinfo->getFilename();
		        	
	        	$exploded_file = explode('_', pathinfo($imagename, PATHINFO_FILENAME));
				
				//do not include page 1 and page 2 in image compression
				//if($exploded_file[3] > 2){
					$image = new Imagick($png_file_path.'/'.$imagename);
					$image->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
					$image->setResolution(300, 300);
					$image->setImageFormat('jpeg');
					$image->setImageCompression(imagick::COMPRESSION_JPEG); 
					$image->setImageCompressionQuality(100);
					//8.5 inches X 11 inches
					$image->resizeImage(816,1056,Imagick::FILTER_CUBIC,1);
					$image->writeImage($png_file_path.'/'.$imagename);
		        //}
	        }
	    }
		
			
	    $process_result[] = array("success" => true, "message" => "Successfully converted to pdf png file ({$filename})");
	}

	$message = '';
	foreach($process_result as $response){
		$style = "display: block; color: red;";
		if($response['success']){
			$style = "display: block; color: green;";
		}
		$message .= "<li style='{$style}'>{$response['message']}</li>";
	}
	$results = array("success" => true, "message" => "{$message}");
}else{
	$results = array("success" => false, "message" => "You have not selected split page to convert!");
}
?>