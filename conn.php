<?php
include 'composer/vendor/autoload.php';
ini_set('session.save_path',getcwd(). '/');
session_start();

//CONFIGURATION START HERE//
include "Config.php";

//if not production on all php error reporting
if($GGProductionMode == 'ON'){
	error_reporting(0);
}else{
	error_reporting(E_ALL);
}

include "functions.php";

ini_set('upload_max_filesize', '80M');
ini_set('post_max_size', '80M');
ini_set('max_input_time', 0);
ini_set('max_execution_time',240);
//ini_set('memory_limit', -1);

if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
    $base_url = "https";
else
    $base_url = "http";
  
$base_url .= "://";
// Append the host(domain name, ip) to the URL.
$base_url .= !empty($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST']: '';
   
////CONFIGURATION END HERE//
//echo phpinfo();
//exit;
$con=mysqli_connect($mySQLServer,$mySQLUsername,$mySQLPassword,$mySQLDbase);
// Check connection
if (mysqli_connect_errno())
{
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}


$version = getVersion();

if ($Mode=='SQLDirect'){

	$conSearchnet=odbc_connect("Driver={SQL Server};Server={$SearchNetServerName};Database={$SearchNetDBName}",$SearchnetUsername,$SearchnetPword);
		if (!$conSearchnet)
		{exit("Connection Failed: " . $conSearchnet);}

	$conWMS=odbc_connect("Driver={SQL Server};Server={$WMSserverName};Database={$WMSDBName}",$WMSUsername,$WMSPassword);
		if (!$conWMS)
		{exit("Connection Failed: " . $conWMS);}

}

?>
  