<?php
	$bundle = $post['bundle'];
	if(!empty($bundle)){
		$files = $_FILES;
		$attachments = array();
		$total = $total = count($files['files']['name']);
		$process_results = array();
		for( $i=0; $i < $total; $i++) {
	        $tmpFilePath = $files['files']['tmp_name'][$i];

	        if(!empty($tmpFilePath)){
	            $bundle_path = __DIR__.'/'.$data_inventory_path.'/QA Results/'.$bundle;
                if(!file_exists($bundle_path)){
                    mkdir($bundle_path, 0777, true);
                    chmod($bundle_path, 077);
                }
	            
	            $newFilePath = $bundle_path."/".$files['files']['name'][$i];
                move_uploaded_file($tmpFilePath, $newFilePath);
                $file_part = pathinfo($newFilePath);
                $attachments[] = array("path" => $newFilePath, "name" => $file_part['basename']);
			}
	    }

	   	$subject = 'TPCPP Bundle('.$bundle.') QA Result('.$post['qa_status'].')';
		$body = 'Bundle validation it completed. Please check the results on the system!';
		$mail = sendMail($subject, $body, $attachments);
		if($mail['success']){
			 $sql = "UPDATE files_for_batching SET status = 'Ongoing' WHERE prec_bundle = '{$bundle}' ";
			ExecuteQuery($sql,$con);

			$date = date('Y-m-d H:i:s');
			$sql_update_qa = "UPDATE qa_review SET `updated_by` = '{$_SESSION['UserID']}', `date_updated` = '{$date}', `status` = '{$post['qa_status']}' WHERE bundle= '{$bundle}' ";
			
		    ExecuteQuery($sql_update_qa, $con);
			$results = array("success" => true, "message" => "Successfully notify PD for bundle({$bundle}) latest updates!");
		}else{
			$results = array("success" => false, "message" => $mail['message']);
		}
	}else{
		$results = array("success" => false, "message" => "Error cannot find specified bundle!");
	}
?>