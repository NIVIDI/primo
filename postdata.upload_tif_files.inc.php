<?php
use Smalot\PdfParser\Parser;
use Smalot\PdfParser\XObject\Image;

$files = $_FILES;
$total = count($files['files']['name']);
$postdata = $post;
$id = !empty($post['id']) ? $post['id'] : '';

$sql = "SELECT * FROM files_for_batching WHERE id = '{$id}'";
$sql_result = mysqli_query($con, $sql)->fetch_assoc();
$filename = !empty($sql_result['file_name']) ? $sql_result['file_name'] : '';
$filename_dir = pathinfo($filename, PATHINFO_FILENAME);
$bundle_dir = !empty($sql_result['prec_bundle']) ? $sql_result['prec_bundle'] : '';
$file_path = __DIR__.'/'.$data_inventory_path.'/TIFFILES/'.$bundle_dir.'/'.$filename_dir;
if(!file_exists($file_path)){
	mkdir($file_path, 0777, true);
}

if(!empty($id)){
    if($total > 0){
	    $results = array();
	    $process_result = array();
	    for( $i=0; $i < $total; $i++) {
	        $tmpFilePath = $files['files']['tmp_name'][$i];
	        if(!empty($tmpFilePath)){
	            $newFilePath = $file_path."/".$files['files']['name'][$i];
	            $ext = pathinfo($newFilePath, PATHINFO_EXTENSION);
	            $zip_filename = pathinfo($newFilePath, PATHINFO_FILENAME);
	            if($filename_dir != $zip_filename){
	            	$process_result[] = array("success" => false, "message" => "Error uploaded file ({$zip_filename}) not belong to this bundle file.");
	            }else{
		            if(strtoupper($ext) != 'ZIP'){
		                //not zip file will not be included
		                $process_result[] = array("success" => false, "message" => "Only ZIP File is allowed ({$zip_filename})");
		            }else{

		                if(move_uploaded_file($tmpFilePath, $newFilePath)) {
		                	$zip = new ZipArchive;
							if ($zip->open($newFilePath) === TRUE) {
								$zip->extractTo($file_path);
							    
							    //get extracted tif files and process conversion back to pdf
							    $pdf_name = '';
							    $tif_to_compile = array();
							    $bmp_to_compile = array();
							    $directory_path = '';
							    $pdf = new \Imagick();
							    			
							    for ($idx = 0; $zipFile = $zip->statIndex($idx); $idx++) {
							    	$zip_file_info = pathinfo($zipFile['name']); 
							    	$ext = !empty(pathinfo($zip_file_info['basename'], PATHINFO_EXTENSION)) ?  strtoupper(pathinfo($zip_file_info['basename'], PATHINFO_EXTENSION)) : '';
							    	if($ext == 'TIF'){
							    		$tif_to_compile["{$zip_file_info['dirname']}"][] = $zip_file_info['basename'];  
							    	}

							    	if($ext == 'BMP'){
							    		$bmp_to_compile["{$zip_file_info['dirname']}"][] = $zip_file_info['basename']; 
							    	}
							    }

							    //convert tif to pdf per batch
							    if(!empty($tif_to_compile)){
							    	$y = 0;
							    	
							    	foreach($tif_to_compile as $key => $values){
							    		$y ++;
							    		$x = 0;
							    		$tif_dir = $file_path.'/'.$key;
										
							    		$source_file_path = $SourceFilePath.'/'.$bundle_dir.'/'.$key.'.pdf';
							    		$pages_to_split = array();
									    $parser = new Parser();
									    $pdf    = $parser->parseFile($source_file_path);
									    $pages  = $pdf->getPages();
									    $total_pages = COUNT($pages);
									    $pdf = new \Imagick();
										$pdf2 = new \Imagick();

									    foreach ($pages as $page) {
									      	$x ++;

									      	$tif_file = $tif_dir.'/'.$key."_{$x}.tif";
									      	$pdf2->addImage(new \Imagick($tif_file));
									      	if($x > 2){
										    	$pdf->addImage(new \Imagick($tif_file));
										    }
									    }

									    try{
											$pdf->writeImages($tif_dir.'/'.$key.'.pdf', true);
											$pdf2->writeImages($tif_dir.'/'.$key.$y.'.pdf', true);

											if(file_exists($SourceFilePath.'/'.$bundle_dir.'/deliverables/'.$key.'.pdf')){
												unlink($SourceFilePath.'/'.$bundle_dir.'/deliverables/'.$key.'.pdf');
											}
							    			copy($tif_dir.'/'.$key.'.pdf', $SourceFilePath.'/'.$bundle_dir.'/deliverables/'.$key.'.pdf');
							    			unlink($tif_dir.'/'.$key.'.pdf');
							    			
							    			copy($tif_dir.'/'.$key.$y.'.pdf', $SourceFilePath.'/'.$bundle_dir.'/'.$key.'.pdf');
							    			unlink($tif_dir.'/'.$key.$y.'.pdf');
							    		}catch(Exception $e){
							    			//error
							    			//var_dump("error");
							    		}
									}
								}
							    
							    //for bmp files
							    if(!empty($bmp_to_compile)){
							    	$x = 0;
									foreach($bmp_to_compile as $key => $values){
										
										$bmp_dir = $file_path.'/'.$key;
										foreach($values as $value){
											$x ++;
											if(file_exists($bmp_dir.'/'.$value) && !is_dir($bmp_dir.'/'.$value)){
												$pathinfo = pathinfo($bmp_dir);
												$bmp_name = str_replace('CARSWELL','',strtoupper($pathinfo['filename'])).'_'.$x;
											
												copy($bmp_dir.'/'.$value, $bmp_dir.'/'.$bmp_name.'.BMP');
												unlink($bmp_dir.'/'.$value);
											}
										}
									}
							    }

							    $zip->close();
							    if(file_exists($newFilePath)){
							    	unlink($newFilePath);
								}
								
								$process_result[] = array("success" => true, "message" => "Successfully uploaded ({$zip_filename}) ");
		                	} else {
							    $process_result[] = array("success" => false, "message" => "Error extracting zip file($zip_filename)");
		                	}
						}else{
		                    $process_result[] = array("success" => false, "message" => "Error uploading file ({$zip_filename})");
		                    if(file_exists($newFilePath)){
		                    	unlink($newFilePath);
		                	}
		                }
		            }
		        }
	        }
	    }

	    $message = '';
		foreach($process_result as $response){
			$style = "display: block; color: red;";
			if($response['success']){
				$style = "display: block; color: green;";
			}
			$message .= "<li style='{$style}'>{$response['message']}</li>";
		}
		$results = array("success" => true, "message" => "{$message}");
	}else{
		$results = array("success" => false, "message" => "Empty uploaded file");
	}
}else{
	$results = array("success" => false, "message" => "Error bundle ID cannot be empty!");
}
?>