<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '935567c52596b0726081f9971dd5b3785a39f38c',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '935567c52596b0726081f9971dd5b3785a39f38c',
    ),
    'imal-h/pdf-box' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8d0005072e958f17a2130b8e93c1ec6bc7f1f749',
    ),
    'laravie/parser' => 
    array (
      'pretty_version' => 'v2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e4f72a458ea7644702a48d352447b6030f1b8555',
    ),
    'paragonie/constant_time_encoding' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f34c2b11eb9d2c9318e13540a1dbc2a3afbd939c',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.100',
      'version' => '9.99.100.0',
      'aliases' => 
      array (
      ),
      'reference' => '996434e5492cb4c3edcb9168db6fbb1359ef965a',
    ),
    'phpmailer/phpmailer' => 
    array (
      'pretty_version' => 'v6.4.1',
      'version' => '6.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9256f12d8fb0cd0500f93b19e18c356906cbed3d',
    ),
    'phpseclib/phpseclib' => 
    array (
      'pretty_version' => '3.0.8',
      'version' => '3.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd9615a6fb970d9933866ca8b4036ec3407b020b6',
    ),
    'sabre/uri' => 
    array (
      'pretty_version' => '2.2.1',
      'version' => '2.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f502edffafea8d746825bd5f0b923a60fd2715ff',
    ),
    'sabre/xml' => 
    array (
      'pretty_version' => '2.2.3',
      'version' => '2.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c3b959f821c19b36952ec4a595edd695c216bfc6',
    ),
    'smalot/pdfparser' => 
    array (
      'pretty_version' => 'v1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd4148fd8160f2e9cfc03f0bbfa4efba8a4e15fa4',
    ),
    'spatie/array-to-xml' => 
    array (
      'pretty_version' => '2.15.0',
      'version' => '2.15.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1795afad4e5a1f4b7af2e0e09802550eaa00a6f8',
    ),
    'spatie/pdf-to-image' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ea81a2d5f4fb0ca9f4c4fc0d4f8813135e993b2d',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5232de97ee3b75b0360528dae24e73db49566ab1',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eca0bf41ed421bed1b57c4958bab16aa86b757d0',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v4.4.25',
      'version' => '4.4.25.0',
      'aliases' => 
      array (
      ),
      'reference' => '31ea689a8e7d2410016b0d25fc15a1ba05a6e2e0',
    ),
    'tightenco/collect' => 
    array (
      'pretty_version' => 'v8.34.0',
      'version' => '8.34.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b069783ab0c547bb894ebcf8e7f6024bb401f9d2',
    ),
    'vyuldashev/xml-to-array' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae1713b167093716b8e8d56bfb318befac4a8c4b',
    ),
  ),
);
