<?php
include "conn.php";

$sql="SELECT * FROM tblUserAccess Where UserID=' $_SESSION[UserID]'";
 
if ($result=mysqli_query($con,$sql))
{
// Fetch one and one row
	while ($row=mysqli_fetch_row($result))
	{
		$ACQUIRE=$row[1];
		$ENRICH=$row[2];
		$DELIVER=$row[3];
		$USER_MAINTENANCE=$row[4];
		$EDITOR_SETTINGS=$row[5];
		$ML_SETTINGS=$row[6];
		$TRANSFORMATION=$row[7];
		$TRANSMISSION=$row[8];
	}
}

include("header.php");
include("header_nav.php");
include ("sideBar.php");


?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Editor Settings
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Editor Settings</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
  
   <?php


$sql="SELECT * FROM tblstyles";

?>
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><button type="button" class="btn btn-block btn-primary"  onclick="location.href='addnew_style.php'">Add New</button></h3>
                 <div class="pull-right">
                  <button type="button" class="btn btn-block btn-success"  onclick="location.href='ATC.php'">ATC</button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Style Name</th>
                  <th>Back Color</th>
                  <th>Font Color</th>
                  <th>Inline</th>
				          <th>Shortcut Key</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
				<?php
				if ($result=mysqli_query($con,$sql))
  {
  // Fetch one and one row
  while ($row=mysqli_fetch_row($result))
    {
    
 
?>
                <tr>
                  <td><?php echo $row[0];?></td>
                  <td><?php echo $row[1];?></td>
                  <td><input type="Color"  name="Color" value="<?php echo $row[2];?>"></td>
                  <td><input type="Color"  name="FontColor" value="<?php echo $row[3];?>"></td>
				  <?php
				  if ($row[4]==1){
					  $Inline='checked';
				  }
				  else{
					  $Inline='';
				  }
				  
				   if ($row[5]==1){
					  $ctrl='CTRL';
				  }
				  else{
					  $ctrl='';
				  }
				
				
				 if ($row[6]==1){
					  $Shift='Shift';
				  }
				  else{
					  $Shift='';
				  }
					$keyVal=$row[7];
					
					
					$ShortcutKey='';
					
					if ($ctrl!=''){
						$ShortcutKey=$ctrl;
					}
					
					if ($Shift!=''){
						if ($ctrl!=''){
						$ShortcutKey=$ShortcutKey.'+'.$Shift;
						}
						else{
							$ShortcutKey=$Shift;
						}
					}
					
					if ($keyVal!=''){
						if ($Shift!=''){
							if ($ctrl!=''){
							$ShortcutKey=$ctrl.'+'.$Shift.'+'.$keyVal;
							}
							else{
								$ShortcutKey=$Shift.'+'.$keyVal;
							}
						}
						else{
							if ($ctrl!=''){
								$ShortcutKey=$ctrl.'+'.$keyVal;
							}
						}
					}
					else{
						$ShortcutKey='';
					}
					
					
				  ?>
                   <td><input type="checkbox"  name="Inline" <?php echo $Inline;?>></td>
				   <td> <?php echo $ShortcutKey;?> </td>
                  <td> <button type="button" class="btn btn-xs btn-info"  onclick="location.href='AddNew_Style.php?UID=<?php echo $row[0];?>&TransType=Update'">Update</button>
				  <button class="btn btn-xs btn-danger"  data-toggle="modal" data-target="#modal-danger" onclick="Javascript:SetTextBoxValue(<?php echo $row[0];?>)">Delete</button></td>
                </tr>
      <?php
	}
	 
}
?>	  
                </tbody>
                <tfoot>
                <tr>
                <th>ID</th>
                  <th>Style Name</th>
                  <th>Back Color</th>
                  <th>Font Color</th>
                  <th>Inline</th>
				  <th>Shortcut Key</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
			
	 <form method="GET" action="saveStyle.php">
        <div class="modal modal-danger fade" id="modal-danger">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Record Deletion</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure you want to delete this record?</p>
                <input type="hidden" name="txtID" id="txtID" />
                <input type="hidden" name="TransType" id="TransType" value="Delete" />
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>

                <button type="submit" class="btn btn-outline">Ok</button>
              </div>
            </div>
          </div>
        </div>
      </form>
           
          </div>
        </div>
      </div>
    </section>
</div>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
