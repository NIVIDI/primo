<?php
include "conn.php";
include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>
<script language="Javascript" type="text/javascript" src="edit_area/edit_area_full.js"></script>
<script language="Javascript" type="text/javascript">
	// initialisation
	editAreaLoader.init({
		id: "example_1"	// id of the textarea to transform		
		,start_highlight: true	// if start with highlight
		,allow_resize: "y"
		,allow_toggle: true
		,display: "later"
		,word_wrap: true
		,show_line_colors: true
		,language: "en"
		,syntax: "xml"	
		,font_size:10
		,save_callback: "my_save"
	});
	
	editAreaLoader.init({
		id: "example_2"	// id of the textarea to transform	
		,start_highlight: true
		,allow_toggle: false
		,language: "en"
		,allow_resize: "y"
		,syntax: "xml"	
		,toolbar: "search, go_to_line, |, undo, redo, |, select_font, |, syntax_selection, |, change_smooth_selection, highlight, reset_highlight, |, help"
		,syntax_selection_allow: "css,html,js,php,python,vb,xml,c,cpp,sql,basic,pas,brainfuck"
		,is_multi_files: true
		,EA_load_callback: "editAreaLoaded"
		,show_line_colors: true
	});
	
	editAreaLoader.init({
		id: "example_3"	// id of the textarea to transform	
		,start_highlight: true	
		,font_size: "8"
		,font_family: "verdana, monospace"
		,allow_resize: "y"
		,allow_toggle: false
		,language: "fr"
		,syntax: "css"	
		,toolbar: "new_document, save, load, |, charmap, |, search, go_to_line, |, undo, redo, |, select_font, |, change_smooth_selection, highlight, reset_highlight, |, help"
		,load_callback: "my_load"
		,save_callback: "my_save"
		,plugins: "charmap"
		,charmap_default: "arrows"
			
	});
	
	editAreaLoader.init({
		id: "example_4"	// id of the textarea to transform		
		//,start_highlight: true	// if start with highlight
		//,font_size: "10"	
		,allow_resize: "no"
		,allow_toggle: true
		,language: "de"
		,syntax: "python"
		,load_callback: "my_load"
		,save_callback: "my_save"
		,display: "later"
		,replace_tab_by_spaces: 4
		,min_height: 350
	});
	
	// callback functions
	function my_save(id, content){
		alert("Here is the content of the EditArea '"+ id +"' as received by the save callback function:\n"+content);
	}
	
	function my_load(id){
		editAreaLoader.setValue(id, "The content is loaded from the load_callback function into EditArea");
	}
	
	function test_setSelectionRange(id){
		editAreaLoader.setSelectionRange(id, 100, 150);
	}
	
	function test_getSelectionRange(id){
		var sel =editAreaLoader.getSelectionRange(id);
		alert("start: "+sel["start"]+"\nend: "+sel["end"]); 
	}
	
	function test_setSelectedText(id){
		text= "[REPLACED SELECTION]"; 
		editAreaLoader.setSelectedText(id, text);
	}
	
	function test_getSelectedText(id){
		alert(editAreaLoader.getSelectedText(id)); 
	}
	
	function editAreaLoaded(id){
		if(id=="example_2")
		{
			open_file1();
			open_file2();
		}
	}
	
	function open_file1()
	{
		var new_file= {id: "to\\ é # € to", text: "$authors= array();\n$news= array();", syntax: 'php', title: 'beautiful title'};
		editAreaLoader.openFile('example_2', new_file);
	}
	
	function open_file2()
	{
		var new_file= {id: "Filename", text: "<a href=\"toto\">\n\tbouh\n</a>\n<!-- it's a comment -->", syntax: 'html'};
		editAreaLoader.openFile('example_2', new_file);
	}
	
	function close_file1()
	{
		editAreaLoader.closeFile('example_2', "to\\ é # € to");
	}
	
	function toogle_editable(id)
	{
		editAreaLoader.execCommand(id, 'set_editable', !editAreaLoader.execCommand(id, 'is_editable'));
	}

</script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transformation Settings
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Transformation Settings</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
  
   <?php


$sql="SELECT * FROM tblstyles";

?>
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
                 
            </div>
            <!-- /.box-header -->
			<form method="post" action="SaveXMLTemplate.php">
            <div class="box-body">
			<fieldset>

				<textarea id="example_1" style="height:15vw; width: 100%;" name="XMLTemplate">
				<?php
				$sql="SELECT * FROM tblxmltemplate";
 
if ($result=mysqli_query($con,$sql))
{
// Fetch one and one row
	while ($row=mysqli_fetch_row($result))
	{
		echo $row[0];
		 
	}
}
				?>
				</textarea>
			</fieldset>
			<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save Template</button>
            </div>
			</form>
	
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
	  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Mapping Table</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover" id="user_data">
                <tr>
					<th>ID</th>
                  <th>Style Name</th>
                  <th>Start Tag</th>
                  <th>End Tag</th>
				  <th></th>
                </tr>
				<form method="post" action="SaveMappingTag.php">
                <tr>
					<td></td>
                  <td><select name="StyleName" style="width: 300px;">
				  <?php
				$sql="SELECT * FROM tblstyles";
 
					if ($result=mysqli_query($con,$sql))
					{
					// Fetch one and one row
						while ($row=mysqli_fetch_row($result))
						{
							?>
							
							<option value="<?php echo $row[1];?>"><?php echo $row[1];?></option>
							<?php 
						}
					}
									?>
					  
					  
					</select></td>
                  <td><input type="text" name="StartTag" style="width: 300px;"></td>
                  <td><input type="text" name="EndTag" style="width: 300px;"></td>
				  <td><button type="submit" class="btn btn-success"> Add</button></td>
                </tr>
				</form>
				<?php
				$sql="SELECT * FROM tbltagmapping";
 
					if ($result=mysqli_query($con,$sql))
					{
					// Fetch one and one row
						while ($row=mysqli_fetch_row($result))
						{
							?>
							
							
				<tr id="row<?php echo $row[0];?>">
                  <td id="id_val<?php echo $row[0];?>"><?php echo $row[0];?></td>
                  <td id="stylename_val<?php echo $row[1];?>"><?php echo $row[1];?></td>
                  <td id="starttag_val<?php echo $row[2];?>"><?php echo $row[2];?></td>
				  <td id="endtag_val<?php echo $row[3];?>"><?php echo $row[3];?></td>
				  <td><a href="DeleteMappingTag.php?ID=<?php echo $row[0];?>"><button class="btn btn-xs btn-danger">Delete</button></a></td>
                </tr>
				 
							<?php 
						}
					}
					?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
	   <form method="GET" action="saveStyle.php">
        <div class="modal modal-danger fade" id="modal-danger">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Record Deletion</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure you want to delete this record?</p>
                <input type="hidden" name="txtID" id="txtID" />
                <input type="hidden" name="TransType" id="TransType" value="Delete" />
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>

                <button type="submit" class="btn btn-outline">Ok</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </form>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
