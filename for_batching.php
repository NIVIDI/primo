<?php
include "conn.php";
include("header.php");
include("header_nav.php");
include ("sideBar.php");
$bundle = !empty($_GET['bundle']) ? $_GET['bundle'] : '';

$where_sql = 'WHERE 1';

if(!empty($bundle)){
    $where_sql .= " AND prec_bundle = '{$bundle}' ";
}
?>


<div class="content-wrapper">
    <section class="content-header">
        <h1>Source Files</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Source Files</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box box-primary">
                    <div class="box-header with-border pull-right">
                           <h3 class="box-title"><button type="button" class="btn btn-block btn-primary" id="new_batch">Add </button></h3>
                     </div>
                    <div class="box-body">
                        <div class="col-sm-12" style="margin-bottom: 30px;">
                              <form class="form-inline" action="for_batching.php">
                                  <input type="hidden" name="page" value="Acquire">
                                  <div class="form-group">
                                        <label for="filter_bundle">Bundle:</label>
                                        <select name="bundle" id="bundle" class="form-control">
                                            <option value="">Please Select</option>
                                            <?php
                                                $sql = "SELECT * FROM files_for_batching GROUP BY prec_bundle";
                                                $sql_results = ExecuteMySql($con, $sql);
                                                if($sql_results->num_rows){
                                                    while($row = $sql_results->fetch_assoc()){
                                                        $selected = '';
                                                        if($bundle == $row['prec_bundle']){
                                                            $selected = "selected";
                                                        }
                                                        echo "<option {$selected} value='{$row['prec_bundle']}'>{$row['prec_bundle']}</option>";
                                                    }
                                                }
                                            ?>
                                        </select>
                                  </div>
                                  <button type="filter_bundle" class="btn btn-primary">Filter</button>
                              </form>
                        </div>
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="example2" class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Filename</th>
                                            <th>.Key File</th>
                                            <th>.Key Logs</th>
                                            <th>Reference Code</th>
                                            <th>PrecShip Date</th>
                                            <th>Bundle</th>
                                            <th>TAT</th>
                                            <th>No. of Docs</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                            <th with="15%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $sql = "SELECT * FROM files_for_batching {$where_sql} ORDER BY id DESC";

                                        $results = ExecuteMySql($con, $sql);
                                        if($results->num_rows){
                                            while($row = $results->fetch_assoc()){
                                                $delete_btn = '';

                                                //if((int) $row['created_by'] == (int) $_SESSION['UserID']){
                                                if($_SESSION['UserType'] == 'Admin'){
                                                    $delete_btn = '<button file_id = "'.$row['id'].'" onclick="Batching.delete(this);" class="btn btn-danger btn-sm">Delete</button>';
                                                }
                                                
                                                $numofdocs = 0;
                                                $sql = "SELECT * FROM segregated_files WHERE batched_parent_id = '{$row['id']}' ";
                                                $result = mysqli_query($con, $sql);
                                                $total_done = 0;
                                                if($result->num_rows){
                                                    while($row1 = $result->fetch_assoc()){
                                                          $numofdocs ++;
                                                          if(!empty($row1['wms_job_id'])){
                                                              $wms_status = GetWMSValue("SELECT StatusString FROM primo_view_Jobs WHERE JobId = '{$row1['wms_job_id']}' AND ProcessCode = 'QC' ","StatusString",$conWMS);
                                                              if(strtolower($wms_status) == 'done'){
                                                                  $total_done ++;
                                                              }
                                                          }
                                                    }
                                                }

                                                $key_filename = generateKeyFilename($row['file_name']);
                                                $key_path = $SourceFilePath.'/'.$row['prec_bundle'].'/deliverables/'.$key_filename;

                                                $compile_name = "Compile";
                                                if(file_exists($key_path)){
                                                    $compile_name = "Re-Compile";
                                                }else{
                                                    $key_filename = '';
                                                }

                                                $keylogs_filename = '';
                                                $keylogs_path = $SourceFilePath.'/'.$row['prec_bundle'].'/'.pathinfo($key_filename, PATHINFO_FILENAME).'.logs';

                                                if(file_exists($keylogs_path)){
                                                    $keylogs_filename = pathinfo($key_filename, PATHINFO_FILENAME).'.logs';
                                                }
                                                $compile_btn = '';
                                                if($_SESSION['UserType'] != 'QA'){
                                                      $compile_btn = "<a href='javascript:void(0);' filename='{$row['file_name']}' bundle='{$row['prec_bundle']}' file_id = '{$row['id']}' onclick='Batching.compile(this);' title='{$compile_name} Files under this source' class='btn btn-success btn-sm'>
                                                                  <span class='glyphicon glyphicon glyphicon-list-alt'></span>
                                                                  {$compile_name}
                                                              </a>";
                                                }

                                                //ovveride btn
                                                if($row['status'] == 'Completed'){
                                                    $delete_btn = '';
                                                    $compile_btn = '';
                                                }             
                                                $time = time();
                                                $status = "<label class='text-muted'><i>{$total_done} out of {$numofdocs} completed</i></label>";

                                                echo "<tr id='trlist_{$row['id']}'>
                                                          <td>{$row['id']}</td>
                                                          <td><a href='for_batching_view.php?page=Acquire&id={$row['id']}'>{$row['file_name']}</a></td>
                                                          <td>
                                                              <a target='_blank' href='{$base_url}/xml_viewer.php?url=uploadfiles/SourceFiles/{$row['prec_bundle']}/deliverables/{$key_filename}'>{$key_filename}</a>
                                                          </td>
                                                          <td>
                                                              <a target='_blank' href='{$base_url}/uploadfiles/SourceFiles/{$row['prec_bundle']}/{$keylogs_filename}?time={$time}'>{$keylogs_filename}</a>
                                                          </td>
                                                          <td>{$row['reference_code']}</td>
                                                          <td>{$row['precship_date']}</td>
                                                          <td>{$row['prec_bundle']}</td>
                                                          <td>{$row['tat']}</td>
                                                          <td>{$numofdocs}</td>
                                                          <td>{$status}</td>
                                                          <td>{$row['date_created']}</td>
                                                          <td style='text-align:center'>
                                                              <a href='javascript:void(0);' filename='{$row['file_name']}' bundle='{$row['prec_bundle']}' file_id = '{$row['id']}' onclick='Batching.view(this);' title='View Details' class='btn btn-warning btn-sm'>
                                                                <span class='glyphicon glyphicon-eye-open'></span>
                                                                View
                                                              </a>
                                                              {$delete_btn}
                                                              {$compile_btn}
                                                          </td>
                                                      </tr>";
                                            }
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer"></div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="add_new_batch">
    <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title">Add Source File</h4>
          </div>
          <div class="modal-body">
              <form role="form" id="new_source_form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Reference Code <span class="text-muted">(sample: precship1838)</span></label>
                        <input type="text" name="reference_code" id="reference_code" class="form-control" req="true" message="Reference Code is required">
                    </div>
                    <div class="form-group">
                        <label>Shipment Date</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" name="preship_date" id="preship_date" req="true" message="Shipment Date is required"required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Precedents Bundle <span class="text-muted">(sample: Lit-4_Bundle36)</span></label>
                        <input type="text" name="prec_bundle" id="prec_bundle" class="form-control" req="true" message="Prec Bundle is required">
                    </div>
                    <div class="form-group">
                        <label>Days Turn Around Time</label>
                        <input type="number" name="tat" id="tat" class="form-control" value="6" req="true" message="TAT is required">
                    </div>
                    <div class="form-group">
                        <label>Source File <span class="text-muted">(pdf file only, none pdf will not be uploaded)</span></label>
                        <input type="file" class="form-control" id="textFiles" name="txtFiles[]" multiple="multiple" req="true" message="Source Files is required">
                    </div>
                    <div class="form-group">
                        <input class="form-check-input" name="auto_segregate" type="checkbox" value="1"  id="auto_segregate"/>
                        <label class="form-check-label" for="auto_segregate">
                            Auto Segregate <span style="font-size: 12px" class="text-muted">(Note: When enabled, the file will automatically segregated/split)</span>
                        </label>
                    </div>
                    <div class="form-group" id="div_auto_upload_to_GG" style="display:none;">
                        <input class="form-check-input" name="auto_upload_to_GG" type="checkbox" value="1"  id="auto_upload_to_GG"/>
                        <label class="form-check-label" for="auto_upload_to_GG">
                            Auto Upload To GG <span style="font-size: 12px" class="text-muted">(Note: When enabled, the file segregrated will automatically uploaded to GG for processing)</span>
                        </label>
                    </div>
              </form>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-primary" id="submit_source_file" value="Submit">
          </div>
      </div>
    </div>
</div>
<div class="modal fade" id="view_details_modal">
    <div class="modal-dialog" style="width: 50%">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title">Source File Details</h4>
          </div>
          <div class="modal-body">
                <div class="col-sm-12" style="margin-top: -20px;">
                    <h4><strong>Batched Files</strong></h4>
                </div>
                <div class="col-sm-12">
                  <div class="table-responsive">
                      <table class="table table-striped table-hover table-bordered">
                          <thead>
                              <tr>
                                  <th>Filename</th>
                                  <th>Size</th>
                                  <th>GG Status</th>
                                  <th>WMS Status</th>
                              </tr>      
                          </thead>
                          <tbody id="tbody_details"></tbody>
                      </table>
                  </div>
                </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
          </div>
      </div>
    </div>
</div>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
<script type="text/javascript">
    $('#new_batch').click(function(){
        $('#add_new_batch').modal({
            backdrop: 'static',
            keyboard: false
        });
        
    });

    $('#submit_source_file').click(function(){
        if(Form.validate('#new_source_form')){
            Batching.submit();
        }
    });

    var Batching = {
        delete : function(obj){
            var id = $(obj).attr('file_id');
            Swal.fire({
                title: "Are you sure want to delete selected files?",
                html: '<strong>Deleting the file will delete also corresponding data in WMS and GG, if any</strong>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                allowOutsideClick: false
            }).then((result) => {
                if (result.isConfirmed) {
                    Batching.submitDelete(id);
                }
            })
        },
        view: function(elem){
            var id = $(elem).attr('file_id');
            var bundle = $(elem).attr('bundle');
            var filename = $(elem).attr('filename');
            $('#view_details_modal .modal-title').html(filename +' (<small class="text-muted">Bundle:'+bundle+'</small>)');
            $('#view_details_modal').modal({
                    backdrop: 'static',
                    keyboard: false
            });

            Batching.fetchSegregatedFiles(id, bundle);
        },
        compile : function(elem){
            var id = $(elem).attr('file_id');
            var bundle = $(elem).attr('bundle');
            var filename = $(elem).attr('filename');
            Swal.fire({
                title: "Are you sure want to compile all batch transformation file for("+filename+")?",
                html: '<strong>Compile of batch file will generate single .key file</strong>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                allowOutsideClick: false
            }).then((result) => {
                if (result.isConfirmed) {
                    Batching.submitCompile(id, filename);
                }
            })
            
        },
        submitCompile : function(id, filename){
            var data = [
                    {name:'action', value: 'compile_sources_file'},
                    {name:'id', value: id},
                    {name: 'filename', value: filename}
                ];
            Page.loading('Compiling ...');
            $.post('postdata.php', data, function(response){
                Swal.close();
                try{
                    var result = JSON.parse(response);
                    if(result.success){
                        Swal.fire({
                            allowOutsideClick: false,
                            title: '<strong>Result</strong>',
                            icon: 'info',
                            html: result.message,
                            showCloseButton: false,
                            showCancelButton: false,
                            focusConfirm: false,
                            confirmButtonText:'<i class="fa fa-thumbs-up"></i> OK',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                location.reload();
                            }
                        })
                    }else{
                        Batching.error(result.message);
                    }
                }catch(e){
                    Batching.error(e);
                }
            });
        },
        fetchSegregatedFiles : function(id, bundle){
            var data = [{name:'action', value: 'get-segregated-files'}, {name:'id', value: id}];

            $('#tbody_details').html("<tr><td colspan='4' style='text-align:center'><i>Please wait while fetching data...</i></td></tr>");
            $.post('postdata.php', data, function(response){
                try{
                    var result = JSON.parse(response);
                    if(result.success){
                        if(result.data.length > 0){
                            $('#tbody_details').html('');
                            $.each(result.data, function(i,v){
                                var _href = "<a href='<?= $base_url;?>/uploadfiles/SourceFiles/"+bundle+"/"+v.filename+"' target='_blank'>"+v.filename+"</a>";
                                $('#tbody_details').append(
                                    $('<tr>').append(
                                        $('<td>').html(_href),
                                        $('<td>').html(v.size +' KB'),
                                        $('<td>').html(v.gg_status),
                                        $('<td>').html(v.wms_status)
                                    )
                                );
                            });
                        }else{
                            $('#tbody_details').html("<tr><td colspan='4' style='text-align:center; color:red;'>No Files Available</td></tr>"); 
                        }
                    }else{
                        $('#tbody_details').html("<tr><td colspan='4' style='text-align:center; color:red;'>"+result.message+"</td></tr>");
                    }
                }catch(e){
                    $('#tbody_details').html("<tr><td colspan='4' style='text-align:center; color:red;'>"+e+"</td></tr>");
                }
            });
        },
        submitDelete : function(id){
            var data = [
                    {name:'action', value: 'delete_bundle_file'},
                    {name:'id', value: id}
                ];
            Swal.fire({
                  title: 'Deleting ...',
                  showConfirmButton: false,
                  onBeforeOpen () {
                     Swal.showLoading ()
                  },
                  onAfterClose () {
                     Swal.hideLoading()
                  },
                  allowOutsideClick: false,
                  allowEscapeKey: false,
                  allowEnterKey: false
            });
            $.post('postdata.php', data, function(response){
                Swal.close();
                try{
                    var result = JSON.parse(response);
                    if(result.success){
                        Swal.fire({
                            allowOutsideClick: false,
                            title: '<strong>Deleting Result</strong>',
                            icon: 'info',
                            html: result.message,
                            showCloseButton: false,
                            showCancelButton: false,
                            focusConfirm: false,
                            confirmButtonText:'<i class="fa fa-thumbs-up"></i> OK',
                        }).then((result) => {
                            if (result.isConfirmed) {}
                        })

                        $('#trlist_'+id).remove();
                    }else{
                        Batching.error(result.message);
                    }
                }catch(e){
                    Batching.error(e);
                }
            });
        },
        submit : function(){
            var totalfiles = document.getElementById('textFiles').files.length;
            var formData = new FormData();
            if(totalfiles > 0){

                var auto_segregate = 0;
                if($('#auto_segregate').is(":checked")){
                    auto_segregate = 1;
                }
                var auto_upload_to_GG = 0;
                if($('#auto_upload_to_GG').is(":checked")){
                    auto_upload_to_GG = 1;
                }

                formData.append("reference_code", $('#reference_code').val());
                formData.append("preship_date", $('#preship_date').val());
                formData.append("prec_bundle", $('#prec_bundle').val());
                formData.append("tat", $('#tat').val());
                formData.append("action", "for_splitting");
                formData.append("auto_segregate", auto_segregate);
                formData.append("auto_upload_to_GG", auto_upload_to_GG);
                
                var files = $("#textFiles").get(0).files;
                for (var index = 0; index < files.length; index++) {
                    formData.append("files[]", files[index]);
                }

                Page.loading("Uploading ...");
                $.ajax({
                      url : 'postdata.php',
                      type : 'POST',
                      data : formData,
                      processData: false,  // tell jQuery not to process the data
                      contentType: false,  // tell jQuery not to set contentType
                      success : function(data) {
                           Swal.close();
                           try{ 
                              var res = JSON.parse(data);
                              if(res.success){
                                   Batching.success(res.message); 
                              }else{
                                   Batching.error(res.message);
                              }
                           }catch(e){
                              Batching.error(e);
                           }
                      },
                      error: function (err) {
                          Swal.close();
                          Batching.error(err);
                      }
                });
            }else{
               Batching.error("Source File is required");
            }
        },
        success : function(msg){
            Swal.fire({
                allowOutsideClick: false,
                title: '<strong>Uploading File Result</strong>',
                icon: 'info',
                html: msg,
                showCloseButton: false,
                showCancelButton: false,
                focusConfirm: false,
                confirmButtonText:'<i class="fa fa-thumbs-up"></i> OK',
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    location.reload();
                }
            })
        },
        error : function (msg){
            Swal.fire({
                allowOutsideClick: false,
                icon: 'error',
                title: 'Oops...',
                html: "<strong>"+msg+"</strong>"
            });
        }
    };

    $('#preship_date').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayHighlight: true,
    });

    $('#auto_segregate').click(function(){
        $('#div_auto_upload_to_GG').hide();
        $('#auto_upload_to_GG').prop('checked', false);
        if($(this).is(':checked')){
            $('#div_auto_upload_to_GG').show();
        }
    });
</script>