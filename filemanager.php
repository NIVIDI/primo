<?php
include "conn.php";
include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>


<div class="content-wrapper">
    <section class="content-header">
        <h1>File Manager</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">File Manager</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box box-primary">
                    <div class="box-header with-border"></div>
                    <div class="box-body">
                        <embed style="width:100%; height:47vw;" src="filemanager/tinyfilemanager.php" frameborder="0"></embed>
                    </div>
                    <div class="box-footer"></div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>