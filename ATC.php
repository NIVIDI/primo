<?php
include "conn.php";

$sql="SELECT * FROM tblUserAccess Where UserID='{$_SESSION['UserID']}'";
 
if ($result=mysqli_query($con,$sql))
{
// Fetch one and one row
	while ($row=mysqli_fetch_row($result))
	{
		$ACQUIRE=$row[1];
		$ENRICH=$row[2];
		$DELIVER=$row[3];
		$USER_MAINTENANCE=$row[4];
		$EDITOR_SETTINGS=$row[5];
		$ML_SETTINGS=$row[6];
		$TRANSFORMATION=$row[7];
		$TRANSMISSION=$row[8];
	}
}

include("header.php");
include("header_nav.php");
include ("sideBar.php");

?>


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ATC
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">ATC</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
  
 
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><button type="button" class="btn btn-block btn-primary"  onclick="location.href='AddNew_ATC.php'">Add New</button></h3>
                 <div class="pull-right">
                  <button type="button" class="btn btn-block btn-success"  onclick="location.href='Editor_Settings.php'">Back</button>
                   
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Search String</th>
                  <th>Replace String</th>
                  
                  <th></th>
                </tr>
                </thead>
                <tbody>
<?php
  $sql="SELECT * FROM tblATC";
  $rs=odbc_exec($conWMS,$sql);
   
    while(odbc_fetch_row($rs))
    {
       
?>
                <tr>
                  <td><?php echo odbc_result($rs,"id");?></td>
                  <td><?php echo odbc_result($rs,"SearchString");?></td>
                 <td><?php echo odbc_result($rs,"ReplaceString");?></td>
                  <td> <button type="button" class="btn btn-xs btn-info"  onclick="location.href='AddNew_ATC.php?UID=<?php echo odbc_result($rs,"id");?>&TransType=Update'">Update</button>
				            <button class="btn btn-xs btn-danger"  data-toggle="modal" data-target="#modal-danger" onclick="Javascript:document.getElementById('txtID').value = <?php echo odbc_result($rs,"id");?>">Delete</button>
                </td>
                </tr>
      <?php
	}
	 

?>	  
                </tbody>
                <tfoot>
                <tr>
                <th>ID</th>
                  <th>Search String</th>
                  <th>Replace String</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
			
	 <form method="GET" action="saveATC.php">
        <div class="modal modal-danger fade" id="modal-danger">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Record Deletion</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure you want to delete this record?</p>
                <input type="hidden" name="txtID" id="txtID" />
                <input type="hidden" name="TransType" id="TransType" value="Delete" />
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>

                <button type="submit" class="btn btn-outline">Ok</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </form>
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>