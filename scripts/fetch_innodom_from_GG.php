<?php
	require realpath(dirname(__FILE__).'/../conn.php');

	$sql = "SELECT * FROM files_for_batching where `status` <> 'Completed' AND (GGJobsID IS NOT NULL OR GGJobsID <> '') AND (file_name IS NOT NULL OR file_name <> '')";
	$result = mysqli_query($con,$sql);
	$content = 'here'.date('Y-m-d H:i:s');
	$token = getAPIKey($GGUserName,$GGPassword,$GGProductionMode);
	$base_dir = realpath(dirname(__FILE__).'/..');
	$log_file = "scripts_log_".date('Y-m-d').".log";
	$logs_dir = $base_dir.'/logs';
	if(!file_exists($logs_dir)){
		mkdir($logs_dir);
		chmod($logs_dir, 0776);
	}

	if(!file_exists($logs_dir.'/'.$log_file)){
		$fp = fopen($logs_dir.'/'.$log_file,"w");
		fclose($fp);
	}

	while($row=mysqli_fetch_assoc($result)){
		$content .= ' '.$row['GGJobsID'];
		$ggobj = getGGStatus($row['GGJobsID'], $token);
		$jobj = json_decode($ggobj);
		$ggstatus =  !empty($jobj->response->status) ? $jobj->response->status: ''; 
		if($ggstatus == 'completed'){
			$uri = empty($jobj->response->output_content->uri) ? '' : $jobj->response->output_content->uri;
			if(!empty($uri)){
				$innodom_data = getGGInnodom($uri, $token);
				$save_result = saveInnodom($row['file_name'], $innodom_data, $row['prec_bundle']);

				if($save_result === true){
					$sql = "UPDATE files_for_batching SET `status` = 'Completed' where `id` = '{$row['id']}'";
					mysqli_query($con,$sql);
				}else{
					file_put_contents($logs_dir.'/'.$log_file, PHP_EOL.'Batching Error('.$row['id'].'): Filename('.$row['file_name'].')'.$save_result);
				}
			}
		}
	}

	echo "success";
?>