<?php
include "conn.php";
include("header.php");
include("header_nav.php");
include ("sideBar.php");

?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>On Hold Files</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">On Hold Files</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box box-primary">
                 	<div class="box-header with-border"></div>
                    <div class="box-body">
                        <div class="col-sm-12">
                        	<div class="row">
	                            <div class="table-responsive">
	                                <table id="example2" class="table table-bordered table-hover table-striped">
	                                    <thead>
	                                        <tr>
	                                            <th>Bundle</th>
	                                            <th>JobName</th>
	                                            <th>Filename</th>
	                                            <th>Task</th>
								                <th>User</th>
								                <th>Status</th>
								                <th>Reason</th>
								                <th>Date Registered</th>
								                <th width="5%"></th>
	                                        </tr>
	                                    </thead>
	                                    <tbody>
	                                    	<?php
	                                    		if ($_SESSION['UserType'] == 'Admin'){
	                                    			$strSQL="SELECT * from primo_view_jobs where StatusString = 'Hold' ";
												}else{
													$strSQL="SELECT * from primo_view_jobs where StatusString = 'Hold' AND AssignedTo = '{$_SESSION['login_user']}' ";
												}
	                                    		
	                                    		$objExec = odbc_exec($conWMS,$strSQL);

	                                    		while ($row = odbc_fetch_array($objExec)){

	                                    			$status = $row["StatusString"];
											        $BatchID = $row["BatchId"];    
											        $FileName = trim($row["Filename"]);
											        $BatchName = $row["BatchName"];
											        $sql = "SELECT * FROM segregated_files WHERE filename = '{$FileName}' LIMIT 1";

											        $sql_result = mysqli_query($con, $sql)->fetch_assoc();
											        $filepath = $base_url.'/uploadfiles/SourceFiles/'.$sql_result['prec_bundle'].'/'.$FileName;

											        $button_unhold = '';
											        if($_SESSION['UserType'] == 'Admin'){
											        	$button_unhold = "<button type='button' user='{$row['AssignedTo']}' filename='{$FileName}' batchid = '{$BatchID}' class='btn btn-primary small' onclick='UNHOLD.unhold(this)'>Un-Hold</button>";
											        }
											        
											        echo "<tr id='tr_{$BatchID}'>
											        		<td>{$sql_result['prec_bundle']}</td>
											        		<td>{$BatchName}</td>
											        		<td><a href='{$filepath}?time=".time()."' target='_blank'>{$FileName}</a></td>
											        		<td>{$row['ProcessCode']}</td>
											        		<td>{$row['AssignedTo']}</td>
											        		<td>{$row['StatusString']}</td>
											        		<td>{$row['HoldRemarks']}</td>
											        		<td>{$row['DateRegistered']}</td>
											        		<td>
											        			{$button_unhold}
											        		</td>
											        	</tr>";
											    }
	                                    	?>
	                                    </tbody>
	                                </table>
	                            </div>
	                        </div>
                        </div>
                    </div>
                    <div class="box-footer"></div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="onhold_modal">
    <div class="modal-dialog">
      	<div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	            </button>
	            <h4 class="modal-title">Onhold</h4>
	        </div>
	        <div class="modal-body">
	            <form role="form" id="unhold_form">
	            	<div class="form-group">
                        <label>Allocate</label>
                    	<select name="UserName" id="onhold_user" class="form-control" style="width: 100%;" req="true" message="User is required!">
			            <?php

			            	$strSQL="SELECT  * from tbluser where UserType='Operator'";
			           		if ($result = mysqli_query($con,$strSQL)){
			                	while ($row=mysqli_fetch_array($result)){
			                  		$UserID=$row["id"];
			                  		$UserName = $row["UserName"];
			                  		echo "<option value=$UserName>$UserName</option>";
			                	}
			              	}
			              ?> 
			            </select>    
                    </div>
                </form>
	        </div>
	        <div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              	<input type="submit" class="btn btn-primary" id="submit_onhold" value="Submit">
	        </div>
      	</div>
  	</div>
</div>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
<script type="text/javascript">
	
	$('#submit_onhold').click(function(){
		if(Form.validate('#unhold_form')){
			UNHOLD.submitOnhold();
		}
	});

	var UNHOLD = {
		id : '',
		user: '',
		unhold : function(el){
			var filename = $(el).attr('filename');
			var batchid = $(el).attr('batchid');
			var user = $(el).attr('user');
			UNHOLD.id = batchid;
			UNHOLD.user = user;

			Swal.fire({
                title: "Are you sure want to unhold("+filename+")?",
                html: '',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                allowOutsideClick: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#onhold_modal').modal({
			            backdrop: 'static',
			            keyboard: false
			        });
                    $('#onhold_user').val(user);
			        $('#onhold_modal .modal-title').html('Un Hold ('+filename+')');
                }
            })
		},
		submitOnhold : function(){
			var data = $('#unhold_form').serializeArray();
			data.push({name:'action', value: 'unhold'});
			data.push({name:'id', value: UNHOLD.id});
			
			$.post('postdata.php', data, function(res){
				try{
					var result = JSON.parse(res);
					if(result.success){
						Page.success(result.message);
						$('#tr_'+UNHOLD.id).remove();
						$('#onhold_modal').modal('hide');
					}else{
						Page.error(result.message);
					}
				}catch(e){
					Page.error(e);
				}
			});
		}	
	};
</script>
