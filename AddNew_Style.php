<?php
include "conn.php";
  $UID= !empty($_GET['UID']) ? $_GET['UID'] :'';
	$sql="SELECT * FROM tblStyles Where StyleID='{$UID}'";
	
  $StyleID='';
  $StyleName='';
  $Color='';
  $FontColor='';
  $Inline='';
  $ctrlKey='';
  $Shftkey='';
  $KeyVal='';
	
  if ($result=mysqli_query($con,$sql))
	  {
	  // Fetch one and one row
	  while ($row=mysqli_fetch_row($result))
		{
			$StyleID=$row[0];
			$StyleName=$row[1];
			$Color=$row[2];
      $FontColor=$row[3];
			$Inline=$row[4];
			$ctrlKey=$row[5];
			$Shftkey=$row[6];
			$KeyVal=$row[7];
		}
	  }

	  if ($Inline==1){
		    $Inline='checked';
	  }
	  else{
		  $Inline='';
	  }
		
	if ($ctrlKey==1){
		  $ctrlKey='checked';
	  }
	  else{
		  $ctrlKey='';
	  }
		
	if ($Shftkey==1){
		  $Shftkey='checked';
	  }
	  else{
		  $Shftkey='';
	  }
				
				 
	  
$sql="SELECT * FROM tblUserAccess Where UserID=' $_SESSION[UserID]'";
 
if ($result=mysqli_query($con,$sql))
{
// Fetch one and one row
	while ($row=mysqli_fetch_row($result))
	{
		$ACQUIRE=$row[1];
		$ENRICH=$row[2];
		$DELIVER=$row[3];
		$USER_MAINTENANCE=$row[4];
		$EDITOR_SETTINGS=$row[5];
		$ML_SETTINGS=$row[6];
		$TRANSFORMATION=$row[7];
		$TRANSMISSION=$row[8];
	}
}
include("header.php");
include("header_nav.php");
include ("sideBar.php");


?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User List
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="UserList.php"><i class="fa fa-dashboard"></i> User Maintenance</a></li>
        <li class="active">User List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
  
      <?php $sql="SELECT * FROM tblUser"; ?>
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
			   <form role="form" method="Post" Action="saveStyle.php">
                  <div class="col-lg-6">
                       
                    <div class="form-group">
                    <label>Style Name</label>
                    <input type="text" class="form-control" placeholder="Style Name" name="StyleName" value="<?php echo $StyleName;?>">
                    </div>

                    <div class="form-group">
                    <label>Back Color</label>
                    <input type="Color"  name="Color" value="<?php echo $Color;?>">

                    </div>

                    <div class="form-group">
                    <label>Font Color</label>
                    <input type="Color"  name="FontColor" value="<?php echo $FontColor;?>">

                    </div>

                    <div class="form-group">
                    <label>In Line Style?</label>
                    <input type="checkbox"  name="InLine" <?php echo $Inline;?>>
                    </div>

                    <div class="form-group">
                    <label>Shortcut Key</label>
                    <input type="checkbox"  name="ctrlKey" <?php echo $ctrlKey;?>>CTRL <input type="checkbox"  name="Shftkey" <?php echo $Shftkey;?>>SHIFT<input type="text" placeholder="key" name="KeyVal" value="<?php echo $KeyVal;?>" size="1" maxlength="1">
                    </div>

                  </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <input type="hidden" class="form-control" placeholder="" name="UID" value="<?php echo $StyleID;?>">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="reset" class="btn btn-danger" onclick="location.href='Editor_Settings.php'">Cancel</button>
              </div>
           
          </div>
		          
              </form>
			  
            </div>
            
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
