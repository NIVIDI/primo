<?php
include "conn.php";
include "goldengateupload.php";
session_start();
$post_data = $_POST['data'];
$Task = $_POST['Task'];

$fdata = explode('|||',$post_data);
$BookID= $fdata[1];
$A2=explode("/", $fdata[0]);
$pdfFile =$A2[3];

$pages = $fdata[2];

$idVal = 'CM-'.date("mdY").'-';
$Prefix='CM';
$WorkFlow ='COMMENTARY';
$ProductType='COMMENTARY';

$sDate= date("Y-m-d h:i:sa");


 
$gBookID=GenerateBookID("Select Top 1 * From tblbook Where BookID like '%".$idVal."%' ORDER BY BookID DESC",$idVal,"BookID",$conWMS);



$outpdf =$gBookID.".pdf";

// $outpdf = str_replace(".pdf", "_output.pdf", $pdfFile);
$input = "C:\\XAMPP\\htdocs\\primoTRPLE\\uploadfiles\\".$BookID."\\Source\\".$pdfFile;
$output="C:\\XAMPP\\htdocs\\primoTRPLE\\uploadfiles\\".$BookID."\\".$outpdf;

$cmd = "C:\\XAMPP\\htdocs\\primoTRPLE\\pdfSplitter\\pdftk.exe " .' "'.$input.'"'." cat ".$pages."output ".' "'.$output.'"'; 

exec($cmd, $outputFile);

$nfilename=pathinfo($outpdf, PATHINFO_FILENAME);

 

$isImage=$_POST['PDFImage'];


if ($isImage=="YES"){
	// echo "test";
	if (!is_dir("uploadfiles/DSRS/OCR/INPUT/".$nfilename)){
		mkdir("uploadfiles/DSRS/OCR/INPUT/".$nfilename);	
	}
	$forOCR= "C:\\XAMPP\\htdocs\\primoTRPLE\\uploadfiles\\DSRS\\OCR\\INPUT\\".$nfilename."\\".$outpdf;
	file_put_contents("C:\\XAMPP\\htdocs\\primoTRPLE\\uploadfiles\\DSRS\\OCR\\INPUT\\".$nfilename."\\".$nfilename.".sts", "Batch Completed");

	copy($output, $forOCR);
	echo $forOCR;

	// exec("C:\\Tools\\TRPLE\\DSRS01_v2.8.0.0_OCRMode\\DSR System.exe", $outputFile);

}
else{
	if (!is_dir("uploadfiles/DSRS/NORMAL/INPUT/".$nfilename)){
		mkdir("uploadfiles/DSRS/NORMAL/INPUT/".$nfilename);	
	}
	$forOCR= "C:\\XAMPP\\htdocs\\primoTRPLE\\uploadfiles\\DSRS\\NORMAL\\INPUT\\".$nfilename."\\".$outpdf;
	file_put_contents("C:\\XAMPP\\htdocs\\primoTRPLE\\uploadfiles\\DSRS\\NORMAL\\INPUT\\".$nfilename."\\".$nfilename.".sts", "Batch Completed");

	copy($output, $forOCR);
	echo $forOCR;

	// exec("C:\\Tools\\TRPLE\\DSRS01_v2.8.0.0_NonOCRMode\\DSR System.exe", $outputFile);

	// $cmd = '"C:\\XAMPP\\htdocs\\primoTRPLE\\uploadfiles\\'.$BookID.'\\'.$outpdf.'"--"C:\\XAMPP\\htdocs\\primoTRPLE\\uploadfiles\\'.$BookID.'\\'.$nfilename.'.html"';

	// // echo $cmd;
 // 	file_put_contents("uploadfiles/HTMLConverter/".$nfilename.".txt", $cmd);
}


 


$BookTitle=GetWMSValue("Select BookTitle from tblbookinfo Where BookID='$BookID'","BookTitle",$conWMS);


$BookTitle= str_replace("'","''", $BookTitle);
$Filename=$pdfFile;
$nBookID=$BookID;
$WorkFlowID=1;

$page1 = explode( ' ',$pages);
$pagecount = count($page1);

ExecuteQuerySQLSERVER("Update tblbookinfo Set CurrentStatus='Registered',CurrentTask='TEXT EDITING',MaxPageCount='$pagecount' Where BookID='$BookID'",$conWMS);



ExecuteQuerySQLSERVER("INSERT INTO tblbook (BookID,BookTitle,ProductType,Country,DateRegistered,BookSource,ReferenceID) VALUES ('".$gBookID."','".$BookTitle."','".$ProductType."','Cases','".$sDate."','".$nBookID."/".$outpdf."','".$nBookID."')",$conWMS);


    $ShipmentName='New Content';
    $sqls="exec USP_PRIMO_WKTAA_REGISTERJOB @ShipmentName='".$ShipmentName."',@Filename='".$gBookID."',@JobPrefix='".$Prefix."',@WorkflowName='".$WorkFlow."',@nFilename='".$Filename."'";
    ExecuteQuerySQLSERVER ($sqls,$conWMS);



if ($Task=='COPY EDITING'){
		$BatchID=GetWMSValue("Select BatchId from primo_view_jobs Where ExternalReference='$gBookID' AND ProcessCode='AUTHORING'","BatchId",$conWMS);
		$BatchName=GetWMSValue("Select BatchName from primo_view_jobs Where ExternalReference='$gBookID'  AND ProcessCode='AUTHORING'","BatchName",$conWMS); 

		 $sqls="exec usp_PRIMO_SkipBatchTask @BatchId='".$BatchID."',@BatchName='".$BatchName."',@ProcessCode='AUTHORING'";

		
	ExecuteQuerySQLSERVER ($sqls,$conWMS); 	
	if (!is_dir("uploadfiles/$nBookID/AUTHORING")){
		mkdir("uploadfiles/$nBookID/AUTHORING");	
	}

	copy("uploadfiles/$nBookID/".str_replace(".pdf", ".html", $pdfFile),"uploadfiles/$nBookID/AUTHORING/".$BookID.".html");
  
}

// Golden Gate Integration
 

// $token=$_POST['Token'];
 

// $JobId= $_SESSION['JobID'];
// // data fields for POST request
// $fields = array();
// $_SESSION['token']=$token;
// // files to upload
// $filenames = array($output);
// $filename=$Filename;
// $files = array();
// foreach ($filenames as $f){
//    $files[$f] = file_get_contents($f);
// }
 
 

// // URL to upload to
// $url = "https://api.innodata.com/v1.1/documents";


// $curl = curl_init();

// $url_data = http_build_query($fields);
// $ext = pathinfo($filename, PATHINFO_EXTENSION);

// $boundary = uniqid();
// $delimiter = '-------------' . $boundary;

// $post_data = build_data_files($boundary, $fields, $files);

// if (strtoupper($ext)=='PDF'){
//   $xType='application/pdf';
// }
// else{
//   $xType='text/'.$ext;
// }


// curl_setopt_array($curl, array(
//   CURLOPT_URL => $url,
//   CURLOPT_RETURNTRANSFER => 1,
//   CURLOPT_MAXREDIRS => 10,
//   CURLOPT_TIMEOUT => 30,
//   //CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//   CURLOPT_CUSTOMREQUEST => "POST",
//   CURLOPT_POST => 1,
//   CURLOPT_POSTFIELDS => $post_data,
//   CURLOPT_USERPWD => $token.":".$token,
//   // "Authorization: Basic dXNlci1saXZlLTYzMmE1YTYzLWQ2ZDYtNDI0Ni05MWNhLWQ1NDY2MzI2OThkMzo=",
//   CURLOPT_HTTPHEADER => array(
//     "Content-Type: application/octet-stream; boundary=" . $delimiter,
//     "X-Name: " . $filename,
//     "X-type: ".$xType ,
//     "Content-Length: " . strlen($post_data)

//   ),

  
// ));

 
// $response = curl_exec($curl);

// $jobj = json_decode($response);

// $ContentURI = $jobj->response->contents_uri;
// $fSize = $jobj->response->size;

// ExecuteQuerySQLSERVER("Update PRIMO_integration SET ContentURI='".$ContentURI."' Where Filename='".$filename."'",$conWMS);
 
 
// $info = curl_getinfo($curl);
 
// curl_close($curl);
  

// PostJob($token,$ContentURI,$filename,$fSize,$conWMS,$isImage);
// END OF INTEGRATION

?>