<?php
use Smalot\PdfParser\Parser;
use Smalot\PdfParser\XObject\Image;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

function getAPIKey($GGUserName,$GGPassword,$GGProductionMode){
  	$ch = curl_init();

  	$post_fields = array(
  		"authentication_method" => "password",
  		"password" => $GGPassword,
  		"request_root" => true,
  		"username" => $GGUserName
  	);
	curl_setopt($ch, CURLOPT_URL, 'https://api.innodata.com/v1.1/users/login');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_fields));
	//curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"authentication_method\":\"password\",\"password\":\"".$GGPassword."\",\"request_root\":true,\"username\":\"".$GGUserName."\"}");

	$headers = array();
	$headers[] = 'Accept: application/json';
	$headers[] = 'Content-Type: application/json';
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	$jobj = json_decode($result);

	// $token = $jobj->response->api_keys->live;
	if ($GGProductionMode=='ON'){
		  $token = $jobj->response->api_keys->live;
	}
	else{
		  $token = $jobj->response->api_keys->test;	
	}



	if (curl_errno($ch)) {
	    echo 'Error:' . curl_error($ch);
	}
	curl_close($ch);

	return $token;
}



function GetRecordCount($sql,$conWMS){
	
	$rs=odbc_exec($conWMS,$sql);
	$ctr = odbc_num_rows($rs);
	 
	return $ctr;
		
}

function ExecuteQuery($prSQL,$con){
	
	if (!mysqli_query($con,$prSQL))
	{
		  return 'Error: ' . mysqli_error($con);
	}  

}

function ExecuteMySql($con, $sql){
    $results = mysqli_query($con,$sql);
    if(!$results){
        return false; 
    }else{
        return $results;
    }
}

function ExecuteQuerySQLSERVER($prSQL,$conWMS){
		 
	$res= odbc_exec($conWMS,$prSQL);
	
  	if (!$res) {
  		  return odbc_errormsg();
	    // print("SQL statement failed with error:\n");
	  } else {
			  return ''; // print("One data row inserted.\n");
  	}  
}
	
function GetFieldValue($sql,$fieldVal,$con){
 
	if ($result=mysqli_query($con,$sql))
  	{
	  // Fetch one and one row
	  while ($row=mysqli_fetch_row($result))
		{
			 
			return $row[0];
			 
		}
  	}
}
	
function GetWMSValue($sql,$fieldVal,$conWMS){
	  
	$Val='';	
	$rs=odbc_exec($conWMS,$sql);
	$ctr = odbc_num_rows($rs);
	while(odbc_fetch_row($rs))
	{
		  $Val=odbc_result($rs,$fieldVal);
	}
	return $Val;
	
}

	
function GenerateGraphData($prDateFrom,$prDateTO,$prStatus,$prCode,$conWMS) {
  	$prDateFrom = $prDateFrom.' 11:59:59 PM';
  	$prDateTO = $prDateTO.' 12:00:00 AM';
  	if ($prStatus=='On-Going'){
  		$sqlInfo = "Select Count(*) as TotalCount from  primo_view_jobs  Where StatusString IN ('Allocated','Started','Ongoing') AND LastUpdate>='$prDateFrom' AND LastUpdate<='$prDateTO' AND ProcessCode='STYLING'  ";
  	}
  	elseif ($prStatus=='New'){
  		$sqlInfo = "Select Count(*) as TotalCount from  primo_view_jobs  Where StatusString IN ('New') AND LastUpdate>='$prDateFrom' AND LastUpdate<='$prDateTO' AND ProcessCode='STYLING' and SourceURL<>'test'";
  	}
  	else{
  		$sqlInfo = "Select Count(*) as TotalCount from  primo_view_jobs  Where StatusString IN ('$prStatus') AND LastUpdate>='$prDateFrom' AND LastUpdate<='$prDateTO' AND ProcessCode='STYLING' ";

    }

  	$rsInfo = odbc_exec($conWMS,$sqlInfo);
  	$ADM=odbc_result($rsInfo,"TotalCount");
  
  	if ($ADM==''){
	     $ADM=0;
  	}
  	if (empty($DataVault)){
    	 $DataVault=0;
  	}

  	if (empty($Dissertations)){
    	   $Dissertations=0;
  	}

  	$valAmount = $ADM.','.$DataVault.','.$Dissertations;
    
  	return $valAmount;
}

	
function GenerateBookID($sql,$idVal,$fieldVal,$conWMS){
		  	
	$rs=odbc_exec($conWMS,$sql);
	$ctr = odbc_num_rows($rs);
	$Val="";
	while(odbc_fetch_row($rs))
	{
		$nVal=odbc_result($rs,$fieldVal);
		$Val=odbc_result($rs,$fieldVal);
	}

	if ($Val!=''){
		$Val= substr($Val,-6);
		
		$Val=intval($Val)+1;
		$Val = $idVal.str_pad($Val, 6, '0', STR_PAD_LEFT);
	}
	else{
		$Val = $idVal.str_pad(1, 6, '0', STR_PAD_LEFT);	
	}
	
	return $Val;
		
} 

	

function formatXmlString($xml) {
  	// add marker linefeeds to aid the pretty-tokeniser (adds a linefeed between all tag-end boundaries)
  	$xml = preg_replace('/(>)(<)(\/*)/', "$1\n$2$3", $xml);
  	// now indent the tags
  	$token      = strtok($xml, "\n");
  	$result     = ''; // holds formatted version as it is built
  	$pad        = 0; // initial indent
  	$matches    = array(); // returns from preg_matches()
  	// scan each line and adjust indent based on opening/closing tags
  	while ($token !== false) :
	    // test for the various tag states
	    // 1. open and closing tags on same line - no change
	    if (preg_match('/.+<\/\w[^>]*>$/', $token, $matches)) :
	      $indent=0;
	    // 2. closing tag - outdent now
	    elseif (preg_match('/^<\/\w/', $token, $matches)) :
	      $pad--;
	    // 3. opening tag - don't pad this one, only subsequent tags
	    elseif (preg_match('/^<\w[^>]*[^\/]>.*$/', $token, $matches)) :
	      $indent=1;
	    // 4. no indentation needed
	    else :
	      $indent = 0;
	    endif;
	    // pad the line with the required number of leading spaces
	    $line    = str_pad($token, strlen($token)+$pad, "\t", STR_PAD_LEFT);
	    $result .= $line . "\n"; // add to the cumulative result, with linefeed
	    $token   = strtok("\n"); // get the next token
	    $pad    += $indent; // update the pad size for subsequent lines
  	endwhile;
  	return $result;
}


function build_data_files($boundary, $fields, $files){
    $data = '';
    $eol = "\r\n";

    $delimiter = '-------------' . $boundary;

    foreach ($fields as $name => $content) {
        $data .= "--" . $delimiter . $eol
            . 'Content-Disposition: form-data; name="' . $name . "\"".$eol.$eol
            . $content . $eol;
    }


    foreach ($files as $name => $content) {
      $fieldname='file';
        $data .= "--" . $delimiter . $eol
            . 'Content-Disposition: form-data; name="' . $fieldname . '"; filename="' . $name . '"' . $eol
            . 'Content-Type: xml/text'.$eol
            // . 'Content-Transfer-Encoding: binary'.$eol
            ;

        $data .= $eol;
        $data .= $content . $eol;
       
    }
    $data .= "--" . $delimiter . "--".$eol;


    return $data;
}

function UploadPhoto($prFilename,$prUser){
  
  if(isset($_FILES[$prFilename])){
      $name_array = $_FILES[$prFilename]['name'];
      $tmp_name_array = $_FILES[$prFilename]['tmp_name'];
      $type_array = $_FILES[$prFilename]['type'];
      $size_array = $_FILES[$prFilename]['size'];
      $error_array = $_FILES[$prFilename]['error'];
      
      mkdir("uploadfiles/".$prUser, 0777, true);

      if(move_uploaded_file($tmp_name_array, "uploadfiles/".$prUser."/".$prUser.".".$type_array)){
        // echo $name_array[$i]." upload is complete<br>";
      } else {
          //echo "move_uploaded_file function failed for ".$name_array[$i]."<br>";
      }
  
  }
}

function uploadSourceFiles($postdata, $files){
    global $SourceFilePath, $con;
    $total = count($files['files']['name']);
    $reference_code = !empty($postdata['reference_code']) ? trim($postdata['reference_code']): '';
    $preship_date = !empty($postdata['preship_date']) ? $postdata['preship_date'] : '';
    $prec_bundle = !empty($postdata['prec_bundle']) ? $postdata['prec_bundle'] : '';
    $tat = !empty($postdata['tat']) ? $postdata['tat'] : 0;
    $auto_segregate = !empty($postdata['auto_segregate']) ? true: false;
    $auto_GG = ($auto_segregate) ? (!empty($postdata['auto_upload_to_GG']) ? true: false) : false; 

    $results = array();
    
    //check for reference code if exists to other bundle
    $sql_check_ref = "SELECT COUNT(*) as total_cnt, prec_bundle FROM files_for_batching WHERE reference_code = '{$reference_code}' AND prec_bundle <> '{$prec_bundle}' ";

    $sql_check_ref_result = mysqli_query($con, $sql_check_ref)->fetch_assoc();
    if($sql_check_ref_result['total_cnt'] > 0){
      $results[] = array("success" => false, "message" => "Error: Reference Code already belong to bundle({$sql_check_ref_result['prec_bundle']})!");

      return $results;
    }

    //check for reference code if match with bundle
    $sql_check_bundle = "SELECT * FROM files_for_batching WHERE prec_bundle = '{$prec_bundle}' LIMIT 1";
    $sql_check_bundle_result = mysqli_query($con, $sql_check_bundle)->fetch_assoc();
    if(!empty($sql_check_bundle_result)){
        if($sql_check_bundle_result['reference_code'] != $reference_code){
            $results[] = array("success" => false, "message" => "Error: Reference Code not match with existing bundle ref code({$sql_check_bundle_result['reference_code']})!");

            return $results;
        }
    }

    for( $i=0; $i < $total; $i++) {
        $tmpFilePath = $files['files']['tmp_name'][$i];

        if(!empty($tmpFilePath)){
            $newFilePath = $SourceFilePath."/".$files['files']['name'][$i];
            $bundle_path = '';
            if(!empty($postdata['prec_bundle'])){
                $bundle_path =$SourceFilePath.'/'.$postdata['prec_bundle'];
                if(!file_exists($bundle_path)){
                    mkdir($bundle_path, 0777, true);
                    chmod($bundle_path, 077);
                }
            }
            if(!empty($bundle_path)){
                $newFilePath = $bundle_path.'/'.$files['files']['name'][$i];
            }

            $ext = pathinfo($newFilePath, PATHINFO_EXTENSION);
            $filename=pathinfo($newFilePath, PATHINFO_FILENAME).".".$ext;
            
            if(strtoupper($ext) != 'PDF'){
                //not pdf file will not be included
                $results[] = array("success" => false, "message" => "Only PDF File is allowed ({$filename})");
            }else{

                if(move_uploaded_file($tmpFilePath, $newFilePath)) {

                    $sql="INSERT INTO files_for_batching (`file_name`, `reference_code`, `precship_date`, `prec_bundle`, `tat`, `created_by`) 
                          VALUES ('{$filename}','{$reference_code}', '{$preship_date}', '{$prec_bundle}', '{$tat}', '{$_SESSION['UserID']}')";
        
                    ExecuteQuery($sql,$con);

                    $postdata['auto_upload_to_gg'] = $auto_GG;
                    $last_inserted_id = mysqli_insert_id($con);
                    if($auto_segregate){
                        sleep(7);
                        auto_segregate($newFilePath, $last_inserted_id, $postdata);
                    }
                    $results[] = array("success" => true, "message" => "Successfully uploaded ({$filename}) ");
                }else{
                    $results[] = array("success" => false, "message" => "Error uploading file ({$filename})");
                }
            }
        }
    }

    return $results;
}

function MultipleFileUploadEX($prFileName,$TLID,$SubFolder,$conWMS,$GGUserName,$GGPassword,$GGProductionMode, $for_batching = false, $postdata = array()){
  	global $base_url, $WorkflowID;

  	$total = count($_FILES[$prFileName]['name']);
  	if(!file_exists($SubFolder)){
  		 mkdir($SubFolder, 0777, true);
  	}
  	
    $results = array();
  	// Loop through each file
  	for($i=0; $i<$total; $i++) {
  	    //Get the temp file path
  	    $tmpFilePath = $_FILES[$prFileName]['tmp_name'][$i];

  	    //Make sure we have a filepath
  	    if ($tmpFilePath != ""){
  	      	//Setup our new file path
            $newFilePath = $SubFolder."/". $_FILES[$prFileName]['name'][$i];
            $bundle_path = '';
            if(!empty($postdata['prec_bundle'])){
                $bundle_path = $SubFolder.'/'.$postdata['prec_bundle'];
                if(!file_exists($bundle_path)){
                    mkdir($bundle_path, 0777, true);
                    chmod($bundle_path, 077);
                }
            }
            if(!empty($bundle_path)){
                $newFilePath = $bundle_path.'/'.$_FILES[$prFileName]['name'][$i];
            }

  	      	$ext = pathinfo($newFilePath, PATHINFO_EXTENSION);
            $filename=pathinfo($newFilePath, PATHINFO_FILENAME).".".$ext;
            
            if(strtoupper($ext) != 'PDF'){
                $results[] = array("response" => "error", "message" => "Only PDF file allowed", "filename" => $filename);
            }else{
                $file_path = $bundle_path."/".$filename;
                
                if(!file_exists($file_path)){
          	      	//Upload the file into the temp dir
          	      	if(move_uploaded_file($tmpFilePath, $newFilePath)) {
          	             //for batching no need to go thru wms
                        if($for_batching){
                           $results[] = uploadToGoldenGateOnly($filename, $postdata);
                        }else{
              	          	$ext = pathinfo($newFilePath, PATHINFO_EXTENSION);
              	   
              	          	$filename=pathinfo($newFilePath, PATHINFO_FILENAME).".".$ext;

              	          	$sqls="EXEC USP_PRIMO_INTEGRATE_old @ExecutionId=1,  @mainUrl='www.example.com',  @SourceUrl='".$base_url."/".$newFilePath."',@Filename='".$filename."',@Jobid=''";
              	        
              	          	$integrate_result = ExecuteQuerySQLSERVER ($sqls,$conWMS);
                            $JobID= GetWMSValue("Select JobID From PRIMO_Integration WHERE Filename='".$filename."'","JobId",$conWMS);

              	          	// $BatchId=GetWMSValue("Select BatchId from primo_view_Jobs Where JobID='$JobID'","BatchId",$conWMS);

              	          	// $sqls="EXEC USP_PRIMO_HOLDBATCH @BatchId=".$BatchId;
              	     
              	          	// ExecuteQuerySQLSERVER ($sqls,$conWMS);
              	   
              	          	// ExecuteQuerySQLSERVER ("Update PRIMO_Integration Set DocumentType='Awards' Where JobId='".$JobID."'",$conWMS);
                            if(empty($JobID)){
                                $results[] = array("response" => "error", "message" => "Error integrating file to WMS", "filename" => $filename);
                                unlink($newFilePath); 
                            }else{
                                $sFilename=$filename;
                                $token = getAPIKey($GGUserName,$GGPassword,$GGProductionMode);

                                $results[] = UploadToGoldenGate($filename,$JobID,$token,$conWMS,$sFilename, $postdata);
                            }
              	        }
                    }
                }else{
                    $results[] = array("response" => "error", "message" => "File already been uploaded and processed!", "filename" => $filename);
                } 
            }
  	    }
  	}

    return $results;
}

/*upload to golden gate only without connection to WMS, without zoning, mapping and reading ML*/
function uploadToGoldenGateOnly($prFilename, $postdata = array()){
    global $GGUserName, $GGPassword, $GGProductionMode, $SourceFilePath;
    global $GGTeam, $GGTaxonomy, $GGZoningTaxonomy;
    global $con;
   
    $token = getAPIKey($GGUserName,$GGPassword,$GGProductionMode);

    $fields = array();

    $_SESSION['token']=$token;
    // files to upload
    $filenames = array($SourceFilePath.$prFilename);

    $files = array();
    foreach ($filenames as $f){
       $files[$f] = file_get_contents($f);
    }
   
    // URL to upload to
    $url = "https://api.innodata.com/v1.1/documents";
    $curl = curl_init();

    $url_data = http_build_query($fields);
    $ext = pathinfo($prFilename, PATHINFO_EXTENSION);

    $boundary = uniqid();
    $delimiter = '-------------' . $boundary;

    $post_data = build_data_files($boundary, $fields, $files);

    if (strtoupper($ext)=='PDF'){
        $xType='application/pdf';
    }
    else{
        $xType='text/'.$ext;
    }
    
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        //CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => $post_data,
        CURLOPT_USERPWD => $token.":".$token,
        // "Authorization: Basic dXNlci1saXZlLTYzMmE1YTYzLWQ2ZDYtNDI0Ni05MWNhLWQ1NDY2MzI2OThkMzo=",
        CURLOPT_HTTPHEADER => array(
          "Content-Type: application/octet-stream; boundary=" . $delimiter,
          "X-Name: " . $prFilename,
          "X-type: ".$xType ,
          "Content-Length: " . strlen($post_data)

        )
    ));

    //
    $response = curl_exec($curl);
    $jobj = json_decode($response);
    if($jobj){
        $GGUploadID = $jobj->response->id;
        $ContentURI = $jobj->response->contents_uri;

        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, 'https://api.innodata.com/v1.1/jobs');
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_POST, 1);
        $post_fields = array(
            "collaboration" => array(
                "teams" => array(["name" => $GGTeam, "steps" => ["*"]])
            ),
            "input_content" => array(
                "role" => "input", 
                "uri" => $ContentURI
            ),
            "metadata" => array(
                "mapping" => array(
                    "high_confidence_threshold" => "0",
                    "qa" => array(
                      "teams" => []
                    ),
                    "taxonomy" => $GGTaxonomy
                ),
                "zoning" => array(
                    "high_confidence_threshold" => "0",
                    "taxonomy" => $GGZoningTaxonomy
                ),
                "reading" => array(
                    "high_confidence_threshold" => "0"
                ),
                "text-extraction" => array(
                    "autoclean" => "true",
                    "ocr" => "true"
                )
           
            ),
            "type" => "doc2xml",//"data-point-extraction",
            "use_for_training" => false
        );


        curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($post_fields));
        curl_setopt($ch2, CURLOPT_USERPWD, $token . ":" . $token);  
        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch2);


        $jobj2 = json_decode($result);
        $GGJobID = $jobj2->response->id;
        
        $reference_code = !empty($postdata['reference_code']) ? $postdata['reference_code'] : '';
        $preship_date = !empty($postdata['preship_date']) ? $postdata['preship_date'] : '';
        $prec_bundle = !empty($postdata['prec_bundle']) ? $postdata['prec_bundle'] : '';
        $tat = !empty($postdata['tat']) ? $postdata['tat'] : 0;
        $num_pdf_files = !empty($postdata['num_pdf_files']) ? $postdata['num_pdf_files'] : 0;
            
        $sql="INSERT INTO files_for_batching (`file_name`,`GGUploadID`, `GGJobsID`, `content_uri`, `reference_code`, `precship_date`, `prec_bundle`, `tat`, `num_pdf_files`) 
                  VALUES ('{$prFilename}','{$GGUploadID}', '{$GGJobID}', '{$ContentURI}', '{$reference_code}', '{$preship_date}', '{$prec_bundle}', '{$tat}', '{$num_pdf_files}')";
        
        ExecuteQuery($sql,$con);

        return array("response" => "success", "message" => "successfully uploaded file for batching", "filename" => $prFilename);
    }else{
        return array("response" => "error", "message" => "error uploading file for batching", "filename" => $prFilename);
    }

}

function getGGData($GGJobID, $token = ''){

    global $GGUserName, $GGPassword, $GGProductionMode;

    $token = getAPIKey($GGUserName,$GGPassword,$GGProductionMode);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://api.innodata.com/v1.1/jobs/'.$GGJobID.'/contents/zoning-pagesmeta-json/1');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

    curl_setopt($ch, CURLOPT_USERPWD, $token . ":" . $token);  
    $headers = array();
    $headers[] = 'Accept: application/json';
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);

    curl_close($ch);

    return $result;
}

function getGGStatus($GGJobID, $token = ''){
    global $GGUserName, $GGPassword, $GGProductionMode;
    $token = getAPIKey($GGUserName,$GGPassword,$GGProductionMode);

    $ch = curl_init();
     
    curl_setopt($ch, CURLOPT_URL, 'https://api.innodata.com/v1.1/jobs/'.$GGJobID);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

    curl_setopt($ch, CURLOPT_USERPWD, $token . ":" . $token);  
    $headers = array();
    $headers[] = 'Accept: application/json';
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);

    curl_close($ch);

    return $result;
}

function getGGInnodom($uri, $token){
    
    $start = curl_init();
    curl_setopt($start, CURLOPT_URL, $uri);
    curl_setopt($start, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($start, CURLOPT_CUSTOMREQUEST, 'GET');

    curl_setopt($start, CURLOPT_USERPWD, $token . ":" . $token);  
    $file_data = curl_exec($start);
    curl_close($start);

    return $file_data;
}

function saveInnodom($filename, $file_data, $bundle = ''){
      global $SourceFilePath;
      
      try{
          $path_parts = pathinfo($filename);
          $nFilename= $path_parts['filename'];
          //bundle directory
          $exe_path = realpath(__DIR__.'/..');
          $exe = $exe_path.'/TPCPP17-InnoDomTransformation/TPCPP17-InnoDomTransformation.exe';

          $bundle_path = $SourceFilePath.'/'.$bundle;
          if(!file_exists($bundle_path)){
              mkdir($bundle_path, 0777, true);
              chmod($bundle_path, 0777);
          }

          // client path
          if(!file_exists($bundle_path)){
              mkdir($bundle_path, 0777, true);
          }

          if(!file_exists($bundle_path.'/sources')){
              mkdir($bundle_path.'/sources', 0777, true);
              chmod($bundle_path.'/sources', 0777);
          }

          $file_path = $bundle_path.'/'. $nFilename . '.xml';
          //innodom path
          $file_path1 = $bundle_path.'/sources/'. $nFilename . '_response.xml';
          $file = fopen($file_path1, 'w+');
          fputs($file, $file_data);
          fclose($file);

          //temp innodom, remove the content only transform the meta
          $file_path2 = $bundle_path.'/sources/'. $nFilename . '_response2.xml';
          $file2 = fopen($file_path2, 'w+');
          $file_data2 = preg_replace("/<\/inno:content>/", "ƒ</inno:content>", $file_data); 
          $file_data2 = preg_replace("/<inno:content>[^ƒ]+ƒ<\/inno:content>/", '<inno:content><inno:text id="text-9" type="text"><inno:block page="3" page-height="512" page-width="395" x-max="224.0" x-min="95.0" y-max="126.0" y-min="111.0">Final Transformation is after styling </inno:block></inno:text></inno:content>', $file_data2); 
          fputs($file2, $file_data2);
          fclose($file2);

          //cropping image
          $cropping_exe = $exe_path.'/GGImageCropping/GGImageCrop.exe';
          $cropping_source_pdf = $bundle_path.'/'.$nFilename.'.pdf';
          $cropping_outputpath = $bundle_path.'/'.$nFilename;
          if(!file_exists($cropping_outputpath)){
              mkdir($cropping_outputpath, 0777, true);
          }

          $explode_name = explode('_',$nFilename);
          $folder = $explode_name[0];
          $source_dir = $bundle_path.'/deliverables';
          if(!file_exists($source_dir)){
              mkdir($source_dir, 077, true);
          }

          //delete existing BMP
          if(file_exists($cropping_outputpath.'/images')){
              $dir = new DirectoryIterator($cropping_outputpath.'/images');
              foreach ($dir as $fileinfo) {
                  if (!$fileinfo->isDot()) {
                      unlink($cropping_outputpath.'/images/'.$fileinfo->getFilename());
                      if(file_exists($source_dir.'/'.pathinfo($fileinfo->getFilename(), PATHINFO_FILENAME).'.BMP')){
                          unlink($source_dir.'/'.pathinfo($fileinfo->getFilename(), PATHINFO_FILENAME).'.BMP');
                      }
                  }
              }
          }

          //transformation
          $cmd = '"'.$exe.'" "'.$file_path2.'" "'.$file_path.'"';
          exec($cmd);

          //check if successfully created then reformat
          if(file_exists($file_path)){
              $xml = file_get_contents($file_path);
              $xml = replaceXMLInvalidTag($xml);
              $xml = reformatXML($xml);
              $exploded_xml = explode('<', $xml);
              $doc_type = $exploded_xml[1];

              $tag_N = strtoupper(substr($doc_type, 0, 1));
              $graphic_filename = $nFilename.'.PDF';
              $graphic_tag = '<GRAPHIC FILENAME="'.$graphic_filename.'"><LINKTEXT>Original Image of this Document (PDF)</LINKTEXT></GRAPHIC>';
              $xml = preg_replace("/<({$doc_type})(.*?)(<DOCTI>)/", "<$1<N>{$tag_N}</N>{$graphic_tag}$2$3", $xml); 
              $xml = str_replace(">".PHP_EOL."<N>","><N>", $xml);

              $xml = returnBackReplaceInvalidXMLTag($xml);
              file_put_contents($file_path, $xml);

              //sreplace 
              $ereplace_exe = $exe_path.'/MetaDataSReplace\SReplace.NET.exe';
              $sreplace_cmd = '"'.$ereplace_exe.'" "'.$file_path.'"';
              exec($sreplace_cmd);
              
              //process cropping renaming
              $cropping_cmd = '"'.$cropping_exe.'" "'.$cropping_source_pdf.'" "'.$file_path1.'" "'.$cropping_outputpath.'"';
              exec($cropping_cmd);
              $crop_img_path = $cropping_outputpath.'/images';
              
              if(file_exists($crop_img_path)){
                  $xml = file_get_contents($file_path);
                  
                  $xml_crop = file_get_contents($cropping_outputpath.'/'.$nFilename . '_response.xml');
                  $dom = new DOMDocument;
                  libxml_use_internal_errors(true);
                  $dom->loadXML($xml_crop);

                  $xpath = new DOMXPath($dom);

                  $item_tags = $xpath->query('//inno:content/inno:image/inno:block/inno:img_source');
                  $x = 0;
                  foreach ($item_tags as $tag) {
                      if(!empty($tag->getAttribute('value'))){
                          $x++;
                          $image = explode('/', $tag->getAttribute('value'));
                          $img_name = $image[count($image) - 1];
                          
                          $bmp_name = str_replace('CARSWELL','',strtoupper($folder)).$x;
                          $bmp_name = bmpFileName($source_dir.'/'.$bmp_name.'.BMP', $folder, $x);
                          if(file_exists($crop_img_path.'/'.$img_name)){
                              $image = new Imagick($crop_img_path.'/'.$img_name);
                              $image->setResolution(300, 300);
                              $image->setImageFormat('jpeg');
                              $image->setImageCompression(imagick::COMPRESSION_JPEG); 
                              $image->setImageCompressionQuality(100);
                              $image->writeImage($source_dir.'/'.$bmp_name.'.BMP');
                              rename($crop_img_path.'/'.$img_name, $crop_img_path.'/'.$bmp_name.'.png');

                              $xml = str_replace(array('<IMAGE/>', '<IMAGE />'), '<IMAGE>', $xml);
                              $xml = str_replace(array('</IMAGE>'), '', $xml);
                              $xml = preg_replace('/<IMAGE>/', '<GRAPHIC FILENAME="'.$bmp_name.'.BMP"><LINKTEXT>Graphic '.substr($bmp_name, -1).'</LINKTEXT></GRAPHIC>', $xml, 1);
                          }
                      }
                  }

                  //update the transformation to set the BMP file
                  $xml = reformatXML($xml);
                  file_put_contents($file_path, $xml);
                  $sreplace_cmd = '"'.$ereplace_exe.'" "'.$file_path.'"';
                  exec($sreplace_cmd);
              
              }
          }
          
          return true;
          
    }catch(Exception $e){
        return $e->getMessage();
    }
}

function replaceXMLInvalidTag($xml){
    for($i = 1; $i <= 9999; $i++){
        //in case no end tag
        $tobe_replace = array('<TBLROWXX'.$i.' >', '<TBLROWXX '.$i.'>','<TBLROWXX'.$i.'>', '<TBLROWXX '.$i.' >');
        $replace_with = '<?TBLROW '.$i.'>';
        $xml = str_replace($tobe_replace, $replace_with, $xml);

        $tobe_replace = array('<?TBLROW '.$i.'>', '<? TBLROW '.$i.'>', '<? TBLROW '.$i.' >', '<?TBLROW '.$i.' >');
        $replace_with = '<TBLROWXX'.$i.'/>';
        $xml = str_replace($tobe_replace, $replace_with, $xml);
    }
    
    $xml = str_replace('&', '....XXXX....XXXX....', $xml);
    $xml = preg_replace("/<(TBLCDEF )(.*?)>/", "<$1$2></$1>", $xml);
    $xml = str_replace(array("/></TBLCDEF>", "/></TBLCDEF >"),  "></TBLCDEF>", $xml);
    return $xml;
}

function returnBackReplaceInvalidXMLTag($xml){
    $xml = str_replace('....XXXX....XXXX....', '&', $xml);
    for($i = 1; $i <= 9999; $i ++){
        $replace_with = '<?TBLROW '.$i.'>';
        $xml = str_replace(array('</TBLROWXX'.$i.' >', '</TBLROWXX '.$i.'>','</TBLROWXX'.$i.'>', '</TBLROWXX '.$i.' >'), "", $xml);
        $tobe_replace = array('<TBLROWXX'.$i.' />', '<TBLROWXX '.$i.'/>','<TBLROWXX'.$i.'/>', '<TBLROWXX '.$i.' />');
        $xml = str_replace($tobe_replace, $replace_with, $xml);

        //in case no end tag
        $tobe_replace = array('<TBLROWXX'.$i.' >', '<TBLROWXX '.$i.'>','<TBLROWXX'.$i.'>', '<TBLROWXX '.$i.' >');
        $xml = str_replace($tobe_replace, $replace_with, $xml);
    }
    $xml = str_replace(array('</TBLCDEF>', '</TBLCDEF >'), '', $xml);
    return $xml;
}
function bmpFileName($file, $folder, $cnt){

    $bmp_name = pathinfo($file, PATHINFO_FILENAME);
    if(file_exists($file)){
        $cnt++;
        $bmp_name = str_replace('CARSWELL','',strtoupper($folder)).$cnt;
        $path = pathinfo($file, PATHINFO_DIRNAME).'/'.$bmp_name;
        $bmp_name = bmpFileName($path, $folder, $cnt);
    }

    return $bmp_name;
}

function reformatXML($xml){

    $reformatted_xml = preg_replace('/\h+/', ' ', $xml); //multiple spaces
    $reformatted_xml = preg_replace('/^[ \t]*[\r\n]+/m', '', $reformatted_xml);// remove multiple blank new line and tab
    $reformatted_xml = preg_replace('/( <)/', "<", $reformatted_xml); //remove space before start tag
    //$reformatted_xml = preg_replace('/>\s+/', '>', $reformatted_xml); //multiple spaces
    //$reformatted_xml = preg_replace('/><+/', '>'.PHP_EOL.'<', $reformatted_xml); //set new line for new tags

    return $reformatted_xml;
} 

function UploadToGoldenGate($prFilename,$prJobID,$token,$conWMS,$sFilename, $postdata = array()){
    global $SourceFilePath, $con;
    $fields = array();

    $_SESSION['token']=$token;
    // files to upload
    $bundle_path = !empty($postdata['prec_bundle']) ? $postdata['prec_bundle'] : '';
    $filenames = array($SourceFilePath.'/'.$bundle_path.'/'.$prFilename);

    $files = array();
    foreach ($filenames as $f){
        if(file_exists($f)){
           $files[$f] = file_get_contents($f);
        }
    }
   
    // URL to upload to
    $url = "https://api.innodata.com/v1.1/documents";
	  $curl = curl_init();

    $url_data = http_build_query($fields);
    $ext = pathinfo($prFilename, PATHINFO_EXTENSION);

    $boundary = uniqid();
    $delimiter = '-------------' . $boundary;

    $post_data = build_data_files($boundary, $fields, $files);

    if (strtoupper($ext)=='PDF'){
      	$xType='application/pdf';
    }
    else{
      	$xType='text/'.$ext;
    }
    
    curl_setopt_array($curl, array(
      	CURLOPT_URL => $url,
      	CURLOPT_RETURNTRANSFER => 1,
      	CURLOPT_MAXREDIRS => 10,
      	CURLOPT_TIMEOUT => 0,
      	//CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      	CURLOPT_CUSTOMREQUEST => "POST",
      	CURLOPT_POST => 1,
      	CURLOPT_POSTFIELDS => $post_data,
      	CURLOPT_USERPWD => $token.":".$token,
      	// "Authorization: Basic dXNlci1saXZlLTYzMmE1YTYzLWQ2ZDYtNDI0Ni05MWNhLWQ1NDY2MzI2OThkMzo=",
      	CURLOPT_HTTPHEADER => array(
        	"Content-Type: application/octet-stream; boundary=" . $delimiter,
        	"X-Name: " . $prFilename,
        	"X-type: ".$xType ,
        	"Content-Length: " . strlen($post_data)

      	)
	  ));

    //
    $response = curl_exec($curl);
    $jobj = json_decode($response);

    if($jobj){
        $ContentURI = $jobj->response->contents_uri;
        $fSize = $jobj->response->size;

        ExecuteQuerySQLSERVER("Update PRIMO_integration SET ContentURI='".$ContentURI."' Where JobId='".$prJobID."'",$conWMS);

        //save to primo DB
        $reference_code = !empty($postdata['reference_code']) ? $postdata['reference_code'] : '';
        $preship_date = !empty($postdata['preship_date']) ? $postdata['preship_date'] : '';
        $prec_bundle = !empty($postdata['prec_bundle']) ? $postdata['prec_bundle'] : '';
        $tat = !empty($postdata['tat']) ? $postdata['tat'] : 0;
        $num_pdf_files = !empty($postdata['num_pdf_files']) ? $postdata['num_pdf_files'] : 0;
        $segragated_id = !empty($postdata['id']) ? $postdata['id'] : 0;

        if(empty($segragated_id)){
            $sql = "
                  INSERT INTO `segregated_files` 
                    (`wms_job_id`, 
                    `batched_parent_id`, 
                    `reference_code`, 
                    `precship_date`, 
                    `prec_bundle`, 
                    `tat`, 
                    `num_pdf_files`
                    )
                    VALUES
                    ('{$prJobID}', 
                    '0', 
                    '{$reference_code}', 
                    '{$preship_date}', 
                    '{$prec_bundle}', 
                    '{$tat}', 
                    '{$num_pdf_files}'
                    );
                ";
            ExecuteQuery($sql,$con);
        
        }else{
            $sql = "UPDATE `segregated_files` SET wms_job_id ='{$prJobID}' WHERE id = '{$segragated_id}'";
            ExecuteQuery($sql,$con);
        }

        
        $info = curl_getinfo($curl);
        curl_close($curl);

        $isImage = false;
        PostJob($token,$ContentURI,$prFilename,$fSize,$conWMS,$isImage,$prJobID);

        return array("response" => "success", "message" => "successfully uploaded file", "filename" => $prFilename);
    }else{
        return array("response" => "error", "message" => "error uploading file", "filename" => $prFilename);
    }

}


function PostJob($token,$ContentURI,$filename,$fSize,$conWMS,$isImage,$prJobID){
    
   	global $GGTeam, $GGTaxonomy, $GGZoningTaxonomy;

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'https://api.innodata.com/v1.1/jobs');

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // CURLOPT_USERPWD => $token.":".$token,
    curl_setopt($ch, CURLOPT_POST, 1);
 

    $post_fields = array(
     		"collaboration" => array(
     			  "teams" => array(["name" => $GGTeam, "steps" => ["*"]])
     		),
     		"input_content" => array(
       			"role" => "input", 
       			"uri" => $ContentURI
     		),
     		"metadata" => array(
       			"mapping" => array(
         				"high_confidence_threshold" => "1.0",
         				"qa" => array(
         					"teams" => []
         				),
         				"taxonomy" => $GGTaxonomy
       			),
       			"zoning" => array(
         				"high_confidence_threshold" => "1.0",
         				"taxonomy" => $GGZoningTaxonomy
      			),
      			"reading" => array(
      				  "high_confidence_threshold" => "1.0"
      			),
            "text-extraction" => array(
                "autoclean" => "true",
                "ocr" => "true",
                "dehyphenation" => "true"
            )
     		),
        "type" => "data-point-extraction", //"doc2xml",//
     		"use_for_training" => true
   	);


    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_fields));
 	  //curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"collaboration\":{\"teams\":[{\"name\":\"".$GGTeam."\",\"steps\":[\"*\"]}]},\"input_content\":{\"role\":\"input\",\"uri\":\"".$ContentURI."\"},\"metadata\":{\"mapping\":{\"high_confidence_threshold\":\"0\",\"qa\":{\"teams\":[]},\"taxonomy\":\"".$GGTaxonomy."\"},\"zoning\":{\"high_confidence_threshold\":\"1\",\"taxonomy\":\"".$GGZoningTaxonomy."\"},\"reading\":{\"high_confidence_threshold\":\"1\"}},\"type\":\"data-point-extraction\",\"use_for_training\":true}");

 	  curl_setopt($ch, CURLOPT_USERPWD, $token . ":" . $token);  
    $headers = array();
    $headers[] = 'Accept: application/json';
    $headers[] = 'Content-Type: application/json';
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);


    $jobj = json_decode($result);
    $GGJobID = $jobj->response->id;
    
    ExecuteQuerySQLSERVER("Update PRIMO_integration SET GGJobID='".$GGJobID."'  Where JobId='".$prJobID."'",$conWMS);
    
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);
}

function reArrayFiles($file)
{
    $file_ary = array();
    $file_count = count($file['name']);
    $file_key = array_keys($file);
   
    for($i=0;$i<$file_count;$i++)
    {
        foreach($file_key as $val)
        {
            $file_ary[$i][$val] = $file[$val][$i];
        }
    }
    return $file_ary;
}

function BookFileContent($prFileName,$TLID,$conWMS){
    
    $total = count($_FILES[$prFileName]['name']);

    // Loop through each file
    for($i=0; $i<$total; $i++) {
        //Get the temp file path
        $tmpFilePath = $_FILES[$prFileName]['tmp_name'][$i];

        //Make sure we have a filepath
        if ($tmpFilePath != ""){
          	//Setup our new file path

          	$newFilePath = "uploadfiles/".$TLID."/". $_FILES[$prFileName]['name'][$i];
          	$prFileName=$_FILES[$prFileName]['name'][$i];
           	ExecuteQuerySQLSERVER("Update tblBookInfo SET BaseFile='".$prFileName."' WHERE BookID='".$TLID."'",$conWMS);
          	//Upload the file into the temp dir
          	move_uploaded_file($tmpFilePath, $newFilePath);
          
        }
    }
}

function MultipleFileUpload($prFileName,$TLID){
    
    $total = count($_FILES[$prFileName]['name']);

    // Loop through each file
    for($i=0; $i<$total; $i++) {
        //Get the temp file path
        $tmpFilePath = $_FILES[$prFileName]['tmp_name'][$i];

        //Make sure we have a filepath
        if ($tmpFilePath != ""){
            //Setup our new file path
            $newFilePath = "uploadfiles/".$TLID."/". $_FILES[$prFileName]['name'][$i];
            //Upload the file into the temp dir
            move_uploaded_file($tmpFilePath, $newFilePath);
        }
    }
}

function MultipleFileUpload1($prFileName,$TLID,$SubFolder){
    
    $total = count($_FILES[$prFileName]['name']);

    if ($total>0) {
        // delete_directory($dirname);
        mkdir("uploadfiles/".$SubFolder, 0777, true);
    }

    for($i=0; $i<$total; $i++) {
        //Get the temp file path
        $tmpFilePath = $_FILES[$prFileName]['tmp_name'][$i];

        //Make sure we have a filepath
        if ($tmpFilePath != ""){
            //Setup our new file path
            $newFilePath = "uploadfiles/".$SubFolder."/". $_FILES[$prFileName]['name'][$i];
            //Upload the file into the temp dir
            move_uploaded_file($tmpFilePath, $newFilePath);
        }
    }
}

function _utf8_decode($string)
{
    $tmp = $string;
    $count = 0;
    while (mb_detect_encoding($tmp)=="UTF-8")
    {
      $tmp = utf8_decode($tmp);
      $count++;
    }
    
    for ($i = 0; $i < $count-1 ; $i++)
    {
      $string = utf8_decode($string);
      
    }
    return $string;
  
}

function rrmdir($dir) { 
    if (is_dir($dir)) { 
       $objects = scandir($dir);
       foreach ($objects as $object) { 
         if ($object != "." && $object != "..") { 
            if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
                rrmdir($dir. DIRECTORY_SEPARATOR .$object);
            else
                unlink($dir. DIRECTORY_SEPARATOR .$object); 
         } 
       }
       rmdir($dir); 
   }else{
      unlink($dir);
   }
}

function auto_segregate($file_path, $parent_id, $post){
    global $con, $GGUserName,$GGPassword,$GGProductionMode, $conWMS, $base_url;
    $parser = new Parser();
    $pdf    = $parser->parseFile($file_path);
    $pages  = $pdf->getPages();

    $x = 0;
    $pages_to_split = array();
    foreach ($pages as $page) {
      $x ++;
        $contents = strtolower($page->getText());
        $output_content = preg_replace('/\s+/', '', $contents);
        if (stripos($output_content, "documenttitle:") !== false){
            $pages_to_split[] = $x;
        }
    } 
    
    $split_ranges = array();
    $total_pages = count($pages);
    $total_to_split = count($pages_to_split);

    $directory_path = dirname($file_path);
    $file = pathinfo($file_path);
    $filename = $file['filename'];
    $input = $file_path;
    
    //create summary page
    $outpdf = $filename."_P1_1.pdf";
    $output = $directory_path.'/'.$outpdf;
    $cmd = __DIR__.'/page_select/pdfSplitter/pdftk.exe '.$input.' cat 1-1 output '.$output; 
    exec($cmd, $output);

    if(!empty($pages_to_split)){
        foreach ($pages_to_split as $key => $value) {

            $start_split2 = $value + 1;
            $start_split = $value;

            if($key == $total_to_split - 1){
                $end_split = $total_pages;
            }else{
                $end_split = ($pages_to_split[$key + 1]) - 1;
            }

            $split_ranges[] = $start_split.'-'.$end_split;

            $outpdf = $filename."_P{$start_split}_{$end_split}1.pdf";
            $output = $directory_path.'/'.$outpdf;
            
            
            $cmd = __DIR__.'/page_select/pdfSplitter/pdftk.exe '.$input.' cat '.$start_split.'-'.$end_split.' output '.$output; 
            exec($cmd, $output);


            $output_filename = $filename.'_P'.$start_split.'_'.$end_split.'.pdf';
            $output_dir = $directory_path.'/'.$output_filename;

            //put to deliverables with cover page and summary sheet
            if(!file_exists($directory_path.'/deliverables')){
                mkdir($directory_path.'/deliverables', 0777,true);
            }
            
            $output_deliverables = $directory_path.'/deliverables/'.$output_filename;
            $cmd = __DIR__.'/page_select/pdfSplitter/pdftk.exe '.$input.' cat '.$start_split2.'-'.$end_split.' output '.$output_deliverables; 
            exec($cmd, $output);

            //convert auto segregated pdf to png then to pdf again because there is a problem in merging drectly 2 pdf
            $parser = new Parser();
            $pdfP    = $parser->parseFile($directory_path.'/'.$outpdf);
            $pages  = $pdfP->getPages();
            
            $temp_file_path = __DIR__.'/TEMP/'.$_SESSION['UserID'];
            if(!file_exists($temp_file_path)){
                mkdir($temp_file_path, 0777, true);
            }
            $cmd = __DIR__.'/mupdf/mutool convert -o '.$temp_file_path.'/'.$filename.'_%d.png '.$directory_path.'/'.$outpdf.' 1-N';
            exec($cmd);

            $x = 0;
            $image_pdf = new \Imagick();
            foreach ($pages as $page) {
                $x ++;
                $image_pdf->addImage(new \Imagick($temp_file_path.'/'.$filename.'_'.$x.'.png'));
                unlink($temp_file_path.'/'.$filename.'_'.$x.'.png');
            } 

            $image_pdf->writeImages($directory_path.'/'.$outpdf, true);
            //end summary auto segregated convert to image

            //convert summary sheet pdf to png then to pdf again because there is a problem in merging drectly 2 pdf
            $parser = new Parser();
            $pdfP    = $parser->parseFile($directory_path.'/'.$filename.'_P1_1.pdf');
            $pages  = $pdfP->getPages();
            
            $temp_file_path = __DIR__.'/TEMP/'.$_SESSION['UserID'];
            if(!file_exists($temp_file_path)){
                mkdir($temp_file_path, 0777, true);
            }
            $cmd = __DIR__.'/mupdf/mutool convert -o '.$temp_file_path.'/'.$filename.'_%d.png '.$directory_path.'/'.$filename.'_P1_1.pdf 1-N';
            exec($cmd);

            $x = 0;
            $image_pdf = new \Imagick();
            foreach ($pages as $page) {
                $x ++;
                $image_pdf->addImage(new \Imagick($temp_file_path.'/'.$filename.'_'.$x.'.png'));
                unlink($temp_file_path.'/'.$filename.'_'.$x.'.png');
            } 

            $image_pdf->writeImages($directory_path.'/'.$filename.'_P1_1.pdf', true);
            //end summary sheet convert to image

            //merge the summary sheet
            $cmd_merge = __DIR__.'/mupdf/mutool merge -o '.$output_dir.' '.$directory_path.'/'.$filename.'_P1_1.pdf '.$directory_path.'/'.$outpdf;
            exec($cmd_merge);

            if(file_exists($directory_path.'/'.$outpdf)){
                rrmdir($directory_path.'/'.$outpdf);
            }
            
            $sql2 = "SELECT * FROM segregated_files WHERE  batched_parent_id = '{$parent_id}' AND filename='{$output_filename}' LIMIT 1";
            $check_result = mysqli_query($con, $sql2)->fetch_assoc();
            if(empty($check_result)){
                $JobID = 0;
                

                $reference_code = !empty($post['reference_code']) ? $post['reference_code'] : '';
                $preship_date = !empty($post['preship_date']) ? $post['preship_date'] : '';
                $prec_bundle = !empty($post['prec_bundle']) ? $post['prec_bundle'] : '';
                $tat = !empty($post['tat']) ? $post['tat'] : 0;
            

                $auto_upload_to_gg = !empty($post['auto_upload_to_gg']) ? true: false;

                //primo DB 
                $sql = "
                      INSERT INTO `segregated_files` 
                        (`wms_job_id`, 
                        `batched_parent_id`, 
                        `reference_code`, 
                        `precship_date`, 
                        `prec_bundle`,
                        `tat`,
                        `filename`,
                        `page_start`,
                        `page_end`
                        )
                        VALUES
                        ('{$JobID}', 
                        '{$parent_id}', 
                        '{$reference_code}', 
                        '{$preship_date}', 
                        '{$prec_bundle}', 
                        '{$tat}',
                        '{$output_filename}',
                        '{$start_split}',
                        '{$end_split}'
                        );
                    ";

                ExecuteQuery($sql,$con);

                $inserted_id = mysqli_insert_id($con);
                $post['id'] = $inserted_id;
            
                //process golden gate
                if($auto_upload_to_gg){

                      $sqls="EXEC USP_PRIMO_INTEGRATE_old @ExecutionId=1,  @mainUrl='www.example.com',  @SourceUrl='".$base_url."/uploadfiles/SourceFiles/".$prec_bundle."/".$output_filename."',@Filename='".$output_filename."',@Jobid=''";
                      
                      $integrate_result = ExecuteQuerySQLSERVER ($sqls,$conWMS);
                      $JobID = GetWMSValue("Select JobID From PRIMO_Integration WHERE Filename='".$output_filename."'","JobId",$conWMS);

                      $token = getAPIKey($GGUserName,$GGPassword,$GGProductionMode);
                      $sFilename=$output_filename;
                      UploadToGoldenGate($output_filename,$JobID,$token,$conWMS,$sFilename, $post);
                }

            }
        }

        if(file_exists($directory_path.'/'.$filename."_P1_1.pdf")){
          rrmdir($directory_path.'/'.$filename."_P1_1.pdf");
        }
    }
}

function allocateBatchToUser($batch_id, $user_id){
    global $conWMS;

    $sqls="EXEC usp_wms_Allocate_RemoveAllocation @BatchID=".$batch_id;     
    $res1 = ExecuteQuerySQLSERVER ($sqls,$conWMS);
    
    $sqls="EXEC usp_wms_Allocate_BatchToUser @BatchID=".$batch_id.", @UserID=".$user_id;
    $res = ExecuteQuerySQLSERVER ($sqls,$conWMS);

    return $res;
}

function deleteGGJobs($GGJobID){
    global $GGUserName,$GGPassword,$GGProductionMode;
    
    $token = getAPIKey($GGUserName,$GGPassword,$GGProductionMode);
    $url = "https://api.innodata.com/v1.1/jobs/".$GGJobID."?token=".$token;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_exec($ch);
    curl_close($ch);
}

function ftpRecursiveDownload($sftp, $file_path, $type = 'pdf-image', $dl_results = array()){
      global $downloadedFilePathPDFImg, $downloadedFilePath;

      if($type == 'pdf-image'){
          $local_path = $downloadedFilePathPDFImg;
      }else{
          $local_path = $downloadedFilePath;
      }

      if($sftp->chdir($file_path)){
          // Loop through each file and download
          foreach($sftp->nlist() as $file_from_dir) {
              if($file_from_dir != "." && $file_from_dir != ".."){
                  $remote_dir = preg_replace('/(\/+)/','/','/'.$file_path.'/'.$file_from_dir);
                  if($sftp->chdir($remote_dir)){
                      ftpRecursiveDownload($sftp, $remote_dir, $type, $dl_results);
                  }else{
                      $local_file_name = '';
                      try{
                            $local_dir = preg_replace('/(\/+)/','/',$local_path.'/'.$remote_dir);
                            if(!file_exists(dirname($local_dir))){
                                mkdir(dirname($local_dir), 0777, true);
                            }
                          
                            $local_file_name = $local_dir;
                            if(file_exists($local_file_name)){
                                unlink($local_file_name);
                            }
                                
                            $dl_result = $sftp->get($remote_dir, $local_file_name);
                            if($dl_result){
                                $dl_results[] = array("success" => true, "message" => "Successfully downloaded (".$remote_dir.")", "data" => "");
                            }else{
                                if(file_exists($local_file_name)){
                                    unlink($local_file_name);
                                }

                                $dl_results[] = array("success" => false, "message" => "Something went wrong downloading file (".$remote_dir.")", "data" => "");
                            }
                      }catch(Exception $e){
                          if(file_exists($local_file_name)){
                              unlink($local_file_name);
                          }

                          $dl_results[] = array("success" => false, "message" => "(File: {$file->filename}) ".$e->getMessage(), "data" => "");
                      }
                  }
              }
          }
      }else{
          $local_file_name = '';
          try{
              $local_dir = preg_replace('/(\/+)/','/',$local_path.'/'.$file_path);
              
              if(!file_exists(dirname($local_dir))){
                  mkdir(dirname($local_dir), 0777, true);
              }
              

              $local_file_name = $local_dir;
              
              if(file_exists($local_file_name)){
                  unlink($local_file_name);
              }
            
              $dl_result = $sftp->get($file_path, $local_file_name);
              if($dl_result){
                  $dl_results[] = array("success" => true, "message" => "Successfully downloaded ({$file_path})", "data" => "");
              }else{
                if(file_exists($local_file_name)){
                    unlink($local_file_name);
                }

                $dl_results[] = array("success" => false, "message" => "Something went wrong downloading file ({$file_path})", "data" => "");
              }
          }catch(Exception $e){
              if(file_exists($local_file_name)){
                  unlink($local_file_name);
              }

              $dl_results[] = array("success" => false, "message" => "(File: {$file_path}) ".$e->getMessage(), "data" => "");
          }
      }

      return $dl_results;

}


function zipFiles($rootPath, $zip_name){
    // Initialize archive object
    if(file_exists($rootPath.'/'.$zip_name.'.zip')){
       unlink($rootPath.'/'.$zip_name.'.zip'); 
    }
    $zip = new ZipArchive();

    $zip->open($rootPath.'/'.$zip_name.'.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

    // Create recursive directory iterator
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($rootPath),
        RecursiveIteratorIterator::LEAVES_ONLY
    );

    foreach ($files as $name => $file)
    {
        // Skip directories (they would be added automatically)
        if (!$file->isDir())
        {
            // Get real and relative path for current file
            $filePath = $file->getRealPath();
            $relativePath = substr($filePath, strlen($rootPath) + 1);
            
            // Add current file to archive
            $zip->addFile($filePath, $relativePath);
        }
    }

    // Zip archive will be created only after closing object
    $zip->close();
}

function zipFilesV2($rootPath, $zip_name){
    // Initialize archive object
    if(file_exists($rootPath.'/'.$zip_name.'.zip')){
       unlink($rootPath.'/'.$zip_name.'.zip'); 
    }
    $zip = new ZipArchive();

    $zip->open($rootPath.'/'.$zip_name.'.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

    // Create recursive directory iterator
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($rootPath),
        RecursiveIteratorIterator::LEAVES_ONLY
    );

    foreach ($files as $name => $file)
    {
        // Skip directories (they would be added automatically)
        if (!$file->isDir())
        {
            // Get real and relative path for current file
            $filePath = $file->getRealPath();
            $relativePath = substr($filePath, strlen($rootPath) + 1);
            
            // Add current file to archive
            $zip->addFile($filePath, basename($filePath));
        }
    }

    // Zip archive will be created only after closing object
    $zip->close();
}

function generateDollarFileName($bundle_name){
  global $con;
  $sql = "SELECT * FROM bundle_sequence_number WHERE bundle_name = '{$bundle_name}' ORDER BY id DESC LIMIT 1";
  $bundle_sequence = mysqli_query($con, $sql)->fetch_assoc();
  $sequence_no = str_pad((!empty($bundle_sequence['sequence_no']) ? $bundle_sequence['sequence_no'] : 0),4,0,STR_PAD_LEFT); 

  $file_name = '';

  $prefix = "TPP";
  $exploded_bundle = explode('_', $bundle_name);
  
  $lit = $exploded_bundle[0];
  $exploded_lit = explode('-', $lit);
  $lit_prefix = !empty($exploded_lit[1]) ? 'L'.$exploded_lit[1] : 'L';
  
  $prefix_bundle = str_pad((int) filter_var($exploded_bundle[1], FILTER_SANITIZE_NUMBER_INT),4,0,STR_PAD_LEFT); 
  $file_name = $prefix.$lit_prefix.$prefix_bundle.$sequence_no;
  
  return $file_name;
}

function generatePrecedentName($bundle_name){
    return str_replace('-','', $bundle_name);
}

function getDollarDefaultContent($precedent_name){
  $content ="CLIENT NAME                               : Thomson Reuters Canada Limited1
PROJECT NUMBER                            : NA
PROJECT NAME                              : Precedents Project - Precedents Bundle {$precedent_name}

CONTACT PERSON                            : Anita Busmanis
CLIENT PARTNER                            : Karen Allanson
PRODUCTION CONTACT                        : Leleth Maamo

%s                                          
 ========================================================================

 Thank you for choosing Innodata to handle your data conversion needs.
 For comments, inquiries and suggestions, please write or call:
 Jingle Largo at Customer Service Dept. or Leleth Maamo
 Email address  : JLargo@innodata-isogen.com
                  <mailto:JLargo@innodata-isogen.com>
                  Customer-Service@innodata-isogen.com
                  <mailto: Customer-Service@innodata-isogen.com>
                   LMaamo@INNODATA.COM
 Phone Number   : **1-877-454-8400
                  (201) 371-2575
 Fax            : 011-632-815-0000 x 703
======================================================================
";

return $content;
}

function getDelivarablesFiles($bundle_name, $type = 'key'){
    global $SourceFilePath;
    $type = strtoupper($type);

    $file_path = $SourceFilePath.'/'.$bundle_name.'/deliverables';
    $filename = '';
    $cnt = 0;
    $bytes = 0;
    $total_pages = 0;
    $ocr_bytes = 0;
    $non_ocr_bytes = 0;
    
    if(file_exists($file_path)){
        $dir = new DirectoryIterator($file_path);
        foreach ($dir as $fileinfo) {
            if (!$fileinfo->isDot()) {
                if(strtoupper($fileinfo->getExtension()) == $type){
                    $filename .= $fileinfo->getFilename().PHP_EOL;
                    $bytes += $fileinfo->getSize();
                    $cnt ++;

                    //count total pages
                    if($type == 'PDF'){
                        $pages = 0;
                        $pdftext = file_get_contents($fileinfo->getPath().'/'.$fileinfo->getFilename());
                        $pages = preg_match_all("/\/Page\W/", $pdftext, $dummy);
                        $total_pages += $pages;
                    }

                    if($type == 'KEY'){
                        $source_file = str_replace('PREC','',pathinfo(strtoupper($fileinfo->getFilename()), PATHINFO_FILENAME)).'.pdf';
                        $source_path = $SourceFilePath.'/'.$bundle_name.'/'.$source_file;
                        $filecontent = file_get_contents($source_path);
                        //non OCR
                        if (preg_match("/^%PDF-1.5/", $filecontent)) {
                            $non_ocr_bytes += $fileinfo->getSize();
                        } else {
                            $ocr_bytes += $fileinfo->getSize();
                        }
                    }
                } 
            }
        }
    }

    $data = array("files" => $filename, "count" => $cnt, "bytes" => $bytes, 'total_pages' => $total_pages, "ocr_bytes" => $ocr_bytes, "non_ocr_bytes" => $non_ocr_bytes);
    
    return $data;
}

function generateDollarFile($bundle_name, $type = null){
  global $SourceFilePath, $con;

  $sql = "SELECT * FROM files_for_batching WHERE prec_bundle = '{$bundle_name}' ORDER BY id ASC LIMIT 1";
  $bundle_data = mysqli_query($con, $sql)->fetch_assoc();
  $reference_code = !empty($bundle_data['reference_code']) ? $bundle_data['reference_code'] : '';
  $precship_date = !empty($bundle_data['precship_date']) ? date('F d, Y', strtotime($bundle_data['precship_date'])) : '';
  $date_received = !empty($bundle_data['date_created']) ? date('F d, Y', strtotime($bundle_data['date_created'])) : '';
  $time_received = !empty($bundle_data['date_created']) ? date('h:i:s A', strtotime($bundle_data['date_created'])) : '';
  
  $file_path = $SourceFilePath.'/'.$bundle_name;
  $file_name = generateDollarFileName($bundle_name);
  $file_name_dollar = $file_name.".$$$";
  $file_name_zip = $file_name.".zip";

  if($bundle_data['status'] == 'Completed'){
      if(file_exists($file_path.'/'.$file_name_dollar)){
          return file_get_contents($file_path.'/'.$file_name_dollar);
      }else{
          return '';
      }
  }

  $date_created = $bundle_data['date_created'];
  $tat = !empty($bundle_data['tat']) ? $bundle_data['tat'] : 6;
  $due_date = date('F d, Y', strtotime(date('Y-m-d', strtotime($bundle_data['precship_date']. " + {$tat} days"))));

  $precedent_name = generatePrecedentName($bundle_name);

  $content = '';
  if(!file_exists($file_path)){
     mkdir($file_path, 0777, true);
  }

  $key_files = getDelivarablesFiles($bundle_name, 'key');
  
  $key_file = !empty($key_files['files']) ? $key_files['files'] : '';
  $key_count = !empty($key_files['count']) ? $key_files['count'] : 0;
  $key_ocr_bytes = !empty($key_files['ocr_bytes']) ? number_format($key_files['ocr_bytes']) : 0;
  $key_nonocr_bytes = !empty($key_files['non_ocr_bytes']) ? number_format($key_files['non_ocr_bytes']) : 0;;

  $bmp_files = getDelivarablesFiles($bundle_name, 'bmp');
  $bmp_count = !empty($bmp_files['count']) ? $bmp_files['count'] : 0;;;

  $pdf_files = getDelivarablesFiles($bundle_name, 'pdf');
  $pdf_count = !empty($pdf_files['count']) ? $pdf_files['count'] : 0;
  $pdf_total_pages = !empty($pdf_files['total_pages']) ? number_format($pdf_files['total_pages']) : 0;
  
  $transmitted_date = '';
  $transmitted_time = '';
  $transmission_status = 'Pending';
  if($type == 'deliver'){
      $transmitted_date = date('F d, Y');
      $transmitted_time = date('h:i:s A') .' (Philippine time)';
      $transmission_status = 'Final/Complete';
  }

  $dollar_content_default = getDollarDefaultContent($precedent_name);
  
  $automated_content = "DATE SHIPPED                              : {$precship_date}
DATE RECEIVED                             : {$date_received}
TIME RECEIVED                             : {$time_received} (Philippine time)
DATE DUE                                  : {$due_date}
DATE TRANSMITTED                          : {$transmitted_date}
TRANSMISSION TIME                         : {$transmitted_time}
TURNAROUND TIME                           : {$tat}-days
TRANSMISSION TYPE                         : FTP
OUTPUT FORMAT                             : ASCII Generic
ACCURACY LEVEL                            : Data capture @ 99.95%, plus fielded data @ 99.995%
BATCH #                                   : {$reference_code}
REGISTER #                                : NA

FACILITY                                  : 17
GROUP                                     : TP-Carswell

TRANSMISSION FILENAME                     : {$file_name_zip}
CUSTOMER SHIPMENT #                       : {$reference_code}

BILLING ITEM NUMBER                       : TPCPPCAR
AUTOBATCH ITEM NUMBER                     : TPCPPCAR
SHIPMENT #                                : N/A
TOTAL NUMBER OF BILLABLE OCR'ed BYTES     : {$key_ocr_bytes}

BILLING ITEM NUMBER                       : TPCPPCAS
AUTOBATCH ITEM NUMBER                     : TPCPPCAS
SHIPMENT #                                : N/A
TOTAL NUMBER OF BILLABLE Keyboarding Bytes: {$key_nonocr_bytes}

BILLING ITEM NUMBER                       : TPCPPCAD
AUTOBATCH ITEM NUMBER                     : TPCPPCAD
SHIPMENT #                                : N/A
TOTAL NUMBER OF BILLABLE PDF IMAGES       : {$pdf_total_pages}

BILLING ITEM NUMBER                       : TPCPPCAT
AUTOBATCH ITEM NUMBER                     : TPCPPCAT
SHIPMENT #                                : N/A
TOTAL NUMBER OF BILLABLE BMP IMAGES       : {$bmp_count}


TRANSMISSION STATUS                       : {$transmission_status}
OTHER INFORMATION                         :

  
{$key_file}
            
            : Final transmission of {$key_count} .key, {$bmp_count} .BMP and {$pdf_count} PDF files of Precedents Bundle {$precedent_name}
";


  $dollar_content = sprintf($dollar_content_default, $automated_content);

  $file = fopen($file_path.'/'.$file_name_dollar, 'w+');
  fputs($file, $dollar_content);
  fclose($file);
  
  return $dollar_content;
}

function generateKeyFileName($filename){
    return 'prec'.strtolower(pathinfo($filename,PATHINFO_FILENAME)).'.key';
}

function sendMail($subject = null, $body = null, $attachments = null, $mail_to = null, $mail_cc = null){
    global $smtp_server, $smtp_auth, $smtp_username, $smtp_password, $smtp_secure, $smtp_port;
    global $con;

    $sql = "SELECT * FROM tbltransmission WHERE TransmissionType = 'MAIL' ORDER BY id DESC LIMIT 1";
    $mail_result = mysqli_query($con, $sql)->fetch_assoc();  
    if($mail_result){
        $to = empty($mail_result['EmailAddress']) ? '': $mail_result['EmailAddress'];
        $mail_subject = !empty($subject) ? $subject : (empty($mail_result['Subject']) ? '': $mail_result['Subject']);
        $mail_body = !empty($body) ? $body : (empty($mail_result['MailBody']) ? '': $mail_result['MailBody']);

        if(!empty($mail_to)){
            $to = $mail_to;
        }

        $cc = empty($mail_result['CC']) ? array(): (explode(',', $mail_result['CC']));

        if(!empty($mail_cc)){
            $cc = explode(',', $mail_cc);
        }
        
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->isSMTP();                                    
            $mail->Host       = $smtp_server;                 
            $mail->SMTPAuth   = $smtp_auth;                      
            $mail->Username   = $smtp_username;                    
            $mail->Password   = $smtp_password;                              
            $mail->SMTPSecure = $smtp_secure;         
            $mail->Port       = $smtp_port;                                   

            //Recipients
            $mail->setFrom('noreply@innodata.com', 'Primo TPCPP');
            $mail->addAddress($to);     //Add a recipient
            if(!empty($cc)){
                foreach($cc as $cc_email){
                    $mail->addCC(trim($cc_email));
                }
            }
            //$mail->addBCC('bcc@example.com');

            //Attachments
            if(!empty($attachments)){
                foreach($attachments as $attachment){
                    $mail->addAttachment($attachment['path'], $attachment['name']);  
                }
            }

            //Content
            $mail->isHTML(true);  
            $mail->Subject = $mail_subject;
            $mail->Body    = $mail_body;
            $mail->send();
            if(!$mail->send()){
                return array("message" => "Message could not be sent. Mailer Error: {$mail->ErrorInfo}", "success" =>false);
            }else{
                return array("message" => "Successfully sent!", "success" =>true);
            }
        } catch (Exception $e) {
            return array("message" => "Message could not be sent. Mailer Error: {$mail->ErrorInfo}", "success" =>false);
        }
    }else{
        return array("message" => "Message could not be sent. Mailer Error: transmission email not yet setup!", "success" =>false);
    }
}

function generateSequenceNumber($bundle_name){
    global $con;
    
    $sequence_number = "";
    $sql = "SELECT * FROM bundle_sequence_number WHERE bundle_name = '{$bundle_name}' ORDER BY id DESC LIMIT 1";
    $bundle_sequence = mysqli_query($con, $sql)->fetch_assoc();
    $latest_no = '';
      
    //get the lastest sequence
    $sql = "SELECT * FROM bundle_sequence_number ORDER BY sequence_no DESC LIMIT 1";
    $latest_sequence = mysqli_query($con, $sql)->fetch_assoc();
    $latest_sequence_no = '';
    if(!empty($latest_sequence)){
        $latest_sequence_no = !empty($latest_sequence['sequence_no']) ? ((int) $latest_sequence['sequence_no'] ) + 1 : '';
    }

    if(empty($bundle_sequence)){
        //create new sequence
        $sql_insert = "INSERT INTO bundle_sequence_number(`bundle_name`, `sequence_no`) VALUES('{$bundle_name}', '{$latest_no}') ";
        ExecuteQuery($sql_insert, $con);
    
    }else{
        $latest_no = $bundle_sequence['sequence_no'];

        if(empty($latest_no) && !empty($latest_sequence_no)){
            //update to latest sequence
            $sql_update = "UPDATE bundle_sequence_number SET `sequence_no` = '{$latest_sequence_no}' WHERE bundle_name = '{$bundle_name}' ";
            ExecuteQuery($sql_update, $con);
          
            $latest_no = $latest_sequence_no;
        }
    }

    return $latest_no;
}

function getVersion(){
    global $con;

    $sql = "SELECT * FROM project_version ORDER BY id DESC LIMIT 1";
    $data = mysqli_query($con, $sql)->fetch_assoc();

    return !empty($data['version']) ? $data['version'] : '';
}

Class Custom_XML extends SimpleXMLElement
{
    public function prependChild($name, $value = '')
    {
        $dom = dom_import_simplexml($this);

        $new = $dom->insertBefore(
            $dom->ownerDocument->createElement($name, $value),
            $dom->firstChild
        );

        return simplexml_import_dom($new, get_class($this));
    }
}
?>