<?php
include "conn.php";
// error_reporting(0);
$JobID= !empty($_POST['data']) ? $_POST['data'] :'';
$filename = !empty($_POST['filename']) ? $_POST['filename'] : '';
if(empty($JobID)){
    echo "GG ID is empty, cannot fetch the data!";
    exit;
}

$token = getAPIKey($GGUserName,$GGPassword,$GGProductionMode);
$result = getGGStatus($JobID, $token);
$jobj = json_decode($result);

$GGStatus = !empty($jobj->response->status) ? $jobj->response->status: ''; 

if ($GGStatus=='completed'){
    $GGProgress='100';
    $URL= $jobj->response->output_content->uri;

    $file_data = getGGInnodom($URL, $token);

    $sql = "SELECT * FROM segregated_files WHERE filename = '{$filename}' LIMIT 1";
    $sql_result = mysqli_query($con, $sql)->fetch_assoc();
    
    $bundle = !empty($sql_result['prec_bundle']) ? $sql_result['prec_bundle'] : '';

    $task = !empty($_POST['task']) ? $_POST['task'] : '';
    $sqls="SELECT * FROM primo_view_Jobs Where ProcessCode='{$task}' AND GGJobID='{$JobID}'";    
    $status_string = GetWMSValue($sqls,"StatusString",$conWMS);

    $nFilename = pathinfo($filename, PATHINFO_FILENAME);
    $bundle_path = $SourceFilePath.'/'.$bundle;
    $innodom_path = $bundle_path.'/sources/'. $nFilename . '_response.xml';
    $innodom_data = '';
    if(file_exists($innodom_path)){
        $innodom_data = file_get_contents($innodom_path);
    }

    $transformation_path = $bundle_path.'/'.$nFilename.'.xml';
    $has_transformation = false;
    if(file_exists($transformation_path)){
        $transformation_data = trim(file_get_contents($transformation_path));

        if(!empty($transformation_data)){
            $has_transformation = true;   
        }
    }

    if(trim($innodom_data) != trim($file_data) || !$has_transformation){
        $save_result = saveInnodom($filename, $file_data, $bundle);
    }


    if(!file_exists("C:\\xampp\htdocs\\TPCPP_PRIMO\\primo\\uploadfiles\\SourceFiles\\".$bundle.'\\'.$nFilename . '_response.html') || trim($innodom_data) != trim($file_data)){
        $space = " ";
        $bundle_dir = $bundle.'\\'.$nFilename . '_response.xml';
        $exe =  __DIR__.'/HTMLTransformation/HTMLTransformation.exe';

        //remove meta in innodom here
        $file_html = fopen("C:\\xampp\htdocs\\TPCPP_PRIMO\\primo\\uploadfiles\\SourceFiles\\".$bundle_dir, 'w+');
        $file_html_data = preg_replace("/<\/inno:meta>/", "ƒ</inno:meta>", $file_data); 
        $file_html_data = preg_replace("/<inno:meta>[^ƒ]+ƒ<\/inno:meta>/", "", $file_html_data); 
                            
        fputs($file_html, $file_html_data);
        fclose($file_html);
        //copy($innodom_path, "C:\\xampp\htdocs\\TPCPP_PRIMO\\primo\\uploadfiles\\SourceFiles\\".$bundle_dir);
        $cmd = "C:\\xampp\\htdocs\TPCPP_PRIMO\\primo\\HTMLTransformation\\HTMLTransformation.exe".$space."C:\\xampp\htdocs\\TPCPP_PRIMO\\primo\\uploadfiles\\SourceFiles\\".$bundle_dir;//'"'.$exe.'" "'.$innodom_path;
        shell_exec($cmd);
        if(file_exists("C:\\xampp\htdocs\\TPCPP_PRIMO\\primo\\uploadfiles\\SourceFiles\\".$bundle_dir)){
            unlink("C:\\xampp\htdocs\\TPCPP_PRIMO\\primo\\uploadfiles\\SourceFiles\\".$bundle_dir);
        }
    }
    
}else{
    if(empty($GGStatus)){
        if(!empty($jobj->response->status_code) && $jobj->response->status_code == 404){
            $GGStatus =  !empty($jobj->response->message) ?  $jobj->response->message : '';
        }
    }

    $GGProgress =  !empty($jobj->response->progress->current) ? $jobj->response->progress->current : 'Failed 0';
}
 


echo $GGStatus."(".$GGProgress."%)" ;
?>