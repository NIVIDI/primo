<?php
include "conn.php";
include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        New Content
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">New Content</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
  
        <?php $sql="SELECT * FROM tblmlconfig";?>
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
            <div class="box-body">
                <div class="table-responsive">
                    <table id="example2" class="table table-bordered table-hover">
                      <thead>
                      <tr>
                          <th width="10%">ID</th>
                          <th width="25%">Source URL</th>
                          <th width="10%">Filename</th>
                          <th width="10%">Date Registered</th>
                          <th width="7%"></th>
                      </tr>
                      </thead>
                      <tbody>
                          <?php
                          			  $token = getAPIKey($GGUserName,$GGPassword,$GGProductionMode);
                                  $strSQL = "SELECT GGJobID, JobId, JobName,SourceUrl,RequestID, Requestor, collabkey, courtname, Priority, Transpriority, lowpriority, filenamePath, DocumentType, Filename,DateRegistered,PageNo From  primo_view_Jobs WHERE StatusString='NEW' and ProcessCode='Styling' ORDER BY DateRegistered DESC";
                               
                          				$objExec= odbc_exec($conWMS,$strSQL);
                          				while ($row = odbc_fetch_array($objExec)) 
                          				{
                                      $SourceUrl = $row['SourceUrl'];
                          ?>
                                  <tr>
                                      <td><?php echo $row["JobName"];?></td>
                                      <td><a target="_blank" href="<?= $SourceUrl?>" target="_blank"><?= $SourceUrl;?></td>
                                      <td><?php echo $row["Filename"];?></td>
                                      <td><?php echo $row["DateRegistered"];?></td>
                                      <td>
                                          <a style="margin-left: 1px !important" href="javascript:void(0);" id="delete_job" onclick="NewContent.delete(this);" class="btn btn-danger btn-xs pull-right" jobname="<?= $row["JobName"]; ?>" job_id = "<?= $row["JobId"]?>" title= "Delete Specific Job">Delete</a>
                                          <?php
                                              if(strtolower($_SESSION['UserType']) == 'admin'){
                                                  echo '<a style="margin-right: 1px !important" href="javascript:void(0);" id="allocate_job" onclick="NewContent.allocate(this);" class="btn btn-success btn-xs pull-right" jobname="'.$row["JobName"].'" job_id = "'.$row["JobId"].'" title= "Allocate Job">Allocate</a>';
                                              }
                                          ?>
                                          
                                      </td>
                                  </tr>
                        	<?php
                        				}
                        			?>	  
                      </tbody>
                    </table>
                </div>    
            </div>
          </div>
        </div>
      </div>
  </section>
</div>
 
<div class="modal fade" id="allocate_job_modal">
    <div class="modal-dialog" style="width: 50%"> 
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Allocate Job To User</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="allocate_job_modal_form" enctype="multipart/form-data">
                      <input type="hidden" value='' name="job_id" id="job_id">
                      <div class="form-group">
                          <label>Users</label>
                          <select name="user_id" id="user_id" class="form-control" req="true" message= "User is required">
                                <option value=''>Please Select Operator</option>
                                <?php
                                    $sql = "SELECT * FROM tbluser WHERE UserType = 'Operator' ";
                                    $results = mysqli_query($con, $sql);
                                    while($row = $results->fetch_assoc()){
                                        echo "<option value='{$row['id']}'>{$row['Name']}</option>";
                                    }
                                ?>
                          </select>
                      </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" id="submit_allocate_job_modal" value="Submit">
            </div>
        </div>
    </div>
</div>
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
<script type="text/javascript">
    $('#submit_allocate_job_modal').click(function(){
        if(Form.validate('#allocate_job_modal_form')){
            NewContent.submitAllocate();
        }
    });

    var NewContent = {
          delete : function(el){
              var JobId = $(el).attr('job_id');
              var jobname = $(el).attr('jobname');
              Swal.fire({
                  title: "Are you sure want to delete the item ("+jobname+")?",
                  text: '',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes',
                  allowOutsideClick: false
              }).then((result) => {
                  if (result.isConfirmed) {
                      NewContent.submitDelete(JobId);
                  }
              })
          },
          allocate : function(el){
              var JobId = $(el).attr('job_id');
              var jobname = $(el).attr('jobname');
              $('#allocate_job_modal .modal-title').html('Allocate Job('+jobname+') to User');
              Swal.fire({
                  title: "Are you sure want to allocate selected item ("+jobname+")?",
                  text: '',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes',
                  allowOutsideClick: false
              }).then((result) => {
                  if (result.isConfirmed) {
                      NewContent.allocateForm(JobId);
                  }
              })
          },
          submitAllocate : function(){
              var data = $('#allocate_job_modal_form').serializeArray();
              data.push({name: 'action', value: 'alocate_batch_to_user'});
              Page.loading('Allocating ...');
              $.post('postdata.php', data, function(res){
                  Swal.close();
                  location.reload();
                  
              });
          },
          allocateForm : function(JobId){
              $('#job_id').val(JobId);
              $('#allocate_job_modal').modal({
                  backdrop: 'static',
                  keyboard: false
              });
          },
          submitDelete : function(JobId){
              Page.loading('Deleting ...');

              var data = [
                  {name:'action', value: 'delete_wms_job'},
                  {name:'job_id', value: JobId}
              ];

              $.post('postdata.php', data, function(response){
                  Swal.close();
                  try{
                      var result = JSON.parse(response);
                      if(result.success){
                          NewContent.success(result.message);
                      }else{
                          NewContent.error(result.message);
                      }
                  }catch(e){
                      NewContent.error(e);
                  }
              });
          },
          success : function(msg){
                Swal.fire({
                    allowOutsideClick: false,
                    title: '<strong>Submit Status</strong>',
                    icon: 'info',
                    html: msg,
                    showCloseButton: false,
                    showCancelButton: false,
                    focusConfirm: false,
                    confirmButtonText:'<i class="fa fa-thumbs-up"></i> OK',
                }).then((result) => {
                    if (result.isConfirmed) {
                        location.reload();
                    }
                })
          },
          error : function (msg){
              Swal.fire({
                  allowOutsideClick: false,
                  icon: 'error',
                  title: 'Oops...',
                  html: "<strong>"+msg+"</strong>"
              });
          }
    }
</script>
