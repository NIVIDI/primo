<?php
	require "conn.php";
	
	use phpseclib3\Net\SFTP;
	use Smalot\PdfParser\Parser;
	use Smalot\PdfParser\XObject\Image;
	
	ini_set('max_execution_time', 60000);
	//set_time_limit(0);

	$results = array();
	$post = $_POST;
	$action = $post['action'];

	if(file_exists("postdata.{$action}.inc.php")){
		require_once "postdata.{$action}.inc.php";
		
		echo json_encode($results);	
		exit;
	}

	if(!empty($action)){
		switch($action){
			case 'submit_version':
					$version = $post['project_version'];
					if(!empty($version)){
						try{
							$sql="INSERT INTO project_version (`version`) VALUES ('{$version}')";
						 	ExecuteQuery($sql,$con);

						 	$results = array("success" => true, "message" => "Successfully submitted version!");
						}catch(Exception $e){
							$results = array("success" => false, "message" => $e->getMessage());
						}
					}else{
						$results = array("success" => false, "message" => "Version is required!");
					}
				break;
			case 'for_splitting':
					$responses = uploadSourceFiles($_POST, $_FILES);
					if(!empty($responses)){
						$message = "";
						foreach($responses as $response){
							$style = "display: block; color: red;";
							if($response['success']){
								$style = "display: block; color: green;";
							}
							$message .= "<li style='{$style}'>{$response['message']}</li>";
						}
						$results = array("success" => true, "message" => "{$message}");
					}else{
						$results = array("success" => false, "message" => "Something wrong uploding the file/s");
					}
				break;
			case 'fetch_existing_bundle':
					try{
						$bundle = !empty($post['bundle']) ? $post['bundle'] : ''; 
						$sql = "SELECT * FROM files_for_batching WHERE prec_bundle = '{$bundle}' LIMIT 1";
						$data = mysqli_query($con, $sql)->fetch_assoc();

						$results = array("success" => true, "message" => "success", "data" => $data);
					}catch(Exception $e){
						$results = array("success" => false, "message" => $e->getMessage());
					}
				break;
			case 'split_page':
					$id = $post['id'];
					$page_from = $post['page_from'];
					$page_to = $post['page_to'];
					$file_source = $post['file_source'];
					$bundle_dir = $post['bundle_dir'];
					$filename = pathinfo($file_source, PATHINFO_FILENAME);
					$preship_date = $post['precship_date'];
					$tat = $post['tat'];
					$reference_code = $post['reference_code']; 

					$input = $SourceFilePath.'/'.$bundle_dir.'/'.$post['filename'];
					
					$outpdf1 = $filename."_P1{$page_from}_{$page_to}.pdf";
					$output1 = $SourceFilePath.'/'.$bundle_dir.'/'.$outpdf1;

					$cmd = __DIR__.'/page_select/pdfSplitter/pdftk.exe '.$input.' cat '.$page_from.'-'.$page_to.' output '.$output1; 
					$results = $cmd;
					exec($cmd, $output1);


					$outpdf_summarysheet = $filename."_P1_1.pdf";
				    $output_summarysheet = $SourceFilePath.'/'.$bundle_dir.'/'.$outpdf_summarysheet;
				    $cmd = __DIR__.'/page_select/pdfSplitter/pdftk.exe '.$input.' cat 1-1 output '.$output_summarysheet; 
				    exec($cmd, $output_summarysheet);

					$outpdf = $filename."_P{$page_from}_{$page_to}.pdf";
            		$output = $SourceFilePath.'/'.$bundle_dir.'/'.$outpdf;
            		
            		//put to deliverables with cover page and summary sheet
		            if(!file_exists($SourceFilePath.'/'.$bundle_dir.'/deliverables')){
		                mkdir($SourceFilePath.'/'.$bundle_dir.'/deliverables', 0777,true);
		            }
		            
		            $output_deliverables = $SourceFilePath.'/'.$bundle_dir.'/deliverables/'.$outpdf;
		            $page_from2 = $page_from + 1;
		            if($page_from2 > $page_to){
		            	$page_from2 = $page_from;
		            }
		            $cmd = __DIR__.'/page_select/pdfSplitter/pdftk.exe '.$input.' cat '.$page_from2.'-'.$page_to.' output '.$output_deliverables; 
		            exec($cmd, $output);

		            //convert auto segregated to png then to pdf again because there is a problem in merging drectly 2 pdf
		            $parser = new Parser();
				    $pdf    = $parser->parseFile($SourceFilePath.'/'.$bundle_dir.'/'.$outpdf1);
				    $pages  = $pdf->getPages();
				    
				    $temp_file_path = __DIR__.'/TEMP/'.$_SESSION['UserID'];
					if(!file_exists($temp_file_path)){
						mkdir($temp_file_path, 0777, true);
					}
					$cmd = __DIR__.'/mupdf/mutool convert -o '.$temp_file_path.'/'.$filename.'_%d.png '.$SourceFilePath.'/'.$bundle_dir.'/'.$outpdf1.' 1-N';
					exec($cmd);

					$x = 0;
				    $pdf = new \Imagick();
				    foreach ($pages as $page) {
				      	$x ++;
				      	$pdf->addImage(new \Imagick($temp_file_path.'/'.$filename.'_'.$x.'.png'));
						unlink($temp_file_path.'/'.$filename.'_'.$x.'.png');
					} 

					$pdf->writeImages($SourceFilePath.'/'.$bundle_dir.'/'.$outpdf1, true);
					//end auto segregated to png

					//convert summary sheet pdf to png then to pdf again because there is a problem in merging drectly 2 pdf
		            $parser = new Parser();
		            $pdfP    = $parser->parseFile($SourceFilePath.'/'.$bundle_dir.'/'.$outpdf_summarysheet);
		            $pages  = $pdfP->getPages();
		            
		            $temp_file_path = __DIR__.'/TEMP/'.$_SESSION['UserID'];
		            if(!file_exists($temp_file_path)){
		                mkdir($temp_file_path, 0777, true);
		            }
		            $cmd = __DIR__.'/mupdf/mutool convert -o '.$temp_file_path.'/'.$filename.'_%d.png '.$SourceFilePath.'/'.$bundle_dir.'/'.$outpdf_summarysheet.' 1-N';
		            exec($cmd);

		            $x = 0;
		            $image_pdf = new \Imagick();
		            foreach ($pages as $page) {
		                $x ++;
		                $image_pdf->addImage(new \Imagick($temp_file_path.'/'.$filename.'_'.$x.'.png'));
		                unlink($temp_file_path.'/'.$filename.'_'.$x.'.png');
		            } 

		            $image_pdf->writeImages($SourceFilePath.'/'.$bundle_dir.'/'.$outpdf_summarysheet, true);
		            //end summary sheet convert to image

					//merge summary sheet
            		$cmd_merge = __DIR__.'/mupdf/mutool merge -o '.$SourceFilePath.'/'.$bundle_dir.'/'.$outpdf.' '.$SourceFilePath.'/'.$bundle_dir.'/'.$outpdf_summarysheet.' '.$SourceFilePath.'/'.$bundle_dir.'/'.$outpdf1;
            		exec($cmd_merge);
            		
		            if(file_exists($SourceFilePath.'/'.$bundle_dir.'/'.$outpdf1)){
		                rrmdir($SourceFilePath.'/'.$bundle_dir.'/'.$outpdf1);
		            }

		            if(file_exists($SourceFilePath.'/'.$bundle_dir.'/'.$outpdf_summarysheet)){
		            	rrmdir($SourceFilePath.'/'.$bundle_dir.'/'.$outpdf_summarysheet);
		            }

					$sql2 = "SELECT * FROM segregated_files WHERE  batched_parent_id = '{$id}' AND filename='{$outpdf}' LIMIT 1";
					$check_result = mysqli_query($con, $sql2)->fetch_assoc();
					if(empty($check_result)){
						$JobID = 0;
						$auto_upload_to_gg = $post['auto_upload_to_gg'];
						//process golden gate
						if($auto_upload_to_gg){

	              	        $sqls="EXEC USP_PRIMO_INTEGRATE_old @ExecutionId=1,  @mainUrl='www.example.com',  @SourceUrl='".$base_url."/uploadfiles/SourceFiles/".$bundle_dir."/".$outpdf."',@Filename='".$outpdf."',@Jobid=''";
	              	        
	              	        $integrate_result = ExecuteQuerySQLSERVER ($sqls,$conWMS);
	                        $JobID = GetWMSValue("Select JobID From PRIMO_Integration WHERE Filename='".$outpdf."'","JobId",$conWMS);

	                        $token = getAPIKey($GGUserName,$GGPassword,$GGProductionMode);
	                        $sFilename=$outpdf;
	                        UploadToGoldenGate($outpdf,$JobID,$token,$conWMS,$sFilename, $post);
						}

						//primo DB 
						$sql = "
			                INSERT INTO `segregated_files` 
			                  (`wms_job_id`, 
			                  `batched_parent_id`, 
			                  `reference_code`, 
			                  `precship_date`, 
			                  `prec_bundle`,
			                  `tat`,
			                  `filename`,
			                  `page_start`,
			                  `page_end`
			                  )
			                  VALUES
			                  ('{$JobID}', 
			                  '{$id}', 
			                  '{$reference_code}', 
			                  '{$preship_date}', 
			                  '{$bundle_dir}', 
			                  '{$tat}',
			                  '{$outpdf}',
			                  '{$page_from}',
			                  '{$page_to}'
			                  );
		                ";

        				ExecuteQuery($sql,$con);

        				$results = array("success" => true, "message" => "Successfully split pages");
				
					}else{

        				$results = array("success" => false, "message" => "Selected pages already split");
				
					}

				break;
		    case 'delete_split':
	    			$ids = json_decode($post['ids']);
	    			if(!empty($ids)){
	    				$process_result = array();
	    				foreach($ids as $id){
	    					$sql = "SELECT * FROM segregated_files WHERE id = '{$id}'";
	    					$sql_result = mysqli_query($con, $sql)->fetch_assoc();
	    					if(empty($sql_result)){
	    						$results = array("success" => false, "message" => "Cannot find selected batched!");
	    						echo json_encode($results);
	    						exit;
	    					}

	    					$filename = !empty($sql_result['filename']) ? $sql_result['filename'] : '';
	    					$bundle_dir = !empty($sql_result['prec_bundle']) ? $sql_result['prec_bundle'] : '';
	    					$file_path = $SourceFilePath.'/'.$bundle_dir.'/'.$filename;
	    					$wms_job_id = !empty($sql_result['wms_job_id']) ? $sql_result['wms_job_id'] : '';

	    					if(empty($bundle_dir)){
	    						$results = array("success" => false, "message" => "Cannot find bundle attached to batched file!");
	    						echo json_encode($results);
	    						exit;
	    					}

	    					//delete batched file
	    					if(file_exists($file_path)){
	    						unlink($file_path);
	    					}

	    					$file_name_without_ext = pathinfo($filename, PATHINFO_FILENAME);

	    					//delete deliverables
	    					if(file_exists($SourceFilePath.'/'.$bundle_dir.'/deliverables/'.$filename)){
								rrmdir($SourceFilePath.'/'.$bundle_dir.'/deliverables/'.$filename);
							}

							//delete transformation xml
							if(file_exists($SourceFilePath.'/'.$bundle_dir.'/'.$file_name_without_ext.'.xml')){
								unlink($SourceFilePath.'/'.$bundle_dir.'/'.$file_name_without_ext.'.xml');
							}

							//delete innodom
							if(file_exists($SourceFilePath.'/'.$bundle_dir.'/sources/'.$file_name_without_ext.'_response.xml')){
								unlink($SourceFilePath.'/'.$bundle_dir.'/sources/'.$file_name_without_ext.'_response.xml');
							}

							//delete meta
							if(file_exists($SourceFilePath.'/'.$bundle_dir.'/'.$file_name_without_ext.'_meta.xml')){
								unlink($SourceFilePath.'/'.$bundle_dir.'/'.$file_name_without_ext.'_meta.xml');
							}

							//delete equivalent BMP files
							$bmp_file_path = $SourceFilePath.'/'.$bundle_dir.'/'.$file_name_without_ext.'/images';
							if(file_exists($bmp_file_path)){
								$bmp_dir = new DirectoryIterator($bmp_file_path);
				                foreach ($bmp_dir as $fileinfo) {
				                    if (!$fileinfo->isDot()) {
				                    	$image_without_ext = pathinfo($fileinfo->getFilename(), PATHINFO_FILENAME);
				                    	if(file_exists($SourceFilePath.'/'.$bundle_dir.'/deliverables/'.$image_without_ext.'.BMP')){
				                    		unlink($SourceFilePath.'/'.$bundle_dir.'/deliverables/'.$image_without_ext.'.BMP');
				                    	}

				                    	if(file_exists($bmp_file_path.'/'.$fileinfo->getFilename())){
				                    		rrmdir($bmp_file_path.'/'.$fileinfo->getFilename());
				                    	}
									}
				                }
				            }

							//check cropping and delete
							if(file_exists($SourceFilePath.'/'.$bundle_dir.'/'.$file_name_without_ext)){
								rrmdir($SourceFilePath.'/'.$bundle_dir.'/'.$file_name_without_ext);
							}

							//delete deliverables that are already compiled
							$sql_deliverables = "SELECT * FROM deliverables_segregated_files WHERE segregated_id = '{$id}' LIMIT 1";
				            $deliverables_data = mysqli_query($con, $sql_deliverables)->fetch_assoc();
							if(!empty($deliverables_data)){
								$deliverable_path = $SourceFilePath.'/'.$bundle_dir.'/deliverables/'.$deliverables_data['filename'];
								if(file_exists($deliverable_path)){
									unlink($deliverable_path);
								}

								$sql_delete = "DELETE FROM deliverables_segregated_files WHERE segregated_id = '{$id}'";
				            	ExecuteQuery($sql_delete,$con);
							}

							$sql="DELETE FROM segregated_files WHERE id = '{$id}'";
							ExecuteQuery($sql,$con);

							$process_result[] = array("success" => true, "message" => "Successfully deleted file {$filename}");
	    					

							//delete jobs in GG and WMS if any
							if(!empty($wms_job_id)){
								$GGJobId = GetWMSValue("SELECT TOP 1 GGJobID FROM primo_view_Jobs WHERE JobId = '{$wms_job_id}'","GGJobID",$conWMS);
	    						
								$sqls="EXEC usp_PRIMO_DELETEJOB @JobId=".$wms_job_id;
  								ExecuteQuerySQLSERVER ($sqls,$conWMS);

	    						if(!empty($GGJobId)){
									deleteGGJobs($GGJobId);
								}
	    					}
	    					
	    				}

	    				$message = '';
	    				foreach($process_result as $response){
							$style = "display: block; color: red;";
							if($response['success']){
								$style = "display: block; color: green;";
							}
							$message .= "<li style='{$style}'>{$response['message']}</li>";
						}
						$results = array("success" => true, "message" => "{$message}");
	    			}else{
	    				$results = array("success" => false, "message" => "You have not selected split page to delete!");
	    			}
		    	break;
		    case 'delete_bundle_file':
		    		$id = !empty($post['id']) ? (int) $post['id'] : '';
		    		
		    		if(empty($id)){
		    			$results = array("success" => false, "message" => "Error deleting file, ID not found!");
		    		}else{
		    			$sql = "SELECT * FROM segregated_files WHERE batched_parent_id = '{$id}'";
		    			$sql_result = mysqli_query($con, $sql);
	    				if($sql_result->num_rows){
	    					while($row = $sql_result->fetch_assoc()){

	    						if(empty($row['prec_bundle'])){
		    						continue;
		    					}

	    						//delete WMS and GG, if any
	    						if(!empty($row['wms_job_id'])){
	    							$GGJobId = GetWMSValue("SELECT TOP 1 GGJobID FROM primo_view_Jobs WHERE JobId = '{$row['wms_job_id']}'","GGJobID",$conWMS);
	    						
									$sqls="EXEC usp_PRIMO_DELETEJOB @JobId=".$row['wms_job_id'];
	  								ExecuteQuerySQLSERVER ($sqls,$conWMS);

		    						if(!empty($GGJobId)){
										deleteGGJobs($GGJobId);
									}
								}

								$file_name_without_ext = pathinfo($row['filename'], PATHINFO_FILENAME);

								//batched source
								$path_to_delete = $SourceFilePath.'/'.$row['prec_bundle'].'/'.$row['filename'];
								if(file_exists($path_to_delete)){
									unlink($path_to_delete);
								}

								//delete meta
								if(file_exists($SourceFilePath.'/'.$row['prec_bundle'].'/'.$file_name_without_ext.'_meta.xml')){
									unlink($SourceFilePath.'/'.$row['prec_bundle'].'/'.$file_name_without_ext.'_meta.xml');
								}

								//delete transformation
								if(file_exists($SourceFilePath.'/'.$row['prec_bundle'].'/'.$file_name_without_ext.'.xml')){
									unlink($SourceFilePath.'/'.$row['prec_bundle'].'/'.$file_name_without_ext.'.xml');
								}

								//delete innodom
								if(file_exists($SourceFilePath.'/'.$row['prec_bundle'].'/sources/'.$file_name_without_ext.'_response.xml')){
									unlink($SourceFilePath.'/'.$row['prec_bundle'].'/sources/'.$file_name_without_ext.'_response.xml');
								}

								//delete deliverables
								if(file_exists($SourceFilePath.'/'.$row['prec_bundle'].'/deliverables/'.$row['filename'])){
									unlink($SourceFilePath.'/'.$row['prec_bundle'].'/deliverables/'.$row['filename']);
								}

								//delete equivalent BMP files
								$bmp_file_path = $SourceFilePath.'/'.$row['prec_bundle'].'/'.$file_name_without_ext.'/images';
								if(file_exists($bmp_file_path)){
									$bmp_dir = new DirectoryIterator($bmp_file_path);
					                foreach ($bmp_dir as $fileinfo) {
					                    if (!$fileinfo->isDot()) {
					                    	$image_without_ext = pathinfo($fileinfo->getFilename(), PATHINFO_FILENAME);
					                    	if(file_exists($SourceFilePath.'/'.$row['prec_bundle'].'/deliverables/'.$image_without_ext.'.BMP')){
					                    		unlink($SourceFilePath.'/'.$row['prec_bundle'].'/deliverables/'.$image_without_ext.'.BMP');
					                    	}

					                    	if(file_exists($bmp_file_path.'/'.$fileinfo->getFilename())){
					                    		rrmdir($bmp_file_path.'/'.$fileinfo->getFilename());
					                    	}
										}
					                }
					            }

								//check cropping and delete
								if(file_exists($SourceFilePath.'/'.$row['prec_bundle'].'/'.$file_name_without_ext)){
									rrmdir($SourceFilePath.'/'.$row['prec_bundle'].'/'.$file_name_without_ext);
								}

								//delete deliverables that are already compiled
								$sql_deliverables = "SELECT * FROM deliverables_segregated_files WHERE segregated_id = '{$row['id']}' LIMIT 1";
					            $deliverables_data = mysqli_query($con, $sql_deliverables)->fetch_assoc();
								if(!empty($deliverables_data)){
									$deliverable_path = $SourceFilePath.'/'.$row['prec_bundle'].'/deliverables/'.$deliverables_data['filename'];
									if(file_exists($deliverable_path)){
										unlink($deliverable_path);
									}

									$sql_delete = "DELETE FROM deliverables_segregated_files WHERE segregated_id = '{$row['id']}'";
					            	ExecuteQuery($sql_delete,$con);
								}

								//delete batch file in DB
								$sql = "DELETE FROM segregated_files WHERE id = '{$row['id']}' ";
								ExecuteQuery($sql,$con);
							}
	    				}

	    				$sql = "SELECT * FROM files_for_batching WHERE id = '{$id}'";
	    				$sql_result = mysqli_query($con, $sql)->fetch_assoc();
    					$filename = !empty($sql_result['file_name']) ? $sql_result['file_name'] : '';
	    				$bundle_dir = !empty($sql_result['prec_bundle']) ? $sql_result['prec_bundle'] : '';
	    				$path_to_delete = $SourceFilePath.'/'.$bundle_dir.'/'.$filename;
	    				if(!empty($filename) && !empty($bundle_dir)){
	    					if(file_exists($path_to_delete)){
	    						unlink($path_to_delete);
	    					}
	    				}
		    			
		    			//delete key file
		    			$key_filaname = generateKeyFilename($filename);
		    			if(file_exists($SourceFilePath.'/'.$bundle_dir.'/deliverables/'.$key_filaname)){
		    				unlink($SourceFilePath.'/'.$bundle_dir.'/deliverables/'.$key_filaname);
		    			}

		    			//delete key validation logs
		    			if(file_exists($SourceFilePath.'/'.$bundle_dir.'/'.pathinfo($key_filaname, PATHINFO_FILENAME).'.logs')){
		    				unlink($SourceFilePath.'/'.$bundle_dir.'/'.pathinfo($key_filaname, PATHINFO_FILENAME).'.logs');
		    			}

	    				$sql = "DELETE FROM files_for_batching WHERE id = '{$id}' ";
						ExecuteQuery($sql,$con);

						$results = array("success" => true, "message" => "Successfully deleted selected file!");
					}
		    	break;
		    case 'upload_to_GG':
		    		$ids = json_decode($post['ids']);
	    			if(!empty($ids)){
	    				$process_result = array();
	    				foreach($ids as $id){
	    					$sql = "SELECT * FROM segregated_files WHERE id = '{$id}'";
	    					$sql_result = mysqli_query($con, $sql)->fetch_assoc();
	    					$filename = !empty($sql_result['filename']) ? $sql_result['filename'] : '';
	    					$precship_date = !empty($sql_result['precship_date']) ? $sql_result['precship_date'] : '';
	    					$bundle_dir = !empty($sql_result['prec_bundle']) ? $sql_result['prec_bundle'] : '';
	    					$tat = !empty($sql_result['tat']) ? $sql_result['tat'] : '';
	    					$file_path = $SourceFilePath.'/'.$bundle_dir.'/'.$filename;
	    					$wms_job_id = !empty($sql_result['wms_job_id']) ? $sql_result['wms_job_id'] : '';

	    					if(empty($wms_job_id)){
		    					$sqls="EXEC USP_PRIMO_INTEGRATE_old @ExecutionId=1,  @mainUrl='www.example.com',  @SourceUrl='".$base_url."/uploadfiles/SourceFiles/".$bundle_dir."/".$filename."',@Filename='".$filename."',@Jobid=''";
	              	        
		              	        $integrate_result = ExecuteQuerySQLSERVER ($sqls,$conWMS);
		                        $JobID = GetWMSValue("Select JobID From PRIMO_Integration WHERE Filename='".$filename."'","JobId",$conWMS);

		                        $token = getAPIKey($GGUserName,$GGPassword,$GGProductionMode);
		                        $sFilename=$filename;

		                        $post['prec_bundle'] = $bundle_dir;
		                        $post['filename'] = $filename;
								$post['precship_date'] = $precship_date;
								$post['tat'] = $tat;
								$post['id'] = $id;
								
								UploadToGoldenGate($filename,$JobID,$token,$conWMS,$sFilename, $post);
								$process_result[] = array("success" => true, "message" => "Successfully upload file ({$filename}) to GG");
							}else{
								$process_result[] = array("success" => false, "message" => "Selected file({$filename})  already uploaded to GG");
							}
	    				}

	    				$message = '';
	    				foreach($process_result as $response){
							$style = "display: block; color: red;";
							if($response['success']){
								$style = "display: block; color: green;";
							}
							$message .= "<li style='{$style}'>{$response['message']}</li>";
						}
						$results = array("success" => true, "message" => "{$message}");
	    			}else{
	    				$results = array("success" => false, "message" => "You have not selected split page to upload!");
	    			}
		    	break;
			case 'get-segrated-files':
					try{
						$id = $post['id'];
						$sql = "SELECT * FROM segregated_files WHERE batched_parent_id = '{$id}' ORDER BY page_start ASC";

						$result_rows = mysqli_query($con, $sql);
						$data = array();
						if($result_rows->num_rows){
							while($row = $result_rows->fetch_assoc()){
								$data[] = $row;
							}
						}

						$results = array("success" => true, "message" => "", "data" => $data);
					}catch(Exception $e){
						$results = array("success" => false, "message" => $e->getMessage());
					}
				break;

			case 'fetch_ftp':
					try{
						$dir = $post['dir'];
						$sql = "SELECT * FROM tbltransmission WHERE TransmissionType = 'FTP' LIMIT 1"; 
                        $sql_results = mysqli_query($con, $sql)->fetch_assoc();
                        $ftp_server = !empty($sql_results['FTPSite']) ? $sql_results['FTPSite'] : '';
                        $ftp_username = !empty($sql_results['UserName']) ? $sql_results['UserName'] : '';
                        $ftp_pass = !empty($sql_results['Password']) ? $sql_results['Password'] : '';


                        $sftp = new SFTP($ftp_server);
                        $sftp_login = $sftp->login($ftp_username, $ftp_pass);
                        if (!$sftp_login) {
                        	$results = array("success" => false, "message" => "FTP Login Failed!", "data" => '');
                        }else{
                        	$return_files = array();
                        	
                        	if(empty($post['file_detail'])){
	                        	$ftp_files = $sftp->rawlist($dir);
	                        	if(!empty($ftp_files)){
	                                foreach($ftp_files as $key => $file){
	                                    if($key != '.' AND $key != '..') {
	                                    	$file['modified'] = date('Y-m-d H:i:s', $file['mtime']);
	                                        $return_files[] = $file;
	                                    }
	                                }
	                            }


                            	$results = array("success" => true, "message" => "success", "data" => $return_files);
                            }else{
                            	$file_details = $sftp->stat($dir);
                            	$file_details['modified'] = date('Y-m-d', $file_details['mtime']);

                            	$results = array("success" => true, "message" => "success", "data" => $file_details);
                            }	
                        }
						
					}catch(Exception $e){
						$results = array("success" => false, "message" => $e->getMessage());
					}
				break;
			case 'ftp_download':
					if(!file_exists($downloadedFilePath)){
						mkdir($downloadedFilePath, 0777, true);
						chmod($downloadedFilePath, 0777);
					}
					
					$files = json_decode($post['files']);
					if(!empty($files)){
						$sql = "SELECT * FROM tbltransmission WHERE TransmissionType = 'FTP' LIMIT 1"; 
                        $sql_results = mysqli_query($con, $sql)->fetch_assoc();
                        $ftp_server = !empty($sql_results['FTPSite']) ? $sql_results['FTPSite'] : '';
                        $ftp_username = !empty($sql_results['UserName']) ? $sql_results['UserName'] : '';
                        $ftp_pass = !empty($sql_results['Password']) ? $sql_results['Password'] : '';


                        	$dl_results = array();
                        	foreach($files as $file){
                        		$sftp = new SFTP($ftp_server, 22, 120);
                        		$sftp_login = $sftp->login($ftp_username, $ftp_pass);
                        		if (!$sftp_login) {
                        			$dl_results[] = array("success" => false, "message" => "FTP Login Failed, download not completed!({$file->filename})", "data" => '');
                        		}else{
                        			$remote_file_path = $file->directory.'/'.$file->filename;
                        			$dl_results[] = ftpRecursiveDownload($sftp, $remote_file_path, 'source-pdf');
                        		}
							}

                        	$message = '';

		    				foreach($dl_results as $key => $response){
		    					$style = "display: block; color: red;";
								if($response[0]['success']){
									$style = "display: block; color: green;";
								}
								$message .= "<li style='{$style}'>{$response[0]['message']}</li>";
							}
							$results = array("success" => true, "message" => "{$message}" , "data" => "");
                    }else{
						$results = array("success" => false, "message" => "No FTP files selected!", "data" => '');
					}
					
				break;
			case 'ftp_pdf_image_clean_download':
					if(!file_exists($downloadedFilePathPDFImg)){
						mkdir($downloadedFilePathPDFImg, 0777, true);
						chmod($downloadedFilePathPDFImg, 0777);
					}
					
					$files = json_decode($post['files']);
					if(!empty($files)){
						$sql = "SELECT * FROM tbltransmission WHERE TransmissionType = 'FTP' LIMIT 1"; 
                        $sql_results = mysqli_query($con, $sql)->fetch_assoc();
                        $ftp_server = !empty($sql_results['FTPSite']) ? $sql_results['FTPSite'] : '';
                        $ftp_username = !empty($sql_results['UserName']) ? $sql_results['UserName'] : '';
                        $ftp_pass = !empty($sql_results['Password']) ? $sql_results['Password'] : '';


                        $sftp = new SFTP($ftp_server);
                        $sftp_login = $sftp->login($ftp_username, $ftp_pass);
                        if (!$sftp_login) {
                        	$results = array("success" => false, "message" => "FTP Login Failed, download not completed!", "data" => '');
                        }else{
                        	$dl_results = array();
                        	foreach($files as $file){
                        			$remote_file_path = $file->directory.'/'.$file->filename;
									$dl_results = ftpRecursiveDownload($sftp, $remote_file_path, 'pdf-image');
							}

							$message = '';
		    				foreach($dl_results as $response){
								$style = "display: block; color: red;";
								if($response['success']){
									$style = "display: block; color: green;";
								}
								$message .= "<li style='{$style}'>{$response['message']}</li>";
							}
							$results = array("success" => true, "message" => "{$message}" , "data" => "");
                        }
					}else{
						$results = array("success" => false, "message" => "No FTP files selected!", "data" => '');
					}
				break;
			case 'fetch_dl':
					$file_path = $post['dir'];
							
	                if(file_exists($file_path)){
		                $dir = new DirectoryIterator($file_path);
		                $data = array();
		                foreach ($dir as $fileinfo) {
		                    if (!$fileinfo->isDot()) {
		                    	$modified = date("Y-m-d H:i:s", $fileinfo->getMTime());
		                    	$data[] = array('modified' => $modified,'filename' => $fileinfo->getFilename(), 'size' => $fileinfo->getSize(), 'type' => ($fileinfo->isFile()) ? 1 : 2);
		                    }
		                }
		                $results = array("success" => true, "message" => "success", 'data' => $data);
		            }else{
		            	$results = array("success" => false, "message" => "No File Available");
					}
				break;
			case 'downloaded_to_segregate':
					$files = json_decode($post['files']);
					$reference_code = !empty($post['reference_code']) ? trim($post['reference_code']) : '';
    				$preship_date = !empty($post['preship_date']) ? $post['preship_date'] : '';
    				$prec_bundle = !empty($post['prec_bundle']) ? $post['prec_bundle'] : '';
    				$tat = !empty($post['tat']) ? $post['tat'] : 0;
    				$auto_segregate = !empty($post['auto_segregate']) ? true: false;
    				$auto_GG = ($auto_segregate) ? (!empty($post['auto_upload_to_GG']) ? true: false) : false; 

					if(!empty($files)){
						$save_results = array();

						//check for reference code if exists to other bundle
						$sql_check_ref = "SELECT COUNT(*) as total_cnt, prec_bundle FROM files_for_batching WHERE reference_code = '{$reference_code}' AND prec_bundle <> '{$prec_bundle}' ";

						$sql_check_ref_result = mysqli_query($con, $sql_check_ref)->fetch_assoc();
						if($sql_check_ref_result['total_cnt'] > 0){
							$results = array("success" => false, "message" => "Error: Reference Code already belong to bundle({$sql_check_ref_result['prec_bundle']})!");
							echo json_encode($results);
							exit;
						}
						
						//check for reference code if match with bundle
					    $sql_check_bundle = "SELECT * FROM files_for_batching WHERE prec_bundle = '{$prec_bundle}' LIMIT 1";
					    $sql_check_bundle_result = mysqli_query($con, $sql_check_bundle)->fetch_assoc();
					    if(!empty($sql_check_bundle_result)){
					        if($sql_check_bundle_result['reference_code'] != $reference_code){
					            $results = array("success" => false, "message" => "Error: Reference Code not match with existing bundle ref code({$sql_check_bundle_result['reference_code']})!");

					            echo json_encode($results);
					            exit;
					        }
					    }

						foreach($files as $file){
							$filename = $file->filename;
							$directory = $file->directory;
							
							$newFilePath = $SourceFilePath."/".$filename;
				            $bundle_path = '';
				            if(!empty($prec_bundle)){
				                $bundle_path = $SourceFilePath.'/'.$prec_bundle;
				                if(!file_exists($bundle_path)){
				                    mkdir($bundle_path, 0777, true);
				                }
				            }

				            if(!empty($bundle_path)){
				                $newFilePath = $bundle_path.'/'.$filename;
				            }

				            if(file_exists($newFilePath)){
				            	$save_results[] = array("success" => false, "message" => "File already exist ({$filename}) ");
				            }else{
								copy(__DIR__.'/'.$directory.'/'.$filename, $newFilePath);
								
								$sql="INSERT INTO files_for_batching (`file_name`, `reference_code`, `precship_date`, `prec_bundle`, `tat`, `created_by`) 
	                          VALUES ('{$filename}','{$reference_code}', '{$preship_date}', '{$prec_bundle}', '{$tat}', '{$_SESSION['UserID']}')";
	        
			                    ExecuteQuery($sql,$con);
			                    $post['auto_upload_to_gg'] = $auto_GG;
			                    $last_inserted_id = mysqli_insert_id($con);
			                    if($auto_segregate){
			                    	 auto_segregate($newFilePath, $last_inserted_id, $post);
			                    }

			                    $save_results[] = array("success" => true, "message" => "Successfully processed ({$filename})");
			                }
						}

						$message = '';
	    				foreach($save_results as $response){
							$style = "display: block; color: red;";
							if($response['success']){
								$style = "display: block; color: green;";
							}
							$message .= "<li style='{$style}'>{$response['message']}</li>";
						}
						$results = array("success" => true, "message" => "{$message}" , "data" => "");
					}else{
						$results = array("success" => false, "message" => "Error: no files selected!");
					}
				break;
			case 'downloaded_to_compile':
					$prec_bundle = !empty($post['prec_bundle']) ? $post['prec_bundle'] : '';
					$files = json_decode($post['files']);
					if(!empty($files)){
						$dcd_files = array();
						$directory_to_compile = '';
						foreach ($files as $file) {
							$ext = pathinfo($file->filename, PATHINFO_EXTENSION);
						    if(!empty($ext) && strtolower($ext) == 'dcd'){
						    	$dcd_files[strtoupper(str_replace('.dcd','', strtolower($file->filename)))] = array('filename' => $file->filename, 'directory' => $file->directory);
						    }
						}
						
						if(!empty($dcd_files)){
							$dcd_results = array();
							foreach($dcd_files as $index => $dcd_file){
								$path = $dcd_file['directory'].'/'.$dcd_file['filename'];
								if(file_exists($path)){
									$dcd_data = file_get_contents($path);
									//explode to get the tif file
									$dcd_data_exploded = explode(PHP_EOL, $dcd_data);
									if(!empty($dcd_data_exploded)){
										$file_data_to_compile = array();
										$total_pages = 0;
										$filename = '';
										$pages_to_split = array();
										foreach ($dcd_data_exploded as $key => $value) {
											if($key > 3 && !empty($value)){
												//explode .tif, to get the directory and file
												$exploded_tif_file = explode('|', $value);
												
												if(!empty($exploded_tif_file[1]) && !empty($exploded_tif_file[0])){
													$total_pages ++;

													if((int) $exploded_tif_file[1] < 10){
														$folder_name = $index.'0'.$exploded_tif_file[1];
													}else{
														$folder_name = $index.$exploded_tif_file[1];
													}	

													$file_data_to_compile[$folder_name][] = $exploded_tif_file[0];  
													$exploded_value = explode('_Page_', $exploded_tif_file[0]);
													$filename = !empty($exploded_value[0]) ? $exploded_value[0] : ''; 
													if(!file_exists(dirname($path).'/'.$filename)){
														mkdir(dirname($path).'/'.$filename, 0777, true);
													}

													$tif_file_wo_ext = str_replace('.tif','',$exploded_tif_file[0]);
													//convert tif file to text for easy reading of on content
													$tesser_ocr_cmd = __DIR__.'/Tesseract/tesseract.exe '.__DIR__.'/'.dirname($path).'/'.$folder_name.'/'.$exploded_tif_file[0].' '.__DIR__.'/'.dirname($path).'/'.$filename.'/'.$tif_file_wo_ext;
													
													exec($tesser_ocr_cmd);
													
													$txt_content = file_get_contents(__DIR__.'/'.dirname($path).'/'.$filename.'/'.$tif_file_wo_ext.'.txt');
													$contents = strtolower($txt_content);
												    $output_content = preg_replace('/\s+/', '', $contents);
												    
												    if (stripos($output_content, "documenttitle:") !== false){
												    	$pages_to_split[] = $total_pages;
													}

													//remove created txt  
												    unlink(__DIR__.'/'.dirname($path).'/'.$filename.'/'.$tif_file_wo_ext.'.txt');
												}
											}
										}
										
										//traverse thru the compile data to get the path and contents and put into single pdf file
										if(!empty($file_data_to_compile)){
											$pdf = new \Imagick();
											foreach($file_data_to_compile as $key => $values){
												$tif_dir = dirname($path).'/'.$key;
												foreach($values as $value){
													if(file_exists(($tif_dir.'/'.$value))){
														$pdf->addImage(new \Imagick(__DIR__.'/'.$tif_dir.'/'.$value));
													}
												}
											}

											$pdf->writeImages(__DIR__.'/'.dirname($path).'/'.$filename.'/'.$filename.'.pdf', true);
										}	

										//split full compiled pdf to per docs
										$total_to_split = count($pages_to_split);
										$input = __DIR__.'/'.dirname($path).'/'.$filename.'/'.$filename.'.pdf';
									    if(file_exists($input)){        
										    if(!empty($pages_to_split)){
										        foreach ($pages_to_split as $key => $value) {
										            $start_split = $value + 1;
										            
										            if($key == $total_to_split - 1){
										                $end_split = $total_pages;
										            }else{
										                $end_split = ($pages_to_split[$key + 1]) - 1;
										            }

										           	$outpdf = $filename."_P{$start_split}_{$end_split}.pdf";
													$output = __DIR__.'/'.dirname($path).'/'.$filename.'/'.$outpdf;

										            $cmd = __DIR__.'/page_select/pdfSplitter/pdftk.exe '.$input.' cat '.$start_split.'-'.$end_split.' output '.$output; 
										            exec($cmd, $output);
										        }
										    }
										}
									}
 								}else{
									$dcd_results[] = array("success" => false, "message" => "Error: cant found selected DCD file ({$path})");
								}
							}

							$message = 'Successfully compiled file to pdf';
		    				$results = array("success" => true, "message" => "{$message}" , "data" => "");
						}else{
							$results = array("success" => false, "message" => "Error: only .DCD file allow for compilation!");
						}
					}else{
						$results = array("success" => false, "message" => "Error: no files selected!");
					}
				break;
			case 'delete_download':
					$files = json_decode($post['files']);
					if(!empty($files)){
						foreach($files as $file){
							rrmdir($file->directory.'/'.$file->filename);
						}

						$results = array("success" => true, "message" => "Successfully deleted files");
					}else{
						$results = array("success" => false, "message" => "Error: no files selected!");
					}
				break;
			case 'delete_wms_job':
					$job_id = !empty($post['job_id']) ? $post['job_id'] : 0;
					
					if(!empty($job_id)){
						try{
							$GGJobId = GetWMSValue("SELECT TOP 1 GGJobID FROM primo_view_Jobs WHERE JobId = '{$job_id}'","GGJobID",$conWMS);

							$sqls="EXEC usp_PRIMO_DELETEJOB @JobId=".$job_id;
  
							ExecuteQuerySQLSERVER ($sqls,$conWMS);

							//update equivalent data in primo
							$sql = "UPDATE segregated_files SET wms_job_id = '0' WHERE wms_job_id = '{$job_id}' ";
							ExecuteQuery($sql,$con);

							$results = array("success" => true, "message" => 'Successfully deleted Jobs!');
							
							//delete jobs in GG
							if(!empty($GGJobId)){
								deleteGGJobs($GGJobId);
							}

						}catch(Exception $e){
							$results = array("success" => false, "message" => $e->getMessage());
						}
					}else{
						$results = array("success" => false, "message" => "Job ID ({$job_id}) not found!");
					}
				break;
			case 'alocate_batch_to_user':

					$job_id = !empty($post['job_id']) ? $post['job_id'] : ''; 

					if(!empty($job_id)){

						//get the batch ID by JOB ID 
						$batch_id = GetWMSValue("Select BatchId from primo_view_Jobs Where JobID='{$job_id}' AND ProcessCode= 'STYLING' ","BatchId",$conWMS);
						if(!empty($batch_id)){ 	
							
							$user_id = !empty($post['user_id']) ? $post['user_id'] : '';
							//check user exist in WMS
							$user_in_wms =GetWMSValue("SELECT UserId FROM NM_Users WHERE UserId='{$user_id}'",'UserId',$conWMS);
							
							if(!empty($user_in_wms)){
								try{
									$res = allocateBatchToUser($batch_id, $user_in_wms);
									if(empty($res))
										$results = array("success" => true, "message" => "Successfully allocated selected Job!");
									else
										$results = array("success" => false, "message" => "Error processing allocation, please contact system admin!");
								}catch(Exception $e){
									$results = array("success" => true, "message" => "Successfully allocated selected Job!");
								}
							}else{
								$results = array("success" => false, "message" => "Process failed, selected user not exist on WMS DB!");
							}
						}else{
							$results = array("success" => false, "message" => "Cannot allocate batch, batch ID({$batch_id}) not found!");
						}
					}else{
						$results = array("success" => false, "message" => "Cannot find JOB ID on WMS DB!");
					}

				break;
			case 'get-source-files':
					$bundle = !empty($post['bundle']) ? trim($post['bundle']) : '';
					if(empty($bundle)){
						$results = array("success" => false, "message" => "Cannot find bundle!");	
					}else{
						$sql = "SELECT * FROM files_for_batching WHERE prec_bundle = '{$bundle}' ";
						$sql_result = mysqli_query($con, $sql);
						if($sql_result->num_rows){
							$data = array();
							while ($row = $sql_result->fetch_assoc()) {
								$sql = "SELECT COUNT(*) as total_docs FROM segregated_files WHERE batched_parent_id = '{$row['id']}' ";
								$result = mysqli_query($con, $sql)->fetch_assoc();

								$size = 0;
								if(file_exists($SourceFilePath.'/'.$bundle.'/'.$row['file_name'])){
									$size = filesize($SourceFilePath.'/'.$bundle.'/'.$row['file_name']);
								}
								$row['size'] = number_format(($size) / 1000, 2);
								$row['total_docs'] = (int) $result['total_docs'];

								$data[] = $row;
							}

							$results = array("success" => true, "message" => "success", "data" => $data);
						}else{
							$results = array("success" => false, "message" => "No Available File!");	
						}
					} 
				break;
			default:
				$results = array("success" => false, "message" => "Sorry post action not determine!");
		}
	}else{
		$results = array("success" => false, "message" => "Sorry post action not determine!");
	}

	echo json_encode($results);
?>