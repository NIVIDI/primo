/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */


CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.specialChars = config.specialChars.concat( [ '&#9633;', '&mdash;','&lsquo;','&rsquo;','&ldquo;','&rdquo;','&#163;','&#162;','&copy;','&euro;','&yen;','&reg;'] );
	config.allowedContent = true;
	config.removePlugins ='newpage';
 
	config.removePlugins ='lite,forms';
	 config.extraPlugins = 'Transform,saveHTML';
	 // config.extraPlugins = '';

		config.toolbar = 'Full';
		config.toolbar_Full =
	[
		['Source','-','saveHTML'],
		['Cut','Copy','Paste','PasteText','PasteFromWord','-', 'SpellChecker', 'Scayt'],
		['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		'/',
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
		['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['Link','Unlink','Anchor'],
		['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
		'/',
		['Styles','Format','Font','FontSize'],
		['TextColor','BGColor'],
		['Maximize', 'ShowBlocks','Transform','-','About']
	];


};
