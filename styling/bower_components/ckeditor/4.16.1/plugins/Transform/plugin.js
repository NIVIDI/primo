﻿CKEDITOR.plugins.add('Transform',
{
    init: function (editor) {
        var pluginName = 'Transform';
        editor.ui.addButton('Transform',
            {
                label: 'XML Transformation',
                command: 'TransformTOXML',
                icon: CKEDITOR.plugins.getPath('Transform') + 'convert.png'
            });
        var cmd = editor.addCommand('TransformTOXML', { exec: TransformTOXML1 });
    }
});
// function showMyDialog(e) {
    
// } 