<?php
	$id = !empty($post['id']) ? $post['id'] : '';

	if(empty($id)){
		$results = array("success" => false, "message" => "Error: cant find the source file ID!");
	}else{
		$sql = "SELECT * FROM segregated_files WHERE batched_parent_id = '{$id}' ORDER BY page_start ASC";

		$sql_result = mysqli_query($con, $sql);

		$data = array();
		$token = getAPIKey($GGUserName,$GGPassword,$GGProductionMode);
		if($sql_result->num_rows){
			while($row = $sql_result->fetch_assoc()){
				$size = 0;
				$bundle = $row['prec_bundle'];
				if(file_exists($SourceFilePath.'/'.$bundle.'/'.$row['filename'])){
					$size = filesize($SourceFilePath.'/'.$bundle.'/'.$row['filename']);
				}
				$wms_job_id = $row['wms_job_id'];
				$row['size'] = number_format(($size) / 1000, 2);
				if(empty($wms_job_id)){
					$row['wms_status'] = '(empty)';
					$row['gg_status'] = '(empty)';
				}else{
					$strSQL = "SELECT * From  primo_view_Jobs WHERE  JobId = '{$wms_job_id}'";
                    $objExec= odbc_exec($conWMS,$strSQL);
                    $row['wms_status'] = '';
                    $job_id = 0;
                    $row['gg_status'] = '';
                    while ($row1 = odbc_fetch_array($objExec)){
                    	if(!empty($row1['GGJobID'])){
                    		//make sure it calls only once per JOB
                    		if($job_id != $row1['JobId']){
	                    		$gg_result = getGGStatus($row1['GGJobID']);
	                        	$jobj = json_decode($gg_result);	
	                        	$GGStatus = !empty($jobj->response->status) ? $jobj->response->status: ''; 
	                        	if ($GGStatus=='completed'){
								    $GGProgress='100';
								}else{
								    if(empty($GGStatus)){
								        if(!empty($jobj->response->status_code) && $jobj->response->status_code == 404){
								            $GGStatus =  !empty($jobj->response->message) ?  $jobj->response->message : '';
								        }
								    }

								    $GGProgress =  !empty($jobj->response->progress->current) ? $jobj->response->progress->current : 'Failed 0';
								}

								$row['gg_status'] = $GGStatus."(".$GGProgress."%)";
							}
                    	}

                    	$job_id = $row1['JobId'];
                    	
                    	$row['wms_status'] .= '<strong>'.$row1['ProcessCode'].'</strong>: (<i>'.$row1['StatusString'].'</i>)</br>';
                    }
				}
				
				$data[] = $row;

			}
		}

		$results = array("success" => true, "message" => "success", "data" => $data);
	}
?>