<?php
include "conn.php";

$id = !empty($_GET['UID']) ? $_GET['UID'] : '';
$sql="SELECT * FROM tbltransmission Where ID='{$id}'";
$UID = '';
$TransmissionType = '';
$FTPSite = '';
$Directory = '';
$UserName = '';
$Password = '';
$EmailAddress = '';
$CC = '';
$Subject = '';
$MailBody = '';

if ($result=mysqli_query($con,$sql))
{
	  // Fetch one and one row
	  while ($row=mysqli_fetch_row($result))
		{
        $UID=$row[0];
  			$TransmissionType=$row[1];
  			$FTPSite=$row[2];
  			$Directory=$row[3];
  			$UserName=$row[4];
  			$Password=$row[5];
  			$EmailAddress=$row[6];
  			$CC=$row[7];
  			$Subject=$row[8];
  			$MailBody=$row[9];
		}
}

$selectedFTP = ''; 
$selectedMail = '';

if ($TransmissionType=='MAIL'){
		$DisplayFTP="style='display:none;'";
		$DisplayMail='';
    $selectedMail = "selected";
}elseif($TransmissionType=='FTP'){
		$DisplayMail="style='display:none;'";
		$DisplayFTP='';
    $selectedFTP = "selected";
}

include("header.php");
include("header_nav.php");
include ("sideBar.php");
?>

<div class="content-wrapper">
    <section class="content-header">
          <h1>User List</h1>
          <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    		      <li><a href="UserList.php"><i class="fa fa-dashboard"></i> User Maintenance</a></li>
              <li class="active">User List</li>
          </ol>
    </section>
    <section class="content">
        <div class="row">
  
        <?php $sql="SELECT * FROM tblUser"; ?>
            <div class="col-md-12">
                <div class="box box-primary">
		                  <form role="form" method="Post" Action="saveTransmissionSetting.php">
                          <div class="box-header with-border">
			                         <label >Transmission Type</label>
                        				<select id="Type" class="form-control" name="TransmissionType" style="width:300px;" onchange="toggle(this)">
                                      <option value=""></option>
                            					<option <?= $selectedFTP;?> value="FTP">FTP</option>
                            					<option <?= $selectedMail;?>  value="MAIL">E-mail</option>
                            		</select>
			                    </div>
                          <div class="box-body">
                                <div class="col-lg-6" id="FTP" <?php echo $DisplayFTP;?>>
                                      <div class="form-group">
                                            <label>FTP Site</label>
      						                          <input type="text" class="form-control" placeholder="FTP Site" name="FTPSite" value="<?php echo $FTPSite;?>">
                                      </div>
                                      <div class="form-group">
                                            <label>Directory</label>
      						                          <input type="text" class="form-control" placeholder="Directory" name="Directory" value="<?php echo $Directory;?>">
                                      </div>
      					                      <div class="form-group">
                                            <label>UserName</label>
      						                          <input type="text" class="form-control" placeholder="UserName" name="UserName" value="<?php echo $UserName;?>">
                                      </div>
      					                      <div class="form-group">
                                          <label>Password</label>
      						                        <input type="password" class="form-control" placeholder="Password" name="Password" value="<?php echo $Password;?>">
                                      </div>
                                </div>
				                        <div class="col-lg-6" id="Email" <?php echo $DisplayMail;?>>
					                            <div class="form-group">
                                          <label>EmailAddress</label>
						                              <input type="text" class="form-control" placeholder="EmailAddress" name="EmailAddress" value="<?php echo $EmailAddress;?>">
                                      </div>
					                            <div class="form-group">
                                          <label>CC</label>
						                              <input type="text" class="form-control" placeholder="CC" name="CC" value="<?php echo $CC;?>">
                                      </div>
					                            <div class="form-group">
                                          <label>Subject</label>
						                              <input type="text" class="form-control" placeholder="Subject" name="Subject" value="<?php echo $Subject;?>">
                                      </div>
					                            <div class="form-group">
                                          <label>Mail Body</label><br>
						                              <textarea name="MailBody" cols="120" rows="10"><?php echo $MailBody;?></textarea>
						                          </div>
					                       </div>
				                  </div>
                          <div class="box-footer">
			                          <input type="hidden" class="form-control" placeholder="" name="UID" value="<?php echo $UID;?>">
                                <button type="submit" class="btn btn-primary">Save</button>
				                        <button type="reset" class="btn btn-danger" onclick="location.href='TransmissionSettings.php'">Cancel</button>
                          </div>
                      </form>
                </div>
		        </div>
        </div>
    </section>
</div>
 
<?php include("control_right_sidebar.php");?>
<?php include("footer.php");?>
<script type="text/javascript">
    function toggle(select){
       if(select.value=='FTP'){
            document.getElementById('FTP').style.display = "block";
    	      document.getElementById('Email').style.display = "none";
       } else{
    	      document.getElementById('FTP').style.display = "none";
            document.getElementById('Email').style.display = "block";
       }
    } 
</script>
